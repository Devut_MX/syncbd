﻿using System;
using System.Windows.Forms;
using SyncBD.Vistas;
using SyncDB.Vistas;

namespace SyncDB
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(SplashScreen.ObtenerInstancia());
        }
    }
}

﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace SyncBD.Controladores
{
    class Seguridad
    {
        protected internal string Encriptar(string original)
        {
            try
            {
                string llave = "SolucionesDigitalesSyncBD";
                byte[] obtenerBytes = Encoding.Unicode.GetBytes(original);
                using (Aes encriptador = Aes.Create())
                {
                    Rfc2898DeriveBytes bites = new Rfc2898DeriveBytes(llave, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encriptador.Key = bites.GetBytes(32);
                    encriptador.IV = bites.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encriptador.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(obtenerBytes, 0, obtenerBytes.Length);
                            cs.Close();
                        }

                        original = Convert.ToBase64String(ms.ToArray());
                    }
                }

                return original;
            }
            catch (Exception)
            {
                return null;
            }
        }

        protected internal string Desencriptar(string cifrado)
        {
            try
            {
                string llave = "SolucionesDigitalesSyncBD";
                cifrado = cifrado.Replace(" ", "+");
                byte[] obtenerBytes = Convert.FromBase64String(cifrado);
                using (Aes encriptador = Aes.Create())
                {
                    Rfc2898DeriveBytes bites = new Rfc2898DeriveBytes(llave, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encriptador.Key = bites.GetBytes(32);
                    encriptador.IV = bites.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encriptador.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(obtenerBytes, 0, obtenerBytes.Length);
                            cs.Close();
                        }

                        cifrado = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }

                return cifrado;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}

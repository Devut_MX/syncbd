﻿using System;

namespace SyncBD.Controladores
{
    public class Tema
    {
        public string Nombre { get; set; }
        public string FondoTitulo { get; set; }
        public string TextoTitulo { get; set; }
        public string FondoVentana { get; set; }
        public string TextoVentana { get; set; }
        public string FondoBoton { get; set; }
        public string TextoBoton { get; set; }
    }

    public class Ajustes
    {
        public bool DiferenteSincronizacion { get; set; }
        public bool MinimizarAlIniciar { get; set; }
        public bool IniciarConWindows { get; set; }
        public string CopiarA { get; set; }
        public bool RutaGuardadoPermanente { get; set; }
        public string Apariencia { get; set; }
        public string Idioma { get; set; }
    }

    public class Remoto
    {
        public string Servidor { get; set; } //Server
        public string Usuario { get; set; } //User
        public string Password { get; set; } //Password
        public string Encriptacion { get; set; } //FTPMode
        public string Puerto { get; set; } //Port
        public string Directorio { get; set; } //Find
        public string Anonimo { get; set; } //IsAnonymous
        public string Modo { get; set; } //IsActive
        public string Autenticar { get; set; } //WithCert
        public string Combinado { get; set; } //Combined
        public string Protocolo { get; set; }
    }

    public class Local
    {
        public string Servidor { get; set; } //Server
        public string Usuario { get; set; } //User
        public string Password { get; set; } //Password
        public string Encriptacion { get; set; } //FTPMode
        public string Puerto { get; set; } //Port
        public string Directorio { get; set; } //Find
        public string Anonimo { get; set; } //IsAnonymous
        public string Modo { get; set; } //IsActive
        public string Autenticar { get; set; } //WithCert
        public string Combinado { get; set; } //Combined
        public string Protocolo { get; set; }
    }

    public class Servidores
    {
        public Remoto Remoto { get; set; }
        public Local Local { get; set; }
    }

    public class MovimientoRemoto
    {
        public int Movimiento { get; set; }
        public DateTime Fecha { get; set; }
        public int Archivos { get; set; }
        public string Desde { get; set; }
        public string Hacia { get; set; }
    }

    public class MovimientoLocal
    {
        public int Movimiento { get; set; }
        public DateTime Fecha { get; set; }
        public int Archivos { get; set; }
        public string Desde { get; set; }
        public string Hacia { get; set; }
    }
}

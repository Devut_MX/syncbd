﻿using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Controls;
using SyncBD.Modelos;
using System.Windows.Forms;

namespace SyncBD.Controladores
{
    class EnlaceModelo
    {
        SyncBDModelo modelo = new SyncBDModelo();

        protected internal bool CrearMovimientoRemoto(MovimientoRemoto movimiento)
        {
            return modelo.CrearMovimientoRemoto(movimiento);
        }

        protected internal bool CrearMovimientoLocal(MovimientoLocal movimiento)
        {
            return modelo.CrearMovimientoLocal(movimiento);
        }

        protected internal void ListaMovimientosServidorRemoto(DataGridView aLlenar)
        {
            modelo.ListaMovimientosServidorRemoto(aLlenar);
        }

        protected internal void ListaMovimientosServidorLocal(DataGridView aLlenar)
        {
            modelo.ListaMovimientosServidorLocal(aLlenar);
        }

        protected internal bool BorrarHistorial()
        {
            return modelo.BorrarHistorial();
        }

        protected internal bool ConsultarError(RichTextBoxEx contenido, LabelX titulo, string codigo)
        {
            return modelo.ConsultarError(contenido, titulo, codigo);
        }
    }
}

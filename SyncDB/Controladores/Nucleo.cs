﻿using Newtonsoft.Json;
using SyncDB.Vistas;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using WinSCP;
using static SyncDB.Vistas.PopUp;
using XML = System.Xml;

namespace SyncBD.Controladores
{
    class Nucleo
    {
        //Ruta que indica la carpeta donde se ubican los temas para la aplicacion
        private readonly string ajustesTemas = string.Concat(Application.StartupPath, @"\Apariencia");
        //Ruta que indica la direccion del archivo de host conocidos
        private readonly string dirHostsConocidos = string.Concat(Application.StartupPath, @"\General\Hosts.xml");
        //Ruta que indica la carpeta donde se ubican los idiomas para la aplicacion
        private readonly string idiomas = string.Concat(Application.StartupPath, @"\Idiomas");
        //Ruta que indica la carpeta donde se guardan los logs de los servidores
        private readonly string registros = string.Concat(Application.StartupPath, @"\Registros");
        //Ruta que indica la carpeta donde se guardan los ajustes y demás de la aplicacion
        private readonly string general = string.Concat(Application.StartupPath, @"\General");
        //Ruta que indica la direccion del archivo donde se guardan los ajustes de la aplicacion
        private readonly string ajustesJSON = string.Concat(Application.StartupPath, @"\General", @"\ajustes.json");
        //Ruta que indica la direccion del archivo donde se guardan los ajustes de la aplicacion
        private readonly string ajustesServidores = string.Concat(Application.StartupPath, @"\General", @"\servidoresConfig.json");
        //Ruta que indica la direccion del archivo donde se guardan los registros del servidor remoto
        private readonly string logRemoto = string.Concat(Application.StartupPath, @"\Registros", @"\logRemoto.txt");
        //Ruta que indica la direccion del archivo donde se guardan los registros del servidor local
        private readonly string logLocal = string.Concat(Application.StartupPath, @"\Registros", @"\logLocal.txt");
        //Ruta predefinida que indica donde realizar la copia de archivos sincronizados
        private readonly string dirCopiado = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"\SyncBD");

        #region Nuevos Ajustes

        public void ComprobarTemas()
        {
            if (!Directory.Exists(ajustesTemas))
            {
                Directory.CreateDirectory(ajustesTemas);
            }
        }

        public void ComprobarIdiomas()
        {
            if (!Directory.Exists(idiomas))
            {
                Directory.CreateDirectory(idiomas);
            }
        }

        public void ComprobarGeneral()
        {
            if (!Directory.Exists(general))
            {
                Directory.CreateDirectory(general);
            }

            if(!Directory.Exists(dirCopiado))
            {
                Directory.CreateDirectory(dirCopiado);
            }

            if (!File.Exists(ajustesJSON))
            {
                using (File.Create(ajustesJSON))
                {

                }

                Ajustes ajustes = new Ajustes
                {
                    CopiarA = dirCopiado
                };

                string datos = JsonConvert.SerializeObject(ajustes, Formatting.Indented);

                File.WriteAllText(ajustesJSON, datos);
            }

            if (!File.Exists(ajustesServidores))
            {
                using (File.Create(ajustesServidores))
                {

                }
            }

        }

        public void ComprobarRegistros()
        {
            if (!Directory.Exists(registros))
            {
                Directory.CreateDirectory(registros);
            }
        }

        public bool GuardarAjustes(Ajustes ajustes)
        {
            try
            {
                ComprobarGeneral();

                string datos = JsonConvert.SerializeObject(ajustes, Formatting.Indented);

                File.WriteAllText(ajustesJSON, datos);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Ajustes LeerAjustes()
        {
            try
            {
                ComprobarGeneral();

                string datos = File.ReadAllText(ajustesJSON);

                Ajustes ajustes = JsonConvert.DeserializeObject<Ajustes>(datos);

                return ajustes;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool GuardarTema(Tema tema)
        {
            try
            {
                ComprobarTemas();

                if(File.Exists(string.Concat(ajustesTemas, @"\", tema.Nombre, ".json")))
                {
                    File.WriteAllText(string.Concat(ajustesTemas, @"\", tema.Nombre, ".json"), string.Empty);
                }

                string datos = JsonConvert.SerializeObject(tema, Formatting.Indented);

                File.WriteAllText(string.Concat(ajustesTemas, @"\", tema.Nombre, ".json"), datos);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Tema LeerTema(string nombre)
        {
            try
            {
                ComprobarTemas();

                if(nombre != "" || nombre != null)
                {
                    string datos = File.ReadAllText(string.Concat(ajustesTemas, @"\", nombre, ".json"));

                    Tema tema = JsonConvert.DeserializeObject<Tema>(datos);

                    return tema;
                }

                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool BorrarTema(string nombre)
        {
            try
            {
                ComprobarTemas();

                if(File.Exists(string.Concat(ajustesTemas, @"\", nombre, ".json")))
                {
                    File.Delete(string.Concat(ajustesTemas, @"\", nombre, ".json"));
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<string> ObtenerLenguajes()
        {
            try
            {
                ComprobarIdiomas();

                DirectoryInfo info = new DirectoryInfo(idiomas);

                List<string> lenguajes = new List<string>();

                foreach (var archivo in info.GetFiles())
                {
                    if (archivo.Extension == ".json")
                    {
                        string json = File.ReadAllText(archivo.FullName);
                        dynamic deserealizacion = JsonConvert.DeserializeObject(json);
                        string dato = deserealizacion["Lenguaje"];
                        lenguajes.Add(dato);
                    }
                }

                return lenguajes.Count <= 0 ? null : lenguajes;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<string> ObtenerTemas()
        {
            try
            {
                ComprobarTemas();

                DirectoryInfo info = new DirectoryInfo(ajustesTemas);

                List<string> apariencias = new List<string>();
                
                foreach (var archivo in info.GetFiles())
                {
                    if (archivo.Extension == ".json")
                    {
                        apariencias.Add(archivo.FullName);
                    }
                }

                return apariencias.Count <= 0 ? null : apariencias;
            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion

        Seguridad seguridad = new Seguridad();

        public bool ValidarYGuardarAjustesServidores(Local servidorLocal, Remoto servidorRemoto)
        {
            try
            {
                if (!File.Exists(ajustesServidores))
                {
                    using (File.Create(ajustesServidores)) { }
                }

                string datos = File.ReadAllText(ajustesServidores);

                Servidores servidores = new Servidores { Local = servidorLocal, Remoto = servidorRemoto };

                if (datos == null || string.IsNullOrWhiteSpace(datos))
                {
                    string configuraciones = JsonConvert.SerializeObject(servidores, Formatting.Indented);

                    File.WriteAllText(ajustesServidores, configuraciones);
                }

                else
                {
                    var servidorTemporal = JsonConvert.DeserializeObject<Servidores>(datos);

                    if (servidorTemporal.Local.Servidor == "")
                    {
                        if (servidorLocal.Servidor != "")
                        {
                            servidorTemporal.Local = servidorLocal;

                            string configuraciones = JsonConvert.SerializeObject(servidorTemporal, Formatting.Indented);

                            File.WriteAllText(ajustesServidores, configuraciones);
                        }
                    }

                    else
                    {
                        if (servidorLocal.Servidor != "")
                        {
                            servidorTemporal.Local = servidorLocal;

                            string configuraciones = JsonConvert.SerializeObject(servidorTemporal, Formatting.Indented);

                            File.WriteAllText(ajustesServidores, configuraciones);
                        }
                    }

                    if (servidorTemporal.Remoto.Servidor == "")
                    {
                        if (servidorRemoto.Servidor != "")
                        {
                            servidorTemporal.Remoto = servidorRemoto;

                            string configuraciones = JsonConvert.SerializeObject(servidorTemporal, Formatting.Indented);

                            File.WriteAllText(ajustesServidores, configuraciones);
                        }
                    }

                    else
                    {
                        if (servidorRemoto.Servidor != "")
                        {
                            servidorTemporal.Remoto = servidorRemoto;

                            string configuraciones = JsonConvert.SerializeObject(servidorTemporal, Formatting.Indented);

                            File.WriteAllText(ajustesServidores, configuraciones);
                        }
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Servidores LeerConfiguracionServidores()
        {
            try
            {
                if (!File.Exists(ajustesServidores))
                {
                    using (File.Create(ajustesServidores)) { }

                    return null;
                }

                string datos = File.ReadAllText(ajustesServidores);

                Servidores configuraciones = JsonConvert.DeserializeObject<Servidores>(datos);

                return configuraciones;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public SessionOptions ValidarServidorRemoto(Servidores servidores)
        {
            try
            {
                SessionOptions cliente = new SessionOptions();

                int puerto = Convert.ToInt32(seguridad.Desencriptar(servidores.Remoto.Puerto));

                if (puerto <= 0)
                {
                    puerto = 0;
                }
                
                string tipoEncriptacion = seguridad.Desencriptar(servidores.Remoto.Encriptacion);

                string protocolo = seguridad.Desencriptar(servidores.Remoto.Protocolo);

                cliente.HostName = seguridad.Desencriptar(servidores.Remoto.Servidor);
                cliente.UserName = seguridad.Desencriptar(servidores.Remoto.Usuario);
                cliente.Password = seguridad.Desencriptar(servidores.Remoto.Password);
                cliente.PortNumber = puerto;
                cliente.TimeoutInMilliseconds = 30000;
                cliente.AddRawSettings("ProxyPort", "0");

                //Se tira la seguridad y se acepta cualquier certificado
                if (seguridad.Desencriptar(servidores.Remoto.Autenticar) == "True")
                {
                    cliente.GiveUpSecurityAndAcceptAnyTlsHostCertificate = true;
                }

                //Modificar a servidor FTP Activo
                if (seguridad.Desencriptar(servidores.Remoto.Modo) == "True")
                {
                    cliente.FtpMode = FtpMode.Active;
                }

                //Via SFTP
                if (protocolo == "SFTP")
                {
                    cliente.Protocol = Protocol.Sftp;

                    string fingerprint = null;

                    XML.XmlDocument hostconocidos = new XML.XmlDocument();

                    if (File.Exists(dirHostsConocidos))
                    {
                        hostconocidos.Load(dirHostsConocidos);
                    }
                    else
                    {
                        hostconocidos.AppendChild(hostconocidos.CreateElement("KnownHosts"));
                    }

                    string sessionKey = seguridad.Desencriptar(servidores.Remoto.Servidor) + ":" + puerto.ToString();
                    XML.XmlNode fingerprintNode = hostconocidos.DocumentElement.SelectSingleNode("KnownHost[@host='" + sessionKey + "']/@fingerprint");

                    if (fingerprintNode != null)
                    {
                        fingerprint = fingerprintNode.Value;
                    }

                    else
                    {
                        using (Session sesion = new Session())
                        {
                            fingerprint = sesion.ScanFingerprint(cliente, "SHA-256");
                        }

                        XML.XmlElement knownHost = hostconocidos.CreateElement("KnownHost");
                        hostconocidos.DocumentElement.AppendChild(knownHost);
                        knownHost.SetAttribute("host", sessionKey);
                        knownHost.SetAttribute("fingerprint", fingerprint);

                        hostconocidos.Save(dirHostsConocidos);
                    }

                    cliente.SshHostKeyFingerprint = fingerprint;
                }

                ////Via FTP
                else
                {
                    cliente.Protocol = Protocol.Ftp;

                    //Encriptación Implicita
                    if (tipoEncriptacion == "1")
                    {
                        cliente.FtpSecure = FtpSecure.Implicit;
                    }

                    //Encriptacion Explicita
                    if (tipoEncriptacion == "2")
                    {
                        cliente.FtpSecure = FtpSecure.Explicit;
                    }
                }

                using (Session sesion = new Session())
                {
                    sesion.DebugLogLevel = 0;

                    sesion.DebugLogPath = logRemoto;

                    sesion.Open(cliente);

                    RemoteDirectoryInfo infoDirectorio = sesion.ListDirectory(seguridad.Desencriptar(servidores.Remoto.Directorio));

                    return infoDirectorio != null ? cliente : null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        public SessionOptions ValidarServidorLocal(Servidores servidores)
        {
            try
            {
                SessionOptions cliente = new SessionOptions();

                string protocolo = seguridad.Desencriptar(servidores.Local.Protocolo);

                int puerto = Convert.ToInt32(seguridad.Desencriptar(servidores.Local.Puerto));

                if (puerto <= 0)
                {
                    puerto = 0;
                }

                string tipoEncriptacion = seguridad.Desencriptar(servidores.Local.Encriptacion);
                
                cliente.HostName = seguridad.Desencriptar(servidores.Local.Servidor);
                cliente.UserName = seguridad.Desencriptar(servidores.Local.Usuario);
                cliente.Password = seguridad.Desencriptar(servidores.Local.Password);
                cliente.PortNumber = puerto;
                cliente.TimeoutInMilliseconds = 30000;
                cliente.AddRawSettings("ProxyPort", "0");
                
                //Se tira la seguridad y se acepta cualquier certificado
                if (seguridad.Desencriptar(servidores.Local.Autenticar) == "True")
                {
                    cliente.GiveUpSecurityAndAcceptAnyTlsHostCertificate = true;
                }

                //Modificar a servidor FTP Activo
                if (seguridad.Desencriptar(servidores.Local.Modo) == "True")
                {
                    cliente.FtpMode = FtpMode.Active;
                }

                //Via SFTP
                if (protocolo == "SFTP")
                {
                    cliente.Protocol = Protocol.Sftp;

                    string fingerprint = null;

                    XML.XmlDocument hostconocidos = new XML.XmlDocument();

                    if (File.Exists(dirHostsConocidos))
                    {
                        hostconocidos.Load(dirHostsConocidos);
                    }
                    else
                    {
                        hostconocidos.AppendChild(hostconocidos.CreateElement("KnownHosts"));
                    }

                    string sessionKey = seguridad.Desencriptar(servidores.Local.Servidor) + ":" + puerto.ToString();
                    XML.XmlNode fingerprintNode = hostconocidos.DocumentElement.SelectSingleNode("KnownHost[@host='" + sessionKey + "']/@fingerprint");

                    if (fingerprintNode != null)
                    {
                        fingerprint = fingerprintNode.Value;
                    }

                    else
                    {
                        using (Session sesion = new Session())
                        {
                            fingerprint = sesion.ScanFingerprint(cliente, "SHA-256");
                        }

                        XML.XmlElement knownHost = hostconocidos.CreateElement("KnownHost");
                        hostconocidos.DocumentElement.AppendChild(knownHost);
                        knownHost.SetAttribute("host", sessionKey);
                        knownHost.SetAttribute("fingerprint", fingerprint);

                        hostconocidos.Save(dirHostsConocidos);
                    }

                    cliente.SshHostKeyFingerprint = fingerprint;
                }

                ////Via FTP
                else
                {
                    cliente.Protocol = Protocol.Ftp;

                    //Encriptación Implicita
                    if (tipoEncriptacion == "1")
                    {
                        cliente.FtpSecure = FtpSecure.Implicit;
                    }

                    //Encriptacion Explicita
                    if (tipoEncriptacion == "2")
                    {
                        cliente.FtpSecure = FtpSecure.Explicit;
                    }
                }

                using (Session sesion = new Session())
                {
                    sesion.DebugLogLevel = 0;

                    sesion.DebugLogPath = logLocal;

                    sesion.Open(cliente);

                    RemoteDirectoryInfo infoDirectorio = sesion.ListDirectory(seguridad.Desencriptar(servidores.Local.Directorio));

                    return infoDirectorio != null ? cliente : null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void MostrarPopUp(string mensaje, string titulo, Icono icono)
        {
            PopUp mostrar = PopUp.ObtenerInstancia(mensaje, titulo, icono);

            mostrar.ShowDialog();

            mostrar.Dispose();
        }
    }
}

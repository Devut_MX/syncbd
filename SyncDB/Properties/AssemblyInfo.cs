﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// La información general de un ensamblado se controla mediante el siguiente 
// conjunto de atributos. Cambie estos valores de atributo para modificar la información
// asociada con un ensamblado.
[assembly: AssemblyTitle("SyncBD")]
[assembly: AssemblyDescription("Sincroniza tus servidores de datos")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Soluciones Digitales y de Procesos S.A. de C.V.")]
[assembly: AssemblyProduct("SyncBD")]
[assembly: AssemblyCopyright("Copyright ©  2018 Soluciones Digitales")]
[assembly: AssemblyTrademark("EduManager")]
[assembly: AssemblyCulture("")]

// Si establece ComVisible en false, los tipos de este ensamblado no estarán visibles 
// para los componentes COM.  Si es necesario obtener acceso a un tipo en este ensamblado desde 
// COM, establezca el atributo ComVisible en true en este tipo.
[assembly: ComVisible(false)]

// El siguiente GUID sirve como id. de typelib si este proyecto se expone a COM.
[assembly: Guid("20f43f36-2188-417e-b8ad-dcd7cc0c3d0a")]

// La información de versión de un ensamblado consta de los cuatro valores siguientes:
//
//      Versión principal
//      Versión secundaria
//      Número de compilación
//      Revisión
//
// Puede especificar todos los valores o utilizar los números de compilación y de revisión predeterminados
// mediante el carácter '*', como se muestra a continuación:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.4.0")]
[assembly: AssemblyFileVersion("1.0.4.0")]

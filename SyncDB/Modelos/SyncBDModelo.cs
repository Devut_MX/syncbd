﻿using SyncBD.Controladores;
using System;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Controls;
using System.Collections.Generic;

namespace SyncBD.Modelos
{
    class SyncBDModelo
    {
        private readonly string cadenaConexionHistorial = "Data Source = " + Application.StartupPath + @"\General\Historial.db; Version = 3;";
        private readonly string cadenaConexionSoporte = "Data Source = " + Application.StartupPath + @"\Soporte.db; Version = 3;";

        private SQLiteConnection cnn;
        private SQLiteCommand cmd;
        private SQLiteDataReader lector;
        private SQLiteDataAdapter adaptadorDatos;
        private DataTable tabla;

        protected internal bool CrearMovimientoRemoto(MovimientoRemoto movimiento)
        {
            try
            {
                using (cnn = new SQLiteConnection(cadenaConexionHistorial))
                {
                    string query = "INSERT INTO MovimientosRemoto VALUES( null, @a, @b, @c, @d);";

                    cnn.Open();

                    cmd = new SQLiteCommand(query, cnn);

                    cmd.Parameters.AddWithValue("@a", movimiento.Fecha);
                    cmd.Parameters.AddWithValue("@b", movimiento.Archivos);
                    cmd.Parameters.AddWithValue("@c", movimiento.Desde);
                    cmd.Parameters.AddWithValue("@d", movimiento.Hacia);

                    int _response = cmd.ExecuteNonQuery();

                    return _response > 0 ? true : false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected internal bool CrearMovimientoLocal(MovimientoLocal movimiento)
        {
            try
            {
                using (cnn = new SQLiteConnection(cadenaConexionHistorial))
                {
                    string query = "INSERT INTO MovimientosLocal VALUES( null, @a, @b, @c, @d);";

                    cnn.Open();

                    cmd = new SQLiteCommand(query, cnn);

                    cmd.Parameters.AddWithValue("@a", movimiento.Fecha);
                    cmd.Parameters.AddWithValue("@b", movimiento.Archivos);
                    cmd.Parameters.AddWithValue("@c", movimiento.Desde);
                    cmd.Parameters.AddWithValue("@d", movimiento.Hacia);

                    int _response = cmd.ExecuteNonQuery();

                    return _response > 0 ? true : false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected internal void ListaMovimientosServidorRemoto(DataGridView aLlenar)
        {
            try
            {
                using (cnn = new SQLiteConnection(cadenaConexionHistorial))
                {
                    string query = "SELECT Movimiento as 'No. Último movimiento', Fecha as 'Fecha de sincronización', Archivos as 'No. de archivos afectados', Desde, Hacia FROM MovimientosRemoto Order by Fecha Desc Limit 8";

                    cnn.Open();

                    cmd = new SQLiteCommand(query, cnn);

                    tabla = new DataTable();

                    adaptadorDatos = new SQLiteDataAdapter(cmd);

                    adaptadorDatos.Fill(tabla);

                    aLlenar.DataSource = tabla;
                }
            }
            catch (Exception)
            {

            }
        }

        protected internal void ListaMovimientosServidorLocal(DataGridView aLlenar)
        {
            try
            {
                using (cnn = new SQLiteConnection(cadenaConexionHistorial))
                {
                    string query = "SELECT Movimiento as 'No. Último movimiento', Fecha as 'Fecha de sincronización', Archivos as 'No. de archivos afectados', Desde, Hacia FROM MovimientosLocal Order by Fecha Desc Limit 8";

                    cnn.Open();

                    cmd = new SQLiteCommand(query, cnn);

                    tabla = new DataTable();

                    adaptadorDatos = new SQLiteDataAdapter(cmd);

                    adaptadorDatos.Fill(tabla);

                    aLlenar.DataSource = tabla;
                }
            }
            catch (Exception)
            {

            }
        }

        protected internal bool BorrarHistorial()
        {
            try
            {
                using (cnn = new SQLiteConnection(cadenaConexionHistorial))
                {
                    string query = "DROP TABLE MovimientosLocal; DROP TABLE MovimientosRemoto; CREATE TABLE MovimientosLocal ( Movimiento integer primary key autoincrement, Fecha Date not null, Archivos integer not null, Desde text not null, Hacia text not null ); CREATE TABLE MovimientosRemoto ( Movimiento integer primary key autoincrement, Fecha Date not null, Archivos integer not null, Desde text not null, Hacia text not null );";

                    cnn.Open();

                    cmd = new SQLiteCommand(query, cnn);

                    int respuesta = cmd.ExecuteNonQuery();

                    return respuesta >= 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

                return false;
            }
        }

        protected internal bool ConsultarError(RichTextBoxEx contenido, LabelX titulo, string codigo)
        {
            try
            {
                using (cnn = new SQLiteConnection(cadenaConexionSoporte))
                {
                    string query = "SELECT * FROM Errores WHERE Codigo = @codigo";

                    cnn.Open();

                    cmd = new SQLiteCommand(query, cnn);
                    cmd.Parameters.AddWithValue("@codigo", codigo);

                    lector = cmd.ExecuteReader();

                    string[] datos = new string[5];

                    while (lector.Read())
                    {
                        datos[0] = lector[0].ToString();
                        datos[1] = lector[1].ToString();
                        datos[2] = lector[2].ToString();
                        datos[3] = lector[3].ToString();
                        datos[4] = lector[4].ToString();
                    }

                    lector.Close();
                    
                    if(!string.IsNullOrWhiteSpace(datos[0]) || !string.IsNullOrWhiteSpace(datos[1]) || !string.IsNullOrWhiteSpace(datos[2]) || !string.IsNullOrWhiteSpace(datos[3]) || !string.IsNullOrWhiteSpace(datos[4]))
                    {
                        titulo.Text = string.Format("Error {0} - {1}", datos[0], datos[1].ToLower() == "splashscreen" ? "Carga inicial" : datos[1]);

                        string datos3 = null;
                        string datos4 = null;

                        if (datos[3].Contains("•"))
                        {
                            foreach (string separador in datos[3].Split('•'))
                            {
                                datos3 += "•" + separador + "\n";
                            }
                        }

                        if (datos[4].Contains("•"))
                        {
                            foreach (string separador in datos[4].Split('•'))
                            {
                                datos4 += "•" + separador + "\n";
                            }
                        }

                        contenido.Text = string.Format("Descripción del error:\n\n{0}\n\nCausas del error:\n{1}\n\nPosibles soluciones:\n{2}\n", datos[2], datos3 != null ? datos3.Substring(1) : datos[3], datos4 != null ? datos4.Substring(1) : datos[4]);

                        titulo.Refresh();

                        contenido.Refresh();
                    }

                    else
                    {
                        titulo.Text = "No se encontró código";

                        contenido.Text = "";

                        titulo.Refresh();

                        contenido.Refresh();
                    }

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

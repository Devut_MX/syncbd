﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Logger;

namespace SyncDB.Vistas
{
    public partial class Bienvenido : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int Param);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private static Bienvenido singleton;

        public static Bienvenido ObtenerInstancia()
        {
            if (singleton == null || singleton.IsDisposed)
            {
                singleton = new Bienvenido();
            }

            singleton.BringToFront();

            return singleton;
        }

        private Bienvenido()
        {
            InitializeComponent();
        }

        ToLog log = new ToLog();

        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void pbxMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void pbxCerrar_Click(object sender, EventArgs e)
        {
            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿Desea cancelar las configuraciones y cerrar el programa?\nRecuerde que el asistente empezará nuevamente al iniciar el programa", "SyncBD | Confirme cierre", ExtraMessageBox.Boton.OkCancel, ExtraMessageBox.Icono.Info);

            if (DialogResult.OK == mensaje.ShowDialog())
            {
                log.AppendLog("[Configuraciones] El usuario cerró el programa");

                Application.Exit();
            }

            mensaje.Dispose();
        }

        private void btnA1Continuar_Click(object sender, EventArgs e)
        {
            MenuServidores mostrar = MenuServidores.ObtenerInstancia();

            Hide();

            log.AppendLog("[Configuraciones] Mostrando ventana de servidores");

            mostrar.Show();
        }

        private void btnA1Cancelar_Click(object sender, EventArgs e)
        {
            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿Desea cancelar las configuraciones y cerrar el programa?\nRecuerde que el asistente empezará nuevamente al iniciar el programa", "SyncBD | Confirme cierre", ExtraMessageBox.Boton.OkCancel, ExtraMessageBox.Icono.Info);

            if(DialogResult.OK == mensaje.ShowDialog())
            {
                log.AppendLog("[Configuraciones] El usuario cerró el programa");

                Application.Exit();
            }

            mensaje.Dispose();
        }
    }
}

﻿using SyncBD.Controladores;
using SyncBD.Vistas;
using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Logger;

namespace SyncDB.Vistas
{
    public partial class ConfigRemoto : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int Param);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private static ConfigRemoto singleton;

        public static ConfigRemoto ObtenerInstancia()
        {
            if (singleton == null || singleton.IsDisposed)
            {
                singleton = new ConfigRemoto();
            }

            singleton.BringToFront();

            return singleton;
        }

        private ConfigRemoto()
        {
            InitializeComponent();

            cbxA3Encriptacion.SelectedIndex = 0;

            cbxA3Protocolo.SelectedIndex = 0;

            LeerConfiguracionesPrevias();
        }

        Nucleo nucleo = new Nucleo();

        Seguridad seguridad = new Seguridad();

        Servidores servidores = new Servidores();

        ToLog log = new ToLog();

        public static bool combinado = false;

        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void pbxMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void LeerConfiguracionesPrevias()
        {
            try
            {
                Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                if (configuraciones != null)
                {
                    if (configuraciones.Remoto.Servidor != null)
                    {
                        txtA3Servidor.Text = configuraciones.Remoto.Servidor != "" ? seguridad.Desencriptar(configuraciones.Remoto.Servidor) : "";
                        txtA3Usuario.Text = "";
                        txtA3Password.Text = "";
                        //txtA3Usuario.Text = seguridad.Desencriptar(configuraciones.Remoto.Usuario);
                        //txtA3Password.Text = seguridad.Desencriptar(configuraciones.Remoto.Password);
                        txtA3Puerto.Text = configuraciones.Remoto.Puerto != "" ? seguridad.Desencriptar(configuraciones.Remoto.Puerto) : "";
                        txtA3Directorio.Text = configuraciones.Remoto.Directorio != "" ? seguridad.Desencriptar(configuraciones.Remoto.Directorio) : "";
                        cbxA3Encriptacion.SelectedIndex = configuraciones.Remoto.Encriptacion != "" ? Convert.ToInt32(seguridad.Desencriptar(configuraciones.Remoto.Encriptacion)) : 0;
                        cbxA3Protocolo.SelectedItem = configuraciones.Remoto.Protocolo != "" ? seguridad.Desencriptar(configuraciones.Remoto.Protocolo) : "FTP";

                        if (seguridad.Desencriptar(configuraciones.Remoto.Anonimo) == "True")
                        {
                            chkA3Anonimo.Value = true;
                        }

                        if (seguridad.Desencriptar(configuraciones.Remoto.Modo) == "True")
                        {
                            chkA3Modo.Value = true;
                        }

                        if (seguridad.Desencriptar(configuraciones.Remoto.Autenticar) == "True")
                        {
                            chkA3Autenticar.Value = true;
                        }

                        if (seguridad.Desencriptar(configuraciones.Remoto.Combinado) == "True")
                        {
                            combinado = true;
                        }

                        log.AppendLog("[Configuraciones] Ajustes del servidor remoto cargados correctamente");
                    }
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x003, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x003", PopUp.Icono.Error);
            }
        }
        
        private void pbxA3Ver_MouseDown(object sender, MouseEventArgs e)
        {
            txtA3Password.isPassword = false;
            pbxA3Ver.Image = SyncBD.Properties.Resources.Unlock_32px;
        }

        private void pbxA3Ver_MouseUp(object sender, MouseEventArgs e)
        {
            txtA3Password.isPassword = true;
            pbxA3Ver.Image = SyncBD.Properties.Resources.Lock_32px;
        }

        private void pbxCerrar_Click(object sender, EventArgs e)
        {
            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿Desea cancelar la configuración del servidor remoto?\nNo se guardará ningún ajuste realizado en está sección", "SyncBD | Confirme cierre", ExtraMessageBox.Boton.OkCancel, ExtraMessageBox.Icono.Info);

            if (DialogResult.OK == mensaje.ShowDialog())
            {
                mensaje.Dispose();

                DialogResult = DialogResult.Cancel;

                log.AppendLog("[Configuraciones] El usuario cancelo la configuración");

                Close();
            }

            mensaje.Dispose();
        }

        private void btnA3Ayuda_Click(object sender, EventArgs e)
        {
            AyudaConfig mostrar = AyudaConfig.ObtenerInstancia();

            Hide();

            mostrar.ShowDialog();

            Show();

            mostrar.Dispose();
        }

        private void btnA3Cancelar_Click(object sender, EventArgs e)
        {
            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿Desea cancelar la configuración del servidor remoto?\nNo se guardará ningún ajuste realizado en está sección", "SyncBD | Confirme cierre", ExtraMessageBox.Boton.OkCancel, ExtraMessageBox.Icono.Info);

            if (DialogResult.OK == mensaje.ShowDialog())
            {
                mensaje.Dispose();

                DialogResult = DialogResult.Cancel;

                log.AppendLog("[Configuraciones] El usuario cancelo la configuración");

                Close();
            }

            mensaje.Dispose();
        }

        private void btnA3Continuar_Click(object sender, EventArgs e)
        {
            try
            {
                RealizarValidaciones();

                if (txtA3Servidor.Text == "" || txtA3Usuario.Text == "" || txtA3Password.Text == "" || txtA3Puerto.Text == "" || Convert.ToInt32(txtA3Puerto.Text) < 0 || cbxA3Encriptacion.SelectedIndex < 0 || cbxA3Protocolo.SelectedIndex < 0)
                {
                    ExtraMessageBox mostrar = ExtraMessageBox.ObtenerInstancia("Es necesario que proporcione todos los datos de los campos marcados con \"*\" para poder realizar la prueba de conexión con el servidor.", "SyncBD | Faltan datos", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Warning);

                    mostrar.ShowDialog();

                    mostrar.Dispose();

                    txtA3Servidor.Focus();
                }

                else
                {
                    string servidorFTP = "";

                    if (txtA3Servidor.Text.Contains("localhost") || txtA3Servidor.Text.Contains("127.0.0.1"))
                    {
                        servidorFTP = "localhost";
                    }

                    else
                    {
                        //Agregar validaciones para IP
                        //servidorFTP = txtA3Servidor.Text.Contains("ftp.") ? txtA3Servidor.Text : "ftp." + txtA3Servidor.Text;
                        servidorFTP = txtA3Servidor.Text;
                    }

                    //Via FTP
                    if(cbxA3Protocolo.SelectedIndex == 0)
                    {
                        servidores = new Servidores { Remoto = new Remoto { Servidor = seguridad.Encriptar(servidorFTP), Anonimo = seguridad.Encriptar(chkA3Anonimo.Value.ToString()), Usuario = seguridad.Encriptar(txtA3Usuario.Text), Password = seguridad.Encriptar(txtA3Password.Text), Puerto = seguridad.Encriptar(txtA3Puerto.Text), Directorio = seguridad.Encriptar(txtA3Directorio.Text == "" ? "/" : txtA3Directorio.Text), Encriptacion = seguridad.Encriptar(cbxA3Encriptacion.SelectedIndex.ToString()), Modo = seguridad.Encriptar(chkA3Modo.Value.ToString()), Autenticar = seguridad.Encriptar(chkA3Autenticar.Value.ToString()), Combinado = seguridad.Encriptar(combinado.ToString()), Protocolo = seguridad.Encriptar("FTP") }, Local = new Local { Servidor = "", Anonimo = "", Usuario = "", Password = "", Puerto = "", Directorio = "", Encriptacion = "", Modo = "", Autenticar = "", Combinado = seguridad.Encriptar(combinado.ToString()), Protocolo = "" } };
                    }

                    //Via SFTP
                    else
                    {
                        servidores = new Servidores { Remoto = new Remoto { Servidor = seguridad.Encriptar(servidorFTP), Anonimo = seguridad.Encriptar(chkA3Anonimo.Value.ToString()), Usuario = seguridad.Encriptar(txtA3Usuario.Text), Password = seguridad.Encriptar(txtA3Password.Text), Puerto = seguridad.Encriptar(txtA3Puerto.Text), Directorio = seguridad.Encriptar(txtA3Directorio.Text == "" ? "/" : txtA3Directorio.Text), Encriptacion = "", Modo = seguridad.Encriptar(chkA3Modo.Value.ToString()), Autenticar = seguridad.Encriptar(chkA3Autenticar.Value.ToString()), Combinado = seguridad.Encriptar(combinado.ToString()), Protocolo = seguridad.Encriptar("SFTP") }, Local = new Local { Servidor = "", Anonimo = "", Usuario = "", Password = "", Puerto = "", Directorio = "", Encriptacion = "", Modo = "", Autenticar = "", Combinado = seguridad.Encriptar(combinado.ToString()), Protocolo = "" } };
                    }

                    Procesando mostrarOcupado = Procesando.ObtenerInstancia("Conectando, espere por favor...");
                    
                    mostrarOcupado.Show();
                    
                    if (nucleo.ValidarServidorRemoto(servidores) != null)
                    {
                        if (nucleo.ValidarYGuardarAjustesServidores(servidores.Local, servidores.Remoto))
                        {
                            log.AppendLog("[Configuraciones] Ajustes de conexión con el servidor Remoto correctas");

                            mostrarOcupado.Close();

                            mostrarOcupado.Dispose();
                            
                            ExtraMessageBox mostrar = ExtraMessageBox.ObtenerInstancia("Se ha conectado al servidor remoto correctamente.", "SyncBD | Conexión con servidor remoto establecida", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Ok);

                            mostrar.ShowDialog();

                            mostrar.Dispose();

                            DialogResult = DialogResult.OK;
                            
                            Close();
                        }
                    }

                    else
                    {
                        log.AppendLog("[Configuraciones] Imposible establecer conexión con el servidor Remoto");

                        mostrarOcupado.Close();

                        mostrarOcupado.Dispose();
                        
                        ExtraMessageBox mostrar = ExtraMessageBox.ObtenerInstancia("No se ha podido establecer conexión con el servidor remoto, verifique si los datos son correctos y reintente", "SyncBD | Imposible conectar con servidor remoto", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Warning);

                        mostrar.ShowDialog();

                        mostrar.Dispose();

                        txtA3Servidor.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x004, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x004", PopUp.Icono.Error);
            }
        }

        private void txtA3Puerto_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (Char.IsNumber(e.KeyChar))
                {
                    e.Handled = false;
                }

                else if (Char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }

                else
                {
                    e.Handled = true;
                }
            }
            catch (Exception)
            {

            }
        }

        private void RealizarValidaciones()
        {
            try
            {
                //Validar tambien al hacer clic en continuar
                if (txtA3Puerto.Text != "")
                {
                    if (txtA3Puerto.Text.Length > 5)
                    {
                        txtA3Puerto.Text = txtA3Puerto.Text.Substring(0, 5);
                    }
                }

                //Validar tambien al hacer clic en continuar
                if (txtA3Usuario.Text != "")
                {
                    txtA3Usuario.Text = txtA3Usuario.Text.TrimEnd(' ');
                    txtA3Usuario.Text = txtA3Usuario.Text.TrimStart(' ');
                }

                //Validar tambien al hacer clic en continuar
                if (txtA3Servidor.Text != "")
                {
                    txtA3Servidor.Text = txtA3Servidor.Text.TrimEnd(' ');
                    txtA3Servidor.Text = txtA3Servidor.Text.TrimStart(' ');
                }

                //Validar tambien al hacer clic en continuar
                if (txtA3Password.Text != "")
                {
                    txtA3Password.Text = txtA3Password.Text.TrimEnd(' ');
                    txtA3Password.Text = txtA3Password.Text.TrimStart(' ');
                }

                //Validar tambien al hacer clic en continuar
                if (txtA3Directorio.Text != "")
                {
                    txtA3Directorio.Text = txtA3Directorio.Text.TrimEnd(' ');
                    txtA3Directorio.Text = txtA3Directorio.Text.TrimStart(' ');
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x005, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x005", PopUp.Icono.Error);
            }
        }

        private void txtA3Puerto_Leave(object sender, EventArgs e)
        {
            RealizarValidaciones();
        }

        private void txtA3Usuario_Leave(object sender, EventArgs e)
        {
            RealizarValidaciones();
        }

        private void txtA3Servidor_Leave(object sender, EventArgs e)
        {
            RealizarValidaciones();
        }

        private void txtA3Password_Leave(object sender, EventArgs e)
        {
            RealizarValidaciones();
        }

        private void txtA3Directorio_Leave(object sender, EventArgs e)
        {
            RealizarValidaciones();
        }

        private void chkA3Anonimo_OnValueChange(object sender, EventArgs e)
        {
            if (chkA3Anonimo.Value)
            {
                txtA3Usuario.Text = "anonymous";
                txtA3Usuario.Enabled = false;
                txtA3Password.Text = "anonymous@example.com";
                txtA3Password.Enabled = false;
            }

            else
            {
                txtA3Usuario.Text = "";
                txtA3Usuario.Enabled = true;
                txtA3Password.Text = "";
                txtA3Password.Enabled = true;
            }
        }

        private void chkA3Avanzados_OnValueChange(object sender, EventArgs e)
        {
            if (chkA3Avanzados.Value)
            {
                chkA3Modo.Visible = true;
                lblA3Modo.Visible = true;
                chkA3Autenticar.Visible = true;
                lblA3Autenticar.Visible = true;
                chkA3Anonimo.Visible = true;
                lblA3Anonimo.Visible = true;
            }

            else
            {
                chkA3Modo.Visible = false;
                chkA3Modo.Value = false;
                lblA3Modo.Visible = false;
                chkA3Autenticar.Visible = false;
                chkA3Autenticar.Value = false;
                lblA3Autenticar.Visible = false;
                chkA3Anonimo.Visible = false;
                chkA3Anonimo.Value = false;
                lblA3Anonimo.Visible = false;
            }
        }

        private void cbxA3Protocolo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(cbxA3Protocolo.SelectedIndex == 1)
                {
                    cbxA3Encriptacion.Visible = false;
                    lblA3Encriptacion.Visible = false;
                }

                else
                {
                    cbxA3Encriptacion.Visible = true;
                    lblA3Encriptacion.Visible = true;
                }
            }
            catch (Exception)
            {
                
            }
        }
    }
}

﻿using SyncBD.Controladores;
using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Logger;

namespace SyncDB.Vistas
{
    public partial class FinAsistente : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int Param);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private static FinAsistente singleton;

        public static FinAsistente ObtenerInstancia(Visualizacion forma)
        {
            if (singleton == null || singleton.IsDisposed)
            {
                singleton = new FinAsistente(forma);
            }

            singleton.BringToFront();

            return singleton;
        }
        
        private FinAsistente(Visualizacion forma)
        {
            InitializeComponent();

            LeerConfiguracionesServidores();

            Determinar(forma);
        }

        Nucleo nucleo = new Nucleo();

        Seguridad seguridad = new Seguridad();

        ToLog log = new ToLog();

        Servidores servidorRemoto;

        Servidores servidorLocal;

        public enum Visualizacion
        {
            Remoto,
            Local,
            Ambos
        }

        private void LeerConfiguracionesServidores()
        {
            try
            {
                Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                if (configuraciones != null)
                {
                    if (configuraciones.Remoto.Servidor != null)
                    {
                        servidorRemoto = new Servidores
                        {
                            Remoto = new Remoto
                            {
                                Servidor = seguridad.Desencriptar(configuraciones.Remoto.Servidor),
                                Usuario = seguridad.Desencriptar(configuraciones.Remoto.Usuario),
                                Directorio = seguridad.Desencriptar(configuraciones.Remoto.Directorio)
                            }
                        };
                    }

                    if (configuraciones.Local.Servidor != null)
                    {
                        servidorLocal = new Servidores
                        {
                            Local = new Local
                            {
                                Servidor = seguridad.Desencriptar(configuraciones.Local.Servidor),
                                Usuario = seguridad.Desencriptar(configuraciones.Local.Usuario),
                                Directorio = seguridad.Desencriptar(configuraciones.Local.Directorio)
                            }
                        };
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void pbxMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void pbxCerrar_Click(object sender, EventArgs e)
        {
            Principal mostrar = Principal.ObtenerInstancia();

            Hide();

            log.AppendLog("[Configuraciones] Mostrando la ventana principal");

            mostrar.ShowDialog();
        }

        private void Determinar(Visualizacion forma)
        {
            switch (forma)
            {
                case Visualizacion.Remoto:
                    pnlSegundo.Visible = false;

                    lblA5Servidor1.Text = servidorRemoto.Remoto.Servidor;
                    lblA5Usuario1.Text = servidorRemoto.Remoto.Usuario;
                    lblA5Directorio1.Text = servidorRemoto.Remoto.Directorio;

                    break;

                case Visualizacion.Local:
                    pnlSegundo.Visible = false;
                    lblA5PrimeroTitulo.Text = "Datos del servidor local";

                    lblA5Servidor1.Text = servidorLocal.Local.Servidor;
                    lblA5Usuario1.Text = servidorLocal.Local.Usuario;
                    lblA5Directorio1.Text = servidorLocal.Local.Directorio;

                    break;

                case Visualizacion.Ambos:
                    pnlPrimero.Visible = true;
                    pnlSegundo.Visible = true;

                    lblA5Servidor1.Text = servidorRemoto.Remoto.Servidor;
                    lblA5Usuario1.Text = servidorRemoto.Remoto.Usuario;
                    lblA5Directorio1.Text = servidorRemoto.Remoto.Directorio;

                    lblA5Servidor2.Text = servidorLocal.Local.Servidor;
                    lblA5Usuario2.Text = servidorLocal.Local.Usuario;
                    lblA5Directorio2.Text = servidorLocal.Local.Directorio;

                    break;

                default:
                    pnlPrimero.Visible = true;
                    pnlSegundo.Visible = true;

                    lblA5Servidor1.Text = servidorRemoto.Remoto.Servidor;
                    lblA5Usuario1.Text = servidorRemoto.Remoto.Usuario;
                    lblA5Directorio1.Text = servidorRemoto.Remoto.Directorio;

                    lblA5Servidor2.Text = servidorLocal.Local.Servidor;
                    lblA5Usuario2.Text = servidorLocal.Local.Usuario;
                    lblA5Directorio2.Text = servidorLocal.Local.Directorio;

                    break;
            }
        }

        private void btnA5Continuar_Click(object sender, EventArgs e)
        {
            Principal mostrar = Principal.ObtenerInstancia();

            Hide();

            log.AppendLog("[Configuraciones] Mostrando la ventana principal");

            mostrar.ShowDialog();
        }
    }
}

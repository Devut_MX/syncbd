﻿namespace SyncDB.Vistas
{
    partial class AyudaServidores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AyudaServidores));
            this.lblAH1Cuerpo2 = new DevComponents.DotNetBar.LabelX();
            this.btnAH1Regresar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.lblAH1Cuerpo1 = new DevComponents.DotNetBar.LabelX();
            this.lineAH1Divisor = new DevComponents.DotNetBar.Controls.Line();
            this.lblAH1Encabezado = new DevComponents.DotNetBar.LabelX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblAH1Titulo = new DevComponents.DotNetBar.LabelX();
            this.pbxMinimizar = new System.Windows.Forms.PictureBox();
            this.pbxCerrar = new System.Windows.Forms.PictureBox();
            this.pbxIcono = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).BeginInit();
            this.SuspendLayout();
            // 
            // lblAH1Cuerpo2
            // 
            // 
            // 
            // 
            this.lblAH1Cuerpo2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAH1Cuerpo2.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lblAH1Cuerpo2.ForeColor = System.Drawing.Color.Black;
            this.lblAH1Cuerpo2.Location = new System.Drawing.Point(192, 153);
            this.lblAH1Cuerpo2.Name = "lblAH1Cuerpo2";
            this.lblAH1Cuerpo2.SingleLineColor = System.Drawing.Color.Black;
            this.lblAH1Cuerpo2.Size = new System.Drawing.Size(396, 266);
            this.lblAH1Cuerpo2.TabIndex = 68;
            this.lblAH1Cuerpo2.Text = resources.GetString("lblAH1Cuerpo2.Text");
            this.lblAH1Cuerpo2.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblAH1Cuerpo2.WordWrap = true;
            // 
            // btnAH1Regresar
            // 
            this.btnAH1Regresar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAH1Regresar.BackColor = System.Drawing.Color.Gray;
            this.btnAH1Regresar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAH1Regresar.BorderRadius = 0;
            this.btnAH1Regresar.ButtonText = "Regresar";
            this.btnAH1Regresar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAH1Regresar.DisabledColor = System.Drawing.Color.Gray;
            this.btnAH1Regresar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAH1Regresar.Iconimage = global::SyncBD.Properties.Resources.Back_16px;
            this.btnAH1Regresar.Iconimage_right = null;
            this.btnAH1Regresar.Iconimage_right_Selected = null;
            this.btnAH1Regresar.Iconimage_Selected = null;
            this.btnAH1Regresar.IconMarginLeft = 0;
            this.btnAH1Regresar.IconMarginRight = 0;
            this.btnAH1Regresar.IconRightVisible = true;
            this.btnAH1Regresar.IconRightZoom = 0D;
            this.btnAH1Regresar.IconVisible = true;
            this.btnAH1Regresar.IconZoom = 40D;
            this.btnAH1Regresar.IsTab = false;
            this.btnAH1Regresar.Location = new System.Drawing.Point(464, 425);
            this.btnAH1Regresar.Name = "btnAH1Regresar";
            this.btnAH1Regresar.Normalcolor = System.Drawing.Color.Gray;
            this.btnAH1Regresar.OnHovercolor = System.Drawing.Color.Green;
            this.btnAH1Regresar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAH1Regresar.selected = false;
            this.btnAH1Regresar.Size = new System.Drawing.Size(124, 43);
            this.btnAH1Regresar.TabIndex = 67;
            this.btnAH1Regresar.Text = "Regresar";
            this.btnAH1Regresar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAH1Regresar.Textcolor = System.Drawing.Color.White;
            this.btnAH1Regresar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAH1Regresar.Click += new System.EventHandler(this.btnAH1Regresar_Click);
            // 
            // lblAH1Cuerpo1
            // 
            // 
            // 
            // 
            this.lblAH1Cuerpo1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAH1Cuerpo1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAH1Cuerpo1.ForeColor = System.Drawing.Color.Black;
            this.lblAH1Cuerpo1.Location = new System.Drawing.Point(192, 109);
            this.lblAH1Cuerpo1.Name = "lblAH1Cuerpo1";
            this.lblAH1Cuerpo1.SingleLineColor = System.Drawing.Color.Black;
            this.lblAH1Cuerpo1.Size = new System.Drawing.Size(396, 38);
            this.lblAH1Cuerpo1.TabIndex = 66;
            this.lblAH1Cuerpo1.Text = "Si presenta alguna duda sobre que servidor configurar, lea la descripción que se " +
    "detalla a continuación para cada uno de ellos:";
            this.lblAH1Cuerpo1.WordWrap = true;
            // 
            // lineAH1Divisor
            // 
            this.lineAH1Divisor.ForeColor = System.Drawing.Color.Black;
            this.lineAH1Divisor.Location = new System.Drawing.Point(192, 80);
            this.lineAH1Divisor.Name = "lineAH1Divisor";
            this.lineAH1Divisor.Size = new System.Drawing.Size(396, 23);
            this.lineAH1Divisor.TabIndex = 65;
            this.lineAH1Divisor.Text = "line1";
            // 
            // lblAH1Encabezado
            // 
            // 
            // 
            // 
            this.lblAH1Encabezado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAH1Encabezado.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblAH1Encabezado.ForeColor = System.Drawing.Color.Black;
            this.lblAH1Encabezado.Location = new System.Drawing.Point(192, 47);
            this.lblAH1Encabezado.Name = "lblAH1Encabezado";
            this.lblAH1Encabezado.SingleLineColor = System.Drawing.Color.Black;
            this.lblAH1Encabezado.Size = new System.Drawing.Size(385, 39);
            this.lblAH1Encabezado.TabIndex = 64;
            this.lblAH1Encabezado.Text = "Ayuda para la selección del servidor";
            this.lblAH1Encabezado.WordWrap = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(186, 450);
            this.panel1.TabIndex = 63;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::SyncBD.Properties.Resources.SyncBD;
            this.pictureBox1.Location = new System.Drawing.Point(4, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.Green;
            this.pnlTitleBar.Controls.Add(this.lblAH1Titulo);
            this.pnlTitleBar.Controls.Add(this.pbxMinimizar);
            this.pnlTitleBar.Controls.Add(this.pbxCerrar);
            this.pnlTitleBar.Controls.Add(this.pbxIcono);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(600, 30);
            this.pnlTitleBar.TabIndex = 62;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // lblAH1Titulo
            // 
            // 
            // 
            // 
            this.lblAH1Titulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAH1Titulo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAH1Titulo.ForeColor = System.Drawing.Color.White;
            this.lblAH1Titulo.Location = new System.Drawing.Point(28, 5);
            this.lblAH1Titulo.Name = "lblAH1Titulo";
            this.lblAH1Titulo.Size = new System.Drawing.Size(508, 20);
            this.lblAH1Titulo.TabIndex = 10;
            this.lblAH1Titulo.Text = "SyncBD - Asistente de configuración";
            this.lblAH1Titulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pbxMinimizar
            // 
            this.pbxMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.pbxMinimizar.Image = global::SyncBD.Properties.Resources.Compress_24px;
            this.pbxMinimizar.Location = new System.Drawing.Point(542, 5);
            this.pbxMinimizar.Name = "pbxMinimizar";
            this.pbxMinimizar.Size = new System.Drawing.Size(20, 20);
            this.pbxMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMinimizar.TabIndex = 3;
            this.pbxMinimizar.TabStop = false;
            this.pbxMinimizar.Click += new System.EventHandler(this.pbxMinimizar_Click);
            // 
            // pbxCerrar
            // 
            this.pbxCerrar.BackColor = System.Drawing.Color.Transparent;
            this.pbxCerrar.Image = global::SyncBD.Properties.Resources.CloseWindow_24px;
            this.pbxCerrar.Location = new System.Drawing.Point(568, 5);
            this.pbxCerrar.Name = "pbxCerrar";
            this.pbxCerrar.Size = new System.Drawing.Size(20, 20);
            this.pbxCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCerrar.TabIndex = 2;
            this.pbxCerrar.TabStop = false;
            this.pbxCerrar.Click += new System.EventHandler(this.pbxCerrar_Click);
            // 
            // pbxIcono
            // 
            this.pbxIcono.BackColor = System.Drawing.Color.Transparent;
            this.pbxIcono.Image = global::SyncBD.Properties.Resources.SyncBDLogo;
            this.pbxIcono.Location = new System.Drawing.Point(6, 8);
            this.pbxIcono.Name = "pbxIcono";
            this.pbxIcono.Size = new System.Drawing.Size(16, 16);
            this.pbxIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxIcono.TabIndex = 1;
            this.pbxIcono.TabStop = false;
            this.pbxIcono.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // AyudaServidores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(600, 480);
            this.Controls.Add(this.lblAH1Cuerpo2);
            this.Controls.Add(this.btnAH1Regresar);
            this.Controls.Add(this.lblAH1Cuerpo1);
            this.Controls.Add(this.lineAH1Divisor);
            this.Controls.Add(this.lblAH1Encabezado);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlTitleBar);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AyudaServidores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asistente de configuración | SyncBD";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlTitleBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX lblAH1Cuerpo2;
        private Bunifu.Framework.UI.BunifuFlatButton btnAH1Regresar;
        private DevComponents.DotNetBar.LabelX lblAH1Cuerpo1;
        private DevComponents.DotNetBar.Controls.Line lineAH1Divisor;
        private DevComponents.DotNetBar.LabelX lblAH1Encabezado;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlTitleBar;
        private DevComponents.DotNetBar.LabelX lblAH1Titulo;
        private System.Windows.Forms.PictureBox pbxMinimizar;
        private System.Windows.Forms.PictureBox pbxCerrar;
        private System.Windows.Forms.PictureBox pbxIcono;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
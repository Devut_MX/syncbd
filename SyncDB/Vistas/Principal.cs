﻿using Microsoft.VisualBasic.FileIO;
using Microsoft.Win32;
using SyncBD.Controladores;
using System;
using System.Deployment.Application;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinSCP;
using Logger;
using System.Collections.Generic;
using SyncBD.Vistas;

namespace SyncDB.Vistas
{
    public partial class Principal : Form
    {
        #region Metodos de creacion

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int Param);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        public string ObtenerVersion
        {
            get
            {
                return ApplicationDeployment.IsNetworkDeployed ? ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString() : Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        #endregion
        
        private static Principal singleton;

        public static Principal ObtenerInstancia()
        {
            if (singleton == null || singleton.IsDisposed)
            {
                singleton = new Principal();
            }

            singleton.BringToFront();

            return singleton;
        }

        private Principal()
        {
            InitializeComponent();

            lblVersion.Text = "<p align=\"center\"><b>SyncBD</b><br/><u><b>v. " + ObtenerVersion.Substring(0, ObtenerVersion.Length - 4) + "</b></u></p>";
            lblCopyright.Text = "<p align=\"center\">SyncBD<br/><b>v. " + ObtenerVersion.Substring(0, ObtenerVersion.Length - 4) + "</b><br/>Soluciones Digitales y de Procesos S.A. de C.V.<br/>Todos los derechos reservados © "+ DateTime.Now.ToString("yyyy") +"</p> ";

            //ObtenerDispositivos();

            //LeerTemas();

            LeerAjustes();

            
        }

        private string dirArchivosRemotosOriginal = Application.StartupPath + @"\Archivos_Remotos";

        TimeSpan relojLocal = new TimeSpan(0, 0, 0);
        TimeSpan relojRemoto = new TimeSpan(0, 0, 0);
        private int archivosRemotos = 0;
        private int archivosLocales = 0;

        Nucleo nucleo = new Nucleo();

        Seguridad seguridad = new Seguridad();

        EnlaceModelo enlace = new EnlaceModelo();

        ToLog log = new ToLog();

        #region Efectos de Ventana

        /// <summary>
        /// Detecta cuando se presiona el mouse sobre el titulo de la ventana, para poder arrastrarlo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        /// <summary>
        /// Minimiza el sistema para pasarlo a segundo plano
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbxMinimizar_Click(object sender, EventArgs e)
        {
            try
            {
                WindowState = FormWindowState.Minimized;
            }
            catch (Exception)
            {
                
            }
        }

        #endregion

        #region Metodos Internos

        /// <summary>
        /// Meotodo que enlista los dispositivos logicos y los enlista en un combobox
        /// </summary>
        private void ObtenerDispositivos()
        {
            try
            {
                cbxAJGuardarEn.Items.Clear();

                foreach (var unidad in DriveInfo.GetDrives())
                {
                    if (unidad.DriveType == DriveType.Fixed || unidad.DriveType == DriveType.Removable)
                    {
                        cbxAJGuardarEn.Items.Add(unidad.Name);
                    }
                }

                if (cbxAJGuardarEn.Items.Count < 0)
                {
                    cbxAJGuardarEn.SelectedIndex = 0;
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Obtiene los ajustes del programa configura los controles acorde a las configuraciones
        /// </summary>
        private void LeerAjustes()
        {
            try
            {
                Ajustes ajustes = nucleo.LeerAjustes();

                if (ajustes != null)
                {
                    try
                    {
                        if (ajustes.Apariencia != null)
                        {
                            if (ajustes.Apariencia == "")
                            {
                                cbxAJApariencia.Items.Clear();

                                cbxAJApariencia.Items.Add("Por defecto...");

                                foreach (var tema in LeerTemas())
                                {
                                    cbxAJApariencia.Items.Add(Path.GetFileNameWithoutExtension(tema));
                                }

                                if (cbxAJApariencia.Items.Contains(ajustes.Apariencia))
                                {
                                    cbxAJApariencia.SelectedItem = ajustes.Apariencia;
                                    CargarApariencia();
                                }
                            }

                            else
                            {
                                cbxAJApariencia.Items.Clear();

                                cbxAJApariencia.Items.Add("Por defecto...");

                                foreach (var tema in LeerTemas())
                                {
                                    cbxAJApariencia.Items.Add(Path.GetFileNameWithoutExtension(tema));
                                }

                                if (cbxAJApariencia.Items.Contains(ajustes.Apariencia))
                                {
                                    cbxAJApariencia.SelectedItem = ajustes.Apariencia;
                                    CargarApariencia();
                                }
                            }
                        }

                        else
                        {
                            cbxAJApariencia.Items.Clear();

                            cbxAJApariencia.Items.Add("Por defecto...");

                            cbxAJApariencia.SelectedIndex = 0;
                        }
                    }
                    catch (Exception)
                    {
                        
                    }
                    

                    chkAJDistinguirSincronizacion.Value = ajustes.DiferenteSincronizacion;
                    chkAJIniciarConWindows.Value = ajustes.IniciarConWindows;
                    chkAJMinimizarIniciar.Value = ajustes.MinimizarAlIniciar;
                    chkAJRutaPermanente.Value = ajustes.RutaGuardadoPermanente;
                    lblAJRutaGuardado.Text = ajustes.CopiarA;

                    log.AppendLog("[Principal] Se leyeron los ajustes correctamente");
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x006, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x006", PopUp.Icono.Error);
            }
        }

        /// <summary>
        /// Obtiene un listado de los temas disponibles
        /// </summary>
        private List<string> LeerTemas()
        {
            try
            {
                List<string> temas = nucleo.ObtenerTemas();
                
                return temas != null ? temas : null;
            }
            catch (Exception)
            {
                return null;
                //Agregar codigo de error
            }
        }

        private void CargarApariencia()
        {
            try
            {
                Ajustes ajustes = nucleo.LeerAjustes();

                if (!string.IsNullOrWhiteSpace(ajustes.Apariencia))
                {
                    Tema tema = nucleo.LeerTema(ajustes.Apariencia);

                    if (tema != null)
                    {
                        pnlTitleBar.BackColor = ColorTranslator.FromHtml(tema.FondoTitulo);
                        lblPTitulo.ForeColor = ColorTranslator.FromHtml(tema.TextoTitulo);
                        pnlAjustes.BackColor = ColorTranslator.FromHtml(tema.FondoVentana);
                        pnlAyuda.BackColor = ColorTranslator.FromHtml(tema.FondoVentana);
                        pnlRemoto.BackColor = ColorTranslator.FromHtml(tema.FondoVentana);
                        pnlLocal.BackColor = ColorTranslator.FromHtml(tema.FondoVentana);
                        pnlBaseSeleccion.BackColor = ColorTranslator.FromHtml(tema.FondoVentana);
                        pnlErroresEInformacion.BackColor = ColorTranslator.FromHtml(tema.FondoVentana);
                        BackColor = ColorTranslator.FromHtml(tema.FondoVentana);

                        foreach (Control control in pnlAyuda.Controls)
                        {
                            if (control is DevComponents.DotNetBar.LabelX)
                            {
                                ((DevComponents.DotNetBar.LabelX)control).ForeColor = ColorTranslator.FromHtml(tema.TextoVentana);
                            }

                            if (control is Bunifu.Framework.UI.BunifuFlatButton)
                            {
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Normalcolor = ColorTranslator.FromHtml(tema.FondoBoton);
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Textcolor = ColorTranslator.FromHtml(tema.TextoBoton);
                            }
                        }

                        foreach (Control control in pnlAjustes.Controls)
                        {
                            if (control is DevComponents.DotNetBar.LabelX)
                            {
                                ((DevComponents.DotNetBar.LabelX)control).ForeColor = ColorTranslator.FromHtml(tema.TextoVentana);
                            }

                            if (control is Bunifu.Framework.UI.BunifuFlatButton)
                            {
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Normalcolor = ColorTranslator.FromHtml(tema.FondoBoton);
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Textcolor = ColorTranslator.FromHtml(tema.TextoBoton);
                            }
                        }

                        foreach (Control control in pnlRemoto.Controls)
                        {
                            if (control is DevComponents.DotNetBar.LabelX)
                            {
                                ((DevComponents.DotNetBar.LabelX)control).ForeColor = ColorTranslator.FromHtml(tema.TextoVentana);
                            }

                            if (control is Bunifu.Framework.UI.BunifuFlatButton)
                            {
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Normalcolor = ColorTranslator.FromHtml(tema.FondoBoton);
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Textcolor = ColorTranslator.FromHtml(tema.TextoBoton);
                            }
                        }

                        foreach (Control control in pnlLocal.Controls)
                        {
                            if (control is DevComponents.DotNetBar.LabelX)
                            {
                                ((DevComponents.DotNetBar.LabelX)control).ForeColor = ColorTranslator.FromHtml(tema.TextoVentana);
                            }

                            if (control is Bunifu.Framework.UI.BunifuFlatButton)
                            {
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Normalcolor = ColorTranslator.FromHtml(tema.FondoBoton);
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Textcolor = ColorTranslator.FromHtml(tema.TextoBoton);
                            }
                        }

                        foreach (Control control in pnlBaseSeleccion.Controls)
                        {
                            if (control is DevComponents.DotNetBar.LabelX)
                            {
                                ((DevComponents.DotNetBar.LabelX)control).ForeColor = ColorTranslator.FromHtml(tema.TextoVentana);
                            }

                            if (control is Bunifu.Framework.UI.BunifuFlatButton)
                            {
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Normalcolor = ColorTranslator.FromHtml(tema.FondoBoton);
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Textcolor = ColorTranslator.FromHtml(tema.TextoBoton);
                            }
                        }

                        foreach (Control control in pnlErroresEInformacion.Controls)
                        {
                            if (control is DevComponents.DotNetBar.LabelX)
                            {
                                ((DevComponents.DotNetBar.LabelX)control).ForeColor = ColorTranslator.FromHtml(tema.TextoVentana);
                            }

                            if (control is Bunifu.Framework.UI.BunifuFlatButton)
                            {
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Normalcolor = ColorTranslator.FromHtml(tema.FondoBoton);
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Textcolor = ColorTranslator.FromHtml(tema.TextoBoton);
                            }
                        }

                        txtHBuscarError.ForeColor = ColorTranslator.FromHtml(tema.TextoVentana);
                        rtbHCuerpoCodigo.BackColor = ColorTranslator.FromHtml(tema.FondoVentana);
                        rtbHCuerpoCodigo.ForeColor = ColorTranslator.FromHtml(tema.TextoVentana);
                        txtHBuscarError.Refresh();
                        rtbHCuerpoCodigo.Refresh();
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Guarda los ajustes en el archivo JSON
        /// </summary>
        private void GuardarAjustes()
        {
            try
            {
                nucleo.GuardarAjustes(new Ajustes
                {
                    Apariencia = cbxAJApariencia.SelectedIndex <= 0 ? "" : cbxAJApariencia.SelectedItem.ToString(),
                    CopiarA = lblAJRutaGuardado.Text != "" ? lblAJRutaGuardado.Text : string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"\SyncBD"),
                    Idioma = cbxAJIdioma.Items.Count <= 0 ? "" : cbxAJIdioma.SelectedItem.ToString(),
                    DiferenteSincronizacion = chkAJDistinguirSincronizacion.Value,
                    RutaGuardadoPermanente = chkAJRutaPermanente.Value,
                    MinimizarAlIniciar = chkAJMinimizarIniciar.Value,
                    IniciarConWindows = chkAJIniciarConWindows.Value
                });

                log.AppendLog("[Principal] Se guardaron los ajustes correctamente");
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x007, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x007", PopUp.Icono.Error);
            }
        }

        /// <summary>
        /// Restaura todos los ajustes a forma predeterminada
        /// </summary>
        private void RestaurarAjustes()
        {
            try
            {
                nucleo.GuardarAjustes(new Ajustes
                {
                    Apariencia = "",
                    CopiarA = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"\SyncBD"),
                    Idioma = "",
                    DiferenteSincronizacion = false,
                    RutaGuardadoPermanente = false,
                    MinimizarAlIniciar = false,
                    IniciarConWindows = false
                });

                log.AppendLog("[Principal] Se restauraron los ajustes correctamente");

                ObtenerDispositivos();

                LeerAjustes();
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x008, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x008", PopUp.Icono.Error);
            }
        }

        /// <summary>
        /// Verfica que el equipo cuente con conexion a internet
        /// </summary>
        /// <returns>Devuelve true cuando existe conexion a internet</returns>
        private bool VerificarConexionInternet()
        {
            try
            {
                Ping peticion = new Ping();

                PingReply respuesta = peticion.Send("www.google.com.mx");

                log.AppendLog("[Internet] Conexión a internet detectada");

                return true;
            }
            catch (Exception)
            {
                log.AppendLog("[Internet] Sin conexión a internet");

                return false;
            }
        }

        /// <summary>
        /// Obtiene los movimientos de la base de datos para llenar los datagridview (Ajusta el ancho de las columnas)
        /// </summary>
        private void CargarMovimientos()
        {
            try
            {
                enlace.ListaMovimientosServidorRemoto(dgvSRHistorial);
                enlace.ListaMovimientosServidorLocal(dgvSLHistorial);

                if (dgvSRHistorial.RowCount > 0)
                {
                    dgvSRHistorial.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                }

                if (dgvSLHistorial.RowCount > 0)
                {
                    dgvSLHistorial.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x009, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x009", PopUp.Icono.Error);
            }
        }

        /// <summary>
        /// Determina la visibilidad de los botones de sincronizacion con servidores dependiendo si existen configuraciones o no
        /// </summary>
        private void VerificarVisibilidadServidores()
        {
            try
            {
                Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                if (configuraciones != null)
                {
                    if (!string.IsNullOrEmpty(configuraciones.Remoto.Servidor))
                    {
                        btnPSincronizarRemoto.Visible = true;

                        pnlRemoto.BringToFront();

                        pnlRemoto.Visible = true;
                    }

                    else
                    {
                        btnPSincronizarRemoto.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(configuraciones.Local.Servidor))
                    {
                        btnPSincronizarLocal.Visible = true;

                        pnlLocal.BringToFront();

                        pnlLocal.Visible = true;
                    }

                    else
                    {
                        btnPSincronizarLocal.Visible = false;
                    }

                    if (!string.IsNullOrEmpty(configuraciones.Local.Servidor) && !string.IsNullOrEmpty(configuraciones.Remoto.Servidor))
                    {
                        btnPSincronizarRemoto.Visible = true;

                        btnPSincronizarLocal.Visible = true;

                        pnlRemoto.BringToFront();

                        pnlRemoto.Visible = true;
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Determina que botones, etiquetas y demás controles, serán visibles cuando esta activa o no la sincronización del servidor remoto
        /// </summary>
        /// <param name="visible">True para que haga visible todos los controles, restaurados en su forma original</param>
        private void VerificarVisibilidadBotonesRemoto(bool visible)
        {
            if (visible)
            {
                relojRemoto = new TimeSpan(0, 0, 0);
                tmpRemotoTiempoTranscurrido.Enabled = false;
                lblSRTiempoTranscurrido.Visible = false;
                lblSRTiempoTranscurrido.Text = "";
                pbrSRProgreso.Value = 0;
                btnPSincronizarRemoto.Visible = true;
                btnSRResincronizar.Visible = true;
                btnStartRemoteStrip.Visible = true;
                lblSRPorcentaje.Text = "0% completado";
                lblSRVelocidad.Visible = false;
                lblSRVelocidad.Text = "";
                pbxSRVelocidad.Visible = false;
                pbxSRTiempo.Visible = false;
                lblSRArchivoActual.Visible = false;
                lblSRPorcentajeIndividual.Visible = false;
                lblSRPorcentajeIndividual.Text = "0% completado";
                pbrSRProgresoIndividual.Value = 0;
                pbrSRProgresoIndividual.Visible = false;

                btnSLResincronizar.Visible = true;
                btnStartLocalStrip.Visible = true;
                btnSLAbrirCarpeta.Visible = true;
            }

            else
            {
                pbrSRProgreso.Value = 0;
                tmpRemotoTiempoTranscurrido.Enabled = true;
                lblSRTiempoTranscurrido.Visible = true;
                lblSRTiempoTranscurrido.Text = "";
                btnPSincronizarRemoto.Visible = true;
                btnSRResincronizar.Visible = false;
                btnStartRemoteStrip.Visible = false;
                lblSRVelocidad.Visible = true;
                pbxSRVelocidad.Visible = true;
                pbxSRTiempo.Visible = true;
                lblSRArchivoActual.Visible = true;
                lblSRPorcentajeIndividual.Visible = true;
                pbrSRProgresoIndividual.Value = 0;
                pbrSRProgresoIndividual.Visible = true;

                btnSLResincronizar.Visible = false;
                btnStartLocalStrip.Visible = false;
                btnSLAbrirCarpeta.Visible = false;
            }
        }

        /// <summary>
        /// Determina que botones, etiquetas y demás controles, serán visibles cuando esta activa o no la sincronización del servidor local
        /// </summary>
        /// <param name="visible">True para que haga visible todos los controles, restaurados en su forma original</param>
        private void VerificarVisibilidadBotonesLocal(bool visible)
        {
            if (visible)
            {
                relojLocal = new TimeSpan(0, 0, 0);
                tmpLocalTiempoTranscurrido.Enabled = false;
                lblSLTiempoTranscurrido.Visible = false;
                lblSLTiempoTranscurrido.Text = "";
                pbrSLProgreso.Value = 0;
                btnPSincronizarLocal.Visible = true;
                btnSLResincronizar.Visible = true;
                btnStartLocalStrip.Visible = true;
                lblSLPorcentaje.Text = "0% completado";
                lblSLVelocidad.Visible = false;
                lblSLVelocidad.Text = "";
                pbxSLVelocidad.Visible = false;
                pbxSLTiempo.Visible = false;
                btnSLAbrirCarpeta.Visible = true;

                lblSLArchivoActual.Visible = false;
                lblSLVelocidad.Visible = false;
                pbrSLProgresoIndividual.Value = 0;
                pbrSLProgresoIndividual.Visible = false;
                lblSLPorcentajeIndividual.Text = "0% completado";
                lblSLPorcentajeIndividual.Visible = false;

                btnSRResincronizar.Visible = true;
                btnStartRemoteStrip.Visible = true;
            }

            else
            {
                pbrSLProgreso.Value = 0;
                tmpLocalTiempoTranscurrido.Enabled = true;
                lblSLTiempoTranscurrido.Visible = true;
                lblSLTiempoTranscurrido.Text = "";
                btnPSincronizarLocal.Visible = true;
                btnSLResincronizar.Visible = false;
                btnStartLocalStrip.Visible = false;
                lblSLVelocidad.Visible = true;
                pbxSLVelocidad.Visible = true;
                pbxSLTiempo.Visible = true;
                btnSLAbrirCarpeta.Visible = false;

                lblSLArchivoActual.Visible = true;
                lblSLVelocidad.Visible = true;
                pbrSLProgresoIndividual.Value = 0;
                pbrSLProgresoIndividual.Visible = true;
                lblSLPorcentajeIndividual.Visible = true;


                btnSRResincronizar.Visible = false;
                btnStartRemoteStrip.Visible = false;
            }
        }

        /// <summary>
        /// Verfica la existencia de un directorio
        /// </summary>
        /// <param name="directorio">Path del directorio a validar</param>
        /// <returns>True cuando el directorio existe</returns>
        private bool ValidarDirectorio(string directorio)
        {
            try
            {
                if (Directory.Exists(directorio))
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Verifica la existencia de la base de datos en la carpeta "General", sino existe, procede a copiarla de Origin.db
        /// </summary>
        private void ValidarHistorial()
        {
            try
            {
                if (!Directory.Exists(string.Concat(Application.StartupPath, @"\General")))
                {
                    Directory.CreateDirectory(string.Concat(Application.StartupPath, @"\General"));
                }

                if (!File.Exists(string.Concat(Application.StartupPath, @"\General\Historial.db")))
                {
                    FileSystem.CopyFile(string.Concat(Application.StartupPath, @"\Origin.db"), string.Concat(Application.StartupPath, @"\General\Historial.db"), true);
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x010, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x010", PopUp.Icono.Error);
            }
        }

        /// <summary>
        /// Copia todos los archivos y subcarpetas de un directorio en especifico
        /// </summary>
        /// <param name="pathOrigen">Directorio a transferir</param>
        /// <param name="pathDestino">Destino del directorio a transferir</param>
        /// <returns>True cuando se copió la carpeta correctamente</returns>
        private bool Transferir(string pathOrigen, string pathDestino)
        {
            try
            {
                if (!Directory.Exists(pathDestino))
                {
                    Directory.CreateDirectory(pathDestino);
                }

                FileSystem.CopyDirectory(pathOrigen, pathDestino, UIOption.AllDialogs, UICancelOption.ThrowException);

                return true;
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x011, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x011", PopUp.Icono.Error);

                return false;
            }
        }

        /// <summary>
        /// Copia todos los archivos y subcarpetas de un directorio en especifico de forma asincrona, sin avisos al usuario
        /// </summary>
        /// <param name="pathOrigen">Directorio a transferir</param>
        /// <param name="pathDestino">Destino del directorio a transferir</param>
        /// <param name="copiarSubDirectorios">True cuando se requiere copiar subdirectorios</param>
        /// <returns>True cuando se copió la carpeta correctamente</returns>
        private bool CopiarDirectorioAsincrono(string pathOrigen, string pathDestino, bool copiarSubDirectorios)
        {
            try
            {
                DirectoryInfo directorio = new DirectoryInfo(pathOrigen);

                if (!directorio.Exists)
                {
                    throw new DirectoryNotFoundException("No se encontró la carpeta de origen de datos: " + pathOrigen);
                }

                DirectoryInfo[] directorios = directorio.GetDirectories();

                if (!Directory.Exists(pathDestino))
                {
                    Directory.CreateDirectory(pathDestino);
                }

                FileInfo[] archivos = directorio.GetFiles();

                foreach (FileInfo archivo in archivos)
                {
                    string pathTemporal = Path.Combine(pathDestino, archivo.Name);
                    archivo.CopyTo(pathTemporal, true);
                }

                if (copiarSubDirectorios)
                {
                    foreach (DirectoryInfo subdirectorio in directorios)
                    {
                        string pathTemporal = Path.Combine(pathDestino, subdirectorio.Name);
                        CopiarDirectorioAsincrono(subdirectorio.FullName, pathTemporal, copiarSubDirectorios);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x012, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x012", PopUp.Icono.Error);

                return false;
            }
        }

        #endregion

        #region Inicio del programa

        /// <summary>
        /// Determina y configura el programa la primera vez que carga
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Principal_Shown(object sender, EventArgs e)
        {
            try
            {
                ValidarHistorial();

                CargarMovimientos();

                VerificarVisibilidadServidores();

                log.AppendLog("[Principal] Se verifico y cargo historial");

                Ajustes ajustes = nucleo.LeerAjustes();

                if (ajustes != null)
                {
                    if (ajustes.MinimizarAlIniciar)
                    {
                        WindowState = FormWindowState.Minimized;
                    }
                }

                if (VerificarConexionInternet())
                {
                    Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                    if (configuraciones != null)
                    {
                        if (!string.IsNullOrEmpty(configuraciones.Remoto.Servidor))
                        {
                            SincronizarConRemoto();
                        }

                        else if (!string.IsNullOrEmpty(configuraciones.Local.Servidor))
                        {
                            SincronizarConLocal();
                        }
                    }
                }

                else
                {
                    Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                    if (configuraciones != null)
                    {
                        if (!string.IsNullOrEmpty(configuraciones.Local.Servidor))
                        {
                            SincronizarConLocal();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x013, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x013", PopUp.Icono.Error);
            }
        }

        #endregion

        #region Metodos transferencia Remoto

        /// <summary>
        /// Progreso total y velocidad de conexión con el servidor remoto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sesion_FileTransferProgress(object sender, FileTransferProgressEventArgs e)
        {
            lblSRArchivoActual.Invoke(new Action(() => lblSRArchivoActual.Text = Path.GetFileName(e.FileName)));
            //lblSRArchivoActual.Invoke(new Action(() => lblSRArchivoActual.Refresh()));

            if (pbrSRProgreso.Value > 0)
            {
                pbrSRProgreso.Invoke(new Action(() => pbrSRProgreso.Value = 0));
            }

            if (pbrSRProgresoIndividual.Value > 0)
            {
                pbrSRProgresoIndividual.Invoke(new Action(() => pbrSRProgresoIndividual.Value = 0));
            }
            
            lblSRVelocidad.Invoke(new Action(() => lblSRVelocidad.Text = string.Format("Velocidad actual: {0} Kb/s.", (e.CPS / 1024).ToString())));
            //lblSRVelocidad.Invoke(new Action(() => lblSRVelocidad.Refresh()));

            pbrSRProgreso.Invoke(new Action(() => pbrSRProgreso.Value = (Convert.ToInt32(e.OverallProgress * 100))));
            pbrSRProgresoIndividual.Invoke(new Action(() => pbrSRProgresoIndividual.Value = (Convert.ToInt32(e.FileProgress * 100))));

            lblSRPorcentajeIndividual.Invoke(new Action(() => lblSRPorcentajeIndividual.Text = string.Format("{0}% completado", (e.FileProgress * 100))));
            //lblSRPorcentajeIndividual.Invoke(new Action(() => lblSRPorcentajeIndividual.Refresh()));

            lblSRPorcentaje.Invoke(new Action(() => lblSRPorcentaje.Text = string.Format("{0}% completado", (e.OverallProgress * 100))));
            //lblSRPorcentaje.Invoke(new Action(() => lblSRPorcentaje.Refresh()));

            archivosRemotos++;
        }

        /// <summary>
        /// Sesión que enlaza con el servidor remoto para realizar la sincronización
        /// </summary>
        /// <param name="cliente">Datos de la sesion para conectar con el servidor (Requiere previa validación)</param>
        /// <param name="servidor">Datos del servidor remoto al que se conectará</param>
        /// <returns>Devuelve "Sincronización con el servidor remoto completada" cuando termina correctamente</returns>
        private bool SincronizarDatosRemotos(SessionOptions cliente, Servidores servidor)
        {
            try
            {
                if (!Directory.Exists(dirArchivosRemotosOriginal))
                {
                    Directory.CreateDirectory(dirArchivosRemotosOriginal);
                }

                bool resultado = false;

                using (Session sesion = new Session())
                {
                    //sesion.FileTransferred += Sesion_FileTransferred;

                    sesion.Failed += Sesion_Failed;

                    sesion.FileTransferProgress += Sesion_FileTransferProgress;

                    sesion.ReconnectTimeInMilliseconds = 3000;

                    sesion.XmlLogPreserve = false;

                    sesion.Open(cliente);

                    SynchronizationResult resultadoSincronizacion;

                    resultadoSincronizacion = sesion.SynchronizeDirectories(SynchronizationMode.Local, dirArchivosRemotosOriginal, seguridad.Desencriptar(servidor.Remoto.Directorio), true, false, SynchronizationCriteria.Either);

                    resultadoSincronizacion.Check();

                    resultado = resultadoSincronizacion.IsSuccess;
                }

                return resultado;
            }
            catch (Exception ex)
            {
                VerificarVisibilidadBotonesRemoto(true);

                log.AppendLog("[Error] Se produjo el error con Clave: 0x014, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x014", PopUp.Icono.Error);

                return false;
            }
        }

        private void Sesion_Failed(object sender, FailedEventArgs e)
        {
            try
            {
                ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia(e.Error.Message.ToString(), "SyncBD | Error de transferencia con servidor Remoto", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Error);

                mensaje.ShowDialog();

                mensaje.Dispose();
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Comienza la sincronización con el servidor remoto de forma asincrona
        /// </summary>
        private async void SincronizarConRemoto()
        {
            try
            {
                Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                if (configuraciones.Remoto.Servidor != "")
                {
                    if (string.IsNullOrEmpty(configuraciones.Remoto.Servidor))
                    {
                        lblSREstado.Text = "No ha configurado el servidor remoto";

                        nucleo.MostrarPopUp("No hay configuraciones del servidor remoto", "Servidor remoto sin configurar...", PopUp.Icono.Warning);
                    }

                    else
                    {
                        SessionOptions cliente = nucleo.ValidarServidorRemoto(configuraciones);

                        if (cliente != null)
                        {
                            VerificarVisibilidadBotonesRemoto(false);

                            nucleo.MostrarPopUp("Sincronizando con servidor remoto", "Iniciando conexión...", PopUp.Icono.Info);

                            //lblSREstado.Text = "Inspeccionando servidor remoto e iniciando sincronización...";

                            archivosRemotos = 0;

                            lblSREstado.Refresh();

                            Task<bool> comenzarSincronizacion = new Task<bool>(() => SincronizarDatosRemotos(cliente, configuraciones));
                            comenzarSincronizacion.Start();

                            if (await comenzarSincronizacion == true)
                            {
                                nucleo.MostrarPopUp("Se ha completado la sincronización con el servidor Remoto", "Sincronización completa", PopUp.Icono.Ok);

                                VerificarVisibilidadBotonesRemoto(true);

                                lblSREstado.Text = "SyncBD ya está sincronizado con el servidor Remoto.";

                                enlace.CrearMovimientoRemoto(new MovimientoRemoto { Fecha = DateTime.Now, Archivos = archivosRemotos, Desde = seguridad.Desencriptar(configuraciones.Remoto.Servidor), Hacia = "Equipo Local" });

                                Ajustes ajustes = nucleo.LeerAjustes();

                                if (archivosRemotos > 0 || !Directory.EnumerateDirectories(ajustes.CopiarA).Any())
                                {
                                    if (ajustes.CopiarA != null)
                                    {
                                        string nombreCarpeta = "SyncBD_" + DateTime.Now.ToString("yyyyMMddHHmmss");

                                        if (!Directory.Exists(string.Concat(ajustes.CopiarA, @"\", nombreCarpeta)))
                                        {
                                            Directory.CreateDirectory(string.Concat(ajustes.CopiarA, @"\", nombreCarpeta));
                                        }

                                        if (Transferir(dirArchivosRemotosOriginal, string.Concat(ajustes.CopiarA, @"\", nombreCarpeta)))
                                        {
                                            nucleo.MostrarPopUp("Se han transferido los archivos al directorio copia.", "Archivos transferidos", PopUp.Icono.Ok);
                                        }

                                        else
                                        {
                                            nucleo.MostrarPopUp("Ocurrió un imprevisto al transferir los archivos.", "Archivos no transferidos", PopUp.Icono.Warning);
                                        }
                                    }

                                    else
                                    {
                                        VerificarVisibilidadBotonesRemoto(true);

                                        nucleo.MostrarPopUp("No ha configurado la dirección de guardado de archivos", "Sin dirección de guardado", PopUp.Icono.Warning);
                                    }
                                }

                                else
                                {
                                    VerificarVisibilidadBotonesRemoto(true);

                                    nucleo.MostrarPopUp("Debido a que no hay cambios, puede utilizar la última carpeta copiada", "Sin copia de respaldo", PopUp.Icono.Ok);
                                }
                            }

                            else
                            {
                                VerificarVisibilidadBotonesRemoto(true);

                                nucleo.MostrarPopUp("No se pudo completar la sincronización con el servidor Remoto", "Sincronización incompleta", PopUp.Icono.Error);
                            }
                        }

                        else
                        {
                            VerificarVisibilidadBotonesRemoto(true);

                            nucleo.MostrarPopUp("Se perdió la comunicación con el servidor Remoto, reintente por favor", "Servidor inaccesible", PopUp.Icono.Warning);
                        }
                    }
                }

                else
                {
                    nucleo.MostrarPopUp("El servidor remoto no tiene configuración", "Faltan datos de conexión", PopUp.Icono.Error);
                }

                CargarMovimientos();
            }
            catch (Exception ex)
            {
                VerificarVisibilidadBotonesRemoto(true);

                log.AppendLog("[Error] Se produjo el error con Clave: 0x014, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x014", PopUp.Icono.Error);
            }
        }

        #endregion

        #region Metodos transferencia Local
        
        /// <summary>
        /// Progreso total y velocidad de conexión con el servidor local
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sesion_FileTransferProgress1(object sender, FileTransferProgressEventArgs e)
        {
            lblSLArchivoActual.Invoke(new Action(() => lblSLArchivoActual.Text = Path.GetFileName(e.FileName)));
            //lblSLArchivoActual.Invoke(new Action(() => lblSLArchivoActual.Refresh()));

            if (pbrSLProgreso.Value > 0)
            {
                pbrSLProgreso.Invoke(new Action(() => pbrSLProgreso.Value = 0));
            }

            if (pbrSLProgresoIndividual.Value > 0)
            {
                pbrSRProgresoIndividual.Invoke(new Action(() => pbrSRProgresoIndividual.Value = 0));
            }

            lblSLVelocidad.Invoke(new Action(() => lblSLVelocidad.Text = string.Format("Velocidad actual: {0} Kb/s.", (e.CPS / 1024).ToString())));
            //lblSLVelocidad.Invoke(new Action(() => lblSLVelocidad.Refresh()));
            
            pbrSLProgreso.Invoke(new Action(() => pbrSLProgreso.Value = (Convert.ToInt32(e.OverallProgress * 100))));
            pbrSLProgresoIndividual.Invoke(new Action(() => pbrSLProgresoIndividual.Value = (Convert.ToInt32(e.FileProgress * 100))));
            
            lblSLPorcentajeIndividual.Invoke(new Action(() => lblSLPorcentajeIndividual.Text = string.Format("{0}% completado", (e.FileProgress * 100))));
            //lblSLPorcentajeIndividual.Invoke(new Action(() => lblSLPorcentajeIndividual.Refresh()));

            lblSLPorcentaje.Invoke(new Action(() => lblSLPorcentaje.Text = string.Format("{0}% completado", (e.OverallProgress * 100))));
            //lblSLPorcentaje.Invoke(new Action(() => lblSLPorcentaje.Refresh()));

            archivosLocales++;
        }

        private void Sesion_Failed1(object sender, FailedEventArgs e)
        {
            try
            {
                ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia(e.Error.Message.ToString(), "SyncBD | Error de transferencia con servidor Local", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Error);

                mensaje.ShowDialog();

                mensaje.Dispose();
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Sesión que enlaza con el servidor remoto para realizar la sincronización
        /// </summary>
        /// <param name="cliente">Datos de la sesion para conectar con el servidor (Requiere previa validación)</param>
        /// <param name="servidor">Datos del servidor remoto al que se conectará</param>
        /// <returns>Devuelve "Sincronización con servidor local completada" cuando termina correctamente</returns>
        private bool SincronizarDatosLocales(SessionOptions cliente, Servidores servidor)
        {
            try
            {
                if (!Directory.Exists(dirArchivosRemotosOriginal))
                {
                    Directory.CreateDirectory(dirArchivosRemotosOriginal);
                }

                bool resultado = false;

                using (Session sesion = new Session())
                {
                    //sesion.FileTransferred += Sesion_FileTransferred1;

                    sesion.FileTransferProgress += Sesion_FileTransferProgress1;

                    sesion.Failed += Sesion_Failed1;

                    sesion.ReconnectTimeInMilliseconds = 3000;

                    sesion.XmlLogPreserve = false;

                    sesion.Open(cliente);

                    SynchronizationResult resultadoSincronizacion;
                    resultadoSincronizacion = sesion.SynchronizeDirectories(SynchronizationMode.Remote, dirArchivosRemotosOriginal, seguridad.Desencriptar(servidor.Local.Directorio), false, false, SynchronizationCriteria.Either);

                    resultadoSincronizacion.Check();

                    resultado = resultadoSincronizacion.IsSuccess;
                }

                return resultado;

            }
            catch (Exception ex)
            {
                VerificarVisibilidadBotonesRemoto(true);

                log.AppendLog("[Error] Se produjo el error con Clave: 0x014, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x014", PopUp.Icono.Error);

                return false;
            }
        }

        /// <summary>
        /// Comienza la sincronización con el servidor local de forma asincrona
        /// </summary>
        private async void SincronizarConLocal()
        {
            try
            {
                Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                if (configuraciones.Local.Servidor != null)
                {
                    if (string.IsNullOrEmpty(configuraciones.Local.Servidor))
                    {
                        lblSLEstado.Text = "No ha configurado el servidor local";

                        nucleo.MostrarPopUp("No hay configuraciones del servidor local", "Servidor local sin configurar...", PopUp.Icono.Warning);
                    }

                    else
                    {
                        SessionOptions cliente = nucleo.ValidarServidorLocal(configuraciones);

                        if (cliente != null)
                        {
                            VerificarVisibilidadBotonesLocal(false);

                            nucleo.MostrarPopUp("Sincronizando con servidor local", "Iniciando conexión...", PopUp.Icono.Info);

                            //lblSREstado.Text = "Inspeccionando servidor local e iniciando sincronización...";

                            lblSREstado.Refresh();

                            archivosLocales = 0;

                            Task<bool> comenzarSincronizacion = new Task<bool>(() => SincronizarDatosLocales(cliente, configuraciones));
                            comenzarSincronizacion.Start();

                            if (await comenzarSincronizacion == true)
                            {
                                nucleo.MostrarPopUp("Se ha completado la sincronización con el servidor Local", "Sincronización completa", PopUp.Icono.Ok);

                                VerificarVisibilidadBotonesLocal(true);

                                lblSLEstado.Text = "El servidor Local ya está sincronizado con SyncBD.";

                                lblSLEstado.Refresh();

                                enlace.CrearMovimientoLocal(new MovimientoLocal { Fecha = DateTime.Now, Archivos = archivosLocales, Desde = "Equipo Local", Hacia = seguridad.Desencriptar(configuraciones.Local.Servidor) });
                            }

                            else
                            {
                                VerificarVisibilidadBotonesLocal(true);

                                nucleo.MostrarPopUp("Reintente sincronización", "Verificando los ajustes", PopUp.Icono.Warning);
                            }
                        }

                        else
                        {
                            VerificarVisibilidadBotonesRemoto(true);

                            nucleo.MostrarPopUp("Se perdió la comunicación con el servidor Local, reintente por favor", "Servidor inaccesible", PopUp.Icono.Warning);
                        }
                    }
                }

                else
                {
                    nucleo.MostrarPopUp("El servidor local no tiene configuración", "Faltan datos de conexión", PopUp.Icono.Error);
                }

                CargarMovimientos();
            }
            catch (Exception ex)
            {
                VerificarVisibilidadBotonesRemoto(true);

                log.AppendLog("[Error] Se produjo el error con Clave: 0x014, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x014", PopUp.Icono.Error);
            }
        }

        #endregion

        #region Hamburguer Menu

        /// <summary>
        /// Detecta que boton del menu tiene actualmente el foco, para posicionar la barra lateral
        /// </summary>
        /// <param name="Y">Representa la posicion Y donde se ubicará el panel secundario</param>
        /// <param name="altura">Representa la altura que tendrá el panel secundario</param>
        /// <param name="color">Representa el color que tendrá el panel secundario</param>
        private void MoverPanelSeleccion(int Y, int altura, Color color)
        {
            pnlSeleccion.Location = new Point(pnlSeleccion.Location.X, Y);
            pnlSeleccion.Size = new Size(5, altura);
            pnlSeleccion.BackColor = color;
        }

        private void btnPSalir_Enter(object sender, EventArgs e)
        {
            MoverPanelSeleccion(btnPSalir.Location.Y, btnPSalir.Size.Height, Color.Red);
        }

        private void btnPAyuda_Enter(object sender, EventArgs e)
        {
            MoverPanelSeleccion(btnPAyuda.Location.Y, btnPAyuda.Size.Height, Color.SkyBlue);
        }

        private void btnPAjustes_Enter(object sender, EventArgs e)
        {
            MoverPanelSeleccion(btnPAjustes.Location.Y, btnPAjustes.Size.Height, Color.Teal);
        }

        private void btnPSincronizarLocal_Enter(object sender, EventArgs e)
        {
            MoverPanelSeleccion(btnPSincronizarLocal.Location.Y, btnPSincronizarLocal.Size.Height, Color.Orange);
        }

        private void btnPSincronizarRemoto_Enter(object sender, EventArgs e)
        {
            MoverPanelSeleccion(btnPSincronizarRemoto.Location.Y, btnPSincronizarRemoto.Size.Height, Color.Green);
        }

        private void btnPHamburguer_Enter(object sender, EventArgs e)
        {
            MoverPanelSeleccion(btnPHamburguer.Location.Y, btnPHamburguer.Size.Height, Color.FromArgb(255, 38, 65, 84));
        }

        private void btnPSincronizarRemoto_Click(object sender, EventArgs e)
        {
            CargarMovimientos();
            pnlRemoto.Visible = true;
            pnlRemoto.BringToFront();
        }

        private void btnPSincronizarLocal_Click(object sender, EventArgs e)
        {
            CargarMovimientos();
            pnlLocal.Visible = true;
            pnlLocal.BringToFront();
        }

        private void btnPAjustes_Click(object sender, EventArgs e)
        {
            pnlAjustes.Visible = true;
            pnlAjustes.BringToFront();
        }

        private void btnPAyuda_Click(object sender, EventArgs e)
        {
            pnlAyuda.Visible = true;
            pnlAyuda.BringToFront();
        }

        private void btnPSalir_Click(object sender, EventArgs e)
        {
            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿Desea cancelar las transferencias y cerrar el programa?\nSe conservará lo que halla descargado o cargado hasta el momento", "SyncBD | Confirme cierre", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Warning);

            if (DialogResult.OK == mensaje.ShowDialog())
            {
                Application.Exit();
            }

            mensaje.Dispose();
        }

        /// <summary>
        /// Compacta o expande el menú según su ancho
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPHamburguer_Click(object sender, EventArgs e)
        {
            if (pnlMenu.Width == 140)
            {
                pnlMenu.Width = 70;

                btnPHamburguer.Text = "";
                btnPHamburguer.ImageAlign = ContentAlignment.MiddleCenter;
                btnPSincronizarRemoto.Text = "";
                btnPSincronizarRemoto.ImageAlign = ContentAlignment.MiddleCenter;
                btnPSincronizarLocal.Text = "";
                btnPSincronizarLocal.ImageAlign = ContentAlignment.MiddleCenter;
                btnPAjustes.Text = "";
                btnPAjustes.ImageAlign = ContentAlignment.MiddleCenter;
                btnPAyuda.Text = "";
                btnPAyuda.ImageAlign = ContentAlignment.MiddleCenter;
                btnPSalir.Text = "";
                btnPSalir.ImageAlign = ContentAlignment.MiddleCenter;
            }

            else
            {
                pnlMenu.Width = 140;

                btnPHamburguer.Text = "Menú de opciones";
                btnPHamburguer.ImageAlign = ContentAlignment.MiddleLeft;
                btnPSincronizarRemoto.Text = "Sincronizar remoto";
                btnPSincronizarRemoto.ImageAlign = ContentAlignment.TopCenter;
                btnPSincronizarLocal.Text = "Sincronizar local";
                btnPSincronizarLocal.ImageAlign = ContentAlignment.TopCenter;
                btnPAjustes.Text = "Ajustes";
                btnPAjustes.ImageAlign = ContentAlignment.TopCenter;
                btnPAyuda.Text = "Ayuda";
                btnPAyuda.ImageAlign = ContentAlignment.TopCenter;
                btnPSalir.Text = "Salir";
                btnPSalir.ImageAlign = ContentAlignment.TopCenter;
            }
        }

        #endregion

        #region Eventos de los controles de la ventana

        /// <summary>
        /// Detecta cuando el usuario desea cerrar el programa desde la barra del titulo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbxCerrar_Click(object sender, EventArgs e)
        {
            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿Desea cancelar las transferencias y cerrar el programa?\nSe conservará lo que halla descargado o cargado hasta el momento", "SyncBD | Confirme cierre", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Warning);

            if (DialogResult.OK == mensaje.ShowDialog())
            {
                Application.Exit();
            }

            mensaje.Dispose();
        }

        /// <summary>
        /// Si cambia el valor del control, guarda los ajustes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkAJDistinguirSincronizacion_OnValueChange(object sender, EventArgs e)
        {
            GuardarAjustes();
        }

        /// <summary>
        /// Si cambia el valor del control, guarda los ajustes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkAJMinimizarIniciar_OnValueChange(object sender, EventArgs e)
        {
            GuardarAjustes();
        }

        /// <summary>
        /// Si cambia el valor del control a True, el sistema guardará un registro para iniciar automaticamente de lo contrario, borrará el registro
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkAJIniciarConWindows_OnValueChange(object sender, EventArgs e)
        {
            try
            {
                string dirRegistro = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";

                if (chkAJIniciarConWindows.Value)
                {
                    using (RegistryKey registro = Registry.CurrentUser.OpenSubKey(dirRegistro, true))
                    {
                        if (registro.OpenSubKey("SyncBD", true) == null)
                        {
                            registro.SetValue("SyncBD", Application.ExecutablePath);
                        }
                        else
                        {
                            registro.SetValue("SyncBD", Application.ExecutablePath);
                        }
                    }
                }

                else
                {
                    using (RegistryKey registro = Registry.CurrentUser.OpenSubKey(dirRegistro, true))
                    {
                        if (registro.OpenSubKey("SyncBD", true) == null)
                        {
                            registro.DeleteValue("SyncBD");
                        }
                    }
                }

                GuardarAjustes();
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x015, Mensaje interno: " + ex.Message);

                //PopUp mostrar = new PopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x015", PopUp.Icono.Error);

                //mostrar.Show();
            }
        }

        /// <summary>
        /// Si cambia el valor del control, guarda los ajustes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkAJRutaPermanente_OnValueChange(object sender, EventArgs e)
        {
            GuardarAjustes();
        }

        /// <summary>
        /// Si cambia el valor del control, guarda los ajustes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxAJGuardarEn_SelectedIndexChanged(object sender, EventArgs e)
        {
            GuardarAjustes();
        }

        /// <summary>
        /// Si cambia el valor del control, guarda los ajustes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxAJApariencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            GuardarAjustes();
        }

        /// <summary>
        /// Si cambia el valor del control, guarda los ajustes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxAJIdioma_SelectedIndexChanged(object sender, EventArgs e)
        {
            GuardarAjustes();
        }

        /// <summary>
        /// Advierte al usuario y restaura los ajustes del programa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAJReestablecer_Click(object sender, EventArgs e)
        {
            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿En realidad desea reestablecer todos los ajustes?", "SyncBD | Confirme cierre", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Warning);

            if (DialogResult.OK == mensaje.ShowDialog())
            {
                RestaurarAjustes();
            }

            mensaje.Dispose();
        }

        /// <summary>
        /// Inicia la sincronizacion del servidor remoto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSRResincronizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificarConexionInternet())
                {
                    SincronizarConRemoto();
                }

                else
                {
                    ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("No cuenta con conexión a internet, pero puede intentar en caso de que el servidor \"Remoto\" este conectado de forma local.\n\n¿Desea continuar?", "SyncBD | Sin conexión a internet", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Warning);

                    if (mensaje.ShowDialog() == DialogResult.OK)
                    {
                        SincronizarConRemoto();
                    }
                }
            }
            catch (Exception)
            {
                
            }
        }

        /// <summary>
        /// Inicia la sincronizacion del servidor local
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSLResincronizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!VerificarConexionInternet())
                {
                    SincronizarConLocal();
                }

                else
                {
                    ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("Al estar conectado a internet, SyncBD da preferencia a conexiones con el servidor \"Remoto\",cancelando acciones con el servidor \"Local\", pero aún así puede acceder a este último, si se encuentra accesible.\n\n¿Desea continuar?", "SyncBD | Se detectó conexión a internet", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Warning);

                    if (mensaje.ShowDialog() == DialogResult.OK)
                    {
                        mensaje.Dispose();

                        SincronizarConLocal();
                    }

                    mensaje.Dispose();
                }
            }
            catch (Exception)
            {
                
            }
        }

        /// <summary>
        /// Muestra la carpeta contenedora de las copias sincronizadas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSRAbrirCarpeta_Click(object sender, EventArgs e)
        {
            try
            {
                Ajustes ajustes = nucleo.LeerAjustes();

                Process.Start("explorer", ajustes.CopiarA);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Despues de detectar doble clic en el icono de la barra de tareas, restaura el tamaño normal de SyncBD
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nicBarraTareas_DoubleClick(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            ShowInTaskbar = true;
        }

        /// <summary>
        /// Determina la visibilidad del programa segun su estado, normal o minimizado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Principal_Resize(object sender, EventArgs e)
        {
            try
            {
                if (WindowState == FormWindowState.Minimized)
                {
                    nicBarraTareas.Visible = true;
                    Hide();
                    ShowInTaskbar = false;

                    nucleo.MostrarPopUp("SyncBD entro a segundo plano", "Minimizando", PopUp.Icono.Info);
                }

                else if (WindowState == FormWindowState.Normal || WindowState == FormWindowState.Maximized)
                {
                    nicBarraTareas.Visible = false;
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Detecta cuando el usuario desea cerrar el programa desde el icono de la barra de tareas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExitSysTray_Click(object sender, EventArgs e)
        {
            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿Desea cancelar las transferencias y cerrar el programa?\nSe conservará lo que halla descargado o cargado hasta el momento", "SyncBD | Confirme cierre", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Warning);

            if (DialogResult.OK == mensaje.ShowDialog())
            {
                Application.Exit();
            }

            mensaje.Dispose();
        }

        /// <summary>
        /// Restaura el tamaño normal de SyncBD
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRestaurarMenuStrip_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            ShowInTaskbar = true;
            BringToFront();
        }

        /// <summary>
        /// Aumenta segundo a segundo el tiempo transcurrido al sincronizar con el servidor local
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmpLocalTiempoTranscurrido_Tick(object sender, EventArgs e)
        {
            lblSRTiempoTranscurrido.Refresh();

            relojLocal = relojLocal.Add(TimeSpan.FromSeconds(1));
            
            lblSLTiempoTranscurrido.Text = "Tiempo transcurrido: " + relojLocal.ToString();
        }

        /// <summary>
        /// Aumenta segundo a segundo el tiempo transcurrido al sincronizar con el servidor remoto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmpRemotoTiempoTranscurrido_Tick(object sender, EventArgs e)
        {
            lblSRTiempoTranscurrido.Refresh();
            
            relojRemoto = relojRemoto.Add(TimeSpan.FromSeconds(1));

            lblSRTiempoTranscurrido.Text = "Tiempo transcurrido: " + relojRemoto.ToString();
        }

        /// <summary>
        /// Muestra una ventana de seleccion de carpeta donde se guardarán las copias de las sincronizaciones
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPEscogerCarpeta_Click(object sender, EventArgs e)
        {
            try
            {
                fbdGuardarEn.Description = "Indique donde se copiarán los archivos sincronizados...";
                fbdGuardarEn.ShowNewFolderButton = true;
                
                if(fbdGuardarEn.ShowDialog() == DialogResult.OK)
                {
                    lblAJRutaGuardado.Text = fbdGuardarEn.SelectedPath;

                    GuardarAjustes();
                }
            }
            catch (Exception)
            {
                
            }
        }

        /// <summary>
        /// Se advierte al usuario y borra el historial de la base de datos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAJBorrarHistorial_Click(object sender, EventArgs e)
        {
            try
            {
                ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("Está acción es permanente y no se podrá cancelar.\n\n¿Realmente desea borrar el historial de sincronizaciones?", "SyncBD | Confirme acción", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Warning);

                if(mensaje.ShowDialog() == DialogResult.OK)
                {
                    if(enlace.BorrarHistorial())
                    {
                        nucleo.MostrarPopUp("Se ha borrado el historial correctamente", "Historial borrado", PopUp.Icono.Ok);
                    }

                    else
                    {
                        nucleo.MostrarPopUp("No fue posible borrar el historial", "No se borró el historial", PopUp.Icono.Error);
                    }
                }

                mensaje.Dispose();
            }
            catch (Exception)
            {
                
            }
        }

        /// <summary>
        /// Muestra la ventana de configuración del servidor remoto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAJRemoto_Click(object sender, EventArgs e)
        {
            try
            {
                ConfigRemoto mostrar = ConfigRemoto.ObtenerInstancia();

                Hide();

                if (mostrar.ShowDialog() == DialogResult.OK)
                {
                    CargarMovimientos();

                    VerificarVisibilidadServidores();

                    mostrar.Dispose();

                    Show();
                }

                else
                {
                    CargarMovimientos();

                    VerificarVisibilidadServidores();

                    mostrar.Dispose();

                    Show();

                    ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¡Acción cancelada!, no se modificaron los ajustes para el servidor remoto", "SyncBD | Sin modificaciones", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Warning);

                    mensaje.ShowDialog();

                    mensaje.Dispose();
                }
            }
            catch (Exception)
            {
                
            }
        }

        /// <summary>
        /// Muestra la ventana de configuración del servidor local
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAJLocal_Click(object sender, EventArgs e)
        {
            try
            {
                ConfigLocal mostrar = ConfigLocal.ObtenerInstancia();

                Hide();

                if (mostrar.ShowDialog() == DialogResult.OK)
                {
                    CargarMovimientos();

                    VerificarVisibilidadServidores();

                    mostrar.Dispose();

                    Show();
                }

                else
                {
                    CargarMovimientos();

                    VerificarVisibilidadServidores();

                    mostrar.Dispose();

                    Show();

                    ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¡Acción cancelada!, no se modificaron los ajustes para el servidor local", "SyncBD | Sin modificaciones", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Warning);

                    mensaje.ShowDialog();

                    mensaje.Dispose();
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Permite cargar archivos al servidor local (Borra los archivos del servidor remoto, para colocar estos archivos)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSLAbrirCarpeta_Click(object sender, EventArgs e)
        {
            try
            {
                ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿Desea sincronizar los archivos desde una carpeta externa o medio extraíble?\n\nSi es así, asegurese de conectar ahora el dispositivo. (Este proceso borrará los archivos del servidor remoto)", "SyncBD | Sincronización externa", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Warning);

                if (mensaje.ShowDialog() == DialogResult.OK)
                {
                    mensaje.Dispose();

                    fbdGuardarEn.Description = "Seleccione la carpeta que desee cargar al servidor local...";
                    fbdGuardarEn.ShowNewFolderButton = false;

                    if (fbdGuardarEn.ShowDialog() == DialogResult.OK)
                    {
                        if (Directory.Exists(dirArchivosRemotosOriginal))
                        {
                            DirectoryInfo datosDirectorio = new DirectoryInfo(dirArchivosRemotosOriginal);

                            foreach (FileInfo archivo in datosDirectorio.EnumerateFiles())
                            {
                                archivo.Delete();
                            }

                            foreach (DirectoryInfo directorio in datosDirectorio.GetDirectories())
                            {
                                directorio.Delete(true);
                            }
                        }

                        if (Transferir(fbdGuardarEn.SelectedPath, dirArchivosRemotosOriginal))
                        {
                            ExtraMessageBox mensaje1 = ExtraMessageBox.ObtenerInstancia("El programa a asegurado los archivos, ahora es momento de cargarlos al servidor local.\n\n¿Desea continuar el proceso de carga?", "SyncBD | Archivos asegurados, esperando confirmación", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Info);

                            if(DialogResult.OK == mensaje1.ShowDialog())
                            {
                                if (!VerificarConexionInternet())
                                {
                                    SincronizarConLocal();
                                }

                                else
                                {
                                    SincronizarConLocal();
                                }
                            }

                            mensaje1.Dispose();
                        }

                        else
                        {
                            ExtraMessageBox mensaje1 = ExtraMessageBox.ObtenerInstancia("Se cancelo el proceso de sincronización desde medio externo, verifique que no tenga archivos abiertos o en ejecución que pertenezcan a dicha carpeta", "SyncBD | Acción cancelada", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Warning);

                            mensaje1.ShowDialog();

                            mensaje1.Dispose();
                        }
                    }
                }

                mensaje.Dispose();
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Muestra la ventana de personalización visual de SyncBD
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAJPersonalizarTema_Click(object sender, EventArgs e)
        {
            try
            {
                Temas mostrar = Temas.ObtenerInstancia();

                Hide();

                mostrar.ShowDialog();

                mostrar.Dispose();

                LeerAjustes();

                Show();
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Realiza la consulta del codigo de error mostrandolo a detalle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnHBuscarCodigo_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtHBuscarError.Text == "")
                {
                    ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("Escriba el error a buscar ejemplo:\"0x001\".", "SyncBD | Sin código de error", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Info);

                    mensaje.ShowDialog();

                    txtHBuscarError.Focus();

                    mensaje.Dispose();
                }

                else
                {
                    if (enlace.ConsultarError(rtbHCuerpoCodigo, lblHCodigoTitulo, txtHBuscarError.Text))
                    {
                        txtHBuscarError.Text = "";

                        pnlErroresEInformacion.Visible = true;
                    }

                    else
                    {
                        txtHBuscarError.Text = "";
                        rtbHCuerpoCodigo.Text = "";
                        pnlErroresEInformacion.Visible = false;
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Detecta cuando el usuario presiona enter en la barra de codigo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHBuscarError_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == Convert.ToChar(Keys.Enter))
                {
                    btnHBuscarCodigo_Click(sender, null);
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Inicializa los eventos de sincronizacion con el servidor remoto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStartRemoteStrip_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificarConexionInternet())
                {
                    SincronizarConRemoto();
                }

                else
                {
                    ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("No cuenta con conexión a internet, pero puede intentar en caso de que el servidor \"Remoto\" este conectado de forma local.\n\n¿Desea continuar?", "SyncBD | Sin conexión a internet", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Warning);

                    if (mensaje.ShowDialog() == DialogResult.OK)
                    {
                        SincronizarConRemoto();
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Inicializa los eventos de sincronizacion con el servidor local
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStartLocalStrip_Click(object sender, EventArgs e)
        {
            try
            {
                if (!VerificarConexionInternet())
                {
                    SincronizarConLocal();
                }

                else
                {
                    ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("Al estar conectado a internet, SyncBD da preferencia a conexiones con el servidor \"Remoto\",cancelando acciones con el servidor \"Local\", pero aún así puede acceder a este último, si se encuentra accesible.\n\n¿Desea continuar?", "SyncBD | Se detectó conexión a internet", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Warning);

                    if (mensaje.ShowDialog() == DialogResult.OK)
                    {
                        mensaje.Dispose();

                        SincronizarConLocal();
                    }

                    mensaje.Dispose();
                }
            }
            catch (Exception)
            {

            }
        }

        #endregion
    }
}

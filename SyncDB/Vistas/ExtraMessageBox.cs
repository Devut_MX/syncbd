﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SyncDB.Vistas
{
    public partial class ExtraMessageBox : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handle = base.CreateParams;
                handle.ExStyle |= 0x02000000;
                return handle;
            }
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int Param);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private static ExtraMessageBox singleton;

        public static ExtraMessageBox ObtenerInstancia(string mensaje, string titulo, Boton boton, Icono icono)
        {
            if (singleton == null || singleton.IsDisposed)
            {
                singleton = new ExtraMessageBox(mensaje, titulo, boton, icono);
            }

            singleton.TopMost = true;

            singleton.BringToFront();

            return singleton;
        }

        

        private ExtraMessageBox(string mensaje, string titulo, Boton boton, Icono icono)
        {
            InitializeComponent();

            TipoNotificacion(mensaje, titulo, boton, icono);
        }

        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        public enum Icono
        {
            Ok,
            Info,
            Warning,
            Error
        }

        public enum Boton
        {
            Ok,
            YesNo,
            OkCancel,
            YesNoRetry,
            OkCancelRetry
        }

        private void TipoNotificacion(string mensaje, string titulo, Boton boton, Icono icono)
        {
            switch (icono)
            {
                case Icono.Ok:
                    BackColor = Color.FromArgb(255, 165, 214, 167);
                    pbxIcono1.Image = SyncBD.Properties.Resources.Ok_16px;
                    pbxIcono2.Image = SyncBD.Properties.Resources.Ok_64px;
                    break;

                case Icono.Info:
                    BackColor = Color.FromArgb(255, 129, 212, 250);
                    pbxIcono1.Image = SyncBD.Properties.Resources.Info_16px;
                    pbxIcono2.Image = SyncBD.Properties.Resources.Info_64px;
                    break;

                case Icono.Warning:
                    BackColor = Color.FromArgb(255, 255, 238, 88);
                    pbxIcono1.Image = SyncBD.Properties.Resources.Attention_16px;
                    pbxIcono2.Image = SyncBD.Properties.Resources.Attention_64px;
                    break;

                case Icono.Error:
                    BackColor = Color.FromArgb(255, 239, 115, 115);
                    pbxIcono1.Image = SyncBD.Properties.Resources.Cancel_16px;
                    pbxIcono2.Image = SyncBD.Properties.Resources.Cancel_64px;
                    break;

                default:
                    BackColor = Color.FromArgb(255, 165, 214, 167);
                    pbxIcono1.Image = SyncBD.Properties.Resources.Ok_16px;
                    pbxIcono2.Image = SyncBD.Properties.Resources.Ok_64px;
                    break;
            }

            switch(boton)
            {
                case Boton.Ok:
                    btnContinuar.Visible = true;
                    btnCancelar.Visible = false;
                    btnReintentar.Visible = false;
                    break;

                case Boton.OkCancel:
                    btnContinuar.Visible = true;
                    btnCancelar.Visible = true;
                    btnReintentar.Visible = false;
                    break;

                case Boton.YesNo:
                    btnContinuar.Text = "Sí";
                    btnCancelar.Text = "No";
                    btnContinuar.Visible = true;
                    btnCancelar.Visible = true;
                    btnReintentar.Visible = false;
                    break;

                case Boton.OkCancelRetry:
                    btnContinuar.Visible = true;
                    btnCancelar.Visible = true;
                    btnReintentar.Visible = true;
                    break;

                case Boton.YesNoRetry:
                    btnContinuar.Text = "Sí";
                    btnCancelar.Text = "No";
                    btnContinuar.Visible = true;
                    btnCancelar.Visible = true;
                    btnReintentar.Visible = true;
                    break;

                default:
                    btnContinuar.Visible = true;
                    btnCancelar.Visible = false;
                    btnReintentar.Visible = false;
                    break;
            }

            lblTitulo.Text = titulo;
            lblTitulo.Refresh();
            lblMensaje.Text = mensaje;
            lblMensaje.Refresh();
        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnReintentar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Retry;
            Close();
        }
    }
}

﻿using System.Windows.Forms;

namespace SyncBD.Vistas
{
    public partial class Procesando : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        private static Procesando singleton;

        public static Procesando ObtenerInstancia(string mensaje)
        {
            if (singleton == null || singleton.IsDisposed)
            {
                singleton = new Procesando(mensaje);
            }

            singleton.BringToFront();

            return singleton;
        }

        private Procesando(string mensaje)
        {
            InitializeComponent();

            pbxProceso.Image = Properties.Resources.Procesando;

            lblCuerpo.Text = mensaje;
        }
    }
}

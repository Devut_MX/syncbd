﻿namespace SyncDB.Vistas
{
    partial class ConfigRemoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigRemoto));
            this.cbxA3Encriptacion = new System.Windows.Forms.ComboBox();
            this.lblA3Autenticar = new DevComponents.DotNetBar.LabelX();
            this.lblA3Anonimo = new DevComponents.DotNetBar.LabelX();
            this.lblA3Modo = new DevComponents.DotNetBar.LabelX();
            this.lblA3Directorio = new DevComponents.DotNetBar.LabelX();
            this.lblA3Puerto = new DevComponents.DotNetBar.LabelX();
            this.lblA3Encriptacion = new DevComponents.DotNetBar.LabelX();
            this.lblA3Password = new DevComponents.DotNetBar.LabelX();
            this.lblA3Usuario = new DevComponents.DotNetBar.LabelX();
            this.lblA3Servidor = new DevComponents.DotNetBar.LabelX();
            this.txtA3Directorio = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtA3Puerto = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtA3Password = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtA3Usuario = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtA3Servidor = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.lblA3Cuerpo = new DevComponents.DotNetBar.LabelX();
            this.lineA3Divisor = new DevComponents.DotNetBar.Controls.Line();
            this.lblA3Encabezado = new DevComponents.DotNetBar.LabelX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblA3Titulo = new DevComponents.DotNetBar.LabelX();
            this.pbxMinimizar = new System.Windows.Forms.PictureBox();
            this.pbxCerrar = new System.Windows.Forms.PictureBox();
            this.pbxIcono = new System.Windows.Forms.PictureBox();
            this.pbxA3Ver = new System.Windows.Forms.PictureBox();
            this.chkA3Modo = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.chkA3Anonimo = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.chkA3Autenticar = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.btnA3Ayuda = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnA3Cancelar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnA3Continuar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.lblA3Avanzados = new DevComponents.DotNetBar.LabelX();
            this.chkA3Avanzados = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.cbxA3Protocolo = new System.Windows.Forms.ComboBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA3Ver)).BeginInit();
            this.SuspendLayout();
            // 
            // cbxA3Encriptacion
            // 
            this.cbxA3Encriptacion.BackColor = System.Drawing.Color.SeaGreen;
            this.cbxA3Encriptacion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxA3Encriptacion.DropDownHeight = 120;
            this.cbxA3Encriptacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxA3Encriptacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxA3Encriptacion.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbxA3Encriptacion.ForeColor = System.Drawing.Color.White;
            this.cbxA3Encriptacion.FormattingEnabled = true;
            this.cbxA3Encriptacion.IntegralHeight = false;
            this.cbxA3Encriptacion.Items.AddRange(new object[] {
            "Sin encriptar",
            "Implícita",
            "Explícita"});
            this.cbxA3Encriptacion.Location = new System.Drawing.Point(292, 280);
            this.cbxA3Encriptacion.Name = "cbxA3Encriptacion";
            this.cbxA3Encriptacion.Size = new System.Drawing.Size(125, 27);
            this.cbxA3Encriptacion.TabIndex = 4;
            // 
            // lblA3Autenticar
            // 
            // 
            // 
            // 
            this.lblA3Autenticar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA3Autenticar.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA3Autenticar.ForeColor = System.Drawing.Color.Black;
            this.lblA3Autenticar.Location = new System.Drawing.Point(234, 360);
            this.lblA3Autenticar.Name = "lblA3Autenticar";
            this.lblA3Autenticar.SingleLineColor = System.Drawing.Color.Black;
            this.lblA3Autenticar.Size = new System.Drawing.Size(161, 26);
            this.lblA3Autenticar.TabIndex = 107;
            this.lblA3Autenticar.Text = "Quitar autenticación";
            this.lblA3Autenticar.Visible = false;
            this.lblA3Autenticar.WordWrap = true;
            // 
            // lblA3Anonimo
            // 
            // 
            // 
            // 
            this.lblA3Anonimo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA3Anonimo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA3Anonimo.ForeColor = System.Drawing.Color.Black;
            this.lblA3Anonimo.Location = new System.Drawing.Point(234, 386);
            this.lblA3Anonimo.Name = "lblA3Anonimo";
            this.lblA3Anonimo.SingleLineColor = System.Drawing.Color.Black;
            this.lblA3Anonimo.Size = new System.Drawing.Size(161, 26);
            this.lblA3Anonimo.TabIndex = 106;
            this.lblA3Anonimo.Text = "Ingresar como anónimo";
            this.lblA3Anonimo.Visible = false;
            this.lblA3Anonimo.WordWrap = true;
            // 
            // lblA3Modo
            // 
            // 
            // 
            // 
            this.lblA3Modo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA3Modo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA3Modo.ForeColor = System.Drawing.Color.Black;
            this.lblA3Modo.Location = new System.Drawing.Point(442, 386);
            this.lblA3Modo.Name = "lblA3Modo";
            this.lblA3Modo.SingleLineColor = System.Drawing.Color.Black;
            this.lblA3Modo.Size = new System.Drawing.Size(146, 26);
            this.lblA3Modo.TabIndex = 105;
            this.lblA3Modo.Text = "Modo activo";
            this.lblA3Modo.Visible = false;
            this.lblA3Modo.WordWrap = true;
            // 
            // lblA3Directorio
            // 
            // 
            // 
            // 
            this.lblA3Directorio.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA3Directorio.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA3Directorio.ForeColor = System.Drawing.Color.Black;
            this.lblA3Directorio.Location = new System.Drawing.Point(192, 318);
            this.lblA3Directorio.Name = "lblA3Directorio";
            this.lblA3Directorio.SingleLineColor = System.Drawing.Color.Black;
            this.lblA3Directorio.Size = new System.Drawing.Size(93, 26);
            this.lblA3Directorio.TabIndex = 104;
            this.lblA3Directorio.Text = "Directorio:";
            this.lblA3Directorio.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblA3Directorio.WordWrap = true;
            // 
            // lblA3Puerto
            // 
            // 
            // 
            // 
            this.lblA3Puerto.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA3Puerto.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA3Puerto.ForeColor = System.Drawing.Color.Black;
            this.lblA3Puerto.Location = new System.Drawing.Point(423, 285);
            this.lblA3Puerto.Name = "lblA3Puerto";
            this.lblA3Puerto.SingleLineColor = System.Drawing.Color.Black;
            this.lblA3Puerto.Size = new System.Drawing.Size(59, 26);
            this.lblA3Puerto.TabIndex = 103;
            this.lblA3Puerto.Text = "*Puerto:";
            this.lblA3Puerto.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblA3Puerto.WordWrap = true;
            // 
            // lblA3Encriptacion
            // 
            // 
            // 
            // 
            this.lblA3Encriptacion.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA3Encriptacion.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA3Encriptacion.ForeColor = System.Drawing.Color.Black;
            this.lblA3Encriptacion.Location = new System.Drawing.Point(192, 285);
            this.lblA3Encriptacion.Name = "lblA3Encriptacion";
            this.lblA3Encriptacion.SingleLineColor = System.Drawing.Color.Black;
            this.lblA3Encriptacion.Size = new System.Drawing.Size(93, 26);
            this.lblA3Encriptacion.TabIndex = 102;
            this.lblA3Encriptacion.Text = "*Encriptación:";
            this.lblA3Encriptacion.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblA3Encriptacion.WordWrap = true;
            // 
            // lblA3Password
            // 
            // 
            // 
            // 
            this.lblA3Password.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA3Password.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA3Password.ForeColor = System.Drawing.Color.Black;
            this.lblA3Password.Location = new System.Drawing.Point(192, 254);
            this.lblA3Password.Name = "lblA3Password";
            this.lblA3Password.SingleLineColor = System.Drawing.Color.Black;
            this.lblA3Password.Size = new System.Drawing.Size(93, 26);
            this.lblA3Password.TabIndex = 101;
            this.lblA3Password.Text = "*Contraseña:";
            this.lblA3Password.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblA3Password.WordWrap = true;
            // 
            // lblA3Usuario
            // 
            // 
            // 
            // 
            this.lblA3Usuario.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA3Usuario.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA3Usuario.ForeColor = System.Drawing.Color.Black;
            this.lblA3Usuario.Location = new System.Drawing.Point(192, 224);
            this.lblA3Usuario.Name = "lblA3Usuario";
            this.lblA3Usuario.SingleLineColor = System.Drawing.Color.Black;
            this.lblA3Usuario.Size = new System.Drawing.Size(93, 26);
            this.lblA3Usuario.TabIndex = 100;
            this.lblA3Usuario.Text = "*Usuario:";
            this.lblA3Usuario.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblA3Usuario.WordWrap = true;
            // 
            // lblA3Servidor
            // 
            // 
            // 
            // 
            this.lblA3Servidor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA3Servidor.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA3Servidor.ForeColor = System.Drawing.Color.Black;
            this.lblA3Servidor.Location = new System.Drawing.Point(192, 194);
            this.lblA3Servidor.Name = "lblA3Servidor";
            this.lblA3Servidor.SingleLineColor = System.Drawing.Color.Black;
            this.lblA3Servidor.Size = new System.Drawing.Size(93, 26);
            this.lblA3Servidor.TabIndex = 99;
            this.lblA3Servidor.Text = "*Servidor:";
            this.lblA3Servidor.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblA3Servidor.WordWrap = true;
            // 
            // txtA3Directorio
            // 
            this.txtA3Directorio.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtA3Directorio.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtA3Directorio.ForeColor = System.Drawing.Color.Black;
            this.txtA3Directorio.HintForeColor = System.Drawing.Color.Empty;
            this.txtA3Directorio.HintText = "";
            this.txtA3Directorio.isPassword = false;
            this.txtA3Directorio.LineFocusedColor = System.Drawing.Color.Blue;
            this.txtA3Directorio.LineIdleColor = System.Drawing.Color.Gray;
            this.txtA3Directorio.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.txtA3Directorio.LineThickness = 2;
            this.txtA3Directorio.Location = new System.Drawing.Point(292, 312);
            this.txtA3Directorio.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtA3Directorio.Name = "txtA3Directorio";
            this.txtA3Directorio.Size = new System.Drawing.Size(270, 26);
            this.txtA3Directorio.TabIndex = 6;
            this.txtA3Directorio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtA3Directorio.Leave += new System.EventHandler(this.txtA3Directorio_Leave);
            // 
            // txtA3Puerto
            // 
            this.txtA3Puerto.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtA3Puerto.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtA3Puerto.ForeColor = System.Drawing.Color.Black;
            this.txtA3Puerto.HintForeColor = System.Drawing.Color.Empty;
            this.txtA3Puerto.HintText = "";
            this.txtA3Puerto.isPassword = false;
            this.txtA3Puerto.LineFocusedColor = System.Drawing.Color.Blue;
            this.txtA3Puerto.LineIdleColor = System.Drawing.Color.Gray;
            this.txtA3Puerto.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.txtA3Puerto.LineThickness = 2;
            this.txtA3Puerto.Location = new System.Drawing.Point(491, 280);
            this.txtA3Puerto.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtA3Puerto.Name = "txtA3Puerto";
            this.txtA3Puerto.Size = new System.Drawing.Size(71, 26);
            this.txtA3Puerto.TabIndex = 5;
            this.txtA3Puerto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtA3Puerto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtA3Puerto_KeyPress);
            this.txtA3Puerto.Leave += new System.EventHandler(this.txtA3Puerto_Leave);
            // 
            // txtA3Password
            // 
            this.txtA3Password.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtA3Password.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtA3Password.ForeColor = System.Drawing.Color.Black;
            this.txtA3Password.HintForeColor = System.Drawing.Color.Empty;
            this.txtA3Password.HintText = "";
            this.txtA3Password.isPassword = true;
            this.txtA3Password.LineFocusedColor = System.Drawing.Color.Blue;
            this.txtA3Password.LineIdleColor = System.Drawing.Color.Gray;
            this.txtA3Password.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.txtA3Password.LineThickness = 2;
            this.txtA3Password.Location = new System.Drawing.Point(292, 248);
            this.txtA3Password.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtA3Password.Name = "txtA3Password";
            this.txtA3Password.Size = new System.Drawing.Size(230, 27);
            this.txtA3Password.TabIndex = 3;
            this.txtA3Password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtA3Password.Leave += new System.EventHandler(this.txtA3Password_Leave);
            // 
            // txtA3Usuario
            // 
            this.txtA3Usuario.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtA3Usuario.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtA3Usuario.ForeColor = System.Drawing.Color.Black;
            this.txtA3Usuario.HintForeColor = System.Drawing.Color.Empty;
            this.txtA3Usuario.HintText = "";
            this.txtA3Usuario.isPassword = false;
            this.txtA3Usuario.LineFocusedColor = System.Drawing.Color.Blue;
            this.txtA3Usuario.LineIdleColor = System.Drawing.Color.Gray;
            this.txtA3Usuario.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.txtA3Usuario.LineThickness = 2;
            this.txtA3Usuario.Location = new System.Drawing.Point(292, 218);
            this.txtA3Usuario.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtA3Usuario.Name = "txtA3Usuario";
            this.txtA3Usuario.Size = new System.Drawing.Size(270, 26);
            this.txtA3Usuario.TabIndex = 2;
            this.txtA3Usuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtA3Usuario.Leave += new System.EventHandler(this.txtA3Usuario_Leave);
            // 
            // txtA3Servidor
            // 
            this.txtA3Servidor.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtA3Servidor.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtA3Servidor.ForeColor = System.Drawing.Color.Black;
            this.txtA3Servidor.HintForeColor = System.Drawing.Color.Empty;
            this.txtA3Servidor.HintText = "";
            this.txtA3Servidor.isPassword = false;
            this.txtA3Servidor.LineFocusedColor = System.Drawing.Color.Blue;
            this.txtA3Servidor.LineIdleColor = System.Drawing.Color.Gray;
            this.txtA3Servidor.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.txtA3Servidor.LineThickness = 2;
            this.txtA3Servidor.Location = new System.Drawing.Point(292, 190);
            this.txtA3Servidor.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtA3Servidor.Name = "txtA3Servidor";
            this.txtA3Servidor.Size = new System.Drawing.Size(270, 24);
            this.txtA3Servidor.TabIndex = 1;
            this.txtA3Servidor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtA3Servidor.Leave += new System.EventHandler(this.txtA3Servidor_Leave);
            // 
            // lblA3Cuerpo
            // 
            // 
            // 
            // 
            this.lblA3Cuerpo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA3Cuerpo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA3Cuerpo.ForeColor = System.Drawing.Color.Black;
            this.lblA3Cuerpo.Location = new System.Drawing.Point(192, 109);
            this.lblA3Cuerpo.Name = "lblA3Cuerpo";
            this.lblA3Cuerpo.SingleLineColor = System.Drawing.Color.Black;
            this.lblA3Cuerpo.Size = new System.Drawing.Size(396, 77);
            this.lblA3Cuerpo.TabIndex = 86;
            this.lblA3Cuerpo.Text = resources.GetString("lblA3Cuerpo.Text");
            this.lblA3Cuerpo.WordWrap = true;
            // 
            // lineA3Divisor
            // 
            this.lineA3Divisor.ForeColor = System.Drawing.Color.Black;
            this.lineA3Divisor.Location = new System.Drawing.Point(192, 80);
            this.lineA3Divisor.Name = "lineA3Divisor";
            this.lineA3Divisor.Size = new System.Drawing.Size(396, 23);
            this.lineA3Divisor.TabIndex = 85;
            this.lineA3Divisor.Text = "line1";
            // 
            // lblA3Encabezado
            // 
            // 
            // 
            // 
            this.lblA3Encabezado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA3Encabezado.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblA3Encabezado.ForeColor = System.Drawing.Color.Black;
            this.lblA3Encabezado.Location = new System.Drawing.Point(192, 47);
            this.lblA3Encabezado.Name = "lblA3Encabezado";
            this.lblA3Encabezado.SingleLineColor = System.Drawing.Color.Black;
            this.lblA3Encabezado.Size = new System.Drawing.Size(385, 39);
            this.lblA3Encabezado.TabIndex = 84;
            this.lblA3Encabezado.Text = "Configuración del servidor remoto";
            this.lblA3Encabezado.WordWrap = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbxA3Protocolo);
            this.panel1.Controls.Add(this.labelX1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(186, 450);
            this.panel1.TabIndex = 83;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::SyncBD.Properties.Resources.SyncBD;
            this.pictureBox1.Location = new System.Drawing.Point(4, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.Green;
            this.pnlTitleBar.Controls.Add(this.lblA3Titulo);
            this.pnlTitleBar.Controls.Add(this.pbxMinimizar);
            this.pnlTitleBar.Controls.Add(this.pbxCerrar);
            this.pnlTitleBar.Controls.Add(this.pbxIcono);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(600, 30);
            this.pnlTitleBar.TabIndex = 82;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // lblA3Titulo
            // 
            // 
            // 
            // 
            this.lblA3Titulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA3Titulo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA3Titulo.ForeColor = System.Drawing.Color.White;
            this.lblA3Titulo.Location = new System.Drawing.Point(28, 5);
            this.lblA3Titulo.Name = "lblA3Titulo";
            this.lblA3Titulo.Size = new System.Drawing.Size(508, 20);
            this.lblA3Titulo.TabIndex = 10;
            this.lblA3Titulo.Text = "SyncBD - Asistente de configuración";
            this.lblA3Titulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pbxMinimizar
            // 
            this.pbxMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.pbxMinimizar.Image = global::SyncBD.Properties.Resources.Compress_24px;
            this.pbxMinimizar.Location = new System.Drawing.Point(542, 5);
            this.pbxMinimizar.Name = "pbxMinimizar";
            this.pbxMinimizar.Size = new System.Drawing.Size(20, 20);
            this.pbxMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMinimizar.TabIndex = 3;
            this.pbxMinimizar.TabStop = false;
            this.pbxMinimizar.Click += new System.EventHandler(this.pbxMinimizar_Click);
            // 
            // pbxCerrar
            // 
            this.pbxCerrar.BackColor = System.Drawing.Color.Transparent;
            this.pbxCerrar.Image = global::SyncBD.Properties.Resources.CloseWindow_24px;
            this.pbxCerrar.Location = new System.Drawing.Point(568, 5);
            this.pbxCerrar.Name = "pbxCerrar";
            this.pbxCerrar.Size = new System.Drawing.Size(20, 20);
            this.pbxCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCerrar.TabIndex = 2;
            this.pbxCerrar.TabStop = false;
            this.pbxCerrar.Click += new System.EventHandler(this.pbxCerrar_Click);
            // 
            // pbxIcono
            // 
            this.pbxIcono.BackColor = System.Drawing.Color.Transparent;
            this.pbxIcono.Image = global::SyncBD.Properties.Resources.SyncBDLogo;
            this.pbxIcono.Location = new System.Drawing.Point(6, 8);
            this.pbxIcono.Name = "pbxIcono";
            this.pbxIcono.Size = new System.Drawing.Size(16, 16);
            this.pbxIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxIcono.TabIndex = 1;
            this.pbxIcono.TabStop = false;
            this.pbxIcono.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pbxA3Ver
            // 
            this.pbxA3Ver.BackColor = System.Drawing.Color.Transparent;
            this.pbxA3Ver.Image = global::SyncBD.Properties.Resources.Lock_32px;
            this.pbxA3Ver.Location = new System.Drawing.Point(534, 248);
            this.pbxA3Ver.Name = "pbxA3Ver";
            this.pbxA3Ver.Size = new System.Drawing.Size(28, 28);
            this.pbxA3Ver.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxA3Ver.TabIndex = 98;
            this.pbxA3Ver.TabStop = false;
            this.pbxA3Ver.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbxA3Ver_MouseDown);
            this.pbxA3Ver.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbxA3Ver_MouseUp);
            // 
            // chkA3Modo
            // 
            this.chkA3Modo.BackColor = System.Drawing.Color.Transparent;
            this.chkA3Modo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkA3Modo.BackgroundImage")));
            this.chkA3Modo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkA3Modo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkA3Modo.Location = new System.Drawing.Point(401, 389);
            this.chkA3Modo.Name = "chkA3Modo";
            this.chkA3Modo.OffColor = System.Drawing.Color.Red;
            this.chkA3Modo.OnColor = System.Drawing.Color.Green;
            this.chkA3Modo.Size = new System.Drawing.Size(35, 20);
            this.chkA3Modo.TabIndex = 10;
            this.chkA3Modo.Value = false;
            this.chkA3Modo.Visible = false;
            // 
            // chkA3Anonimo
            // 
            this.chkA3Anonimo.BackColor = System.Drawing.Color.Transparent;
            this.chkA3Anonimo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkA3Anonimo.BackgroundImage")));
            this.chkA3Anonimo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkA3Anonimo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkA3Anonimo.Location = new System.Drawing.Point(193, 389);
            this.chkA3Anonimo.Name = "chkA3Anonimo";
            this.chkA3Anonimo.OffColor = System.Drawing.Color.Red;
            this.chkA3Anonimo.OnColor = System.Drawing.Color.Green;
            this.chkA3Anonimo.Size = new System.Drawing.Size(35, 20);
            this.chkA3Anonimo.TabIndex = 9;
            this.chkA3Anonimo.Value = false;
            this.chkA3Anonimo.Visible = false;
            this.chkA3Anonimo.OnValueChange += new System.EventHandler(this.chkA3Anonimo_OnValueChange);
            // 
            // chkA3Autenticar
            // 
            this.chkA3Autenticar.BackColor = System.Drawing.Color.Transparent;
            this.chkA3Autenticar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkA3Autenticar.BackgroundImage")));
            this.chkA3Autenticar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkA3Autenticar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkA3Autenticar.Location = new System.Drawing.Point(193, 363);
            this.chkA3Autenticar.Name = "chkA3Autenticar";
            this.chkA3Autenticar.OffColor = System.Drawing.Color.Red;
            this.chkA3Autenticar.OnColor = System.Drawing.Color.Green;
            this.chkA3Autenticar.Size = new System.Drawing.Size(35, 20);
            this.chkA3Autenticar.TabIndex = 8;
            this.chkA3Autenticar.Value = false;
            this.chkA3Autenticar.Visible = false;
            // 
            // btnA3Ayuda
            // 
            this.btnA3Ayuda.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnA3Ayuda.BackColor = System.Drawing.Color.Gray;
            this.btnA3Ayuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnA3Ayuda.BorderRadius = 0;
            this.btnA3Ayuda.ButtonText = "Ayuda";
            this.btnA3Ayuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnA3Ayuda.DisabledColor = System.Drawing.Color.Gray;
            this.btnA3Ayuda.Iconcolor = System.Drawing.Color.Transparent;
            this.btnA3Ayuda.Iconimage = global::SyncBD.Properties.Resources.Help_16px;
            this.btnA3Ayuda.Iconimage_right = null;
            this.btnA3Ayuda.Iconimage_right_Selected = null;
            this.btnA3Ayuda.Iconimage_Selected = null;
            this.btnA3Ayuda.IconMarginLeft = 0;
            this.btnA3Ayuda.IconMarginRight = 0;
            this.btnA3Ayuda.IconRightVisible = true;
            this.btnA3Ayuda.IconRightZoom = 0D;
            this.btnA3Ayuda.IconVisible = true;
            this.btnA3Ayuda.IconZoom = 35D;
            this.btnA3Ayuda.IsTab = false;
            this.btnA3Ayuda.Location = new System.Drawing.Point(192, 425);
            this.btnA3Ayuda.Name = "btnA3Ayuda";
            this.btnA3Ayuda.Normalcolor = System.Drawing.Color.Gray;
            this.btnA3Ayuda.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.btnA3Ayuda.OnHoverTextColor = System.Drawing.Color.White;
            this.btnA3Ayuda.selected = false;
            this.btnA3Ayuda.Size = new System.Drawing.Size(93, 43);
            this.btnA3Ayuda.TabIndex = 13;
            this.btnA3Ayuda.Text = "Ayuda";
            this.btnA3Ayuda.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnA3Ayuda.Textcolor = System.Drawing.Color.White;
            this.btnA3Ayuda.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA3Ayuda.Click += new System.EventHandler(this.btnA3Ayuda_Click);
            // 
            // btnA3Cancelar
            // 
            this.btnA3Cancelar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnA3Cancelar.BackColor = System.Drawing.Color.Gray;
            this.btnA3Cancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnA3Cancelar.BorderRadius = 0;
            this.btnA3Cancelar.ButtonText = "Cancelar";
            this.btnA3Cancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnA3Cancelar.DisabledColor = System.Drawing.Color.Gray;
            this.btnA3Cancelar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnA3Cancelar.Iconimage = global::SyncBD.Properties.Resources.Cancel_16px;
            this.btnA3Cancelar.Iconimage_right = null;
            this.btnA3Cancelar.Iconimage_right_Selected = null;
            this.btnA3Cancelar.Iconimage_Selected = null;
            this.btnA3Cancelar.IconMarginLeft = 0;
            this.btnA3Cancelar.IconMarginRight = 0;
            this.btnA3Cancelar.IconRightVisible = true;
            this.btnA3Cancelar.IconRightZoom = 0D;
            this.btnA3Cancelar.IconVisible = true;
            this.btnA3Cancelar.IconZoom = 35D;
            this.btnA3Cancelar.IsTab = false;
            this.btnA3Cancelar.Location = new System.Drawing.Point(334, 425);
            this.btnA3Cancelar.Name = "btnA3Cancelar";
            this.btnA3Cancelar.Normalcolor = System.Drawing.Color.Gray;
            this.btnA3Cancelar.OnHovercolor = System.Drawing.Color.DarkRed;
            this.btnA3Cancelar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnA3Cancelar.selected = false;
            this.btnA3Cancelar.Size = new System.Drawing.Size(124, 43);
            this.btnA3Cancelar.TabIndex = 11;
            this.btnA3Cancelar.Text = "Cancelar";
            this.btnA3Cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnA3Cancelar.Textcolor = System.Drawing.Color.White;
            this.btnA3Cancelar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA3Cancelar.Click += new System.EventHandler(this.btnA3Cancelar_Click);
            // 
            // btnA3Continuar
            // 
            this.btnA3Continuar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnA3Continuar.BackColor = System.Drawing.Color.Gray;
            this.btnA3Continuar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnA3Continuar.BorderRadius = 0;
            this.btnA3Continuar.ButtonText = "Continuar";
            this.btnA3Continuar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnA3Continuar.DisabledColor = System.Drawing.Color.Gray;
            this.btnA3Continuar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnA3Continuar.Iconimage = global::SyncBD.Properties.Resources.Ok_16px;
            this.btnA3Continuar.Iconimage_right = null;
            this.btnA3Continuar.Iconimage_right_Selected = null;
            this.btnA3Continuar.Iconimage_Selected = null;
            this.btnA3Continuar.IconMarginLeft = 0;
            this.btnA3Continuar.IconMarginRight = 0;
            this.btnA3Continuar.IconRightVisible = true;
            this.btnA3Continuar.IconRightZoom = 0D;
            this.btnA3Continuar.IconVisible = true;
            this.btnA3Continuar.IconZoom = 40D;
            this.btnA3Continuar.IsTab = false;
            this.btnA3Continuar.Location = new System.Drawing.Point(464, 425);
            this.btnA3Continuar.Name = "btnA3Continuar";
            this.btnA3Continuar.Normalcolor = System.Drawing.Color.Gray;
            this.btnA3Continuar.OnHovercolor = System.Drawing.Color.Green;
            this.btnA3Continuar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnA3Continuar.selected = false;
            this.btnA3Continuar.Size = new System.Drawing.Size(124, 43);
            this.btnA3Continuar.TabIndex = 12;
            this.btnA3Continuar.Text = "Continuar";
            this.btnA3Continuar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnA3Continuar.Textcolor = System.Drawing.Color.White;
            this.btnA3Continuar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA3Continuar.Click += new System.EventHandler(this.btnA3Continuar_Click);
            // 
            // lblA3Avanzados
            // 
            // 
            // 
            // 
            this.lblA3Avanzados.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA3Avanzados.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA3Avanzados.ForeColor = System.Drawing.Color.Black;
            this.lblA3Avanzados.Location = new System.Drawing.Point(442, 361);
            this.lblA3Avanzados.Name = "lblA3Avanzados";
            this.lblA3Avanzados.SingleLineColor = System.Drawing.Color.Black;
            this.lblA3Avanzados.Size = new System.Drawing.Size(135, 26);
            this.lblA3Avanzados.TabIndex = 111;
            this.lblA3Avanzados.Text = "Ajustes Avanzados";
            this.lblA3Avanzados.WordWrap = true;
            // 
            // chkA3Avanzados
            // 
            this.chkA3Avanzados.BackColor = System.Drawing.Color.Transparent;
            this.chkA3Avanzados.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkA3Avanzados.BackgroundImage")));
            this.chkA3Avanzados.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkA3Avanzados.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkA3Avanzados.Location = new System.Drawing.Point(401, 363);
            this.chkA3Avanzados.Name = "chkA3Avanzados";
            this.chkA3Avanzados.OffColor = System.Drawing.Color.Red;
            this.chkA3Avanzados.OnColor = System.Drawing.Color.Green;
            this.chkA3Avanzados.Size = new System.Drawing.Size(35, 20);
            this.chkA3Avanzados.TabIndex = 7;
            this.chkA3Avanzados.Value = false;
            this.chkA3Avanzados.OnValueChange += new System.EventHandler(this.chkA3Avanzados_OnValueChange);
            // 
            // cbxA3Protocolo
            // 
            this.cbxA3Protocolo.BackColor = System.Drawing.Color.SeaGreen;
            this.cbxA3Protocolo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxA3Protocolo.DropDownHeight = 120;
            this.cbxA3Protocolo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxA3Protocolo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxA3Protocolo.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbxA3Protocolo.ForeColor = System.Drawing.Color.White;
            this.cbxA3Protocolo.FormattingEnabled = true;
            this.cbxA3Protocolo.IntegralHeight = false;
            this.cbxA3Protocolo.Items.AddRange(new object[] {
            "FTP",
            "SFTP"});
            this.cbxA3Protocolo.Location = new System.Drawing.Point(4, 155);
            this.cbxA3Protocolo.Name = "cbxA3Protocolo";
            this.cbxA3Protocolo.Size = new System.Drawing.Size(177, 27);
            this.cbxA3Protocolo.TabIndex = 103;
            this.cbxA3Protocolo.SelectedIndexChanged += new System.EventHandler(this.cbxA3Protocolo_SelectedIndexChanged);
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(4, 123);
            this.labelX1.Name = "labelX1";
            this.labelX1.SingleLineColor = System.Drawing.Color.Black;
            this.labelX1.Size = new System.Drawing.Size(160, 26);
            this.labelX1.TabIndex = 104;
            this.labelX1.Text = "Usar protocolo:";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            this.labelX1.WordWrap = true;
            // 
            // ConfigRemoto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(600, 480);
            this.Controls.Add(this.lblA3Avanzados);
            this.Controls.Add(this.chkA3Avanzados);
            this.Controls.Add(this.cbxA3Encriptacion);
            this.Controls.Add(this.chkA3Modo);
            this.Controls.Add(this.lblA3Modo);
            this.Controls.Add(this.lblA3Autenticar);
            this.Controls.Add(this.lblA3Directorio);
            this.Controls.Add(this.lblA3Anonimo);
            this.Controls.Add(this.lblA3Puerto);
            this.Controls.Add(this.chkA3Autenticar);
            this.Controls.Add(this.lblA3Encriptacion);
            this.Controls.Add(this.lblA3Password);
            this.Controls.Add(this.chkA3Anonimo);
            this.Controls.Add(this.lblA3Usuario);
            this.Controls.Add(this.lblA3Servidor);
            this.Controls.Add(this.pbxA3Ver);
            this.Controls.Add(this.txtA3Directorio);
            this.Controls.Add(this.txtA3Puerto);
            this.Controls.Add(this.txtA3Password);
            this.Controls.Add(this.txtA3Usuario);
            this.Controls.Add(this.txtA3Servidor);
            this.Controls.Add(this.btnA3Ayuda);
            this.Controls.Add(this.btnA3Cancelar);
            this.Controls.Add(this.btnA3Continuar);
            this.Controls.Add(this.lblA3Cuerpo);
            this.Controls.Add(this.lineA3Divisor);
            this.Controls.Add(this.lblA3Encabezado);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlTitleBar);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConfigRemoto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConfigRemoto";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlTitleBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA3Ver)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxA3Encriptacion;
        private DevComponents.DotNetBar.LabelX lblA3Autenticar;
        private DevComponents.DotNetBar.LabelX lblA3Anonimo;
        private DevComponents.DotNetBar.LabelX lblA3Modo;
        private DevComponents.DotNetBar.LabelX lblA3Directorio;
        private DevComponents.DotNetBar.LabelX lblA3Puerto;
        private DevComponents.DotNetBar.LabelX lblA3Encriptacion;
        private DevComponents.DotNetBar.LabelX lblA3Password;
        private DevComponents.DotNetBar.LabelX lblA3Usuario;
        private DevComponents.DotNetBar.LabelX lblA3Servidor;
        private System.Windows.Forms.PictureBox pbxA3Ver;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkA3Modo;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkA3Anonimo;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkA3Autenticar;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtA3Directorio;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtA3Puerto;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtA3Password;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtA3Usuario;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtA3Servidor;
        private Bunifu.Framework.UI.BunifuFlatButton btnA3Ayuda;
        private Bunifu.Framework.UI.BunifuFlatButton btnA3Cancelar;
        private Bunifu.Framework.UI.BunifuFlatButton btnA3Continuar;
        private DevComponents.DotNetBar.LabelX lblA3Cuerpo;
        private DevComponents.DotNetBar.Controls.Line lineA3Divisor;
        private DevComponents.DotNetBar.LabelX lblA3Encabezado;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlTitleBar;
        private DevComponents.DotNetBar.LabelX lblA3Titulo;
        private System.Windows.Forms.PictureBox pbxMinimizar;
        private System.Windows.Forms.PictureBox pbxCerrar;
        private System.Windows.Forms.PictureBox pbxIcono;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.LabelX lblA3Avanzados;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkA3Avanzados;
        private System.Windows.Forms.ComboBox cbxA3Protocolo;
        private DevComponents.DotNetBar.LabelX labelX1;
    }
}
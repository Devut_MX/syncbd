﻿namespace SyncDB.Vistas
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblPTitulo = new DevComponents.DotNetBar.LabelX();
            this.pbxMinimizar = new System.Windows.Forms.PictureBox();
            this.pbxCerrar = new System.Windows.Forms.PictureBox();
            this.pbxIcono = new System.Windows.Forms.PictureBox();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnPSalir = new System.Windows.Forms.Button();
            this.btnPAyuda = new System.Windows.Forms.Button();
            this.btnPAjustes = new System.Windows.Forms.Button();
            this.lblVersion = new DevComponents.DotNetBar.LabelX();
            this.btnPSincronizarLocal = new System.Windows.Forms.Button();
            this.btnPSincronizarRemoto = new System.Windows.Forms.Button();
            this.btnPHamburguer = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlBaseAreaTrabajo = new System.Windows.Forms.Panel();
            this.pnlRemoto = new System.Windows.Forms.Panel();
            this.lblSRArchivoActual = new DevComponents.DotNetBar.LabelX();
            this.pbrSRProgresoIndividual = new Bunifu.Framework.UI.BunifuProgressBar();
            this.lblSRPorcentajeIndividual = new DevComponents.DotNetBar.LabelX();
            this.pbxSRTiempo = new System.Windows.Forms.PictureBox();
            this.pbxSRHistorial = new System.Windows.Forms.PictureBox();
            this.pbxSRVelocidad = new System.Windows.Forms.PictureBox();
            this.pbxSRRemoto = new System.Windows.Forms.PictureBox();
            this.lblSRVelocidad = new DevComponents.DotNetBar.LabelX();
            this.dgvSRHistorial = new System.Windows.Forms.DataGridView();
            this.lblSRTiempoTranscurrido = new DevComponents.DotNetBar.LabelX();
            this.lblSREstado = new DevComponents.DotNetBar.LabelX();
            this.btnSRResincronizar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pbrSRProgreso = new Bunifu.Framework.UI.BunifuProgressBar();
            this.btnSRAbrirCarpeta = new Bunifu.Framework.UI.BunifuFlatButton();
            this.lblSRPorcentaje = new DevComponents.DotNetBar.LabelX();
            this.lblSREncabezado2 = new DevComponents.DotNetBar.LabelX();
            this.lineSRDivisor2 = new DevComponents.DotNetBar.Controls.Line();
            this.lblSREncabezado1 = new DevComponents.DotNetBar.LabelX();
            this.lineSRDivisor = new DevComponents.DotNetBar.Controls.Line();
            this.lblSREncabezado = new DevComponents.DotNetBar.LabelX();
            this.pnlAyuda = new System.Windows.Forms.Panel();
            this.btnHBuscarCodigo = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlErroresEInformacion = new System.Windows.Forms.Panel();
            this.rtbHCuerpoCodigo = new DevComponents.DotNetBar.Controls.RichTextBoxEx();
            this.lineHDivisor3 = new DevComponents.DotNetBar.Controls.Line();
            this.lblHCodigoTitulo = new DevComponents.DotNetBar.LabelX();
            this.lblHBuscarCodigo = new DevComponents.DotNetBar.LabelX();
            this.lblHAcercaDe = new DevComponents.DotNetBar.LabelX();
            this.txtHBuscarError = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.lblCopyright = new DevComponents.DotNetBar.LabelX();
            this.lineHDivisor2 = new DevComponents.DotNetBar.Controls.Line();
            this.lblHDivisor = new DevComponents.DotNetBar.Controls.Line();
            this.lblHEncabezado = new DevComponents.DotNetBar.LabelX();
            this.pnlLocal = new System.Windows.Forms.Panel();
            this.lblSLArchivoActual = new DevComponents.DotNetBar.LabelX();
            this.lblSLPorcentajeIndividual = new DevComponents.DotNetBar.LabelX();
            this.pbrSLProgresoIndividual = new Bunifu.Framework.UI.BunifuProgressBar();
            this.pbxSLTiempo = new System.Windows.Forms.PictureBox();
            this.pbxSLHistorial = new System.Windows.Forms.PictureBox();
            this.pbxSLVelocidad = new System.Windows.Forms.PictureBox();
            this.pbxSLIcono = new System.Windows.Forms.PictureBox();
            this.lblSLVelocidad = new DevComponents.DotNetBar.LabelX();
            this.dgvSLHistorial = new System.Windows.Forms.DataGridView();
            this.lblSLTiempoTranscurrido = new DevComponents.DotNetBar.LabelX();
            this.lblSLEstado = new DevComponents.DotNetBar.LabelX();
            this.lblSLPorcentaje = new DevComponents.DotNetBar.LabelX();
            this.btnSLResincronizar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pbrSLProgreso = new Bunifu.Framework.UI.BunifuProgressBar();
            this.lblSLEncabezado2 = new DevComponents.DotNetBar.LabelX();
            this.btnSLAbrirCarpeta = new Bunifu.Framework.UI.BunifuFlatButton();
            this.lineSLDivisor2 = new DevComponents.DotNetBar.Controls.Line();
            this.lblSLEncabezado1 = new DevComponents.DotNetBar.LabelX();
            this.lineSLDivisor = new DevComponents.DotNetBar.Controls.Line();
            this.lblSLEncabezado = new DevComponents.DotNetBar.LabelX();
            this.pnlAjustes = new System.Windows.Forms.Panel();
            this.btnAJPersonalizarTema = new Bunifu.Framework.UI.BunifuFlatButton();
            this.lblAJRutaGuardado = new DevComponents.DotNetBar.LabelX();
            this.lblAJTituloGuardado = new DevComponents.DotNetBar.LabelX();
            this.btnPEscogerCarpeta = new Bunifu.Framework.UI.BunifuFlatButton();
            this.cbxAJIdioma = new System.Windows.Forms.ComboBox();
            this.cbxAJApariencia = new System.Windows.Forms.ComboBox();
            this.cbxAJGuardarEn = new System.Windows.Forms.ComboBox();
            this.lblAJRutaPermanente = new DevComponents.DotNetBar.LabelX();
            this.lblAJIniciarConWindows = new DevComponents.DotNetBar.LabelX();
            this.lblAJConfigurarServidores = new DevComponents.DotNetBar.LabelX();
            this.lblAJCambiarApariencia = new DevComponents.DotNetBar.LabelX();
            this.lblAJReestablecer = new DevComponents.DotNetBar.LabelX();
            this.lblAJBorrarHistorial = new DevComponents.DotNetBar.LabelX();
            this.lblAJIdioma = new DevComponents.DotNetBar.LabelX();
            this.lblAJGuardarEn = new DevComponents.DotNetBar.LabelX();
            this.lblAJMinimizarIniciar = new DevComponents.DotNetBar.LabelX();
            this.lblAJDistinguirSincronizacion = new DevComponents.DotNetBar.LabelX();
            this.chkAJIniciarConWindows = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.btnAJReestablecer = new Bunifu.Framework.UI.BunifuFlatButton();
            this.chkAJMinimizarIniciar = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.btnAJBorrarHistorial = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnAJLocal = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnAJRemoto = new Bunifu.Framework.UI.BunifuFlatButton();
            this.chkAJRutaPermanente = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.chkAJDistinguirSincronizacion = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.lineAJDivisor = new DevComponents.DotNetBar.Controls.Line();
            this.lblAJEncabezado = new DevComponents.DotNetBar.LabelX();
            this.pnlBaseSeleccion = new System.Windows.Forms.Panel();
            this.pnlSeleccion = new System.Windows.Forms.Panel();
            this.pnlFondoSeleccion = new System.Windows.Forms.Panel();
            this.ttpMensaje = new System.Windows.Forms.ToolTip(this.components);
            this.tmpLocalTiempoTranscurrido = new System.Windows.Forms.Timer(this.components);
            this.cmpMenuBarraTareas = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.remotoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStartRemoteStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPauseRemoteStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStopRemoteStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRemoteTreeStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.descargasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStartLocalStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPauseLocalStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStopLocalStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLocalTreeStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRestaurarMenuStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExitSysTray = new System.Windows.Forms.ToolStripMenuItem();
            this.nicBarraTareas = new System.Windows.Forms.NotifyIcon(this.components);
            this.tmpRemotoTiempoTranscurrido = new System.Windows.Forms.Timer(this.components);
            this.fbdGuardarEn = new System.Windows.Forms.FolderBrowserDialog();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).BeginInit();
            this.pnlMenu.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlBaseAreaTrabajo.SuspendLayout();
            this.pnlRemoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSRTiempo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSRHistorial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSRVelocidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSRRemoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSRHistorial)).BeginInit();
            this.pnlAyuda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlErroresEInformacion.SuspendLayout();
            this.pnlLocal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSLTiempo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSLHistorial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSLVelocidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSLIcono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSLHistorial)).BeginInit();
            this.pnlAjustes.SuspendLayout();
            this.pnlBaseSeleccion.SuspendLayout();
            this.cmpMenuBarraTareas.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.Green;
            this.pnlTitleBar.Controls.Add(this.lblPTitulo);
            this.pnlTitleBar.Controls.Add(this.pbxMinimizar);
            this.pnlTitleBar.Controls.Add(this.pbxCerrar);
            this.pnlTitleBar.Controls.Add(this.pbxIcono);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(800, 30);
            this.pnlTitleBar.TabIndex = 2;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // lblPTitulo
            // 
            // 
            // 
            // 
            this.lblPTitulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblPTitulo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblPTitulo.ForeColor = System.Drawing.Color.White;
            this.lblPTitulo.Location = new System.Drawing.Point(28, 7);
            this.lblPTitulo.Name = "lblPTitulo";
            this.lblPTitulo.Size = new System.Drawing.Size(708, 20);
            this.lblPTitulo.TabIndex = 14;
            this.lblPTitulo.Text = "SyncBD - Sincroniza tus servidores de datos";
            this.lblPTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pbxMinimizar
            // 
            this.pbxMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.pbxMinimizar.Image = global::SyncBD.Properties.Resources.Compress_24px;
            this.pbxMinimizar.Location = new System.Drawing.Point(742, 5);
            this.pbxMinimizar.Name = "pbxMinimizar";
            this.pbxMinimizar.Size = new System.Drawing.Size(20, 20);
            this.pbxMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMinimizar.TabIndex = 3;
            this.pbxMinimizar.TabStop = false;
            this.pbxMinimizar.Click += new System.EventHandler(this.pbxMinimizar_Click);
            // 
            // pbxCerrar
            // 
            this.pbxCerrar.BackColor = System.Drawing.Color.Transparent;
            this.pbxCerrar.Image = global::SyncBD.Properties.Resources.CloseWindow_24px;
            this.pbxCerrar.Location = new System.Drawing.Point(768, 5);
            this.pbxCerrar.Name = "pbxCerrar";
            this.pbxCerrar.Size = new System.Drawing.Size(20, 20);
            this.pbxCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCerrar.TabIndex = 2;
            this.pbxCerrar.TabStop = false;
            this.pbxCerrar.Click += new System.EventHandler(this.pbxCerrar_Click);
            // 
            // pbxIcono
            // 
            this.pbxIcono.BackColor = System.Drawing.Color.Transparent;
            this.pbxIcono.Image = global::SyncBD.Properties.Resources.SyncBDLogo;
            this.pbxIcono.Location = new System.Drawing.Point(6, 8);
            this.pbxIcono.Name = "pbxIcono";
            this.pbxIcono.Size = new System.Drawing.Size(16, 16);
            this.pbxIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxIcono.TabIndex = 1;
            this.pbxIcono.TabStop = false;
            this.pbxIcono.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.Gainsboro;
            this.pnlMenu.Controls.Add(this.btnPSalir);
            this.pnlMenu.Controls.Add(this.btnPAyuda);
            this.pnlMenu.Controls.Add(this.btnPAjustes);
            this.pnlMenu.Controls.Add(this.lblVersion);
            this.pnlMenu.Controls.Add(this.btnPSincronizarLocal);
            this.pnlMenu.Controls.Add(this.btnPSincronizarRemoto);
            this.pnlMenu.Controls.Add(this.btnPHamburguer);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenu.Location = new System.Drawing.Point(0, 30);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(140, 570);
            this.pnlMenu.TabIndex = 3;
            // 
            // btnPSalir
            // 
            this.btnPSalir.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPSalir.Image = global::SyncBD.Properties.Resources.CloseWindow_24px;
            this.btnPSalir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPSalir.Location = new System.Drawing.Point(0, 230);
            this.btnPSalir.Name = "btnPSalir";
            this.btnPSalir.Size = new System.Drawing.Size(140, 50);
            this.btnPSalir.TabIndex = 6;
            this.btnPSalir.Text = "Salir";
            this.btnPSalir.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ttpMensaje.SetToolTip(this.btnPSalir, "Cierra el programa completamente");
            this.btnPSalir.UseVisualStyleBackColor = true;
            this.btnPSalir.Click += new System.EventHandler(this.btnPSalir_Click);
            this.btnPSalir.Enter += new System.EventHandler(this.btnPSalir_Enter);
            // 
            // btnPAyuda
            // 
            this.btnPAyuda.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPAyuda.Image = global::SyncBD.Properties.Resources.Help_24px;
            this.btnPAyuda.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPAyuda.Location = new System.Drawing.Point(0, 180);
            this.btnPAyuda.Name = "btnPAyuda";
            this.btnPAyuda.Size = new System.Drawing.Size(140, 50);
            this.btnPAyuda.TabIndex = 5;
            this.btnPAyuda.Text = "Ayuda";
            this.btnPAyuda.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ttpMensaje.SetToolTip(this.btnPAyuda, "Ayuda de SyncBD");
            this.btnPAyuda.UseVisualStyleBackColor = true;
            this.btnPAyuda.Click += new System.EventHandler(this.btnPAyuda_Click);
            this.btnPAyuda.Enter += new System.EventHandler(this.btnPAyuda_Enter);
            // 
            // btnPAjustes
            // 
            this.btnPAjustes.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPAjustes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPAjustes.Image = global::SyncBD.Properties.Resources.Settings_24px;
            this.btnPAjustes.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPAjustes.Location = new System.Drawing.Point(0, 130);
            this.btnPAjustes.Name = "btnPAjustes";
            this.btnPAjustes.Size = new System.Drawing.Size(140, 50);
            this.btnPAjustes.TabIndex = 4;
            this.btnPAjustes.Text = "Ajustes";
            this.btnPAjustes.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ttpMensaje.SetToolTip(this.btnPAjustes, "Ajustes de SyncBD");
            this.btnPAjustes.UseVisualStyleBackColor = true;
            this.btnPAjustes.Click += new System.EventHandler(this.btnPAjustes_Click);
            this.btnPAjustes.Enter += new System.EventHandler(this.btnPAjustes_Enter);
            // 
            // lblVersion
            // 
            this.lblVersion.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblVersion.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblVersion.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblVersion.ForeColor = System.Drawing.Color.Black;
            this.lblVersion.Location = new System.Drawing.Point(0, 529);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(140, 41);
            this.lblVersion.TabIndex = 16;
            this.lblVersion.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // btnPSincronizarLocal
            // 
            this.btnPSincronizarLocal.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPSincronizarLocal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPSincronizarLocal.Image = global::SyncBD.Properties.Resources.UploadToFTP_24px;
            this.btnPSincronizarLocal.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPSincronizarLocal.Location = new System.Drawing.Point(0, 80);
            this.btnPSincronizarLocal.Name = "btnPSincronizarLocal";
            this.btnPSincronizarLocal.Size = new System.Drawing.Size(140, 50);
            this.btnPSincronizarLocal.TabIndex = 3;
            this.btnPSincronizarLocal.Text = "Sincronizar local";
            this.btnPSincronizarLocal.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ttpMensaje.SetToolTip(this.btnPSincronizarLocal, "Cargar archivos al servidor local");
            this.btnPSincronizarLocal.UseVisualStyleBackColor = true;
            this.btnPSincronizarLocal.Click += new System.EventHandler(this.btnPSincronizarLocal_Click);
            this.btnPSincronizarLocal.Enter += new System.EventHandler(this.btnPSincronizarLocal_Enter);
            // 
            // btnPSincronizarRemoto
            // 
            this.btnPSincronizarRemoto.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPSincronizarRemoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPSincronizarRemoto.Image = global::SyncBD.Properties.Resources.DownloadFromFTP_24px;
            this.btnPSincronizarRemoto.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPSincronizarRemoto.Location = new System.Drawing.Point(0, 30);
            this.btnPSincronizarRemoto.Name = "btnPSincronizarRemoto";
            this.btnPSincronizarRemoto.Size = new System.Drawing.Size(140, 50);
            this.btnPSincronizarRemoto.TabIndex = 2;
            this.btnPSincronizarRemoto.Text = "Sincronizar remoto";
            this.btnPSincronizarRemoto.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ttpMensaje.SetToolTip(this.btnPSincronizarRemoto, "Descargar archivos del servidor remoto");
            this.btnPSincronizarRemoto.UseVisualStyleBackColor = true;
            this.btnPSincronizarRemoto.Click += new System.EventHandler(this.btnPSincronizarRemoto_Click);
            this.btnPSincronizarRemoto.Enter += new System.EventHandler(this.btnPSincronizarRemoto_Enter);
            // 
            // btnPHamburguer
            // 
            this.btnPHamburguer.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPHamburguer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPHamburguer.Image = global::SyncBD.Properties.Resources.Menu_16px;
            this.btnPHamburguer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPHamburguer.Location = new System.Drawing.Point(0, 0);
            this.btnPHamburguer.Name = "btnPHamburguer";
            this.btnPHamburguer.Size = new System.Drawing.Size(140, 30);
            this.btnPHamburguer.TabIndex = 1;
            this.btnPHamburguer.Text = "Menú de opciones";
            this.ttpMensaje.SetToolTip(this.btnPHamburguer, "Expandir/Contraer menú");
            this.btnPHamburguer.UseVisualStyleBackColor = true;
            this.btnPHamburguer.Click += new System.EventHandler(this.btnPHamburguer_Click);
            this.btnPHamburguer.Enter += new System.EventHandler(this.btnPHamburguer_Enter);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pnlBaseAreaTrabajo);
            this.panel2.Controls.Add(this.pnlBaseSeleccion);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(140, 30);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(660, 570);
            this.panel2.TabIndex = 5;
            // 
            // pnlBaseAreaTrabajo
            // 
            this.pnlBaseAreaTrabajo.BackColor = System.Drawing.Color.White;
            this.pnlBaseAreaTrabajo.Controls.Add(this.pnlLocal);
            this.pnlBaseAreaTrabajo.Controls.Add(this.pnlAjustes);
            this.pnlBaseAreaTrabajo.Controls.Add(this.pnlRemoto);
            this.pnlBaseAreaTrabajo.Controls.Add(this.pnlAyuda);
            this.pnlBaseAreaTrabajo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBaseAreaTrabajo.Location = new System.Drawing.Point(5, 0);
            this.pnlBaseAreaTrabajo.Name = "pnlBaseAreaTrabajo";
            this.pnlBaseAreaTrabajo.Size = new System.Drawing.Size(655, 570);
            this.pnlBaseAreaTrabajo.TabIndex = 6;
            // 
            // pnlRemoto
            // 
            this.pnlRemoto.Controls.Add(this.lblSRArchivoActual);
            this.pnlRemoto.Controls.Add(this.pbrSRProgresoIndividual);
            this.pnlRemoto.Controls.Add(this.lblSRPorcentajeIndividual);
            this.pnlRemoto.Controls.Add(this.pbxSRTiempo);
            this.pnlRemoto.Controls.Add(this.pbxSRHistorial);
            this.pnlRemoto.Controls.Add(this.pbxSRVelocidad);
            this.pnlRemoto.Controls.Add(this.pbxSRRemoto);
            this.pnlRemoto.Controls.Add(this.lblSRVelocidad);
            this.pnlRemoto.Controls.Add(this.dgvSRHistorial);
            this.pnlRemoto.Controls.Add(this.lblSRTiempoTranscurrido);
            this.pnlRemoto.Controls.Add(this.lblSREstado);
            this.pnlRemoto.Controls.Add(this.btnSRResincronizar);
            this.pnlRemoto.Controls.Add(this.pbrSRProgreso);
            this.pnlRemoto.Controls.Add(this.btnSRAbrirCarpeta);
            this.pnlRemoto.Controls.Add(this.lblSRPorcentaje);
            this.pnlRemoto.Controls.Add(this.lblSREncabezado2);
            this.pnlRemoto.Controls.Add(this.lineSRDivisor2);
            this.pnlRemoto.Controls.Add(this.lblSREncabezado1);
            this.pnlRemoto.Controls.Add(this.lineSRDivisor);
            this.pnlRemoto.Controls.Add(this.lblSREncabezado);
            this.pnlRemoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRemoto.Location = new System.Drawing.Point(0, 0);
            this.pnlRemoto.Name = "pnlRemoto";
            this.pnlRemoto.Size = new System.Drawing.Size(655, 570);
            this.pnlRemoto.TabIndex = 2;
            this.pnlRemoto.Visible = false;
            // 
            // lblSRArchivoActual
            // 
            this.lblSRArchivoActual.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSRArchivoActual.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSRArchivoActual.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSRArchivoActual.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblSRArchivoActual.ForeColor = System.Drawing.Color.Black;
            this.lblSRArchivoActual.Location = new System.Drawing.Point(9, 450);
            this.lblSRArchivoActual.Name = "lblSRArchivoActual";
            this.lblSRArchivoActual.SingleLineColor = System.Drawing.Color.Black;
            this.lblSRArchivoActual.Size = new System.Drawing.Size(297, 20);
            this.lblSRArchivoActual.TabIndex = 41;
            this.lblSRArchivoActual.WordWrap = true;
            // 
            // pbrSRProgresoIndividual
            // 
            this.pbrSRProgresoIndividual.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbrSRProgresoIndividual.BackColor = System.Drawing.Color.Silver;
            this.pbrSRProgresoIndividual.BorderRadius = 5;
            this.pbrSRProgresoIndividual.Location = new System.Drawing.Point(9, 474);
            this.pbrSRProgresoIndividual.MaximumValue = 100;
            this.pbrSRProgresoIndividual.Name = "pbrSRProgresoIndividual";
            this.pbrSRProgresoIndividual.ProgressColor = System.Drawing.Color.Green;
            this.pbrSRProgresoIndividual.Size = new System.Drawing.Size(478, 24);
            this.pbrSRProgresoIndividual.TabIndex = 40;
            this.pbrSRProgresoIndividual.Value = 0;
            // 
            // lblSRPorcentajeIndividual
            // 
            this.lblSRPorcentajeIndividual.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSRPorcentajeIndividual.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSRPorcentajeIndividual.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSRPorcentajeIndividual.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblSRPorcentajeIndividual.ForeColor = System.Drawing.Color.Black;
            this.lblSRPorcentajeIndividual.Location = new System.Drawing.Point(321, 450);
            this.lblSRPorcentajeIndividual.Name = "lblSRPorcentajeIndividual";
            this.lblSRPorcentajeIndividual.SingleLineColor = System.Drawing.Color.Black;
            this.lblSRPorcentajeIndividual.Size = new System.Drawing.Size(163, 20);
            this.lblSRPorcentajeIndividual.TabIndex = 39;
            this.lblSRPorcentajeIndividual.Text = "0% completado";
            this.lblSRPorcentajeIndividual.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblSRPorcentajeIndividual.WordWrap = true;
            // 
            // pbxSRTiempo
            // 
            this.pbxSRTiempo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbxSRTiempo.BackColor = System.Drawing.Color.Transparent;
            this.pbxSRTiempo.Image = global::SyncBD.Properties.Resources.Clock_24px;
            this.pbxSRTiempo.Location = new System.Drawing.Point(224, 360);
            this.pbxSRTiempo.Name = "pbxSRTiempo";
            this.pbxSRTiempo.Size = new System.Drawing.Size(26, 26);
            this.pbxSRTiempo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxSRTiempo.TabIndex = 38;
            this.pbxSRTiempo.TabStop = false;
            this.pbxSRTiempo.Visible = false;
            // 
            // pbxSRHistorial
            // 
            this.pbxSRHistorial.BackColor = System.Drawing.Color.Transparent;
            this.pbxSRHistorial.Image = global::SyncBD.Properties.Resources.Historical_24px;
            this.pbxSRHistorial.Location = new System.Drawing.Point(6, 54);
            this.pbxSRHistorial.Name = "pbxSRHistorial";
            this.pbxSRHistorial.Size = new System.Drawing.Size(26, 26);
            this.pbxSRHistorial.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxSRHistorial.TabIndex = 37;
            this.pbxSRHistorial.TabStop = false;
            // 
            // pbxSRVelocidad
            // 
            this.pbxSRVelocidad.BackColor = System.Drawing.Color.Transparent;
            this.pbxSRVelocidad.Image = global::SyncBD.Properties.Resources.Speed_24px;
            this.pbxSRVelocidad.Location = new System.Drawing.Point(9, 504);
            this.pbxSRVelocidad.Name = "pbxSRVelocidad";
            this.pbxSRVelocidad.Size = new System.Drawing.Size(26, 26);
            this.pbxSRVelocidad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxSRVelocidad.TabIndex = 36;
            this.pbxSRVelocidad.TabStop = false;
            this.pbxSRVelocidad.Visible = false;
            // 
            // pbxSRRemoto
            // 
            this.pbxSRRemoto.BackColor = System.Drawing.Color.Transparent;
            this.pbxSRRemoto.Image = global::SyncBD.Properties.Resources.DownloadFromFTP_24px;
            this.pbxSRRemoto.Location = new System.Drawing.Point(6, 319);
            this.pbxSRRemoto.Name = "pbxSRRemoto";
            this.pbxSRRemoto.Size = new System.Drawing.Size(26, 26);
            this.pbxSRRemoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxSRRemoto.TabIndex = 35;
            this.pbxSRRemoto.TabStop = false;
            // 
            // lblSRVelocidad
            // 
            this.lblSRVelocidad.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSRVelocidad.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSRVelocidad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSRVelocidad.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblSRVelocidad.ForeColor = System.Drawing.Color.Black;
            this.lblSRVelocidad.Location = new System.Drawing.Point(38, 512);
            this.lblSRVelocidad.Name = "lblSRVelocidad";
            this.lblSRVelocidad.SingleLineColor = System.Drawing.Color.Black;
            this.lblSRVelocidad.Size = new System.Drawing.Size(446, 18);
            this.lblSRVelocidad.TabIndex = 30;
            this.lblSRVelocidad.Text = "Velocidad actual:";
            this.lblSRVelocidad.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblSRVelocidad.Visible = false;
            this.lblSRVelocidad.WordWrap = true;
            // 
            // dgvSRHistorial
            // 
            this.dgvSRHistorial.AllowUserToAddRows = false;
            this.dgvSRHistorial.AllowUserToDeleteRows = false;
            this.dgvSRHistorial.AllowUserToOrderColumns = true;
            this.dgvSRHistorial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSRHistorial.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSRHistorial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSRHistorial.Location = new System.Drawing.Point(6, 86);
            this.dgvSRHistorial.Name = "dgvSRHistorial";
            this.dgvSRHistorial.ReadOnly = true;
            this.dgvSRHistorial.Size = new System.Drawing.Size(637, 215);
            this.dgvSRHistorial.TabIndex = 29;
            // 
            // lblSRTiempoTranscurrido
            // 
            this.lblSRTiempoTranscurrido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSRTiempoTranscurrido.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSRTiempoTranscurrido.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSRTiempoTranscurrido.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblSRTiempoTranscurrido.ForeColor = System.Drawing.Color.Black;
            this.lblSRTiempoTranscurrido.Location = new System.Drawing.Point(256, 365);
            this.lblSRTiempoTranscurrido.Name = "lblSRTiempoTranscurrido";
            this.lblSRTiempoTranscurrido.SingleLineColor = System.Drawing.Color.Black;
            this.lblSRTiempoTranscurrido.Size = new System.Drawing.Size(228, 20);
            this.lblSRTiempoTranscurrido.TabIndex = 28;
            this.lblSRTiempoTranscurrido.WordWrap = true;
            // 
            // lblSREstado
            // 
            this.lblSREstado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSREstado.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSREstado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSREstado.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblSREstado.ForeColor = System.Drawing.Color.Black;
            this.lblSREstado.Location = new System.Drawing.Point(6, 420);
            this.lblSREstado.Name = "lblSREstado";
            this.lblSREstado.SingleLineColor = System.Drawing.Color.Black;
            this.lblSREstado.Size = new System.Drawing.Size(478, 24);
            this.lblSREstado.TabIndex = 27;
            this.lblSREstado.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblSREstado.WordWrap = true;
            // 
            // btnSRResincronizar
            // 
            this.btnSRResincronizar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnSRResincronizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSRResincronizar.BackColor = System.Drawing.Color.Gray;
            this.btnSRResincronizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSRResincronizar.BorderRadius = 0;
            this.btnSRResincronizar.ButtonText = "Resincronizar";
            this.btnSRResincronizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSRResincronizar.DisabledColor = System.Drawing.Color.Gray;
            this.btnSRResincronizar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnSRResincronizar.Iconimage = global::SyncBD.Properties.Resources.Synchronize_16px;
            this.btnSRResincronizar.Iconimage_right = null;
            this.btnSRResincronizar.Iconimage_right_Selected = null;
            this.btnSRResincronizar.Iconimage_Selected = null;
            this.btnSRResincronizar.IconMarginLeft = 0;
            this.btnSRResincronizar.IconMarginRight = 0;
            this.btnSRResincronizar.IconRightVisible = true;
            this.btnSRResincronizar.IconRightZoom = 0D;
            this.btnSRResincronizar.IconVisible = true;
            this.btnSRResincronizar.IconZoom = 40D;
            this.btnSRResincronizar.IsTab = false;
            this.btnSRResincronizar.Location = new System.Drawing.Point(490, 391);
            this.btnSRResincronizar.Name = "btnSRResincronizar";
            this.btnSRResincronizar.Normalcolor = System.Drawing.Color.Gray;
            this.btnSRResincronizar.OnHovercolor = System.Drawing.Color.Green;
            this.btnSRResincronizar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnSRResincronizar.selected = false;
            this.btnSRResincronizar.Size = new System.Drawing.Size(153, 43);
            this.btnSRResincronizar.TabIndex = 22;
            this.btnSRResincronizar.Text = "Resincronizar";
            this.btnSRResincronizar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSRResincronizar.Textcolor = System.Drawing.Color.White;
            this.btnSRResincronizar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSRResincronizar.Click += new System.EventHandler(this.btnSRResincronizar_Click);
            // 
            // pbrSRProgreso
            // 
            this.pbrSRProgreso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbrSRProgreso.BackColor = System.Drawing.Color.Silver;
            this.pbrSRProgreso.BorderRadius = 5;
            this.pbrSRProgreso.Location = new System.Drawing.Point(6, 391);
            this.pbrSRProgreso.MaximumValue = 100;
            this.pbrSRProgreso.Name = "pbrSRProgreso";
            this.pbrSRProgreso.ProgressColor = System.Drawing.Color.Green;
            this.pbrSRProgreso.Size = new System.Drawing.Size(478, 24);
            this.pbrSRProgreso.TabIndex = 25;
            this.pbrSRProgreso.Value = 0;
            // 
            // btnSRAbrirCarpeta
            // 
            this.btnSRAbrirCarpeta.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnSRAbrirCarpeta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSRAbrirCarpeta.BackColor = System.Drawing.Color.Gray;
            this.btnSRAbrirCarpeta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSRAbrirCarpeta.BorderRadius = 0;
            this.btnSRAbrirCarpeta.ButtonText = "Abrir carpeta";
            this.btnSRAbrirCarpeta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSRAbrirCarpeta.DisabledColor = System.Drawing.Color.Gray;
            this.btnSRAbrirCarpeta.Iconcolor = System.Drawing.Color.Transparent;
            this.btnSRAbrirCarpeta.Iconimage = global::SyncBD.Properties.Resources.OpenFolder_16px;
            this.btnSRAbrirCarpeta.Iconimage_right = null;
            this.btnSRAbrirCarpeta.Iconimage_right_Selected = null;
            this.btnSRAbrirCarpeta.Iconimage_Selected = null;
            this.btnSRAbrirCarpeta.IconMarginLeft = 0;
            this.btnSRAbrirCarpeta.IconMarginRight = 0;
            this.btnSRAbrirCarpeta.IconRightVisible = true;
            this.btnSRAbrirCarpeta.IconRightZoom = 0D;
            this.btnSRAbrirCarpeta.IconVisible = true;
            this.btnSRAbrirCarpeta.IconZoom = 40D;
            this.btnSRAbrirCarpeta.IsTab = false;
            this.btnSRAbrirCarpeta.Location = new System.Drawing.Point(490, 455);
            this.btnSRAbrirCarpeta.Name = "btnSRAbrirCarpeta";
            this.btnSRAbrirCarpeta.Normalcolor = System.Drawing.Color.Gray;
            this.btnSRAbrirCarpeta.OnHovercolor = System.Drawing.Color.DarkCyan;
            this.btnSRAbrirCarpeta.OnHoverTextColor = System.Drawing.Color.White;
            this.btnSRAbrirCarpeta.selected = false;
            this.btnSRAbrirCarpeta.Size = new System.Drawing.Size(153, 43);
            this.btnSRAbrirCarpeta.TabIndex = 23;
            this.btnSRAbrirCarpeta.Text = "Abrir carpeta";
            this.btnSRAbrirCarpeta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSRAbrirCarpeta.Textcolor = System.Drawing.Color.White;
            this.btnSRAbrirCarpeta.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSRAbrirCarpeta.Click += new System.EventHandler(this.btnSRAbrirCarpeta_Click);
            // 
            // lblSRPorcentaje
            // 
            this.lblSRPorcentaje.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSRPorcentaje.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSRPorcentaje.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblSRPorcentaje.ForeColor = System.Drawing.Color.Black;
            this.lblSRPorcentaje.Location = new System.Drawing.Point(6, 365);
            this.lblSRPorcentaje.Name = "lblSRPorcentaje";
            this.lblSRPorcentaje.SingleLineColor = System.Drawing.Color.Black;
            this.lblSRPorcentaje.Size = new System.Drawing.Size(212, 20);
            this.lblSRPorcentaje.TabIndex = 22;
            this.lblSRPorcentaje.Text = "0% completado";
            this.lblSRPorcentaje.WordWrap = true;
            // 
            // lblSREncabezado2
            // 
            this.lblSREncabezado2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSREncabezado2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSREncabezado2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSREncabezado2.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblSREncabezado2.ForeColor = System.Drawing.Color.Black;
            this.lblSREncabezado2.Location = new System.Drawing.Point(38, 325);
            this.lblSREncabezado2.Name = "lblSREncabezado2";
            this.lblSREncabezado2.SingleLineColor = System.Drawing.Color.Black;
            this.lblSREncabezado2.Size = new System.Drawing.Size(605, 20);
            this.lblSREncabezado2.TabIndex = 18;
            this.lblSREncabezado2.Text = "Descarga actual";
            this.lblSREncabezado2.WordWrap = true;
            // 
            // lineSRDivisor2
            // 
            this.lineSRDivisor2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lineSRDivisor2.ForeColor = System.Drawing.Color.Black;
            this.lineSRDivisor2.Location = new System.Drawing.Point(6, 301);
            this.lineSRDivisor2.Name = "lineSRDivisor2";
            this.lineSRDivisor2.Size = new System.Drawing.Size(637, 23);
            this.lineSRDivisor2.TabIndex = 10;
            this.lineSRDivisor2.Text = "line4";
            // 
            // lblSREncabezado1
            // 
            this.lblSREncabezado1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSREncabezado1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSREncabezado1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSREncabezado1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblSREncabezado1.ForeColor = System.Drawing.Color.Black;
            this.lblSREncabezado1.Location = new System.Drawing.Point(38, 59);
            this.lblSREncabezado1.Name = "lblSREncabezado1";
            this.lblSREncabezado1.SingleLineColor = System.Drawing.Color.Black;
            this.lblSREncabezado1.Size = new System.Drawing.Size(605, 20);
            this.lblSREncabezado1.TabIndex = 9;
            this.lblSREncabezado1.Text = "Historial de descargas";
            this.lblSREncabezado1.WordWrap = true;
            // 
            // lineSRDivisor
            // 
            this.lineSRDivisor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lineSRDivisor.ForeColor = System.Drawing.Color.Black;
            this.lineSRDivisor.Location = new System.Drawing.Point(6, 39);
            this.lineSRDivisor.Name = "lineSRDivisor";
            this.lineSRDivisor.Size = new System.Drawing.Size(637, 23);
            this.lineSRDivisor.TabIndex = 7;
            this.lineSRDivisor.Text = "line3";
            // 
            // lblSREncabezado
            // 
            this.lblSREncabezado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSREncabezado.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSREncabezado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSREncabezado.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblSREncabezado.ForeColor = System.Drawing.Color.Black;
            this.lblSREncabezado.Location = new System.Drawing.Point(6, 6);
            this.lblSREncabezado.Name = "lblSREncabezado";
            this.lblSREncabezado.SingleLineColor = System.Drawing.Color.Black;
            this.lblSREncabezado.Size = new System.Drawing.Size(637, 39);
            this.lblSREncabezado.TabIndex = 6;
            this.lblSREncabezado.Text = "Opciones del servidor remoto";
            this.lblSREncabezado.WordWrap = true;
            // 
            // pnlAyuda
            // 
            this.pnlAyuda.BackColor = System.Drawing.Color.White;
            this.pnlAyuda.Controls.Add(this.btnHBuscarCodigo);
            this.pnlAyuda.Controls.Add(this.pictureBox1);
            this.pnlAyuda.Controls.Add(this.pnlErroresEInformacion);
            this.pnlAyuda.Controls.Add(this.lblHBuscarCodigo);
            this.pnlAyuda.Controls.Add(this.lblHAcercaDe);
            this.pnlAyuda.Controls.Add(this.txtHBuscarError);
            this.pnlAyuda.Controls.Add(this.lblCopyright);
            this.pnlAyuda.Controls.Add(this.lineHDivisor2);
            this.pnlAyuda.Controls.Add(this.lblHDivisor);
            this.pnlAyuda.Controls.Add(this.lblHEncabezado);
            this.pnlAyuda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAyuda.Location = new System.Drawing.Point(0, 0);
            this.pnlAyuda.Name = "pnlAyuda";
            this.pnlAyuda.Size = new System.Drawing.Size(655, 570);
            this.pnlAyuda.TabIndex = 1;
            this.pnlAyuda.Visible = false;
            // 
            // btnHBuscarCodigo
            // 
            this.btnHBuscarCodigo.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnHBuscarCodigo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHBuscarCodigo.BackColor = System.Drawing.Color.Gray;
            this.btnHBuscarCodigo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHBuscarCodigo.BorderRadius = 0;
            this.btnHBuscarCodigo.ButtonText = "";
            this.btnHBuscarCodigo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHBuscarCodigo.DisabledColor = System.Drawing.Color.Gray;
            this.btnHBuscarCodigo.Iconcolor = System.Drawing.Color.Transparent;
            this.btnHBuscarCodigo.Iconimage = global::SyncBD.Properties.Resources.Search_16px;
            this.btnHBuscarCodigo.Iconimage_right = null;
            this.btnHBuscarCodigo.Iconimage_right_Selected = null;
            this.btnHBuscarCodigo.Iconimage_Selected = null;
            this.btnHBuscarCodigo.IconMarginLeft = 0;
            this.btnHBuscarCodigo.IconMarginRight = 0;
            this.btnHBuscarCodigo.IconRightVisible = true;
            this.btnHBuscarCodigo.IconRightZoom = 0D;
            this.btnHBuscarCodigo.IconVisible = true;
            this.btnHBuscarCodigo.IconZoom = 40D;
            this.btnHBuscarCodigo.IsTab = false;
            this.btnHBuscarCodigo.Location = new System.Drawing.Point(598, 193);
            this.btnHBuscarCodigo.Name = "btnHBuscarCodigo";
            this.btnHBuscarCodigo.Normalcolor = System.Drawing.Color.Gray;
            this.btnHBuscarCodigo.OnHovercolor = System.Drawing.Color.Green;
            this.btnHBuscarCodigo.OnHoverTextColor = System.Drawing.Color.White;
            this.btnHBuscarCodigo.selected = false;
            this.btnHBuscarCodigo.Size = new System.Drawing.Size(33, 31);
            this.btnHBuscarCodigo.TabIndex = 8;
            this.btnHBuscarCodigo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnHBuscarCodigo.Textcolor = System.Drawing.Color.White;
            this.btnHBuscarCodigo.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHBuscarCodigo.Click += new System.EventHandler(this.btnHBuscarCodigo_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(14, 62);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(204, 104);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            // 
            // pnlErroresEInformacion
            // 
            this.pnlErroresEInformacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlErroresEInformacion.AutoScroll = true;
            this.pnlErroresEInformacion.BackColor = System.Drawing.Color.White;
            this.pnlErroresEInformacion.Controls.Add(this.rtbHCuerpoCodigo);
            this.pnlErroresEInformacion.Controls.Add(this.lineHDivisor3);
            this.pnlErroresEInformacion.Controls.Add(this.lblHCodigoTitulo);
            this.pnlErroresEInformacion.Location = new System.Drawing.Point(9, 233);
            this.pnlErroresEInformacion.Name = "pnlErroresEInformacion";
            this.pnlErroresEInformacion.Size = new System.Drawing.Size(634, 325);
            this.pnlErroresEInformacion.TabIndex = 12;
            this.pnlErroresEInformacion.Visible = false;
            // 
            // rtbHCuerpoCodigo
            // 
            this.rtbHCuerpoCodigo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbHCuerpoCodigo.BackColorRichTextBox = System.Drawing.Color.White;
            // 
            // 
            // 
            this.rtbHCuerpoCodigo.BackgroundStyle.Class = "RichTextBoxBorder";
            this.rtbHCuerpoCodigo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rtbHCuerpoCodigo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.rtbHCuerpoCodigo.ForeColor = System.Drawing.Color.Black;
            this.rtbHCuerpoCodigo.Location = new System.Drawing.Point(6, 47);
            this.rtbHCuerpoCodigo.Name = "rtbHCuerpoCodigo";
            this.rtbHCuerpoCodigo.ReadOnly = true;
            this.rtbHCuerpoCodigo.Rtf = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang2058{\\fonttbl{\\f0\\fnil\\fcharset0 Tahoma;}}\r\n" +
    "\\viewkind4\\uc1\\pard\\f0\\fs20\\par\r\n}\r\n";
            this.rtbHCuerpoCodigo.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbHCuerpoCodigo.Size = new System.Drawing.Size(625, 275);
            this.rtbHCuerpoCodigo.TabIndex = 10;
            this.rtbHCuerpoCodigo.TabStop = false;
            // 
            // lineHDivisor3
            // 
            this.lineHDivisor3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lineHDivisor3.ForeColor = System.Drawing.Color.Black;
            this.lineHDivisor3.Location = new System.Drawing.Point(6, 31);
            this.lineHDivisor3.Name = "lineHDivisor3";
            this.lineHDivisor3.Size = new System.Drawing.Size(625, 16);
            this.lineHDivisor3.TabIndex = 9;
            this.lineHDivisor3.Text = "line8";
            // 
            // lblHCodigoTitulo
            // 
            this.lblHCodigoTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHCodigoTitulo.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblHCodigoTitulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblHCodigoTitulo.Font = new System.Drawing.Font("Tahoma", 14F);
            this.lblHCodigoTitulo.ForeColor = System.Drawing.Color.Black;
            this.lblHCodigoTitulo.Location = new System.Drawing.Point(6, 8);
            this.lblHCodigoTitulo.Name = "lblHCodigoTitulo";
            this.lblHCodigoTitulo.SingleLineColor = System.Drawing.Color.Black;
            this.lblHCodigoTitulo.Size = new System.Drawing.Size(625, 25);
            this.lblHCodigoTitulo.TabIndex = 7;
            this.lblHCodigoTitulo.Text = "Ingrese código de error a buscar";
            this.lblHCodigoTitulo.WordWrap = true;
            // 
            // lblHBuscarCodigo
            // 
            this.lblHBuscarCodigo.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblHBuscarCodigo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblHBuscarCodigo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblHBuscarCodigo.ForeColor = System.Drawing.Color.Black;
            this.lblHBuscarCodigo.Location = new System.Drawing.Point(9, 199);
            this.lblHBuscarCodigo.Name = "lblHBuscarCodigo";
            this.lblHBuscarCodigo.SingleLineColor = System.Drawing.Color.Black;
            this.lblHBuscarCodigo.Size = new System.Drawing.Size(127, 28);
            this.lblHBuscarCodigo.TabIndex = 21;
            this.lblHBuscarCodigo.Text = "Buscar código:";
            this.lblHBuscarCodigo.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblHBuscarCodigo.WordWrap = true;
            // 
            // lblHAcercaDe
            // 
            this.lblHAcercaDe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblHAcercaDe.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblHAcercaDe.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblHAcercaDe.ForeColor = System.Drawing.Color.Black;
            this.lblHAcercaDe.Location = new System.Drawing.Point(234, 54);
            this.lblHAcercaDe.Name = "lblHAcercaDe";
            this.lblHAcercaDe.SingleLineColor = System.Drawing.Color.Black;
            this.lblHAcercaDe.Size = new System.Drawing.Size(409, 20);
            this.lblHAcercaDe.TabIndex = 11;
            this.lblHAcercaDe.Text = "Acerca de...";
            this.lblHAcercaDe.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblHAcercaDe.WordWrap = true;
            // 
            // txtHBuscarError
            // 
            this.txtHBuscarError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHBuscarError.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtHBuscarError.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtHBuscarError.ForeColor = System.Drawing.Color.Black;
            this.txtHBuscarError.HintForeColor = System.Drawing.Color.Empty;
            this.txtHBuscarError.HintText = "";
            this.txtHBuscarError.isPassword = false;
            this.txtHBuscarError.LineFocusedColor = System.Drawing.Color.Green;
            this.txtHBuscarError.LineIdleColor = System.Drawing.Color.Gray;
            this.txtHBuscarError.LineMouseHoverColor = System.Drawing.Color.Red;
            this.txtHBuscarError.LineThickness = 3;
            this.txtHBuscarError.Location = new System.Drawing.Point(143, 196);
            this.txtHBuscarError.Margin = new System.Windows.Forms.Padding(4);
            this.txtHBuscarError.Name = "txtHBuscarError";
            this.txtHBuscarError.Size = new System.Drawing.Size(448, 28);
            this.txtHBuscarError.TabIndex = 7;
            this.txtHBuscarError.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtHBuscarError.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHBuscarError_KeyPress);
            // 
            // lblCopyright
            // 
            this.lblCopyright.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblCopyright.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCopyright.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopyright.ForeColor = System.Drawing.Color.Black;
            this.lblCopyright.Location = new System.Drawing.Point(234, 80);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.SingleLineColor = System.Drawing.Color.Black;
            this.lblCopyright.Size = new System.Drawing.Size(409, 100);
            this.lblCopyright.TabIndex = 10;
            this.lblCopyright.Text = "<p align=\"center\">SyncBD<br/>\r\n<b>v0.20</b><br/>\r\nSoluciones Digitales y de Proce" +
    "sos S.A. de C.V.<br/>\r\nTodos los derechos reservados © 2018</p>";
            this.lblCopyright.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblCopyright.WordWrap = true;
            // 
            // lineHDivisor2
            // 
            this.lineHDivisor2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lineHDivisor2.ForeColor = System.Drawing.Color.Black;
            this.lineHDivisor2.Location = new System.Drawing.Point(6, 169);
            this.lineHDivisor2.Name = "lineHDivisor2";
            this.lineHDivisor2.Size = new System.Drawing.Size(637, 23);
            this.lineHDivisor2.TabIndex = 8;
            this.lineHDivisor2.Text = "line7";
            // 
            // lblHDivisor
            // 
            this.lblHDivisor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHDivisor.ForeColor = System.Drawing.Color.Black;
            this.lblHDivisor.Location = new System.Drawing.Point(6, 39);
            this.lblHDivisor.Name = "lblHDivisor";
            this.lblHDivisor.Size = new System.Drawing.Size(637, 23);
            this.lblHDivisor.TabIndex = 7;
            this.lblHDivisor.Text = "line2";
            // 
            // lblHEncabezado
            // 
            this.lblHEncabezado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHEncabezado.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblHEncabezado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblHEncabezado.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblHEncabezado.ForeColor = System.Drawing.Color.Black;
            this.lblHEncabezado.Location = new System.Drawing.Point(6, 6);
            this.lblHEncabezado.Name = "lblHEncabezado";
            this.lblHEncabezado.SingleLineColor = System.Drawing.Color.Black;
            this.lblHEncabezado.Size = new System.Drawing.Size(637, 39);
            this.lblHEncabezado.TabIndex = 6;
            this.lblHEncabezado.Text = "Centro de ayuda";
            this.lblHEncabezado.WordWrap = true;
            // 
            // pnlLocal
            // 
            this.pnlLocal.Controls.Add(this.lblSLArchivoActual);
            this.pnlLocal.Controls.Add(this.lblSLPorcentajeIndividual);
            this.pnlLocal.Controls.Add(this.pbrSLProgresoIndividual);
            this.pnlLocal.Controls.Add(this.pbxSLTiempo);
            this.pnlLocal.Controls.Add(this.pbxSLHistorial);
            this.pnlLocal.Controls.Add(this.pbxSLVelocidad);
            this.pnlLocal.Controls.Add(this.pbxSLIcono);
            this.pnlLocal.Controls.Add(this.lblSLVelocidad);
            this.pnlLocal.Controls.Add(this.dgvSLHistorial);
            this.pnlLocal.Controls.Add(this.lblSLTiempoTranscurrido);
            this.pnlLocal.Controls.Add(this.lblSLEstado);
            this.pnlLocal.Controls.Add(this.lblSLPorcentaje);
            this.pnlLocal.Controls.Add(this.btnSLResincronizar);
            this.pnlLocal.Controls.Add(this.pbrSLProgreso);
            this.pnlLocal.Controls.Add(this.lblSLEncabezado2);
            this.pnlLocal.Controls.Add(this.btnSLAbrirCarpeta);
            this.pnlLocal.Controls.Add(this.lineSLDivisor2);
            this.pnlLocal.Controls.Add(this.lblSLEncabezado1);
            this.pnlLocal.Controls.Add(this.lineSLDivisor);
            this.pnlLocal.Controls.Add(this.lblSLEncabezado);
            this.pnlLocal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLocal.Location = new System.Drawing.Point(0, 0);
            this.pnlLocal.Name = "pnlLocal";
            this.pnlLocal.Size = new System.Drawing.Size(655, 570);
            this.pnlLocal.TabIndex = 3;
            this.pnlLocal.Visible = false;
            // 
            // lblSLArchivoActual
            // 
            this.lblSLArchivoActual.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSLArchivoActual.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSLArchivoActual.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSLArchivoActual.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblSLArchivoActual.ForeColor = System.Drawing.Color.Black;
            this.lblSLArchivoActual.Location = new System.Drawing.Point(6, 454);
            this.lblSLArchivoActual.Name = "lblSLArchivoActual";
            this.lblSLArchivoActual.SingleLineColor = System.Drawing.Color.Black;
            this.lblSLArchivoActual.Size = new System.Drawing.Size(254, 20);
            this.lblSLArchivoActual.TabIndex = 42;
            this.lblSLArchivoActual.Visible = false;
            this.lblSLArchivoActual.WordWrap = true;
            // 
            // lblSLPorcentajeIndividual
            // 
            this.lblSLPorcentajeIndividual.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSLPorcentajeIndividual.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSLPorcentajeIndividual.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSLPorcentajeIndividual.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblSLPorcentajeIndividual.ForeColor = System.Drawing.Color.Black;
            this.lblSLPorcentajeIndividual.Location = new System.Drawing.Point(286, 454);
            this.lblSLPorcentajeIndividual.Name = "lblSLPorcentajeIndividual";
            this.lblSLPorcentajeIndividual.SingleLineColor = System.Drawing.Color.Black;
            this.lblSLPorcentajeIndividual.Size = new System.Drawing.Size(198, 20);
            this.lblSLPorcentajeIndividual.TabIndex = 41;
            this.lblSLPorcentajeIndividual.Text = "0% completado";
            this.lblSLPorcentajeIndividual.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblSLPorcentajeIndividual.Visible = false;
            this.lblSLPorcentajeIndividual.WordWrap = true;
            // 
            // pbrSLProgresoIndividual
            // 
            this.pbrSLProgresoIndividual.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbrSLProgresoIndividual.BackColor = System.Drawing.Color.Silver;
            this.pbrSLProgresoIndividual.BorderRadius = 5;
            this.pbrSLProgresoIndividual.Location = new System.Drawing.Point(6, 479);
            this.pbrSLProgresoIndividual.MaximumValue = 100;
            this.pbrSLProgresoIndividual.Name = "pbrSLProgresoIndividual";
            this.pbrSLProgresoIndividual.ProgressColor = System.Drawing.Color.Green;
            this.pbrSLProgresoIndividual.Size = new System.Drawing.Size(478, 24);
            this.pbrSLProgresoIndividual.TabIndex = 40;
            this.pbrSLProgresoIndividual.Value = 0;
            this.pbrSLProgresoIndividual.Visible = false;
            // 
            // pbxSLTiempo
            // 
            this.pbxSLTiempo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbxSLTiempo.BackColor = System.Drawing.Color.Transparent;
            this.pbxSLTiempo.Image = global::SyncBD.Properties.Resources.Clock_24px;
            this.pbxSLTiempo.Location = new System.Drawing.Point(224, 360);
            this.pbxSLTiempo.Name = "pbxSLTiempo";
            this.pbxSLTiempo.Size = new System.Drawing.Size(26, 26);
            this.pbxSLTiempo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxSLTiempo.TabIndex = 39;
            this.pbxSLTiempo.TabStop = false;
            this.pbxSLTiempo.Visible = false;
            // 
            // pbxSLHistorial
            // 
            this.pbxSLHistorial.BackColor = System.Drawing.Color.Transparent;
            this.pbxSLHistorial.Image = global::SyncBD.Properties.Resources.Historical_24px;
            this.pbxSLHistorial.Location = new System.Drawing.Point(6, 54);
            this.pbxSLHistorial.Name = "pbxSLHistorial";
            this.pbxSLHistorial.Size = new System.Drawing.Size(26, 26);
            this.pbxSLHistorial.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxSLHistorial.TabIndex = 34;
            this.pbxSLHistorial.TabStop = false;
            // 
            // pbxSLVelocidad
            // 
            this.pbxSLVelocidad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pbxSLVelocidad.BackColor = System.Drawing.Color.Transparent;
            this.pbxSLVelocidad.Image = global::SyncBD.Properties.Resources.Speed_24px;
            this.pbxSLVelocidad.Location = new System.Drawing.Point(6, 506);
            this.pbxSLVelocidad.Name = "pbxSLVelocidad";
            this.pbxSLVelocidad.Size = new System.Drawing.Size(26, 26);
            this.pbxSLVelocidad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxSLVelocidad.TabIndex = 33;
            this.pbxSLVelocidad.TabStop = false;
            this.pbxSLVelocidad.Visible = false;
            // 
            // pbxSLIcono
            // 
            this.pbxSLIcono.BackColor = System.Drawing.Color.Transparent;
            this.pbxSLIcono.Image = global::SyncBD.Properties.Resources.UploadToFTP_24px;
            this.pbxSLIcono.Location = new System.Drawing.Point(6, 319);
            this.pbxSLIcono.Name = "pbxSLIcono";
            this.pbxSLIcono.Size = new System.Drawing.Size(26, 26);
            this.pbxSLIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxSLIcono.TabIndex = 32;
            this.pbxSLIcono.TabStop = false;
            // 
            // lblSLVelocidad
            // 
            this.lblSLVelocidad.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSLVelocidad.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSLVelocidad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSLVelocidad.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblSLVelocidad.ForeColor = System.Drawing.Color.Black;
            this.lblSLVelocidad.Location = new System.Drawing.Point(38, 511);
            this.lblSLVelocidad.Name = "lblSLVelocidad";
            this.lblSLVelocidad.SingleLineColor = System.Drawing.Color.Black;
            this.lblSLVelocidad.Size = new System.Drawing.Size(446, 18);
            this.lblSLVelocidad.TabIndex = 31;
            this.lblSLVelocidad.Text = "Velocidad actual:";
            this.lblSLVelocidad.Visible = false;
            // 
            // dgvSLHistorial
            // 
            this.dgvSLHistorial.AllowUserToAddRows = false;
            this.dgvSLHistorial.AllowUserToDeleteRows = false;
            this.dgvSLHistorial.AllowUserToOrderColumns = true;
            this.dgvSLHistorial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSLHistorial.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSLHistorial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSLHistorial.Location = new System.Drawing.Point(6, 86);
            this.dgvSLHistorial.Name = "dgvSLHistorial";
            this.dgvSLHistorial.ReadOnly = true;
            this.dgvSLHistorial.Size = new System.Drawing.Size(637, 215);
            this.dgvSLHistorial.TabIndex = 25;
            this.dgvSLHistorial.TabStop = false;
            this.ttpMensaje.SetToolTip(this.dgvSLHistorial, "Últimos 8 movimientos realizados hacia el servidor local");
            // 
            // lblSLTiempoTranscurrido
            // 
            this.lblSLTiempoTranscurrido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSLTiempoTranscurrido.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSLTiempoTranscurrido.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSLTiempoTranscurrido.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblSLTiempoTranscurrido.ForeColor = System.Drawing.Color.Black;
            this.lblSLTiempoTranscurrido.Location = new System.Drawing.Point(256, 365);
            this.lblSLTiempoTranscurrido.Name = "lblSLTiempoTranscurrido";
            this.lblSLTiempoTranscurrido.SingleLineColor = System.Drawing.Color.Black;
            this.lblSLTiempoTranscurrido.Size = new System.Drawing.Size(228, 20);
            this.lblSLTiempoTranscurrido.TabIndex = 24;
            this.lblSLTiempoTranscurrido.WordWrap = true;
            // 
            // lblSLEstado
            // 
            this.lblSLEstado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSLEstado.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSLEstado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSLEstado.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblSLEstado.ForeColor = System.Drawing.Color.Black;
            this.lblSLEstado.Location = new System.Drawing.Point(6, 420);
            this.lblSLEstado.Name = "lblSLEstado";
            this.lblSLEstado.SingleLineColor = System.Drawing.Color.Black;
            this.lblSLEstado.Size = new System.Drawing.Size(478, 24);
            this.lblSLEstado.TabIndex = 23;
            this.lblSLEstado.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblSLEstado.WordWrap = true;
            // 
            // lblSLPorcentaje
            // 
            this.lblSLPorcentaje.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSLPorcentaje.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSLPorcentaje.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblSLPorcentaje.ForeColor = System.Drawing.Color.Black;
            this.lblSLPorcentaje.Location = new System.Drawing.Point(6, 365);
            this.lblSLPorcentaje.Name = "lblSLPorcentaje";
            this.lblSLPorcentaje.SingleLineColor = System.Drawing.Color.Black;
            this.lblSLPorcentaje.Size = new System.Drawing.Size(212, 20);
            this.lblSLPorcentaje.TabIndex = 22;
            this.lblSLPorcentaje.Text = "0% completado";
            this.lblSLPorcentaje.WordWrap = true;
            // 
            // btnSLResincronizar
            // 
            this.btnSLResincronizar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnSLResincronizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSLResincronizar.BackColor = System.Drawing.Color.Gray;
            this.btnSLResincronizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSLResincronizar.BorderRadius = 0;
            this.btnSLResincronizar.ButtonText = "Resincronizar";
            this.btnSLResincronizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSLResincronizar.DisabledColor = System.Drawing.Color.Gray;
            this.btnSLResincronizar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnSLResincronizar.Iconimage = global::SyncBD.Properties.Resources.Synchronize_16px;
            this.btnSLResincronizar.Iconimage_right = null;
            this.btnSLResincronizar.Iconimage_right_Selected = null;
            this.btnSLResincronizar.Iconimage_Selected = null;
            this.btnSLResincronizar.IconMarginLeft = 0;
            this.btnSLResincronizar.IconMarginRight = 0;
            this.btnSLResincronizar.IconRightVisible = true;
            this.btnSLResincronizar.IconRightZoom = 0D;
            this.btnSLResincronizar.IconVisible = true;
            this.btnSLResincronizar.IconZoom = 40D;
            this.btnSLResincronizar.IsTab = false;
            this.btnSLResincronizar.Location = new System.Drawing.Point(490, 391);
            this.btnSLResincronizar.Name = "btnSLResincronizar";
            this.btnSLResincronizar.Normalcolor = System.Drawing.Color.Gray;
            this.btnSLResincronizar.OnHovercolor = System.Drawing.Color.Green;
            this.btnSLResincronizar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnSLResincronizar.selected = false;
            this.btnSLResincronizar.Size = new System.Drawing.Size(153, 43);
            this.btnSLResincronizar.TabIndex = 9;
            this.btnSLResincronizar.Text = "Resincronizar";
            this.btnSLResincronizar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSLResincronizar.Textcolor = System.Drawing.Color.White;
            this.btnSLResincronizar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ttpMensaje.SetToolTip(this.btnSLResincronizar, "Vuelva a realizar el proceso de sincronización");
            this.btnSLResincronizar.Click += new System.EventHandler(this.btnSLResincronizar_Click);
            // 
            // pbrSLProgreso
            // 
            this.pbrSLProgreso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbrSLProgreso.BackColor = System.Drawing.Color.Silver;
            this.pbrSLProgreso.BorderRadius = 5;
            this.pbrSLProgreso.Location = new System.Drawing.Point(6, 391);
            this.pbrSLProgreso.MaximumValue = 100;
            this.pbrSLProgreso.Name = "pbrSLProgreso";
            this.pbrSLProgreso.ProgressColor = System.Drawing.Color.Green;
            this.pbrSLProgreso.Size = new System.Drawing.Size(478, 24);
            this.pbrSLProgreso.TabIndex = 19;
            this.pbrSLProgreso.Value = 0;
            // 
            // lblSLEncabezado2
            // 
            this.lblSLEncabezado2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSLEncabezado2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSLEncabezado2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSLEncabezado2.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblSLEncabezado2.ForeColor = System.Drawing.Color.Black;
            this.lblSLEncabezado2.Location = new System.Drawing.Point(38, 325);
            this.lblSLEncabezado2.Name = "lblSLEncabezado2";
            this.lblSLEncabezado2.SingleLineColor = System.Drawing.Color.Black;
            this.lblSLEncabezado2.Size = new System.Drawing.Size(605, 20);
            this.lblSLEncabezado2.TabIndex = 18;
            this.lblSLEncabezado2.Text = "Carga actual";
            this.lblSLEncabezado2.WordWrap = true;
            // 
            // btnSLAbrirCarpeta
            // 
            this.btnSLAbrirCarpeta.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnSLAbrirCarpeta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSLAbrirCarpeta.BackColor = System.Drawing.Color.Gray;
            this.btnSLAbrirCarpeta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSLAbrirCarpeta.BorderRadius = 0;
            this.btnSLAbrirCarpeta.ButtonText = "Subir desde...";
            this.btnSLAbrirCarpeta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSLAbrirCarpeta.DisabledColor = System.Drawing.Color.Gray;
            this.btnSLAbrirCarpeta.Iconcolor = System.Drawing.Color.Transparent;
            this.btnSLAbrirCarpeta.Iconimage = global::SyncBD.Properties.Resources.OpenFolder_16px;
            this.btnSLAbrirCarpeta.Iconimage_right = null;
            this.btnSLAbrirCarpeta.Iconimage_right_Selected = null;
            this.btnSLAbrirCarpeta.Iconimage_Selected = null;
            this.btnSLAbrirCarpeta.IconMarginLeft = 0;
            this.btnSLAbrirCarpeta.IconMarginRight = 0;
            this.btnSLAbrirCarpeta.IconRightVisible = true;
            this.btnSLAbrirCarpeta.IconRightZoom = 0D;
            this.btnSLAbrirCarpeta.IconVisible = true;
            this.btnSLAbrirCarpeta.IconZoom = 40D;
            this.btnSLAbrirCarpeta.IsTab = false;
            this.btnSLAbrirCarpeta.Location = new System.Drawing.Point(490, 459);
            this.btnSLAbrirCarpeta.Name = "btnSLAbrirCarpeta";
            this.btnSLAbrirCarpeta.Normalcolor = System.Drawing.Color.Gray;
            this.btnSLAbrirCarpeta.OnHovercolor = System.Drawing.Color.DarkCyan;
            this.btnSLAbrirCarpeta.OnHoverTextColor = System.Drawing.Color.White;
            this.btnSLAbrirCarpeta.selected = false;
            this.btnSLAbrirCarpeta.Size = new System.Drawing.Size(153, 43);
            this.btnSLAbrirCarpeta.TabIndex = 10;
            this.btnSLAbrirCarpeta.Text = "Subir desde...";
            this.btnSLAbrirCarpeta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSLAbrirCarpeta.Textcolor = System.Drawing.Color.White;
            this.btnSLAbrirCarpeta.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ttpMensaje.SetToolTip(this.btnSLAbrirCarpeta, "Cargue archivos desde un directorio o medio extraíble");
            this.btnSLAbrirCarpeta.Click += new System.EventHandler(this.btnSLAbrirCarpeta_Click);
            // 
            // lineSLDivisor2
            // 
            this.lineSLDivisor2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lineSLDivisor2.ForeColor = System.Drawing.Color.Black;
            this.lineSLDivisor2.Location = new System.Drawing.Point(6, 301);
            this.lineSLDivisor2.Name = "lineSLDivisor2";
            this.lineSLDivisor2.Size = new System.Drawing.Size(637, 23);
            this.lineSLDivisor2.TabIndex = 10;
            this.lineSLDivisor2.Text = "line5";
            // 
            // lblSLEncabezado1
            // 
            this.lblSLEncabezado1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSLEncabezado1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSLEncabezado1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSLEncabezado1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblSLEncabezado1.ForeColor = System.Drawing.Color.Black;
            this.lblSLEncabezado1.Location = new System.Drawing.Point(38, 59);
            this.lblSLEncabezado1.Name = "lblSLEncabezado1";
            this.lblSLEncabezado1.SingleLineColor = System.Drawing.Color.Black;
            this.lblSLEncabezado1.Size = new System.Drawing.Size(605, 20);
            this.lblSLEncabezado1.TabIndex = 9;
            this.lblSLEncabezado1.Text = "Historial de cargas";
            this.lblSLEncabezado1.WordWrap = true;
            // 
            // lineSLDivisor
            // 
            this.lineSLDivisor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lineSLDivisor.ForeColor = System.Drawing.Color.Black;
            this.lineSLDivisor.Location = new System.Drawing.Point(6, 39);
            this.lineSLDivisor.Name = "lineSLDivisor";
            this.lineSLDivisor.Size = new System.Drawing.Size(637, 23);
            this.lineSLDivisor.TabIndex = 7;
            this.lineSLDivisor.Text = "line6";
            // 
            // lblSLEncabezado
            // 
            this.lblSLEncabezado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSLEncabezado.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSLEncabezado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSLEncabezado.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblSLEncabezado.ForeColor = System.Drawing.Color.Black;
            this.lblSLEncabezado.Location = new System.Drawing.Point(6, 6);
            this.lblSLEncabezado.Name = "lblSLEncabezado";
            this.lblSLEncabezado.SingleLineColor = System.Drawing.Color.Black;
            this.lblSLEncabezado.Size = new System.Drawing.Size(637, 39);
            this.lblSLEncabezado.TabIndex = 6;
            this.lblSLEncabezado.Text = "Opciones del servidor local";
            this.lblSLEncabezado.WordWrap = true;
            // 
            // pnlAjustes
            // 
            this.pnlAjustes.Controls.Add(this.btnAJPersonalizarTema);
            this.pnlAjustes.Controls.Add(this.lblAJRutaGuardado);
            this.pnlAjustes.Controls.Add(this.lblAJTituloGuardado);
            this.pnlAjustes.Controls.Add(this.btnPEscogerCarpeta);
            this.pnlAjustes.Controls.Add(this.cbxAJIdioma);
            this.pnlAjustes.Controls.Add(this.cbxAJApariencia);
            this.pnlAjustes.Controls.Add(this.cbxAJGuardarEn);
            this.pnlAjustes.Controls.Add(this.lblAJRutaPermanente);
            this.pnlAjustes.Controls.Add(this.lblAJIniciarConWindows);
            this.pnlAjustes.Controls.Add(this.lblAJConfigurarServidores);
            this.pnlAjustes.Controls.Add(this.lblAJCambiarApariencia);
            this.pnlAjustes.Controls.Add(this.lblAJReestablecer);
            this.pnlAjustes.Controls.Add(this.lblAJBorrarHistorial);
            this.pnlAjustes.Controls.Add(this.lblAJIdioma);
            this.pnlAjustes.Controls.Add(this.lblAJGuardarEn);
            this.pnlAjustes.Controls.Add(this.lblAJMinimizarIniciar);
            this.pnlAjustes.Controls.Add(this.lblAJDistinguirSincronizacion);
            this.pnlAjustes.Controls.Add(this.chkAJIniciarConWindows);
            this.pnlAjustes.Controls.Add(this.btnAJReestablecer);
            this.pnlAjustes.Controls.Add(this.chkAJMinimizarIniciar);
            this.pnlAjustes.Controls.Add(this.btnAJBorrarHistorial);
            this.pnlAjustes.Controls.Add(this.btnAJLocal);
            this.pnlAjustes.Controls.Add(this.btnAJRemoto);
            this.pnlAjustes.Controls.Add(this.chkAJRutaPermanente);
            this.pnlAjustes.Controls.Add(this.chkAJDistinguirSincronizacion);
            this.pnlAjustes.Controls.Add(this.lineAJDivisor);
            this.pnlAjustes.Controls.Add(this.lblAJEncabezado);
            this.pnlAjustes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAjustes.Location = new System.Drawing.Point(0, 0);
            this.pnlAjustes.Name = "pnlAjustes";
            this.pnlAjustes.Size = new System.Drawing.Size(655, 570);
            this.pnlAjustes.TabIndex = 0;
            this.pnlAjustes.Visible = false;
            // 
            // btnAJPersonalizarTema
            // 
            this.btnAJPersonalizarTema.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAJPersonalizarTema.BackColor = System.Drawing.Color.Gray;
            this.btnAJPersonalizarTema.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAJPersonalizarTema.BorderRadius = 0;
            this.btnAJPersonalizarTema.ButtonText = "Personalizar tema";
            this.btnAJPersonalizarTema.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAJPersonalizarTema.DisabledColor = System.Drawing.Color.Gray;
            this.btnAJPersonalizarTema.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAJPersonalizarTema.Iconimage = global::SyncBD.Properties.Resources.RGBCircle1_16px;
            this.btnAJPersonalizarTema.Iconimage_right = null;
            this.btnAJPersonalizarTema.Iconimage_right_Selected = null;
            this.btnAJPersonalizarTema.Iconimage_Selected = null;
            this.btnAJPersonalizarTema.IconMarginLeft = 0;
            this.btnAJPersonalizarTema.IconMarginRight = 0;
            this.btnAJPersonalizarTema.IconRightVisible = true;
            this.btnAJPersonalizarTema.IconRightZoom = 0D;
            this.btnAJPersonalizarTema.IconVisible = true;
            this.btnAJPersonalizarTema.IconZoom = 45D;
            this.btnAJPersonalizarTema.IsTab = false;
            this.btnAJPersonalizarTema.Location = new System.Drawing.Point(321, 307);
            this.btnAJPersonalizarTema.Name = "btnAJPersonalizarTema";
            this.btnAJPersonalizarTema.Normalcolor = System.Drawing.Color.Gray;
            this.btnAJPersonalizarTema.OnHovercolor = System.Drawing.Color.DarkCyan;
            this.btnAJPersonalizarTema.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAJPersonalizarTema.selected = false;
            this.btnAJPersonalizarTema.Size = new System.Drawing.Size(240, 43);
            this.btnAJPersonalizarTema.TabIndex = 17;
            this.btnAJPersonalizarTema.Text = "Personalizar tema";
            this.btnAJPersonalizarTema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAJPersonalizarTema.Textcolor = System.Drawing.Color.White;
            this.btnAJPersonalizarTema.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAJPersonalizarTema.Click += new System.EventHandler(this.btnAJPersonalizarTema_Click);
            // 
            // lblAJRutaGuardado
            // 
            // 
            // 
            // 
            this.lblAJRutaGuardado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAJRutaGuardado.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAJRutaGuardado.ForeColor = System.Drawing.Color.Black;
            this.lblAJRutaGuardado.Location = new System.Drawing.Point(321, 163);
            this.lblAJRutaGuardado.Name = "lblAJRutaGuardado";
            this.lblAJRutaGuardado.Size = new System.Drawing.Size(319, 26);
            this.lblAJRutaGuardado.TabIndex = 77;
            this.lblAJRutaGuardado.WordWrap = true;
            // 
            // lblAJTituloGuardado
            // 
            // 
            // 
            // 
            this.lblAJTituloGuardado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAJTituloGuardado.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAJTituloGuardado.ForeColor = System.Drawing.Color.Black;
            this.lblAJTituloGuardado.Location = new System.Drawing.Point(321, 134);
            this.lblAJTituloGuardado.Name = "lblAJTituloGuardado";
            this.lblAJTituloGuardado.Size = new System.Drawing.Size(240, 26);
            this.lblAJTituloGuardado.TabIndex = 76;
            this.lblAJTituloGuardado.Text = "Actualmente guardando en:";
            // 
            // btnPEscogerCarpeta
            // 
            this.btnPEscogerCarpeta.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnPEscogerCarpeta.BackColor = System.Drawing.Color.Gray;
            this.btnPEscogerCarpeta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPEscogerCarpeta.BorderRadius = 0;
            this.btnPEscogerCarpeta.ButtonText = "Seleccionar...";
            this.btnPEscogerCarpeta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPEscogerCarpeta.DisabledColor = System.Drawing.Color.Gray;
            this.btnPEscogerCarpeta.Iconcolor = System.Drawing.Color.Transparent;
            this.btnPEscogerCarpeta.Iconimage = global::SyncBD.Properties.Resources.OpenFolder_24px;
            this.btnPEscogerCarpeta.Iconimage_right = null;
            this.btnPEscogerCarpeta.Iconimage_right_Selected = null;
            this.btnPEscogerCarpeta.Iconimage_Selected = null;
            this.btnPEscogerCarpeta.IconMarginLeft = 0;
            this.btnPEscogerCarpeta.IconMarginRight = 0;
            this.btnPEscogerCarpeta.IconRightVisible = true;
            this.btnPEscogerCarpeta.IconRightZoom = 0D;
            this.btnPEscogerCarpeta.IconVisible = true;
            this.btnPEscogerCarpeta.IconZoom = 45D;
            this.btnPEscogerCarpeta.IsTab = false;
            this.btnPEscogerCarpeta.Location = new System.Drawing.Point(20, 159);
            this.btnPEscogerCarpeta.Name = "btnPEscogerCarpeta";
            this.btnPEscogerCarpeta.Normalcolor = System.Drawing.Color.Gray;
            this.btnPEscogerCarpeta.OnHovercolor = System.Drawing.Color.DarkCyan;
            this.btnPEscogerCarpeta.OnHoverTextColor = System.Drawing.Color.White;
            this.btnPEscogerCarpeta.selected = false;
            this.btnPEscogerCarpeta.Size = new System.Drawing.Size(240, 43);
            this.btnPEscogerCarpeta.TabIndex = 13;
            this.btnPEscogerCarpeta.Text = "Seleccionar...";
            this.btnPEscogerCarpeta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnPEscogerCarpeta.Textcolor = System.Drawing.Color.White;
            this.btnPEscogerCarpeta.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPEscogerCarpeta.Click += new System.EventHandler(this.btnPEscogerCarpeta_Click);
            // 
            // cbxAJIdioma
            // 
            this.cbxAJIdioma.BackColor = System.Drawing.Color.SeaGreen;
            this.cbxAJIdioma.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxAJIdioma.DropDownHeight = 120;
            this.cbxAJIdioma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAJIdioma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxAJIdioma.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbxAJIdioma.ForeColor = System.Drawing.Color.White;
            this.cbxAJIdioma.FormattingEnabled = true;
            this.cbxAJIdioma.IntegralHeight = false;
            this.cbxAJIdioma.Location = new System.Drawing.Point(351, 449);
            this.cbxAJIdioma.Name = "cbxAJIdioma";
            this.cbxAJIdioma.Size = new System.Drawing.Size(240, 27);
            this.cbxAJIdioma.TabIndex = 74;
            this.cbxAJIdioma.Visible = false;
            this.cbxAJIdioma.SelectedIndexChanged += new System.EventHandler(this.cbxAJIdioma_SelectedIndexChanged);
            // 
            // cbxAJApariencia
            // 
            this.cbxAJApariencia.BackColor = System.Drawing.Color.SeaGreen;
            this.cbxAJApariencia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxAJApariencia.DropDownHeight = 120;
            this.cbxAJApariencia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAJApariencia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxAJApariencia.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbxAJApariencia.ForeColor = System.Drawing.Color.White;
            this.cbxAJApariencia.FormattingEnabled = true;
            this.cbxAJApariencia.IntegralHeight = false;
            this.cbxAJApariencia.Location = new System.Drawing.Point(20, 321);
            this.cbxAJApariencia.Name = "cbxAJApariencia";
            this.cbxAJApariencia.Size = new System.Drawing.Size(240, 27);
            this.cbxAJApariencia.TabIndex = 16;
            this.cbxAJApariencia.SelectedIndexChanged += new System.EventHandler(this.cbxAJApariencia_SelectedIndexChanged);
            // 
            // cbxAJGuardarEn
            // 
            this.cbxAJGuardarEn.BackColor = System.Drawing.Color.SeaGreen;
            this.cbxAJGuardarEn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxAJGuardarEn.DropDownHeight = 120;
            this.cbxAJGuardarEn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAJGuardarEn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxAJGuardarEn.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbxAJGuardarEn.ForeColor = System.Drawing.Color.White;
            this.cbxAJGuardarEn.FormattingEnabled = true;
            this.cbxAJGuardarEn.IntegralHeight = false;
            this.cbxAJGuardarEn.Location = new System.Drawing.Point(351, 488);
            this.cbxAJGuardarEn.Name = "cbxAJGuardarEn";
            this.cbxAJGuardarEn.Size = new System.Drawing.Size(240, 27);
            this.cbxAJGuardarEn.TabIndex = 72;
            this.cbxAJGuardarEn.Visible = false;
            this.cbxAJGuardarEn.SelectedIndexChanged += new System.EventHandler(this.cbxAJGuardarEn_SelectedIndexChanged);
            // 
            // lblAJRutaPermanente
            // 
            // 
            // 
            // 
            this.lblAJRutaPermanente.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAJRutaPermanente.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAJRutaPermanente.ForeColor = System.Drawing.Color.Black;
            this.lblAJRutaPermanente.Location = new System.Drawing.Point(321, 79);
            this.lblAJRutaPermanente.Name = "lblAJRutaPermanente";
            this.lblAJRutaPermanente.Size = new System.Drawing.Size(240, 26);
            this.lblAJRutaPermanente.TabIndex = 71;
            this.lblAJRutaPermanente.Text = "Hacer permanente la ruta de guardado:";
            this.lblAJRutaPermanente.Visible = false;
            // 
            // lblAJIniciarConWindows
            // 
            // 
            // 
            // 
            this.lblAJIniciarConWindows.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAJIniciarConWindows.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAJIniciarConWindows.ForeColor = System.Drawing.Color.Black;
            this.lblAJIniciarConWindows.Location = new System.Drawing.Point(20, 79);
            this.lblAJIniciarConWindows.Name = "lblAJIniciarConWindows";
            this.lblAJIniciarConWindows.Size = new System.Drawing.Size(240, 26);
            this.lblAJIniciarConWindows.TabIndex = 70;
            this.lblAJIniciarConWindows.Text = "Iniciar junto con Windows:";
            // 
            // lblAJConfigurarServidores
            // 
            // 
            // 
            // 
            this.lblAJConfigurarServidores.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAJConfigurarServidores.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAJConfigurarServidores.ForeColor = System.Drawing.Color.Black;
            this.lblAJConfigurarServidores.Location = new System.Drawing.Point(20, 208);
            this.lblAJConfigurarServidores.Name = "lblAJConfigurarServidores";
            this.lblAJConfigurarServidores.Size = new System.Drawing.Size(240, 26);
            this.lblAJConfigurarServidores.TabIndex = 69;
            this.lblAJConfigurarServidores.Text = "Configurar conexión a servidor:";
            // 
            // lblAJCambiarApariencia
            // 
            // 
            // 
            // 
            this.lblAJCambiarApariencia.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAJCambiarApariencia.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAJCambiarApariencia.ForeColor = System.Drawing.Color.Black;
            this.lblAJCambiarApariencia.Location = new System.Drawing.Point(20, 293);
            this.lblAJCambiarApariencia.Name = "lblAJCambiarApariencia";
            this.lblAJCambiarApariencia.Size = new System.Drawing.Size(240, 26);
            this.lblAJCambiarApariencia.TabIndex = 68;
            this.lblAJCambiarApariencia.Text = "Cambiar apariencia de SyncBD a:";
            // 
            // lblAJReestablecer
            // 
            // 
            // 
            // 
            this.lblAJReestablecer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAJReestablecer.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAJReestablecer.ForeColor = System.Drawing.Color.Black;
            this.lblAJReestablecer.Location = new System.Drawing.Point(20, 438);
            this.lblAJReestablecer.Name = "lblAJReestablecer";
            this.lblAJReestablecer.Size = new System.Drawing.Size(240, 26);
            this.lblAJReestablecer.TabIndex = 67;
            this.lblAJReestablecer.Text = "Reestablecer los ajustes:";
            // 
            // lblAJBorrarHistorial
            // 
            // 
            // 
            // 
            this.lblAJBorrarHistorial.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAJBorrarHistorial.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAJBorrarHistorial.ForeColor = System.Drawing.Color.Black;
            this.lblAJBorrarHistorial.Location = new System.Drawing.Point(20, 360);
            this.lblAJBorrarHistorial.Name = "lblAJBorrarHistorial";
            this.lblAJBorrarHistorial.Size = new System.Drawing.Size(240, 26);
            this.lblAJBorrarHistorial.TabIndex = 66;
            this.lblAJBorrarHistorial.Text = "Borrar historial de sincronización:";
            // 
            // lblAJIdioma
            // 
            // 
            // 
            // 
            this.lblAJIdioma.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAJIdioma.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAJIdioma.ForeColor = System.Drawing.Color.Black;
            this.lblAJIdioma.Location = new System.Drawing.Point(351, 420);
            this.lblAJIdioma.Name = "lblAJIdioma";
            this.lblAJIdioma.Size = new System.Drawing.Size(240, 26);
            this.lblAJIdioma.TabIndex = 65;
            this.lblAJIdioma.Text = "Cambiar idioma de SyncFTP a:";
            this.lblAJIdioma.Visible = false;
            // 
            // lblAJGuardarEn
            // 
            // 
            // 
            // 
            this.lblAJGuardarEn.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAJGuardarEn.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAJGuardarEn.ForeColor = System.Drawing.Color.Black;
            this.lblAJGuardarEn.Location = new System.Drawing.Point(20, 134);
            this.lblAJGuardarEn.Name = "lblAJGuardarEn";
            this.lblAJGuardarEn.Size = new System.Drawing.Size(240, 26);
            this.lblAJGuardarEn.TabIndex = 62;
            this.lblAJGuardarEn.Text = "Guardar copia de datos en:";
            // 
            // lblAJMinimizarIniciar
            // 
            // 
            // 
            // 
            this.lblAJMinimizarIniciar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAJMinimizarIniciar.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAJMinimizarIniciar.ForeColor = System.Drawing.Color.Black;
            this.lblAJMinimizarIniciar.Location = new System.Drawing.Point(20, 106);
            this.lblAJMinimizarIniciar.Name = "lblAJMinimizarIniciar";
            this.lblAJMinimizarIniciar.Size = new System.Drawing.Size(246, 26);
            this.lblAJMinimizarIniciar.TabIndex = 61;
            this.lblAJMinimizarIniciar.Text = "Minímizar al iniciar:";
            // 
            // lblAJDistinguirSincronizacion
            // 
            // 
            // 
            // 
            this.lblAJDistinguirSincronizacion.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAJDistinguirSincronizacion.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAJDistinguirSincronizacion.ForeColor = System.Drawing.Color.Black;
            this.lblAJDistinguirSincronizacion.Location = new System.Drawing.Point(321, 109);
            this.lblAJDistinguirSincronizacion.Name = "lblAJDistinguirSincronizacion";
            this.lblAJDistinguirSincronizacion.Size = new System.Drawing.Size(246, 26);
            this.lblAJDistinguirSincronizacion.TabIndex = 60;
            this.lblAJDistinguirSincronizacion.Text = "Una carpeta diferente por sincronización:";
            this.lblAJDistinguirSincronizacion.Visible = false;
            // 
            // chkAJIniciarConWindows
            // 
            this.chkAJIniciarConWindows.BackColor = System.Drawing.Color.Transparent;
            this.chkAJIniciarConWindows.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkAJIniciarConWindows.BackgroundImage")));
            this.chkAJIniciarConWindows.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkAJIniciarConWindows.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAJIniciarConWindows.Location = new System.Drawing.Point(271, 80);
            this.chkAJIniciarConWindows.Name = "chkAJIniciarConWindows";
            this.chkAJIniciarConWindows.OffColor = System.Drawing.Color.Red;
            this.chkAJIniciarConWindows.OnColor = System.Drawing.Color.Green;
            this.chkAJIniciarConWindows.Size = new System.Drawing.Size(35, 20);
            this.chkAJIniciarConWindows.TabIndex = 11;
            this.chkAJIniciarConWindows.Value = false;
            this.chkAJIniciarConWindows.OnValueChange += new System.EventHandler(this.chkAJIniciarConWindows_OnValueChange);
            // 
            // btnAJReestablecer
            // 
            this.btnAJReestablecer.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAJReestablecer.BackColor = System.Drawing.Color.Gray;
            this.btnAJReestablecer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAJReestablecer.BorderRadius = 0;
            this.btnAJReestablecer.ButtonText = "Reestablecer";
            this.btnAJReestablecer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAJReestablecer.DisabledColor = System.Drawing.Color.Gray;
            this.btnAJReestablecer.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAJReestablecer.Iconimage = global::SyncBD.Properties.Resources.HighPriority_24px;
            this.btnAJReestablecer.Iconimage_right = null;
            this.btnAJReestablecer.Iconimage_right_Selected = null;
            this.btnAJReestablecer.Iconimage_Selected = null;
            this.btnAJReestablecer.IconMarginLeft = 0;
            this.btnAJReestablecer.IconMarginRight = 0;
            this.btnAJReestablecer.IconRightVisible = true;
            this.btnAJReestablecer.IconRightZoom = 0D;
            this.btnAJReestablecer.IconVisible = true;
            this.btnAJReestablecer.IconZoom = 45D;
            this.btnAJReestablecer.IsTab = false;
            this.btnAJReestablecer.Location = new System.Drawing.Point(20, 463);
            this.btnAJReestablecer.Name = "btnAJReestablecer";
            this.btnAJReestablecer.Normalcolor = System.Drawing.Color.Gray;
            this.btnAJReestablecer.OnHovercolor = System.Drawing.Color.DarkRed;
            this.btnAJReestablecer.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAJReestablecer.selected = false;
            this.btnAJReestablecer.Size = new System.Drawing.Size(240, 43);
            this.btnAJReestablecer.TabIndex = 19;
            this.btnAJReestablecer.Text = "Reestablecer";
            this.btnAJReestablecer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAJReestablecer.Textcolor = System.Drawing.Color.White;
            this.btnAJReestablecer.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAJReestablecer.Click += new System.EventHandler(this.btnAJReestablecer_Click);
            // 
            // chkAJMinimizarIniciar
            // 
            this.chkAJMinimizarIniciar.BackColor = System.Drawing.Color.Transparent;
            this.chkAJMinimizarIniciar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkAJMinimizarIniciar.BackgroundImage")));
            this.chkAJMinimizarIniciar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkAJMinimizarIniciar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAJMinimizarIniciar.Location = new System.Drawing.Point(271, 113);
            this.chkAJMinimizarIniciar.Name = "chkAJMinimizarIniciar";
            this.chkAJMinimizarIniciar.OffColor = System.Drawing.Color.Red;
            this.chkAJMinimizarIniciar.OnColor = System.Drawing.Color.Green;
            this.chkAJMinimizarIniciar.Size = new System.Drawing.Size(35, 20);
            this.chkAJMinimizarIniciar.TabIndex = 12;
            this.chkAJMinimizarIniciar.Value = false;
            this.chkAJMinimizarIniciar.OnValueChange += new System.EventHandler(this.chkAJMinimizarIniciar_OnValueChange);
            // 
            // btnAJBorrarHistorial
            // 
            this.btnAJBorrarHistorial.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAJBorrarHistorial.BackColor = System.Drawing.Color.Gray;
            this.btnAJBorrarHistorial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAJBorrarHistorial.BorderRadius = 0;
            this.btnAJBorrarHistorial.ButtonText = "Borrar";
            this.btnAJBorrarHistorial.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAJBorrarHistorial.DisabledColor = System.Drawing.Color.Gray;
            this.btnAJBorrarHistorial.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAJBorrarHistorial.Iconimage = global::SyncBD.Properties.Resources.HighPriority_24px;
            this.btnAJBorrarHistorial.Iconimage_right = null;
            this.btnAJBorrarHistorial.Iconimage_right_Selected = null;
            this.btnAJBorrarHistorial.Iconimage_Selected = null;
            this.btnAJBorrarHistorial.IconMarginLeft = 0;
            this.btnAJBorrarHistorial.IconMarginRight = 0;
            this.btnAJBorrarHistorial.IconRightVisible = true;
            this.btnAJBorrarHistorial.IconRightZoom = 0D;
            this.btnAJBorrarHistorial.IconVisible = true;
            this.btnAJBorrarHistorial.IconZoom = 45D;
            this.btnAJBorrarHistorial.IsTab = false;
            this.btnAJBorrarHistorial.Location = new System.Drawing.Point(20, 389);
            this.btnAJBorrarHistorial.Name = "btnAJBorrarHistorial";
            this.btnAJBorrarHistorial.Normalcolor = System.Drawing.Color.Gray;
            this.btnAJBorrarHistorial.OnHovercolor = System.Drawing.Color.DarkRed;
            this.btnAJBorrarHistorial.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAJBorrarHistorial.selected = false;
            this.btnAJBorrarHistorial.Size = new System.Drawing.Size(240, 43);
            this.btnAJBorrarHistorial.TabIndex = 18;
            this.btnAJBorrarHistorial.Text = "Borrar";
            this.btnAJBorrarHistorial.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAJBorrarHistorial.Textcolor = System.Drawing.Color.White;
            this.btnAJBorrarHistorial.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAJBorrarHistorial.Click += new System.EventHandler(this.btnAJBorrarHistorial_Click);
            // 
            // btnAJLocal
            // 
            this.btnAJLocal.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAJLocal.BackColor = System.Drawing.Color.Gray;
            this.btnAJLocal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAJLocal.BorderRadius = 0;
            this.btnAJLocal.ButtonText = "Local";
            this.btnAJLocal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAJLocal.DisabledColor = System.Drawing.Color.Gray;
            this.btnAJLocal.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAJLocal.Iconimage = global::SyncBD.Properties.Resources.UploadToFTP_16px;
            this.btnAJLocal.Iconimage_right = null;
            this.btnAJLocal.Iconimage_right_Selected = null;
            this.btnAJLocal.Iconimage_Selected = null;
            this.btnAJLocal.IconMarginLeft = 0;
            this.btnAJLocal.IconMarginRight = 0;
            this.btnAJLocal.IconRightVisible = true;
            this.btnAJLocal.IconRightZoom = 0D;
            this.btnAJLocal.IconVisible = true;
            this.btnAJLocal.IconZoom = 45D;
            this.btnAJLocal.IsTab = false;
            this.btnAJLocal.Location = new System.Drawing.Point(143, 237);
            this.btnAJLocal.Name = "btnAJLocal";
            this.btnAJLocal.Normalcolor = System.Drawing.Color.Gray;
            this.btnAJLocal.OnHovercolor = System.Drawing.Color.DarkCyan;
            this.btnAJLocal.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAJLocal.selected = false;
            this.btnAJLocal.Size = new System.Drawing.Size(117, 43);
            this.btnAJLocal.TabIndex = 15;
            this.btnAJLocal.Text = "Local";
            this.btnAJLocal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAJLocal.Textcolor = System.Drawing.Color.White;
            this.btnAJLocal.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAJLocal.Click += new System.EventHandler(this.btnAJLocal_Click);
            // 
            // btnAJRemoto
            // 
            this.btnAJRemoto.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAJRemoto.BackColor = System.Drawing.Color.Gray;
            this.btnAJRemoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAJRemoto.BorderRadius = 0;
            this.btnAJRemoto.ButtonText = "Remoto";
            this.btnAJRemoto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAJRemoto.DisabledColor = System.Drawing.Color.Gray;
            this.btnAJRemoto.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAJRemoto.Iconimage = global::SyncBD.Properties.Resources.DownloadFromFTP_16px;
            this.btnAJRemoto.Iconimage_right = null;
            this.btnAJRemoto.Iconimage_right_Selected = null;
            this.btnAJRemoto.Iconimage_Selected = null;
            this.btnAJRemoto.IconMarginLeft = 0;
            this.btnAJRemoto.IconMarginRight = 0;
            this.btnAJRemoto.IconRightVisible = true;
            this.btnAJRemoto.IconRightZoom = 0D;
            this.btnAJRemoto.IconVisible = true;
            this.btnAJRemoto.IconZoom = 45D;
            this.btnAJRemoto.IsTab = false;
            this.btnAJRemoto.Location = new System.Drawing.Point(20, 237);
            this.btnAJRemoto.Name = "btnAJRemoto";
            this.btnAJRemoto.Normalcolor = System.Drawing.Color.Gray;
            this.btnAJRemoto.OnHovercolor = System.Drawing.Color.Green;
            this.btnAJRemoto.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAJRemoto.selected = false;
            this.btnAJRemoto.Size = new System.Drawing.Size(117, 43);
            this.btnAJRemoto.TabIndex = 14;
            this.btnAJRemoto.Text = "Remoto";
            this.btnAJRemoto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAJRemoto.Textcolor = System.Drawing.Color.White;
            this.btnAJRemoto.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAJRemoto.Click += new System.EventHandler(this.btnAJRemoto_Click);
            // 
            // chkAJRutaPermanente
            // 
            this.chkAJRutaPermanente.BackColor = System.Drawing.Color.Transparent;
            this.chkAJRutaPermanente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkAJRutaPermanente.BackgroundImage")));
            this.chkAJRutaPermanente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkAJRutaPermanente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAJRutaPermanente.Location = new System.Drawing.Point(572, 82);
            this.chkAJRutaPermanente.Name = "chkAJRutaPermanente";
            this.chkAJRutaPermanente.OffColor = System.Drawing.Color.Red;
            this.chkAJRutaPermanente.OnColor = System.Drawing.Color.Green;
            this.chkAJRutaPermanente.Size = new System.Drawing.Size(35, 20);
            this.chkAJRutaPermanente.TabIndex = 20;
            this.chkAJRutaPermanente.Value = false;
            this.chkAJRutaPermanente.Visible = false;
            this.chkAJRutaPermanente.OnValueChange += new System.EventHandler(this.chkAJRutaPermanente_OnValueChange);
            // 
            // chkAJDistinguirSincronizacion
            // 
            this.chkAJDistinguirSincronizacion.BackColor = System.Drawing.Color.Transparent;
            this.chkAJDistinguirSincronizacion.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkAJDistinguirSincronizacion.BackgroundImage")));
            this.chkAJDistinguirSincronizacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkAJDistinguirSincronizacion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAJDistinguirSincronizacion.Location = new System.Drawing.Point(572, 113);
            this.chkAJDistinguirSincronizacion.Name = "chkAJDistinguirSincronizacion";
            this.chkAJDistinguirSincronizacion.OffColor = System.Drawing.Color.Red;
            this.chkAJDistinguirSincronizacion.OnColor = System.Drawing.Color.Green;
            this.chkAJDistinguirSincronizacion.Size = new System.Drawing.Size(35, 20);
            this.chkAJDistinguirSincronizacion.TabIndex = 21;
            this.chkAJDistinguirSincronizacion.Value = false;
            this.chkAJDistinguirSincronizacion.Visible = false;
            this.chkAJDistinguirSincronizacion.OnValueChange += new System.EventHandler(this.chkAJDistinguirSincronizacion_OnValueChange);
            // 
            // lineAJDivisor
            // 
            this.lineAJDivisor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lineAJDivisor.ForeColor = System.Drawing.Color.Black;
            this.lineAJDivisor.Location = new System.Drawing.Point(6, 39);
            this.lineAJDivisor.Name = "lineAJDivisor";
            this.lineAJDivisor.Size = new System.Drawing.Size(637, 23);
            this.lineAJDivisor.TabIndex = 7;
            this.lineAJDivisor.Text = "line1";
            // 
            // lblAJEncabezado
            // 
            this.lblAJEncabezado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblAJEncabezado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAJEncabezado.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblAJEncabezado.ForeColor = System.Drawing.Color.Black;
            this.lblAJEncabezado.Location = new System.Drawing.Point(6, 6);
            this.lblAJEncabezado.Name = "lblAJEncabezado";
            this.lblAJEncabezado.SingleLineColor = System.Drawing.Color.Black;
            this.lblAJEncabezado.Size = new System.Drawing.Size(634, 39);
            this.lblAJEncabezado.TabIndex = 6;
            this.lblAJEncabezado.Text = "Ajustes del sistema";
            this.lblAJEncabezado.WordWrap = true;
            // 
            // pnlBaseSeleccion
            // 
            this.pnlBaseSeleccion.Controls.Add(this.pnlSeleccion);
            this.pnlBaseSeleccion.Controls.Add(this.pnlFondoSeleccion);
            this.pnlBaseSeleccion.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlBaseSeleccion.Location = new System.Drawing.Point(0, 0);
            this.pnlBaseSeleccion.Name = "pnlBaseSeleccion";
            this.pnlBaseSeleccion.Size = new System.Drawing.Size(5, 570);
            this.pnlBaseSeleccion.TabIndex = 5;
            // 
            // pnlSeleccion
            // 
            this.pnlSeleccion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.pnlSeleccion.ForeColor = System.Drawing.Color.Black;
            this.pnlSeleccion.Location = new System.Drawing.Point(0, 30);
            this.pnlSeleccion.Name = "pnlSeleccion";
            this.pnlSeleccion.Size = new System.Drawing.Size(5, 50);
            this.pnlSeleccion.TabIndex = 3;
            // 
            // pnlFondoSeleccion
            // 
            this.pnlFondoSeleccion.BackColor = System.Drawing.Color.Silver;
            this.pnlFondoSeleccion.ForeColor = System.Drawing.Color.Black;
            this.pnlFondoSeleccion.Location = new System.Drawing.Point(0, 280);
            this.pnlFondoSeleccion.Name = "pnlFondoSeleccion";
            this.pnlFondoSeleccion.Size = new System.Drawing.Size(5, 280);
            this.pnlFondoSeleccion.TabIndex = 4;
            this.pnlFondoSeleccion.Visible = false;
            // 
            // tmpLocalTiempoTranscurrido
            // 
            this.tmpLocalTiempoTranscurrido.Interval = 1000;
            this.tmpLocalTiempoTranscurrido.Tick += new System.EventHandler(this.tmpLocalTiempoTranscurrido_Tick);
            // 
            // cmpMenuBarraTareas
            // 
            this.cmpMenuBarraTareas.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.cmpMenuBarraTareas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.remotoToolStripMenuItem,
            this.descargasToolStripMenuItem,
            this.btnRestaurarMenuStrip,
            this.btnExitSysTray});
            this.cmpMenuBarraTareas.Name = "cmpMenuSysTray";
            this.cmpMenuBarraTareas.Size = new System.Drawing.Size(171, 124);
            // 
            // remotoToolStripMenuItem
            // 
            this.remotoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnStartRemoteStrip,
            this.btnPauseRemoteStrip,
            this.btnStopRemoteStrip,
            this.btnRemoteTreeStrip});
            this.remotoToolStripMenuItem.Image = global::SyncBD.Properties.Resources.DownloadFromFTP_16px;
            this.remotoToolStripMenuItem.Name = "remotoToolStripMenuItem";
            this.remotoToolStripMenuItem.Size = new System.Drawing.Size(170, 30);
            this.remotoToolStripMenuItem.Text = "Servidor Remoto";
            // 
            // btnStartRemoteStrip
            // 
            this.btnStartRemoteStrip.Image = global::SyncBD.Properties.Resources.Synchronize_16px;
            this.btnStartRemoteStrip.Name = "btnStartRemoteStrip";
            this.btnStartRemoteStrip.Size = new System.Drawing.Size(171, 22);
            this.btnStartRemoteStrip.Text = "Resincronizar";
            this.btnStartRemoteStrip.Click += new System.EventHandler(this.btnStartRemoteStrip_Click);
            // 
            // btnPauseRemoteStrip
            // 
            this.btnPauseRemoteStrip.Name = "btnPauseRemoteStrip";
            this.btnPauseRemoteStrip.Size = new System.Drawing.Size(171, 22);
            this.btnPauseRemoteStrip.Text = "Pausar";
            this.btnPauseRemoteStrip.Visible = false;
            // 
            // btnStopRemoteStrip
            // 
            this.btnStopRemoteStrip.Name = "btnStopRemoteStrip";
            this.btnStopRemoteStrip.Size = new System.Drawing.Size(171, 22);
            this.btnStopRemoteStrip.Text = "Detener";
            this.btnStopRemoteStrip.Visible = false;
            // 
            // btnRemoteTreeStrip
            // 
            this.btnRemoteTreeStrip.Name = "btnRemoteTreeStrip";
            this.btnRemoteTreeStrip.Size = new System.Drawing.Size(171, 22);
            this.btnRemoteTreeStrip.Text = "Obtener directorio";
            this.btnRemoteTreeStrip.Visible = false;
            // 
            // descargasToolStripMenuItem
            // 
            this.descargasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnStartLocalStrip,
            this.btnPauseLocalStrip,
            this.btnStopLocalStrip,
            this.btnLocalTreeStrip});
            this.descargasToolStripMenuItem.Image = global::SyncBD.Properties.Resources.UploadToFTP_16px;
            this.descargasToolStripMenuItem.Name = "descargasToolStripMenuItem";
            this.descargasToolStripMenuItem.Size = new System.Drawing.Size(170, 30);
            this.descargasToolStripMenuItem.Text = "Servidor Local";
            // 
            // btnStartLocalStrip
            // 
            this.btnStartLocalStrip.Image = global::SyncBD.Properties.Resources.Synchronize_16px;
            this.btnStartLocalStrip.Name = "btnStartLocalStrip";
            this.btnStartLocalStrip.Size = new System.Drawing.Size(171, 22);
            this.btnStartLocalStrip.Text = "Resincronizar";
            this.btnStartLocalStrip.Click += new System.EventHandler(this.btnStartLocalStrip_Click);
            // 
            // btnPauseLocalStrip
            // 
            this.btnPauseLocalStrip.Name = "btnPauseLocalStrip";
            this.btnPauseLocalStrip.Size = new System.Drawing.Size(171, 22);
            this.btnPauseLocalStrip.Text = "Pausar";
            this.btnPauseLocalStrip.Visible = false;
            // 
            // btnStopLocalStrip
            // 
            this.btnStopLocalStrip.Name = "btnStopLocalStrip";
            this.btnStopLocalStrip.Size = new System.Drawing.Size(171, 22);
            this.btnStopLocalStrip.Text = "Detener";
            this.btnStopLocalStrip.Visible = false;
            // 
            // btnLocalTreeStrip
            // 
            this.btnLocalTreeStrip.Name = "btnLocalTreeStrip";
            this.btnLocalTreeStrip.Size = new System.Drawing.Size(171, 22);
            this.btnLocalTreeStrip.Text = "Obtener directorio";
            this.btnLocalTreeStrip.Visible = false;
            // 
            // btnRestaurarMenuStrip
            // 
            this.btnRestaurarMenuStrip.Image = global::SyncBD.Properties.Resources.SyncBDLogo;
            this.btnRestaurarMenuStrip.Name = "btnRestaurarMenuStrip";
            this.btnRestaurarMenuStrip.Size = new System.Drawing.Size(170, 30);
            this.btnRestaurarMenuStrip.Text = "Mostrar SyncBD";
            this.btnRestaurarMenuStrip.Click += new System.EventHandler(this.btnRestaurarMenuStrip_Click);
            // 
            // btnExitSysTray
            // 
            this.btnExitSysTray.Image = global::SyncBD.Properties.Resources.CloseWindow_16px;
            this.btnExitSysTray.Name = "btnExitSysTray";
            this.btnExitSysTray.Size = new System.Drawing.Size(170, 30);
            this.btnExitSysTray.Text = "Salir";
            this.btnExitSysTray.Click += new System.EventHandler(this.btnExitSysTray_Click);
            // 
            // nicBarraTareas
            // 
            this.nicBarraTareas.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.nicBarraTareas.BalloonTipText = "Trabajando en segundo plano...";
            this.nicBarraTareas.ContextMenuStrip = this.cmpMenuBarraTareas;
            this.nicBarraTareas.Icon = ((System.Drawing.Icon)(resources.GetObject("nicBarraTareas.Icon")));
            this.nicBarraTareas.Text = "SyncBD";
            this.nicBarraTareas.DoubleClick += new System.EventHandler(this.nicBarraTareas_DoubleClick);
            // 
            // tmpRemotoTiempoTranscurrido
            // 
            this.tmpRemotoTiempoTranscurrido.Interval = 1000;
            this.tmpRemotoTiempoTranscurrido.Tick += new System.EventHandler(this.tmpRemotoTiempoTranscurrido_Tick);
            // 
            // fbdGuardarEn
            // 
            this.fbdGuardarEn.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlMenu);
            this.Controls.Add(this.pnlTitleBar);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SyncBD | ¡Sincroniza tus datos!";
            this.Shown += new System.EventHandler(this.Principal_Shown);
            this.Resize += new System.EventHandler(this.Principal_Resize);
            this.pnlTitleBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).EndInit();
            this.pnlMenu.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.pnlBaseAreaTrabajo.ResumeLayout(false);
            this.pnlRemoto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxSRTiempo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSRHistorial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSRVelocidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSRRemoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSRHistorial)).EndInit();
            this.pnlAyuda.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlErroresEInformacion.ResumeLayout(false);
            this.pnlLocal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxSLTiempo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSLHistorial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSLVelocidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSLIcono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSLHistorial)).EndInit();
            this.pnlAjustes.ResumeLayout(false);
            this.pnlBaseSeleccion.ResumeLayout(false);
            this.cmpMenuBarraTareas.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTitleBar;
        private DevComponents.DotNetBar.LabelX lblPTitulo;
        private System.Windows.Forms.PictureBox pbxMinimizar;
        private System.Windows.Forms.PictureBox pbxCerrar;
        private System.Windows.Forms.PictureBox pbxIcono;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnPSalir;
        private System.Windows.Forms.Button btnPAyuda;
        private System.Windows.Forms.Button btnPAjustes;
        private DevComponents.DotNetBar.LabelX lblVersion;
        private System.Windows.Forms.Button btnPSincronizarLocal;
        private System.Windows.Forms.Button btnPSincronizarRemoto;
        private System.Windows.Forms.Button btnPHamburguer;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlBaseAreaTrabajo;
        private System.Windows.Forms.Panel pnlAjustes;
        private System.Windows.Forms.ComboBox cbxAJIdioma;
        private System.Windows.Forms.ComboBox cbxAJApariencia;
        private System.Windows.Forms.ComboBox cbxAJGuardarEn;
        private DevComponents.DotNetBar.LabelX lblAJRutaPermanente;
        private DevComponents.DotNetBar.LabelX lblAJIniciarConWindows;
        private DevComponents.DotNetBar.LabelX lblAJConfigurarServidores;
        private DevComponents.DotNetBar.LabelX lblAJCambiarApariencia;
        private DevComponents.DotNetBar.LabelX lblAJReestablecer;
        private DevComponents.DotNetBar.LabelX lblAJBorrarHistorial;
        private DevComponents.DotNetBar.LabelX lblAJIdioma;
        private DevComponents.DotNetBar.LabelX lblAJGuardarEn;
        private DevComponents.DotNetBar.LabelX lblAJMinimizarIniciar;
        private DevComponents.DotNetBar.LabelX lblAJDistinguirSincronizacion;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkAJIniciarConWindows;
        private Bunifu.Framework.UI.BunifuFlatButton btnAJReestablecer;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkAJMinimizarIniciar;
        private Bunifu.Framework.UI.BunifuFlatButton btnAJBorrarHistorial;
        private Bunifu.Framework.UI.BunifuFlatButton btnAJLocal;
        private Bunifu.Framework.UI.BunifuFlatButton btnAJRemoto;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkAJRutaPermanente;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkAJDistinguirSincronizacion;
        private DevComponents.DotNetBar.Controls.Line lineAJDivisor;
        private DevComponents.DotNetBar.LabelX lblAJEncabezado;
        private System.Windows.Forms.Panel pnlRemoto;
        private DevComponents.DotNetBar.LabelX lblSREstado;
        private Bunifu.Framework.UI.BunifuFlatButton btnSRResincronizar;
        private Bunifu.Framework.UI.BunifuProgressBar pbrSRProgreso;
        private Bunifu.Framework.UI.BunifuFlatButton btnSRAbrirCarpeta;
        private DevComponents.DotNetBar.LabelX lblSRPorcentaje;
        private DevComponents.DotNetBar.LabelX lblSREncabezado2;
        private DevComponents.DotNetBar.Controls.Line lineSRDivisor2;
        private DevComponents.DotNetBar.LabelX lblSREncabezado1;
        private DevComponents.DotNetBar.Controls.Line lineSRDivisor;
        private DevComponents.DotNetBar.LabelX lblSREncabezado;
        private System.Windows.Forms.Panel pnlAyuda;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel pnlErroresEInformacion;
        private DevComponents.DotNetBar.Controls.Line lineHDivisor3;
        private DevComponents.DotNetBar.LabelX lblHCodigoTitulo;
        private DevComponents.DotNetBar.LabelX lblHBuscarCodigo;
        private DevComponents.DotNetBar.LabelX lblHAcercaDe;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtHBuscarError;
        private DevComponents.DotNetBar.LabelX lblCopyright;
        private DevComponents.DotNetBar.Controls.Line lineHDivisor2;
        private DevComponents.DotNetBar.Controls.Line lblHDivisor;
        private DevComponents.DotNetBar.LabelX lblHEncabezado;
        private System.Windows.Forms.Panel pnlLocal;
        private DevComponents.DotNetBar.LabelX lblSLEstado;
        private DevComponents.DotNetBar.LabelX lblSLPorcentaje;
        private Bunifu.Framework.UI.BunifuFlatButton btnSLResincronizar;
        private Bunifu.Framework.UI.BunifuProgressBar pbrSLProgreso;
        private DevComponents.DotNetBar.LabelX lblSLEncabezado2;
        private Bunifu.Framework.UI.BunifuFlatButton btnSLAbrirCarpeta;
        private DevComponents.DotNetBar.Controls.Line lineSLDivisor2;
        private DevComponents.DotNetBar.LabelX lblSLEncabezado1;
        private DevComponents.DotNetBar.Controls.Line lineSLDivisor;
        private DevComponents.DotNetBar.LabelX lblSLEncabezado;
        private System.Windows.Forms.Panel pnlBaseSeleccion;
        private System.Windows.Forms.Panel pnlSeleccion;
        private System.Windows.Forms.Panel pnlFondoSeleccion;
        private System.Windows.Forms.ToolTip ttpMensaje;
        private System.Windows.Forms.Timer tmpLocalTiempoTranscurrido;
        private DevComponents.DotNetBar.LabelX lblSLTiempoTranscurrido;
        private DevComponents.DotNetBar.LabelX lblSRTiempoTranscurrido;
        private System.Windows.Forms.ContextMenuStrip cmpMenuBarraTareas;
        private System.Windows.Forms.ToolStripMenuItem remotoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnStartRemoteStrip;
        private System.Windows.Forms.ToolStripMenuItem btnPauseRemoteStrip;
        private System.Windows.Forms.ToolStripMenuItem btnStopRemoteStrip;
        private System.Windows.Forms.ToolStripMenuItem btnRemoteTreeStrip;
        private System.Windows.Forms.ToolStripMenuItem descargasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnStartLocalStrip;
        private System.Windows.Forms.ToolStripMenuItem btnPauseLocalStrip;
        private System.Windows.Forms.ToolStripMenuItem btnStopLocalStrip;
        private System.Windows.Forms.ToolStripMenuItem btnLocalTreeStrip;
        private System.Windows.Forms.ToolStripMenuItem btnRestaurarMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem btnExitSysTray;
        private System.Windows.Forms.DataGridView dgvSLHistorial;
        private System.Windows.Forms.DataGridView dgvSRHistorial;
        private System.Windows.Forms.NotifyIcon nicBarraTareas;
        private Bunifu.Framework.UI.BunifuFlatButton btnPEscogerCarpeta;
        private System.Windows.Forms.Timer tmpRemotoTiempoTranscurrido;
        private System.Windows.Forms.FolderBrowserDialog fbdGuardarEn;
        private DevComponents.DotNetBar.LabelX lblAJRutaGuardado;
        private DevComponents.DotNetBar.LabelX lblAJTituloGuardado;
        private DevComponents.DotNetBar.LabelX lblSRVelocidad;
        private DevComponents.DotNetBar.LabelX lblSLVelocidad;
        private DevComponents.DotNetBar.Controls.RichTextBoxEx rtbHCuerpoCodigo;
        private Bunifu.Framework.UI.BunifuFlatButton btnAJPersonalizarTema;
        private System.Windows.Forms.PictureBox pbxSLIcono;
        private System.Windows.Forms.PictureBox pbxSLVelocidad;
        private System.Windows.Forms.PictureBox pbxSLHistorial;
        private System.Windows.Forms.PictureBox pbxSRHistorial;
        private System.Windows.Forms.PictureBox pbxSRVelocidad;
        private System.Windows.Forms.PictureBox pbxSRRemoto;
        private System.Windows.Forms.PictureBox pbxSRTiempo;
        private System.Windows.Forms.PictureBox pbxSLTiempo;
        private Bunifu.Framework.UI.BunifuFlatButton btnHBuscarCodigo;
        private Bunifu.Framework.UI.BunifuProgressBar pbrSRProgresoIndividual;
        private DevComponents.DotNetBar.LabelX lblSRPorcentajeIndividual;
        private DevComponents.DotNetBar.LabelX lblSRArchivoActual;
        private DevComponents.DotNetBar.LabelX lblSLPorcentajeIndividual;
        private Bunifu.Framework.UI.BunifuProgressBar pbrSLProgresoIndividual;
        private DevComponents.DotNetBar.LabelX lblSLArchivoActual;
    }
}
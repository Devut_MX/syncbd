﻿using System;
using System.Drawing;
using System.IO;
using System.Media;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SyncDB.Vistas
{
    public partial class PopUp : Form
    {
        protected override bool ShowWithoutActivation { get { return true; } }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        private static PopUp singleton;

        public static PopUp ObtenerInstancia(string mensaje, string titulo, Icono icono)
        {
            if (singleton == null || singleton.IsDisposed)
            {
                singleton = new PopUp(mensaje, titulo, icono);
            }

            singleton.TopMost = true;

            singleton.BringToFront();

            return singleton;
        }

        private PopUp(string mensaje, string titulo, Icono icono)
        {
            InitializeComponent();

            Posicion();

            Comportamiento();

            TipoNotificacion(mensaje, titulo, icono);
        }
        
        public enum Icono
        {
            Ok,
            Info,
            Warning,
            Error
        }

        private void Posicion()
        {
            Point original = new Point(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

            Location = new Point(original.X - 370, original.Y - 150);
        }

        private void TipoNotificacion(string mensaje, string titulo, Icono icono)
        {
            try
            {
                switch (icono)
                {
                    case Icono.Ok:
                        BackColor = Color.FromArgb(255, 165, 214, 167);
                        pbxIcono.Image = SyncBD.Properties.Resources.Ok_48px;
                        break;

                    case Icono.Info:
                        BackColor = Color.FromArgb(255, 129, 212, 250);
                        pbxIcono.Image = SyncBD.Properties.Resources.Info_48px;
                        break;

                    case Icono.Warning:
                        BackColor = Color.FromArgb(255, 255, 238, 88);
                        pbxIcono.Image = SyncBD.Properties.Resources.Attention_48px;
                        break;

                    case Icono.Error:
                        BackColor = Color.FromArgb(255, 239, 115, 115);
                        pbxIcono.Image = SyncBD.Properties.Resources.Cancel_48px;
                        break;

                    default:
                        BackColor = Color.FromArgb(255, 165, 214, 167);
                        pbxIcono.Image = SyncBD.Properties.Resources.Ok_48px;
                        break;
                }

                lblTitulo.Text = titulo;

                lblMensaje.Text = mensaje;

                if (File.Exists(string.Concat(Application.StartupPath, @"\Notify.wav")))
                {
                    SoundPlayer reproductor = new SoundPlayer(string.Concat(Application.StartupPath, @"\Notify.wav"));

                    reproductor.Play();

                    reproductor.Dispose();
                }
            }
            catch (Exception)
            {

            }
        }

        private void Comportamiento()
        {
            tmpVisible.Enabled = true;
        }

        private void tmpVisible_Tick(object sender, EventArgs e)
        {
            tmpVisible.Enabled = false;

            Dispose();

            Close();
        }

        private void PopUp_Click(object sender, EventArgs e)
        {
            tmpVisible.Enabled = false;

            Dispose();

            Close();
        }
    }
}

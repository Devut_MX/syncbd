﻿namespace SyncDB.Vistas
{
    partial class FinAsistente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FinAsistente));
            this.pnlSegundo = new System.Windows.Forms.Panel();
            this.lblA5Directorio2 = new DevComponents.DotNetBar.LabelX();
            this.lblA5Usuario2 = new DevComponents.DotNetBar.LabelX();
            this.lblA5Servidor2 = new DevComponents.DotNetBar.LabelX();
            this.lblA5SegundoTitulo = new DevComponents.DotNetBar.LabelX();
            this.lblA5SegundoCuerpo = new DevComponents.DotNetBar.LabelX();
            this.pnlPrimero = new System.Windows.Forms.Panel();
            this.lblA5Directorio1 = new DevComponents.DotNetBar.LabelX();
            this.lblA5Usuario1 = new DevComponents.DotNetBar.LabelX();
            this.lblA5Servidor1 = new DevComponents.DotNetBar.LabelX();
            this.lblA5PrimeroTitulo = new DevComponents.DotNetBar.LabelX();
            this.lblA5PrimeroCuerpo = new DevComponents.DotNetBar.LabelX();
            this.btnA5Continuar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.lblA5Cuerpo = new DevComponents.DotNetBar.LabelX();
            this.lineA5Divisor = new DevComponents.DotNetBar.Controls.Line();
            this.lblA5Encabezado = new DevComponents.DotNetBar.LabelX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblA5Titulo = new DevComponents.DotNetBar.LabelX();
            this.pbxMinimizar = new System.Windows.Forms.PictureBox();
            this.pbxCerrar = new System.Windows.Forms.PictureBox();
            this.pbxIcono = new System.Windows.Forms.PictureBox();
            this.pnlSegundo.SuspendLayout();
            this.pnlPrimero.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSegundo
            // 
            this.pnlSegundo.BackColor = System.Drawing.Color.Transparent;
            this.pnlSegundo.Controls.Add(this.lblA5Directorio2);
            this.pnlSegundo.Controls.Add(this.lblA5Usuario2);
            this.pnlSegundo.Controls.Add(this.lblA5Servidor2);
            this.pnlSegundo.Controls.Add(this.lblA5SegundoTitulo);
            this.pnlSegundo.Controls.Add(this.lblA5SegundoCuerpo);
            this.pnlSegundo.Location = new System.Drawing.Point(192, 288);
            this.pnlSegundo.Name = "pnlSegundo";
            this.pnlSegundo.Size = new System.Drawing.Size(396, 113);
            this.pnlSegundo.TabIndex = 35;
            // 
            // lblA5Directorio2
            // 
            // 
            // 
            // 
            this.lblA5Directorio2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA5Directorio2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA5Directorio2.ForeColor = System.Drawing.Color.Black;
            this.lblA5Directorio2.Location = new System.Drawing.Point(165, 82);
            this.lblA5Directorio2.Name = "lblA5Directorio2";
            this.lblA5Directorio2.SingleLineColor = System.Drawing.Color.Black;
            this.lblA5Directorio2.Size = new System.Drawing.Size(220, 22);
            this.lblA5Directorio2.TabIndex = 31;
            this.lblA5Directorio2.Text = "Sin configurar";
            this.lblA5Directorio2.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblA5Directorio2.WordWrap = true;
            // 
            // lblA5Usuario2
            // 
            // 
            // 
            // 
            this.lblA5Usuario2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA5Usuario2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA5Usuario2.ForeColor = System.Drawing.Color.Black;
            this.lblA5Usuario2.Location = new System.Drawing.Point(165, 51);
            this.lblA5Usuario2.Name = "lblA5Usuario2";
            this.lblA5Usuario2.SingleLineColor = System.Drawing.Color.Black;
            this.lblA5Usuario2.Size = new System.Drawing.Size(220, 22);
            this.lblA5Usuario2.TabIndex = 30;
            this.lblA5Usuario2.Text = "Sin configurar";
            this.lblA5Usuario2.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblA5Usuario2.WordWrap = true;
            // 
            // lblA5Servidor2
            // 
            // 
            // 
            // 
            this.lblA5Servidor2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA5Servidor2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA5Servidor2.ForeColor = System.Drawing.Color.Black;
            this.lblA5Servidor2.Location = new System.Drawing.Point(165, 20);
            this.lblA5Servidor2.Name = "lblA5Servidor2";
            this.lblA5Servidor2.SingleLineColor = System.Drawing.Color.Black;
            this.lblA5Servidor2.Size = new System.Drawing.Size(220, 22);
            this.lblA5Servidor2.TabIndex = 29;
            this.lblA5Servidor2.Text = "Sin configurar";
            this.lblA5Servidor2.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblA5Servidor2.WordWrap = true;
            // 
            // lblA5SegundoTitulo
            // 
            // 
            // 
            // 
            this.lblA5SegundoTitulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA5SegundoTitulo.Font = new System.Drawing.Font("Tahoma", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblA5SegundoTitulo.ForeColor = System.Drawing.Color.Black;
            this.lblA5SegundoTitulo.Location = new System.Drawing.Point(3, 3);
            this.lblA5SegundoTitulo.Name = "lblA5SegundoTitulo";
            this.lblA5SegundoTitulo.SingleLineColor = System.Drawing.Color.Black;
            this.lblA5SegundoTitulo.Size = new System.Drawing.Size(214, 22);
            this.lblA5SegundoTitulo.TabIndex = 28;
            this.lblA5SegundoTitulo.Text = "Datos del servidor local";
            this.lblA5SegundoTitulo.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblA5SegundoTitulo.WordWrap = true;
            // 
            // lblA5SegundoCuerpo
            // 
            // 
            // 
            // 
            this.lblA5SegundoCuerpo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA5SegundoCuerpo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA5SegundoCuerpo.ForeColor = System.Drawing.Color.Black;
            this.lblA5SegundoCuerpo.Location = new System.Drawing.Point(3, 22);
            this.lblA5SegundoCuerpo.Name = "lblA5SegundoCuerpo";
            this.lblA5SegundoCuerpo.SingleLineColor = System.Drawing.Color.Black;
            this.lblA5SegundoCuerpo.Size = new System.Drawing.Size(156, 88);
            this.lblA5SegundoCuerpo.TabIndex = 27;
            this.lblA5SegundoCuerpo.Text = "Dirección del servidor:\r\n\r\nUsuario:\r\n\r\nDirectorio de archivos:";
            this.lblA5SegundoCuerpo.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblA5SegundoCuerpo.WordWrap = true;
            // 
            // pnlPrimero
            // 
            this.pnlPrimero.BackColor = System.Drawing.Color.Transparent;
            this.pnlPrimero.Controls.Add(this.lblA5Directorio1);
            this.pnlPrimero.Controls.Add(this.lblA5Usuario1);
            this.pnlPrimero.Controls.Add(this.lblA5Servidor1);
            this.pnlPrimero.Controls.Add(this.lblA5PrimeroTitulo);
            this.pnlPrimero.Controls.Add(this.lblA5PrimeroCuerpo);
            this.pnlPrimero.Location = new System.Drawing.Point(192, 169);
            this.pnlPrimero.Name = "pnlPrimero";
            this.pnlPrimero.Size = new System.Drawing.Size(396, 113);
            this.pnlPrimero.TabIndex = 34;
            // 
            // lblA5Directorio1
            // 
            // 
            // 
            // 
            this.lblA5Directorio1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA5Directorio1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA5Directorio1.ForeColor = System.Drawing.Color.Black;
            this.lblA5Directorio1.Location = new System.Drawing.Point(165, 82);
            this.lblA5Directorio1.Name = "lblA5Directorio1";
            this.lblA5Directorio1.SingleLineColor = System.Drawing.Color.Black;
            this.lblA5Directorio1.Size = new System.Drawing.Size(220, 22);
            this.lblA5Directorio1.TabIndex = 31;
            this.lblA5Directorio1.Text = "Sin configurar";
            this.lblA5Directorio1.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblA5Directorio1.WordWrap = true;
            // 
            // lblA5Usuario1
            // 
            // 
            // 
            // 
            this.lblA5Usuario1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA5Usuario1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA5Usuario1.ForeColor = System.Drawing.Color.Black;
            this.lblA5Usuario1.Location = new System.Drawing.Point(165, 51);
            this.lblA5Usuario1.Name = "lblA5Usuario1";
            this.lblA5Usuario1.SingleLineColor = System.Drawing.Color.Black;
            this.lblA5Usuario1.Size = new System.Drawing.Size(220, 22);
            this.lblA5Usuario1.TabIndex = 30;
            this.lblA5Usuario1.Text = "Sin configurar";
            this.lblA5Usuario1.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblA5Usuario1.WordWrap = true;
            // 
            // lblA5Servidor1
            // 
            // 
            // 
            // 
            this.lblA5Servidor1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA5Servidor1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA5Servidor1.ForeColor = System.Drawing.Color.Black;
            this.lblA5Servidor1.Location = new System.Drawing.Point(165, 20);
            this.lblA5Servidor1.Name = "lblA5Servidor1";
            this.lblA5Servidor1.SingleLineColor = System.Drawing.Color.Black;
            this.lblA5Servidor1.Size = new System.Drawing.Size(220, 22);
            this.lblA5Servidor1.TabIndex = 29;
            this.lblA5Servidor1.Text = "Sin configurar";
            this.lblA5Servidor1.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblA5Servidor1.WordWrap = true;
            // 
            // lblA5PrimeroTitulo
            // 
            // 
            // 
            // 
            this.lblA5PrimeroTitulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA5PrimeroTitulo.Font = new System.Drawing.Font("Tahoma", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblA5PrimeroTitulo.ForeColor = System.Drawing.Color.Black;
            this.lblA5PrimeroTitulo.Location = new System.Drawing.Point(3, 3);
            this.lblA5PrimeroTitulo.Name = "lblA5PrimeroTitulo";
            this.lblA5PrimeroTitulo.SingleLineColor = System.Drawing.Color.Black;
            this.lblA5PrimeroTitulo.Size = new System.Drawing.Size(214, 22);
            this.lblA5PrimeroTitulo.TabIndex = 28;
            this.lblA5PrimeroTitulo.Text = "Datos del servidor remoto";
            this.lblA5PrimeroTitulo.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblA5PrimeroTitulo.WordWrap = true;
            // 
            // lblA5PrimeroCuerpo
            // 
            // 
            // 
            // 
            this.lblA5PrimeroCuerpo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA5PrimeroCuerpo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA5PrimeroCuerpo.ForeColor = System.Drawing.Color.Black;
            this.lblA5PrimeroCuerpo.Location = new System.Drawing.Point(3, 22);
            this.lblA5PrimeroCuerpo.Name = "lblA5PrimeroCuerpo";
            this.lblA5PrimeroCuerpo.SingleLineColor = System.Drawing.Color.Black;
            this.lblA5PrimeroCuerpo.Size = new System.Drawing.Size(156, 88);
            this.lblA5PrimeroCuerpo.TabIndex = 27;
            this.lblA5PrimeroCuerpo.Text = "Dirección del servidor:\r\n\r\nUsuario:\r\n\r\nDirectorio de archivos:";
            this.lblA5PrimeroCuerpo.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblA5PrimeroCuerpo.WordWrap = true;
            // 
            // btnA5Continuar
            // 
            this.btnA5Continuar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnA5Continuar.BackColor = System.Drawing.Color.Gray;
            this.btnA5Continuar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnA5Continuar.BorderRadius = 0;
            this.btnA5Continuar.ButtonText = "Continuar";
            this.btnA5Continuar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnA5Continuar.DisabledColor = System.Drawing.Color.Gray;
            this.btnA5Continuar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnA5Continuar.Iconimage = global::SyncBD.Properties.Resources.Ok_16px;
            this.btnA5Continuar.Iconimage_right = null;
            this.btnA5Continuar.Iconimage_right_Selected = null;
            this.btnA5Continuar.Iconimage_Selected = null;
            this.btnA5Continuar.IconMarginLeft = 0;
            this.btnA5Continuar.IconMarginRight = 0;
            this.btnA5Continuar.IconRightVisible = true;
            this.btnA5Continuar.IconRightZoom = 0D;
            this.btnA5Continuar.IconVisible = true;
            this.btnA5Continuar.IconZoom = 40D;
            this.btnA5Continuar.IsTab = false;
            this.btnA5Continuar.Location = new System.Drawing.Point(464, 425);
            this.btnA5Continuar.Name = "btnA5Continuar";
            this.btnA5Continuar.Normalcolor = System.Drawing.Color.Gray;
            this.btnA5Continuar.OnHovercolor = System.Drawing.Color.Green;
            this.btnA5Continuar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnA5Continuar.selected = false;
            this.btnA5Continuar.Size = new System.Drawing.Size(124, 43);
            this.btnA5Continuar.TabIndex = 33;
            this.btnA5Continuar.Text = "Continuar";
            this.btnA5Continuar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnA5Continuar.Textcolor = System.Drawing.Color.White;
            this.btnA5Continuar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA5Continuar.Click += new System.EventHandler(this.btnA5Continuar_Click);
            // 
            // lblA5Cuerpo
            // 
            // 
            // 
            // 
            this.lblA5Cuerpo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA5Cuerpo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA5Cuerpo.ForeColor = System.Drawing.Color.Black;
            this.lblA5Cuerpo.Location = new System.Drawing.Point(192, 109);
            this.lblA5Cuerpo.Name = "lblA5Cuerpo";
            this.lblA5Cuerpo.SingleLineColor = System.Drawing.Color.Black;
            this.lblA5Cuerpo.Size = new System.Drawing.Size(396, 54);
            this.lblA5Cuerpo.TabIndex = 32;
            this.lblA5Cuerpo.Text = "Ha configurado exitosamente los ajustes, si desea cambiar los ajustes de configur" +
    "ación nuevamente, dirijase al botón de ajustes del menú principal.";
            this.lblA5Cuerpo.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblA5Cuerpo.WordWrap = true;
            // 
            // lineA5Divisor
            // 
            this.lineA5Divisor.ForeColor = System.Drawing.Color.Black;
            this.lineA5Divisor.Location = new System.Drawing.Point(192, 80);
            this.lineA5Divisor.Name = "lineA5Divisor";
            this.lineA5Divisor.Size = new System.Drawing.Size(396, 23);
            this.lineA5Divisor.TabIndex = 31;
            this.lineA5Divisor.Text = "line1";
            // 
            // lblA5Encabezado
            // 
            // 
            // 
            // 
            this.lblA5Encabezado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA5Encabezado.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblA5Encabezado.ForeColor = System.Drawing.Color.Black;
            this.lblA5Encabezado.Location = new System.Drawing.Point(192, 47);
            this.lblA5Encabezado.Name = "lblA5Encabezado";
            this.lblA5Encabezado.SingleLineColor = System.Drawing.Color.Black;
            this.lblA5Encabezado.Size = new System.Drawing.Size(385, 39);
            this.lblA5Encabezado.TabIndex = 30;
            this.lblA5Encabezado.Text = "Configuraciones guardadas";
            this.lblA5Encabezado.WordWrap = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(186, 450);
            this.panel1.TabIndex = 29;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::SyncBD.Properties.Resources.SyncBD;
            this.pictureBox1.Location = new System.Drawing.Point(4, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.Green;
            this.pnlTitleBar.Controls.Add(this.lblA5Titulo);
            this.pnlTitleBar.Controls.Add(this.pbxMinimizar);
            this.pnlTitleBar.Controls.Add(this.pbxCerrar);
            this.pnlTitleBar.Controls.Add(this.pbxIcono);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(600, 30);
            this.pnlTitleBar.TabIndex = 28;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // lblA5Titulo
            // 
            // 
            // 
            // 
            this.lblA5Titulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA5Titulo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA5Titulo.ForeColor = System.Drawing.Color.White;
            this.lblA5Titulo.Location = new System.Drawing.Point(28, 5);
            this.lblA5Titulo.Name = "lblA5Titulo";
            this.lblA5Titulo.Size = new System.Drawing.Size(508, 20);
            this.lblA5Titulo.TabIndex = 13;
            this.lblA5Titulo.Text = "SyncBD - Asistente de configuración";
            this.lblA5Titulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pbxMinimizar
            // 
            this.pbxMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.pbxMinimizar.Image = global::SyncBD.Properties.Resources.Compress_24px;
            this.pbxMinimizar.Location = new System.Drawing.Point(542, 5);
            this.pbxMinimizar.Name = "pbxMinimizar";
            this.pbxMinimizar.Size = new System.Drawing.Size(20, 20);
            this.pbxMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMinimizar.TabIndex = 3;
            this.pbxMinimizar.TabStop = false;
            this.pbxMinimizar.Click += new System.EventHandler(this.pbxMinimizar_Click);
            // 
            // pbxCerrar
            // 
            this.pbxCerrar.BackColor = System.Drawing.Color.Transparent;
            this.pbxCerrar.Image = global::SyncBD.Properties.Resources.CloseWindow_24px;
            this.pbxCerrar.Location = new System.Drawing.Point(568, 5);
            this.pbxCerrar.Name = "pbxCerrar";
            this.pbxCerrar.Size = new System.Drawing.Size(20, 20);
            this.pbxCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCerrar.TabIndex = 2;
            this.pbxCerrar.TabStop = false;
            this.pbxCerrar.Click += new System.EventHandler(this.pbxCerrar_Click);
            // 
            // pbxIcono
            // 
            this.pbxIcono.BackColor = System.Drawing.Color.Transparent;
            this.pbxIcono.Image = global::SyncBD.Properties.Resources.SyncBDLogo;
            this.pbxIcono.Location = new System.Drawing.Point(6, 8);
            this.pbxIcono.Name = "pbxIcono";
            this.pbxIcono.Size = new System.Drawing.Size(16, 16);
            this.pbxIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxIcono.TabIndex = 1;
            this.pbxIcono.TabStop = false;
            this.pbxIcono.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // FinAsistente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(600, 480);
            this.Controls.Add(this.pnlSegundo);
            this.Controls.Add(this.pnlPrimero);
            this.Controls.Add(this.btnA5Continuar);
            this.Controls.Add(this.lblA5Cuerpo);
            this.Controls.Add(this.lineA5Divisor);
            this.Controls.Add(this.lblA5Encabezado);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlTitleBar);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FinAsistente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asistente de configuración | SyncBD";
            this.pnlSegundo.ResumeLayout(false);
            this.pnlPrimero.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlTitleBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSegundo;
        private DevComponents.DotNetBar.LabelX lblA5Directorio2;
        private DevComponents.DotNetBar.LabelX lblA5Usuario2;
        private DevComponents.DotNetBar.LabelX lblA5Servidor2;
        private DevComponents.DotNetBar.LabelX lblA5SegundoTitulo;
        private DevComponents.DotNetBar.LabelX lblA5SegundoCuerpo;
        private System.Windows.Forms.Panel pnlPrimero;
        private DevComponents.DotNetBar.LabelX lblA5Directorio1;
        private DevComponents.DotNetBar.LabelX lblA5Usuario1;
        private DevComponents.DotNetBar.LabelX lblA5Servidor1;
        private DevComponents.DotNetBar.LabelX lblA5PrimeroTitulo;
        private DevComponents.DotNetBar.LabelX lblA5PrimeroCuerpo;
        private Bunifu.Framework.UI.BunifuFlatButton btnA5Continuar;
        private DevComponents.DotNetBar.LabelX lblA5Cuerpo;
        private DevComponents.DotNetBar.Controls.Line lineA5Divisor;
        private DevComponents.DotNetBar.LabelX lblA5Encabezado;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlTitleBar;
        private DevComponents.DotNetBar.LabelX lblA5Titulo;
        private System.Windows.Forms.PictureBox pbxMinimizar;
        private System.Windows.Forms.PictureBox pbxCerrar;
        private System.Windows.Forms.PictureBox pbxIcono;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
﻿using SyncBD.Controladores;
using SyncDB.Vistas;
using System;
using System.Windows.Forms;
using Logger;
using System.Deployment.Application;
using System.Reflection;

namespace SyncBD.Vistas
{
    public partial class SplashScreen : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        private static SplashScreen singleton;

        public static SplashScreen ObtenerInstancia()
        {
            if(singleton == null || singleton.IsDisposed)
            {
                singleton = new SplashScreen();
            }

            singleton.BringToFront();

            return singleton;
        }

        public string ObtenerVersion
        {
            get
            {
                return ApplicationDeployment.IsNetworkDeployed ? ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString() : Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        private SplashScreen()
        {
            InitializeComponent();

            log.AppendLog("[Inicio] Iniciando SyncBD Versión: " + ObtenerVersion);
        }

        Nucleo nucleo = new Nucleo();

        Seguridad seguridad = new Seguridad();

        ToLog log = new ToLog();

        private void EstablecerSistema()
        {
            try
            {
                Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                if (configuraciones != null)
                {
                    Servidores servidores = new Servidores();

                    if (configuraciones.Remoto.Servidor == "" && seguridad.Desencriptar(configuraciones.Remoto.Combinado) == "True")
                    {
                        log.AppendLog("[Configuración] Faltan ajustes del servidor Remoto");

                        ExtraMessageBox mostrar = ExtraMessageBox.ObtenerInstancia("Parece que no configuró el servidor \"Remoto\"...\n¿Desea ir a la pantalla de configuración?", "SyncBD | Servidor Remoto no configurado", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Info);

                        if (mostrar.ShowDialog() == DialogResult.OK)
                        {
                            mostrar.Dispose();

                            Hide();

                            MenuServidores abrir = MenuServidores.ObtenerInstancia();

                            abrir.Show();
                        }

                        else
                        {
                            mostrar.Dispose();

                            Principal principal = Principal.ObtenerInstancia();

                            Hide();

                            principal.Show();
                        }
                    }

                    if (configuraciones.Local.Servidor == "" && seguridad.Desencriptar(configuraciones.Local.Combinado) == "True")
                    {
                        log.AppendLog("[Configuración] Faltan ajustes del servidor Local");

                        ExtraMessageBox mostrar = ExtraMessageBox.ObtenerInstancia("Parece que no configuró el servidor \"Local\"...\n¿Desea ir a la pantalla de configuración?", "SyncBD | Servidor Local no configurado", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Info);

                        if (mostrar.ShowDialog() == DialogResult.OK)
                        {
                            mostrar.Dispose();

                            Hide();

                            MenuServidores abrir = MenuServidores.ObtenerInstancia();

                            abrir.Show();
                        }

                        else
                        {
                            mostrar.Dispose();

                            Principal principal = Principal.ObtenerInstancia();

                            Hide();

                            principal.Show();
                        }
                    }

                    if (configuraciones.Remoto.Servidor != "" || configuraciones.Local.Servidor != "")
                    {
                        log.AppendLog("[Inicio] Verificaciones realizadas, iniciando pantalla principal");

                        Principal principal = Principal.ObtenerInstancia();

                        Hide();

                        principal.Show();
                    }
                }

                else
                {
                    tmpCambio.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x001, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x001", PopUp.Icono.Error);
            }
        }

        private void tmpCambio_Tick(object sender, EventArgs e)
        {
            Bienvenido mostrar = Bienvenido.ObtenerInstancia();

            Hide();

            tmpCambio.Enabled = false;

            mostrar.ShowDialog();
        }

        private void SplashScreen_Shown(object sender, EventArgs e)
        {
            nucleo.ComprobarRegistros();

            nucleo.ComprobarGeneral();

            nucleo.ComprobarTemas();

            EstablecerSistema();
        }
    }
}

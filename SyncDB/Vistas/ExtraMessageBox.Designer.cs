﻿namespace SyncDB.Vistas
{
    partial class ExtraMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMensaje = new DevComponents.DotNetBar.LabelX();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblTitulo = new DevComponents.DotNetBar.LabelX();
            this.pbxIcono1 = new System.Windows.Forms.PictureBox();
            this.pbxIcono2 = new System.Windows.Forms.PictureBox();
            this.btnCancelar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnReintentar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnContinuar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMensaje
            // 
            // 
            // 
            // 
            this.lblMensaje.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMensaje.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblMensaje.ForeColor = System.Drawing.Color.Black;
            this.lblMensaje.Location = new System.Drawing.Point(89, 42);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.SingleLineColor = System.Drawing.Color.Black;
            this.lblMensaje.Size = new System.Drawing.Size(499, 103);
            this.lblMensaje.TabIndex = 12;
            this.lblMensaje.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblMensaje.WordWrap = true;
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.Green;
            this.pnlTitleBar.Controls.Add(this.lblTitulo);
            this.pnlTitleBar.Controls.Add(this.pbxIcono1);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(600, 30);
            this.pnlTitleBar.TabIndex = 11;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // lblTitulo
            // 
            // 
            // 
            // 
            this.lblTitulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTitulo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(28, 6);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.SingleLineColor = System.Drawing.Color.Black;
            this.lblTitulo.Size = new System.Drawing.Size(560, 20);
            this.lblTitulo.TabIndex = 11;
            this.lblTitulo.WordWrap = true;
            this.lblTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pbxIcono1
            // 
            this.pbxIcono1.BackColor = System.Drawing.Color.Transparent;
            this.pbxIcono1.Location = new System.Drawing.Point(6, 8);
            this.pbxIcono1.Name = "pbxIcono1";
            this.pbxIcono1.Size = new System.Drawing.Size(16, 16);
            this.pbxIcono1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxIcono1.TabIndex = 1;
            this.pbxIcono1.TabStop = false;
            this.pbxIcono1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pbxIcono2
            // 
            this.pbxIcono2.Image = global::SyncBD.Properties.Resources.HighPriority_64px;
            this.pbxIcono2.Location = new System.Drawing.Point(12, 61);
            this.pbxIcono2.Name = "pbxIcono2";
            this.pbxIcono2.Size = new System.Drawing.Size(62, 62);
            this.pbxIcono2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxIcono2.TabIndex = 16;
            this.pbxIcono2.TabStop = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnCancelar.BackColor = System.Drawing.Color.Gray;
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelar.BorderRadius = 0;
            this.btnCancelar.ButtonText = "Cancelar";
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.DisabledColor = System.Drawing.Color.Gray;
            this.btnCancelar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnCancelar.Iconimage = global::SyncBD.Properties.Resources.Cancel_16px;
            this.btnCancelar.Iconimage_right = null;
            this.btnCancelar.Iconimage_right_Selected = null;
            this.btnCancelar.Iconimage_Selected = null;
            this.btnCancelar.IconMarginLeft = 0;
            this.btnCancelar.IconMarginRight = 0;
            this.btnCancelar.IconRightVisible = true;
            this.btnCancelar.IconRightZoom = 0D;
            this.btnCancelar.IconVisible = true;
            this.btnCancelar.IconZoom = 35D;
            this.btnCancelar.IsTab = false;
            this.btnCancelar.Location = new System.Drawing.Point(334, 151);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Normalcolor = System.Drawing.Color.Gray;
            this.btnCancelar.OnHovercolor = System.Drawing.Color.DarkRed;
            this.btnCancelar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnCancelar.selected = false;
            this.btnCancelar.Size = new System.Drawing.Size(124, 43);
            this.btnCancelar.TabIndex = 15;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Textcolor = System.Drawing.Color.White;
            this.btnCancelar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnReintentar
            // 
            this.btnReintentar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnReintentar.BackColor = System.Drawing.Color.Gray;
            this.btnReintentar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReintentar.BorderRadius = 0;
            this.btnReintentar.ButtonText = "Reintentar";
            this.btnReintentar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReintentar.DisabledColor = System.Drawing.Color.Gray;
            this.btnReintentar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnReintentar.Iconimage = global::SyncBD.Properties.Resources.Synchronize_16px;
            this.btnReintentar.Iconimage_right = null;
            this.btnReintentar.Iconimage_right_Selected = null;
            this.btnReintentar.Iconimage_Selected = null;
            this.btnReintentar.IconMarginLeft = 0;
            this.btnReintentar.IconMarginRight = 0;
            this.btnReintentar.IconRightVisible = true;
            this.btnReintentar.IconRightZoom = 0D;
            this.btnReintentar.IconVisible = true;
            this.btnReintentar.IconZoom = 35D;
            this.btnReintentar.IsTab = false;
            this.btnReintentar.Location = new System.Drawing.Point(204, 151);
            this.btnReintentar.Name = "btnReintentar";
            this.btnReintentar.Normalcolor = System.Drawing.Color.Gray;
            this.btnReintentar.OnHovercolor = System.Drawing.Color.Teal;
            this.btnReintentar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnReintentar.selected = false;
            this.btnReintentar.Size = new System.Drawing.Size(124, 43);
            this.btnReintentar.TabIndex = 14;
            this.btnReintentar.Text = "Reintentar";
            this.btnReintentar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReintentar.Textcolor = System.Drawing.Color.White;
            this.btnReintentar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReintentar.Visible = false;
            this.btnReintentar.Click += new System.EventHandler(this.btnReintentar_Click);
            // 
            // btnContinuar
            // 
            this.btnContinuar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnContinuar.BackColor = System.Drawing.Color.Gray;
            this.btnContinuar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnContinuar.BorderRadius = 0;
            this.btnContinuar.ButtonText = "Continuar";
            this.btnContinuar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnContinuar.DisabledColor = System.Drawing.Color.Gray;
            this.btnContinuar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnContinuar.Iconimage = global::SyncBD.Properties.Resources.Ok_16px;
            this.btnContinuar.Iconimage_right = null;
            this.btnContinuar.Iconimage_right_Selected = null;
            this.btnContinuar.Iconimage_Selected = null;
            this.btnContinuar.IconMarginLeft = 0;
            this.btnContinuar.IconMarginRight = 0;
            this.btnContinuar.IconRightVisible = true;
            this.btnContinuar.IconRightZoom = 0D;
            this.btnContinuar.IconVisible = true;
            this.btnContinuar.IconZoom = 40D;
            this.btnContinuar.IsTab = false;
            this.btnContinuar.Location = new System.Drawing.Point(464, 151);
            this.btnContinuar.Name = "btnContinuar";
            this.btnContinuar.Normalcolor = System.Drawing.Color.Gray;
            this.btnContinuar.OnHovercolor = System.Drawing.Color.Green;
            this.btnContinuar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnContinuar.selected = false;
            this.btnContinuar.Size = new System.Drawing.Size(124, 43);
            this.btnContinuar.TabIndex = 13;
            this.btnContinuar.Text = "Continuar";
            this.btnContinuar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnContinuar.Textcolor = System.Drawing.Color.White;
            this.btnContinuar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnContinuar.Click += new System.EventHandler(this.btnContinuar_Click);
            // 
            // ExtraMessageBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(600, 200);
            this.Controls.Add(this.pbxIcono2);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnReintentar);
            this.Controls.Add(this.btnContinuar);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.pnlTitleBar);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ExtraMessageBox";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ExtraMessageBox";
            this.pnlTitleBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuFlatButton btnCancelar;
        private Bunifu.Framework.UI.BunifuFlatButton btnReintentar;
        private Bunifu.Framework.UI.BunifuFlatButton btnContinuar;
        private DevComponents.DotNetBar.LabelX lblMensaje;
        private System.Windows.Forms.Panel pnlTitleBar;
        private DevComponents.DotNetBar.LabelX lblTitulo;
        private System.Windows.Forms.PictureBox pbxIcono1;
        private System.Windows.Forms.PictureBox pbxIcono2;
    }
}
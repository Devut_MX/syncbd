﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SyncDB.Vistas
{
    public partial class AyudaConfig : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int Param);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private static AyudaConfig singleton;

        public static AyudaConfig ObtenerInstancia()
        {
            if (singleton == null || singleton.IsDisposed)
            {
                singleton = new AyudaConfig();
            }

            singleton.BringToFront();

            return singleton;
        }

        private AyudaConfig()
        {
            InitializeComponent();

            cbxAH2Opcion.SelectedIndex = 0;
        }

        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void pbxMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void pbxCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAH2Regresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbxAH2Opcion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxAH2Opcion.SelectedIndex == 0)
            {
                lblAH2Cuerpo2.Text = "En el campo <b>\"Servidor\"</b>, debe indicar la dirección ftp que apunte al servidor, de forma local o remota.<br/><br/>SyncBD permite el uso de cuatro tipos de formatos válidos, ejemplos:<br/><br/><u>ftp.server.com</u><br/><u>192.168.1.100 (IP)</u><br/><u>localhost ó 127.0.0.1 <b>(Solo para conexiones locales)</b></u>";
            }

            if (cbxAH2Opcion.SelectedIndex == 1)
            {
                lblAH2Cuerpo2.Text = "El usuario y la contraseña son imprescindibles para acceder al servidor, ya que previene el acceso a personas no autorizadas.\n\nEl usuario y la contraseña se deben proporcionar por el administrador del servidor, por lo regular son palabras o un conjunto de letras con símbolos.\n\nSyncBD cuenta con la función \"Anónimo\", evitando escribir el usuario y la contraseña, pero tenga en cuenta que el servidor deberá permitir accesos anónimos.";
            }

            if (cbxAH2Opcion.SelectedIndex == 2)
            {
                lblAH2Cuerpo2.Text = "El número de puerto, es el \"punto de entrada\" hacia el servidor, por defecto los servidores utilizan el número 21 en FTP y el 22 en SFTP, pero puede variar.\n\nSi desconoce el número de puerto de su conexión, simplemente escriba un 0 en el campo y SyncBD tratará de encontrar el número del puerto automáticamente, tenga en cuenta que esta función automática puede ocasionar que se tarde más en conectar al servidor.";
            }

            if (cbxAH2Opcion.SelectedIndex == 3)
            {
                lblAH2Cuerpo2.Text = "Necesita indicar un directorio, ya que a partir de esa dirección se descargarán o subirán archivos.\n\nEn el servidor remoto deberá indicar la ruta desde donde quiere que se descargue la información.\n\nEn el servidor local deberá indicar la ruta hacia donde quiere que se cargue la información.\n\nSino escribe ningún directorio, SyncBD descargará o cargará todas las carpetas y archivos desde o hacia el servidor indicado.";
            }

            if(cbxAH2Opcion.SelectedIndex == 4)
            {
                lblAH2Cuerpo2.Text = "La encriptación es un requermiento de configuración cuando el servidor al que intenta conectarse cuenta con configuraciones extras de seguridad.\n\nPuede seleccionar \"Implicita\" en donde SyncBD agregará datos de conexión TLS/SSL a la conexión o puede seleccionar \"Explicita\", en donde SyncBD espera algún cifrado en TLS/SSL.";
            }

            if (cbxAH2Opcion.SelectedIndex == 5)
            {
                lblAH2Cuerpo2.Text = "Varios proveedores de servicios FTP agregan certificados de autenticidad para validar la conexión con sus sistemas internos, en ciertas ocasiones es necesario quitar la autenticación que proporcione el servidor para poder establecer conexión.";
            }

            if (cbxAH2Opcion.SelectedIndex == 6)
            {
                lblAH2Cuerpo2.Text = "Cuando acepta utilizar el modo \"Activo\", SyncBD permite utilizar un protocolo TCP al establecer la conexión de archivos FTP, en donde el programa empieza a \"escuchar\" desde cualquier puerto disponible para informar al servidor desde que puerto se establecerá la conexión. (Se usa en ocasiones especificas)";
            }

            if (cbxAH2Opcion.SelectedIndex == 7)
            {
                lblAH2Cuerpo2.Text = "El protocolo es el tipo de conexión que tendra SyncBD con el servidor.\n\nEl cambio entre protocolos no implica cambios en la velocidad de transferencia, pero si influye en la seguridad al transferir archivos ya que el protocolo \"SFTP\" permite la transferencia por medio de \"SSH\" (Secure SHell) que mantiene un certificado en su equipo asegurando que se conecta siempre al mismo servidor.";
            }
        }
    }
}

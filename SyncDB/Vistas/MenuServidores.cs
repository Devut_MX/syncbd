﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Logger;
using SyncBD.Controladores;

namespace SyncDB.Vistas
{
    public partial class MenuServidores : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int Param);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private static MenuServidores singleton;

        public static MenuServidores ObtenerInstancia()
        {
            if (singleton == null || singleton.IsDisposed)
            {
                singleton = new MenuServidores();
            }

            singleton.BringToFront();

            return singleton;
        }

        private MenuServidores()
        {
            InitializeComponent();
        }

        ToLog log = new ToLog();

        Nucleo nucleo = new Nucleo();

        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void pbxMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void pbxCerrar_Click(object sender, EventArgs e)
        {
            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿Desea cancelar las configuraciones y cerrar el programa?\nRecuerde que el asistente empezará nuevamente al iniciar el programa", "SyncDB | Confirme cierre", ExtraMessageBox.Boton.OkCancel, ExtraMessageBox.Icono.Info);

            if (DialogResult.OK == mensaje.ShowDialog())
            {
                log.AppendLog("[Configuraciones] El usuario cerró el programa");

                Application.Exit();
            }

            mensaje.Dispose();
        }

        private void btnA2Ayuda_Click(object sender, EventArgs e)
        {
            AyudaServidores mostrar = AyudaServidores.ObtenerInstancia();

            Hide();

            mostrar.ShowDialog();

            Show();

            mostrar.Dispose();
        }

        private void btnA2Cancelar_Click(object sender, EventArgs e)
        {
            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿Desea cancelar las configuraciones y cerrar el programa?\nRecuerde que el asistente empezará nuevamente al iniciar el programa", "SyncBD | Confirme cierre", ExtraMessageBox.Boton.OkCancel, ExtraMessageBox.Icono.Info);

            if (DialogResult.OK == mensaje.ShowDialog())
            {
                log.AppendLog("[Configuraciones] El usuario cerró el programa");

                Application.Exit();
            }

            mensaje.Dispose();
        }

        private void chkA2Remoto_OnValueChange(object sender, EventArgs e)
        {
            if(chkA2Remoto.Value)
            {
                chkA2Local.Value = false;
                chkA2Ambos.Value = false;
            }
        }

        private void chkA2Local_OnValueChange(object sender, EventArgs e)
        {
            if (chkA2Local.Value)
            {
                chkA2Remoto.Value = false;
                chkA2Ambos.Value = false;
            }
        }

        private void chkA2Ambos_OnValueChange(object sender, EventArgs e)
        {
            if (chkA2Ambos.Value)
            {
                chkA2Remoto.Value = false;
                chkA2Local.Value = false;
            }
        }

        private void btnA2Continuar_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkA2Remoto.Value == false && chkA2Local.Value == false && chkA2Ambos.Value == false)
                {
                    ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("Por favor seleccione al menos una opción para poder continuar con la configuración", "SyncBD | Se requiere configuración", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Warning);

                    mensaje.ShowDialog();

                    mensaje.Dispose();

                    chkA2Remoto.Focus();
                }

                else
                {
                    if (chkA2Remoto.Value)
                    {
                        log.AppendLog("[Configuraciones] Mostrando ventana de configuración del servidor Remoto");

                        ConfigRemoto mostrar = ConfigRemoto.ObtenerInstancia();

                        Hide();

                        if (mostrar.ShowDialog() == DialogResult.OK)
                        {
                            mostrar.Dispose();

                            FinAsistente finalizar = FinAsistente.ObtenerInstancia(FinAsistente.Visualizacion.Remoto);

                            finalizar.ShowDialog();
                        }

                        else
                        {
                            Show();

                            mostrar.Dispose();

                            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("Para poder continuar es necesario que configure al menos un servidor", "SyncBD | Se requiere configuración", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Warning);

                            mensaje.ShowDialog();

                            mensaje.Dispose();
                        }
                    }

                    if (chkA2Local.Value)
                    {
                        log.AppendLog("[Configuraciones] Mostrando ventana de configuración del servidor Local");

                        ConfigLocal mostrar = ConfigLocal.ObtenerInstancia();

                        Hide();

                        if (mostrar.ShowDialog() == DialogResult.OK)
                        {
                            mostrar.Dispose();

                            FinAsistente finalizar = FinAsistente.ObtenerInstancia(FinAsistente.Visualizacion.Local);

                            finalizar.ShowDialog();
                        }

                        else
                        {
                            Show();

                            mostrar.Dispose();

                            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("Para poder continuar es necesario que configure al menos un servidor", "SyncBD | Se requiere configuración", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Warning);

                            mensaje.ShowDialog();

                            mensaje.Dispose();
                        }
                    }

                    if (chkA2Ambos.Value)
                    {
                        log.AppendLog("[Configuraciones] Mostrando ventanas de configuración");

                        ConfigRemoto mostrarRemoto = ConfigRemoto.ObtenerInstancia();

                        Hide();

                        DialogResult remoto;

                        DialogResult local;

                        if (mostrarRemoto.ShowDialog() == DialogResult.OK)
                        {
                            remoto = DialogResult.OK;

                            mostrarRemoto.Dispose();
                        }

                        else
                        {
                            remoto = DialogResult.Cancel;

                            mostrarRemoto.Dispose();
                        }

                        ConfigLocal mostrarLocal = ConfigLocal.ObtenerInstancia();

                        if (mostrarLocal.ShowDialog() == DialogResult.OK)
                        {
                            local = DialogResult.OK;

                            mostrarLocal.Dispose();
                        }

                        else
                        {
                            local = DialogResult.Cancel;

                            mostrarLocal.Dispose();
                        }

                        if (local == DialogResult.OK || remoto == DialogResult.OK)
                        {
                            FinAsistente finalizar = FinAsistente.ObtenerInstancia(FinAsistente.Visualizacion.Ambos);

                            finalizar.ShowDialog();
                        }

                        else
                        {
                            Show();

                            log.AppendLog("[Configuraciones] El usuario no configuró ningún servidor, imposible continuar");

                            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("Para poder continuar es necesario que configure al menos un servidor", "SyncBD | Se requiere configuración", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Warning);

                            mensaje.ShowDialog();

                            mensaje.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x002, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x002", PopUp.Icono.Error);
            }
        }
    }
}

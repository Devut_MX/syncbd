﻿namespace SyncBD.Vistas
{
    partial class Temas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Temas));
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblTTitulo = new DevComponents.DotNetBar.LabelX();
            this.pbxMinimizar = new System.Windows.Forms.PictureBox();
            this.pbxCerrar = new System.Windows.Forms.PictureBox();
            this.pbxIcono = new System.Windows.Forms.PictureBox();
            this.pnlTemas = new System.Windows.Forms.Panel();
            this.btnTBorrarTema = new Bunifu.Framework.UI.BunifuFlatButton();
            this.txtTNombreTema = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.lblTNombreTema = new DevComponents.DotNetBar.LabelX();
            this.colorFondoBotones = new DevComponents.DotNetBar.ColorPickers.ColorCombControl();
            this.colorTextoBotones = new DevComponents.DotNetBar.ColorPickers.ColorCombControl();
            this.colorTextoVentana = new DevComponents.DotNetBar.ColorPickers.ColorCombControl();
            this.colorFondoVentana = new DevComponents.DotNetBar.ColorPickers.ColorCombControl();
            this.colorTextoTitulo = new DevComponents.DotNetBar.ColorPickers.ColorCombControl();
            this.colorFondoTitulo = new DevComponents.DotNetBar.ColorPickers.ColorCombControl();
            this.labelX23 = new DevComponents.DotNetBar.LabelX();
            this.btnTTextoVentana = new Bunifu.Framework.UI.BunifuFlatButton();
            this.labelX24 = new DevComponents.DotNetBar.LabelX();
            this.cbxTTemaDisponible = new System.Windows.Forms.ComboBox();
            this.labelX22 = new DevComponents.DotNetBar.LabelX();
            this.btnTAyuda = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnTCancelarTema = new Bunifu.Framework.UI.BunifuFlatButton();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.btnTFondoBotones = new Bunifu.Framework.UI.BunifuFlatButton();
            this.labelX15 = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.btnTTextoBotones = new Bunifu.Framework.UI.BunifuFlatButton();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.btnTFondoVentana = new Bunifu.Framework.UI.BunifuFlatButton();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.btnTTextoTitulo = new Bunifu.Framework.UI.BunifuFlatButton();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.btnTFondoTitulo = new Bunifu.Framework.UI.BunifuFlatButton();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.btnTGuardarTema = new Bunifu.Framework.UI.BunifuFlatButton();
            this.lblSREncabezado1 = new DevComponents.DotNetBar.LabelX();
            this.lineSRDivisor = new DevComponents.DotNetBar.Controls.Line();
            this.lblSREncabezado = new DevComponents.DotNetBar.LabelX();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).BeginInit();
            this.pnlTemas.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.Green;
            this.pnlTitleBar.Controls.Add(this.lblTTitulo);
            this.pnlTitleBar.Controls.Add(this.pbxMinimizar);
            this.pnlTitleBar.Controls.Add(this.pbxCerrar);
            this.pnlTitleBar.Controls.Add(this.pbxIcono);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(800, 30);
            this.pnlTitleBar.TabIndex = 3;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // lblTTitulo
            // 
            // 
            // 
            // 
            this.lblTTitulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTTitulo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblTTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTTitulo.Location = new System.Drawing.Point(28, 7);
            this.lblTTitulo.Name = "lblTTitulo";
            this.lblTTitulo.Size = new System.Drawing.Size(708, 20);
            this.lblTTitulo.TabIndex = 14;
            this.lblTTitulo.Text = "SyncBD - Sincroniza tus servidores de datos";
            this.lblTTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pbxMinimizar
            // 
            this.pbxMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.pbxMinimizar.Image = global::SyncBD.Properties.Resources.Compress_24px;
            this.pbxMinimizar.Location = new System.Drawing.Point(742, 5);
            this.pbxMinimizar.Name = "pbxMinimizar";
            this.pbxMinimizar.Size = new System.Drawing.Size(20, 20);
            this.pbxMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMinimizar.TabIndex = 3;
            this.pbxMinimizar.TabStop = false;
            this.pbxMinimizar.Visible = false;
            this.pbxMinimizar.Click += new System.EventHandler(this.pbxMinimizar_Click);
            // 
            // pbxCerrar
            // 
            this.pbxCerrar.BackColor = System.Drawing.Color.Transparent;
            this.pbxCerrar.Image = global::SyncBD.Properties.Resources.CloseWindow_24px;
            this.pbxCerrar.Location = new System.Drawing.Point(768, 5);
            this.pbxCerrar.Name = "pbxCerrar";
            this.pbxCerrar.Size = new System.Drawing.Size(20, 20);
            this.pbxCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCerrar.TabIndex = 2;
            this.pbxCerrar.TabStop = false;
            this.pbxCerrar.Click += new System.EventHandler(this.pbxCerrar_Click);
            // 
            // pbxIcono
            // 
            this.pbxIcono.BackColor = System.Drawing.Color.Transparent;
            this.pbxIcono.Image = global::SyncBD.Properties.Resources.SyncBDLogo;
            this.pbxIcono.Location = new System.Drawing.Point(6, 8);
            this.pbxIcono.Name = "pbxIcono";
            this.pbxIcono.Size = new System.Drawing.Size(16, 16);
            this.pbxIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxIcono.TabIndex = 1;
            this.pbxIcono.TabStop = false;
            this.pbxIcono.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pnlTemas
            // 
            this.pnlTemas.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlTemas.Controls.Add(this.btnTBorrarTema);
            this.pnlTemas.Controls.Add(this.txtTNombreTema);
            this.pnlTemas.Controls.Add(this.lblTNombreTema);
            this.pnlTemas.Controls.Add(this.colorFondoBotones);
            this.pnlTemas.Controls.Add(this.colorTextoBotones);
            this.pnlTemas.Controls.Add(this.colorTextoVentana);
            this.pnlTemas.Controls.Add(this.colorFondoVentana);
            this.pnlTemas.Controls.Add(this.colorTextoTitulo);
            this.pnlTemas.Controls.Add(this.colorFondoTitulo);
            this.pnlTemas.Controls.Add(this.labelX23);
            this.pnlTemas.Controls.Add(this.btnTTextoVentana);
            this.pnlTemas.Controls.Add(this.labelX24);
            this.pnlTemas.Controls.Add(this.cbxTTemaDisponible);
            this.pnlTemas.Controls.Add(this.labelX22);
            this.pnlTemas.Controls.Add(this.btnTAyuda);
            this.pnlTemas.Controls.Add(this.btnTCancelarTema);
            this.pnlTemas.Controls.Add(this.labelX14);
            this.pnlTemas.Controls.Add(this.btnTFondoBotones);
            this.pnlTemas.Controls.Add(this.labelX15);
            this.pnlTemas.Controls.Add(this.labelX11);
            this.pnlTemas.Controls.Add(this.btnTTextoBotones);
            this.pnlTemas.Controls.Add(this.labelX12);
            this.pnlTemas.Controls.Add(this.labelX8);
            this.pnlTemas.Controls.Add(this.btnTFondoVentana);
            this.pnlTemas.Controls.Add(this.labelX9);
            this.pnlTemas.Controls.Add(this.labelX5);
            this.pnlTemas.Controls.Add(this.btnTTextoTitulo);
            this.pnlTemas.Controls.Add(this.labelX6);
            this.pnlTemas.Controls.Add(this.labelX2);
            this.pnlTemas.Controls.Add(this.btnTFondoTitulo);
            this.pnlTemas.Controls.Add(this.labelX1);
            this.pnlTemas.Controls.Add(this.btnTGuardarTema);
            this.pnlTemas.Controls.Add(this.lblSREncabezado1);
            this.pnlTemas.Controls.Add(this.lineSRDivisor);
            this.pnlTemas.Controls.Add(this.lblSREncabezado);
            this.pnlTemas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTemas.Location = new System.Drawing.Point(0, 30);
            this.pnlTemas.Name = "pnlTemas";
            this.pnlTemas.Size = new System.Drawing.Size(800, 570);
            this.pnlTemas.TabIndex = 8;
            // 
            // btnTBorrarTema
            // 
            this.btnTBorrarTema.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnTBorrarTema.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTBorrarTema.BackColor = System.Drawing.Color.Gray;
            this.btnTBorrarTema.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTBorrarTema.BorderRadius = 0;
            this.btnTBorrarTema.ButtonText = "Borrar tema";
            this.btnTBorrarTema.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTBorrarTema.DisabledColor = System.Drawing.Color.Gray;
            this.btnTBorrarTema.Iconcolor = System.Drawing.Color.Transparent;
            this.btnTBorrarTema.Iconimage = global::SyncBD.Properties.Resources.Cancel2_16px;
            this.btnTBorrarTema.Iconimage_right = null;
            this.btnTBorrarTema.Iconimage_right_Selected = null;
            this.btnTBorrarTema.Iconimage_Selected = null;
            this.btnTBorrarTema.IconMarginLeft = 0;
            this.btnTBorrarTema.IconMarginRight = 0;
            this.btnTBorrarTema.IconRightVisible = true;
            this.btnTBorrarTema.IconRightZoom = 0D;
            this.btnTBorrarTema.IconVisible = true;
            this.btnTBorrarTema.IconZoom = 40D;
            this.btnTBorrarTema.IsTab = false;
            this.btnTBorrarTema.Location = new System.Drawing.Point(476, 515);
            this.btnTBorrarTema.Name = "btnTBorrarTema";
            this.btnTBorrarTema.Normalcolor = System.Drawing.Color.Gray;
            this.btnTBorrarTema.OnHovercolor = System.Drawing.Color.DarkRed;
            this.btnTBorrarTema.OnHoverTextColor = System.Drawing.Color.White;
            this.btnTBorrarTema.selected = false;
            this.btnTBorrarTema.Size = new System.Drawing.Size(153, 43);
            this.btnTBorrarTema.TabIndex = 10;
            this.btnTBorrarTema.Text = "Borrar tema";
            this.btnTBorrarTema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnTBorrarTema.Textcolor = System.Drawing.Color.White;
            this.btnTBorrarTema.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTBorrarTema.Visible = false;
            this.btnTBorrarTema.Click += new System.EventHandler(this.btnTBorrarTema_Click);
            // 
            // txtTNombreTema
            // 
            this.txtTNombreTema.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtTNombreTema.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTNombreTema.ForeColor = System.Drawing.Color.Black;
            this.txtTNombreTema.HintForeColor = System.Drawing.Color.Empty;
            this.txtTNombreTema.HintText = "";
            this.txtTNombreTema.isPassword = false;
            this.txtTNombreTema.LineFocusedColor = System.Drawing.Color.Blue;
            this.txtTNombreTema.LineIdleColor = System.Drawing.Color.Gray;
            this.txtTNombreTema.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.txtTNombreTema.LineThickness = 2;
            this.txtTNombreTema.Location = new System.Drawing.Point(545, 106);
            this.txtTNombreTema.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtTNombreTema.Name = "txtTNombreTema";
            this.txtTNombreTema.Size = new System.Drawing.Size(242, 27);
            this.txtTNombreTema.TabIndex = 2;
            this.txtTNombreTema.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtTNombreTema.Visible = false;
            // 
            // lblTNombreTema
            // 
            this.lblTNombreTema.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTNombreTema.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblTNombreTema.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTNombreTema.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblTNombreTema.ForeColor = System.Drawing.Color.Black;
            this.lblTNombreTema.Location = new System.Drawing.Point(382, 113);
            this.lblTNombreTema.Name = "lblTNombreTema";
            this.lblTNombreTema.SingleLineColor = System.Drawing.Color.Black;
            this.lblTNombreTema.Size = new System.Drawing.Size(156, 20);
            this.lblTNombreTema.TabIndex = 85;
            this.lblTNombreTema.Text = "Nombre del tema:";
            this.lblTNombreTema.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblTNombreTema.Visible = false;
            this.lblTNombreTema.WordWrap = true;
            // 
            // colorFondoBotones
            // 
            this.colorFondoBotones.BackColor = System.Drawing.Color.Transparent;
            this.colorFondoBotones.Location = new System.Drawing.Point(485, 386);
            this.colorFondoBotones.Name = "colorFondoBotones";
            this.colorFondoBotones.Size = new System.Drawing.Size(75, 23);
            this.colorFondoBotones.TabIndex = 84;
            this.colorFondoBotones.Text = "colorCombControl5";
            // 
            // colorTextoBotones
            // 
            this.colorTextoBotones.BackColor = System.Drawing.Color.Transparent;
            this.colorTextoBotones.Location = new System.Drawing.Point(485, 435);
            this.colorTextoBotones.Name = "colorTextoBotones";
            this.colorTextoBotones.Size = new System.Drawing.Size(75, 23);
            this.colorTextoBotones.TabIndex = 83;
            this.colorTextoBotones.Text = "colorCombControl4";
            // 
            // colorTextoVentana
            // 
            this.colorTextoVentana.BackColor = System.Drawing.Color.Transparent;
            this.colorTextoVentana.Location = new System.Drawing.Point(485, 337);
            this.colorTextoVentana.Name = "colorTextoVentana";
            this.colorTextoVentana.Size = new System.Drawing.Size(75, 23);
            this.colorTextoVentana.TabIndex = 82;
            this.colorTextoVentana.Text = "colorCombControl3";
            // 
            // colorFondoVentana
            // 
            this.colorFondoVentana.BackColor = System.Drawing.Color.Transparent;
            this.colorFondoVentana.Location = new System.Drawing.Point(485, 288);
            this.colorFondoVentana.Name = "colorFondoVentana";
            this.colorFondoVentana.Size = new System.Drawing.Size(75, 23);
            this.colorFondoVentana.TabIndex = 81;
            this.colorFondoVentana.Text = "colorCombControl2";
            // 
            // colorTextoTitulo
            // 
            this.colorTextoTitulo.BackColor = System.Drawing.Color.Transparent;
            this.colorTextoTitulo.Location = new System.Drawing.Point(485, 238);
            this.colorTextoTitulo.Name = "colorTextoTitulo";
            this.colorTextoTitulo.Size = new System.Drawing.Size(75, 23);
            this.colorTextoTitulo.TabIndex = 80;
            this.colorTextoTitulo.Text = "colorCombControl1";
            // 
            // colorFondoTitulo
            // 
            this.colorFondoTitulo.BackColor = System.Drawing.Color.Transparent;
            this.colorFondoTitulo.Location = new System.Drawing.Point(485, 189);
            this.colorFondoTitulo.Name = "colorFondoTitulo";
            this.colorFondoTitulo.Size = new System.Drawing.Size(75, 23);
            this.colorFondoTitulo.TabIndex = 79;
            this.colorFondoTitulo.Text = "colorCombControl1";
            // 
            // labelX23
            // 
            this.labelX23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX23.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX23.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelX23.ForeColor = System.Drawing.Color.Black;
            this.labelX23.Location = new System.Drawing.Point(382, 340);
            this.labelX23.Name = "labelX23";
            this.labelX23.SingleLineColor = System.Drawing.Color.Black;
            this.labelX23.Size = new System.Drawing.Size(97, 20);
            this.labelX23.TabIndex = 77;
            this.labelX23.Text = "Color actual:";
            this.labelX23.WordWrap = true;
            // 
            // btnTTextoVentana
            // 
            this.btnTTextoVentana.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnTTextoVentana.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTTextoVentana.BackColor = System.Drawing.Color.Gray;
            this.btnTTextoVentana.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTTextoVentana.BorderRadius = 0;
            this.btnTTextoVentana.ButtonText = "Color";
            this.btnTTextoVentana.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTTextoVentana.DisabledColor = System.Drawing.Color.Gray;
            this.btnTTextoVentana.Iconcolor = System.Drawing.Color.Transparent;
            this.btnTTextoVentana.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnTTextoVentana.Iconimage")));
            this.btnTTextoVentana.Iconimage_right = null;
            this.btnTTextoVentana.Iconimage_right_Selected = null;
            this.btnTTextoVentana.Iconimage_Selected = null;
            this.btnTTextoVentana.IconMarginLeft = 0;
            this.btnTTextoVentana.IconMarginRight = 0;
            this.btnTTextoVentana.IconRightVisible = true;
            this.btnTTextoVentana.IconRightZoom = 0D;
            this.btnTTextoVentana.IconVisible = true;
            this.btnTTextoVentana.IconZoom = 40D;
            this.btnTTextoVentana.IsTab = false;
            this.btnTTextoVentana.Location = new System.Drawing.Point(243, 333);
            this.btnTTextoVentana.Name = "btnTTextoVentana";
            this.btnTTextoVentana.Normalcolor = System.Drawing.Color.Gray;
            this.btnTTextoVentana.OnHovercolor = System.Drawing.Color.DarkCyan;
            this.btnTTextoVentana.OnHoverTextColor = System.Drawing.Color.White;
            this.btnTTextoVentana.selected = false;
            this.btnTTextoVentana.Size = new System.Drawing.Size(119, 34);
            this.btnTTextoVentana.TabIndex = 6;
            this.btnTTextoVentana.Text = "Color";
            this.btnTTextoVentana.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnTTextoVentana.Textcolor = System.Drawing.Color.White;
            this.btnTTextoVentana.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTTextoVentana.Click += new System.EventHandler(this.btnTTextoVentana_Click);
            // 
            // labelX24
            // 
            this.labelX24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX24.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX24.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelX24.ForeColor = System.Drawing.Color.Black;
            this.labelX24.Location = new System.Drawing.Point(30, 340);
            this.labelX24.Name = "labelX24";
            this.labelX24.SingleLineColor = System.Drawing.Color.Black;
            this.labelX24.Size = new System.Drawing.Size(207, 20);
            this.labelX24.TabIndex = 75;
            this.labelX24.Text = "Texto de la ventana:";
            this.labelX24.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX24.WordWrap = true;
            // 
            // cbxTTemaDisponible
            // 
            this.cbxTTemaDisponible.BackColor = System.Drawing.Color.SeaGreen;
            this.cbxTTemaDisponible.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxTTemaDisponible.DropDownHeight = 120;
            this.cbxTTemaDisponible.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTTemaDisponible.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxTTemaDisponible.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbxTTemaDisponible.ForeColor = System.Drawing.Color.White;
            this.cbxTTemaDisponible.FormattingEnabled = true;
            this.cbxTTemaDisponible.IntegralHeight = false;
            this.cbxTTemaDisponible.Location = new System.Drawing.Point(184, 111);
            this.cbxTTemaDisponible.Name = "cbxTTemaDisponible";
            this.cbxTTemaDisponible.Size = new System.Drawing.Size(178, 27);
            this.cbxTTemaDisponible.TabIndex = 1;
            this.cbxTTemaDisponible.SelectedIndexChanged += new System.EventHandler(this.cbxTTemaDisponible_SelectedIndexChanged);
            // 
            // labelX22
            // 
            this.labelX22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX22.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX22.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelX22.ForeColor = System.Drawing.Color.Black;
            this.labelX22.Location = new System.Drawing.Point(36, 113);
            this.labelX22.Name = "labelX22";
            this.labelX22.SingleLineColor = System.Drawing.Color.Black;
            this.labelX22.Size = new System.Drawing.Size(142, 20);
            this.labelX22.TabIndex = 55;
            this.labelX22.Text = "Temas disponibles:";
            this.labelX22.WordWrap = true;
            // 
            // btnTAyuda
            // 
            this.btnTAyuda.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnTAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTAyuda.BackColor = System.Drawing.Color.Gray;
            this.btnTAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTAyuda.BorderRadius = 0;
            this.btnTAyuda.ButtonText = "Ayuda";
            this.btnTAyuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTAyuda.DisabledColor = System.Drawing.Color.Gray;
            this.btnTAyuda.Iconcolor = System.Drawing.Color.Transparent;
            this.btnTAyuda.Iconimage = global::SyncBD.Properties.Resources.Help_16px;
            this.btnTAyuda.Iconimage_right = null;
            this.btnTAyuda.Iconimage_right_Selected = null;
            this.btnTAyuda.Iconimage_Selected = null;
            this.btnTAyuda.IconMarginLeft = 0;
            this.btnTAyuda.IconMarginRight = 0;
            this.btnTAyuda.IconRightVisible = true;
            this.btnTAyuda.IconRightZoom = 0D;
            this.btnTAyuda.IconVisible = true;
            this.btnTAyuda.IconZoom = 40D;
            this.btnTAyuda.IsTab = false;
            this.btnTAyuda.Location = new System.Drawing.Point(12, 515);
            this.btnTAyuda.Name = "btnTAyuda";
            this.btnTAyuda.Normalcolor = System.Drawing.Color.Gray;
            this.btnTAyuda.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.btnTAyuda.OnHoverTextColor = System.Drawing.Color.White;
            this.btnTAyuda.selected = false;
            this.btnTAyuda.Size = new System.Drawing.Size(153, 43);
            this.btnTAyuda.TabIndex = 46;
            this.btnTAyuda.Text = "Ayuda";
            this.btnTAyuda.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnTAyuda.Textcolor = System.Drawing.Color.White;
            this.btnTAyuda.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTAyuda.Visible = false;
            // 
            // btnTCancelarTema
            // 
            this.btnTCancelarTema.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnTCancelarTema.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTCancelarTema.BackColor = System.Drawing.Color.Gray;
            this.btnTCancelarTema.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTCancelarTema.BorderRadius = 0;
            this.btnTCancelarTema.ButtonText = "Cancelar edición";
            this.btnTCancelarTema.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTCancelarTema.DisabledColor = System.Drawing.Color.Gray;
            this.btnTCancelarTema.Iconcolor = System.Drawing.Color.Transparent;
            this.btnTCancelarTema.Iconimage = global::SyncBD.Properties.Resources.Cancel_16px;
            this.btnTCancelarTema.Iconimage_right = null;
            this.btnTCancelarTema.Iconimage_right_Selected = null;
            this.btnTCancelarTema.Iconimage_Selected = null;
            this.btnTCancelarTema.IconMarginLeft = 0;
            this.btnTCancelarTema.IconMarginRight = 0;
            this.btnTCancelarTema.IconRightVisible = true;
            this.btnTCancelarTema.IconRightZoom = 0D;
            this.btnTCancelarTema.IconVisible = true;
            this.btnTCancelarTema.IconZoom = 40D;
            this.btnTCancelarTema.IsTab = false;
            this.btnTCancelarTema.Location = new System.Drawing.Point(317, 515);
            this.btnTCancelarTema.Name = "btnTCancelarTema";
            this.btnTCancelarTema.Normalcolor = System.Drawing.Color.Gray;
            this.btnTCancelarTema.OnHovercolor = System.Drawing.Color.DarkRed;
            this.btnTCancelarTema.OnHoverTextColor = System.Drawing.Color.White;
            this.btnTCancelarTema.selected = false;
            this.btnTCancelarTema.Size = new System.Drawing.Size(153, 43);
            this.btnTCancelarTema.TabIndex = 9;
            this.btnTCancelarTema.Text = "Cancelar edición";
            this.btnTCancelarTema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnTCancelarTema.Textcolor = System.Drawing.Color.White;
            this.btnTCancelarTema.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTCancelarTema.Click += new System.EventHandler(this.btnTCancelarTema_Click);
            // 
            // labelX14
            // 
            this.labelX14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX14.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.Location = new System.Drawing.Point(382, 389);
            this.labelX14.Name = "labelX14";
            this.labelX14.SingleLineColor = System.Drawing.Color.Black;
            this.labelX14.Size = new System.Drawing.Size(97, 20);
            this.labelX14.TabIndex = 43;
            this.labelX14.Text = "Color actual:";
            this.labelX14.WordWrap = true;
            // 
            // btnTFondoBotones
            // 
            this.btnTFondoBotones.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnTFondoBotones.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTFondoBotones.BackColor = System.Drawing.Color.Gray;
            this.btnTFondoBotones.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTFondoBotones.BorderRadius = 0;
            this.btnTFondoBotones.ButtonText = "Color";
            this.btnTFondoBotones.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTFondoBotones.DisabledColor = System.Drawing.Color.Gray;
            this.btnTFondoBotones.Iconcolor = System.Drawing.Color.Transparent;
            this.btnTFondoBotones.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnTFondoBotones.Iconimage")));
            this.btnTFondoBotones.Iconimage_right = null;
            this.btnTFondoBotones.Iconimage_right_Selected = null;
            this.btnTFondoBotones.Iconimage_Selected = null;
            this.btnTFondoBotones.IconMarginLeft = 0;
            this.btnTFondoBotones.IconMarginRight = 0;
            this.btnTFondoBotones.IconRightVisible = true;
            this.btnTFondoBotones.IconRightZoom = 0D;
            this.btnTFondoBotones.IconVisible = true;
            this.btnTFondoBotones.IconZoom = 40D;
            this.btnTFondoBotones.IsTab = false;
            this.btnTFondoBotones.Location = new System.Drawing.Point(243, 382);
            this.btnTFondoBotones.Name = "btnTFondoBotones";
            this.btnTFondoBotones.Normalcolor = System.Drawing.Color.Gray;
            this.btnTFondoBotones.OnHovercolor = System.Drawing.Color.DarkCyan;
            this.btnTFondoBotones.OnHoverTextColor = System.Drawing.Color.White;
            this.btnTFondoBotones.selected = false;
            this.btnTFondoBotones.Size = new System.Drawing.Size(119, 34);
            this.btnTFondoBotones.TabIndex = 7;
            this.btnTFondoBotones.Text = "Color";
            this.btnTFondoBotones.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnTFondoBotones.Textcolor = System.Drawing.Color.White;
            this.btnTFondoBotones.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTFondoBotones.Click += new System.EventHandler(this.btnTFondoAyuda_Click);
            // 
            // labelX15
            // 
            this.labelX15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX15.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX15.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelX15.ForeColor = System.Drawing.Color.Black;
            this.labelX15.Location = new System.Drawing.Point(30, 389);
            this.labelX15.Name = "labelX15";
            this.labelX15.SingleLineColor = System.Drawing.Color.Black;
            this.labelX15.Size = new System.Drawing.Size(207, 20);
            this.labelX15.TabIndex = 41;
            this.labelX15.Text = "Fondo botones selección:";
            this.labelX15.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX15.WordWrap = true;
            // 
            // labelX11
            // 
            this.labelX11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX11.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.Location = new System.Drawing.Point(382, 438);
            this.labelX11.Name = "labelX11";
            this.labelX11.SingleLineColor = System.Drawing.Color.Black;
            this.labelX11.Size = new System.Drawing.Size(97, 20);
            this.labelX11.TabIndex = 39;
            this.labelX11.Text = "Color actual:";
            this.labelX11.WordWrap = true;
            // 
            // btnTTextoBotones
            // 
            this.btnTTextoBotones.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnTTextoBotones.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTTextoBotones.BackColor = System.Drawing.Color.Gray;
            this.btnTTextoBotones.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTTextoBotones.BorderRadius = 0;
            this.btnTTextoBotones.ButtonText = "Color";
            this.btnTTextoBotones.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTTextoBotones.DisabledColor = System.Drawing.Color.Gray;
            this.btnTTextoBotones.Iconcolor = System.Drawing.Color.Transparent;
            this.btnTTextoBotones.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnTTextoBotones.Iconimage")));
            this.btnTTextoBotones.Iconimage_right = null;
            this.btnTTextoBotones.Iconimage_right_Selected = null;
            this.btnTTextoBotones.Iconimage_Selected = null;
            this.btnTTextoBotones.IconMarginLeft = 0;
            this.btnTTextoBotones.IconMarginRight = 0;
            this.btnTTextoBotones.IconRightVisible = true;
            this.btnTTextoBotones.IconRightZoom = 0D;
            this.btnTTextoBotones.IconVisible = true;
            this.btnTTextoBotones.IconZoom = 40D;
            this.btnTTextoBotones.IsTab = false;
            this.btnTTextoBotones.Location = new System.Drawing.Point(243, 431);
            this.btnTTextoBotones.Name = "btnTTextoBotones";
            this.btnTTextoBotones.Normalcolor = System.Drawing.Color.Gray;
            this.btnTTextoBotones.OnHovercolor = System.Drawing.Color.DarkCyan;
            this.btnTTextoBotones.OnHoverTextColor = System.Drawing.Color.White;
            this.btnTTextoBotones.selected = false;
            this.btnTTextoBotones.Size = new System.Drawing.Size(119, 34);
            this.btnTTextoBotones.TabIndex = 8;
            this.btnTTextoBotones.Text = "Color";
            this.btnTTextoBotones.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnTTextoBotones.Textcolor = System.Drawing.Color.White;
            this.btnTTextoBotones.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTTextoBotones.Click += new System.EventHandler(this.btnTTextoBotones_Click);
            // 
            // labelX12
            // 
            this.labelX12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX12.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.Location = new System.Drawing.Point(30, 438);
            this.labelX12.Name = "labelX12";
            this.labelX12.SingleLineColor = System.Drawing.Color.Black;
            this.labelX12.Size = new System.Drawing.Size(207, 20);
            this.labelX12.TabIndex = 37;
            this.labelX12.Text = "Texto de los botones:";
            this.labelX12.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX12.WordWrap = true;
            // 
            // labelX8
            // 
            this.labelX8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX8.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(382, 291);
            this.labelX8.Name = "labelX8";
            this.labelX8.SingleLineColor = System.Drawing.Color.Black;
            this.labelX8.Size = new System.Drawing.Size(97, 20);
            this.labelX8.TabIndex = 35;
            this.labelX8.Text = "Color actual:";
            this.labelX8.WordWrap = true;
            // 
            // btnTFondoVentana
            // 
            this.btnTFondoVentana.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnTFondoVentana.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTFondoVentana.BackColor = System.Drawing.Color.Gray;
            this.btnTFondoVentana.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTFondoVentana.BorderRadius = 0;
            this.btnTFondoVentana.ButtonText = "Color";
            this.btnTFondoVentana.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTFondoVentana.DisabledColor = System.Drawing.Color.Gray;
            this.btnTFondoVentana.Iconcolor = System.Drawing.Color.Transparent;
            this.btnTFondoVentana.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnTFondoVentana.Iconimage")));
            this.btnTFondoVentana.Iconimage_right = null;
            this.btnTFondoVentana.Iconimage_right_Selected = null;
            this.btnTFondoVentana.Iconimage_Selected = null;
            this.btnTFondoVentana.IconMarginLeft = 0;
            this.btnTFondoVentana.IconMarginRight = 0;
            this.btnTFondoVentana.IconRightVisible = true;
            this.btnTFondoVentana.IconRightZoom = 0D;
            this.btnTFondoVentana.IconVisible = true;
            this.btnTFondoVentana.IconZoom = 40D;
            this.btnTFondoVentana.IsTab = false;
            this.btnTFondoVentana.Location = new System.Drawing.Point(243, 284);
            this.btnTFondoVentana.Name = "btnTFondoVentana";
            this.btnTFondoVentana.Normalcolor = System.Drawing.Color.Gray;
            this.btnTFondoVentana.OnHovercolor = System.Drawing.Color.DarkCyan;
            this.btnTFondoVentana.OnHoverTextColor = System.Drawing.Color.White;
            this.btnTFondoVentana.selected = false;
            this.btnTFondoVentana.Size = new System.Drawing.Size(119, 34);
            this.btnTFondoVentana.TabIndex = 5;
            this.btnTFondoVentana.Text = "Color";
            this.btnTFondoVentana.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnTFondoVentana.Textcolor = System.Drawing.Color.White;
            this.btnTFondoVentana.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTFondoVentana.Click += new System.EventHandler(this.btnTFondoVentana_Click);
            // 
            // labelX9
            // 
            this.labelX9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX9.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelX9.ForeColor = System.Drawing.Color.Black;
            this.labelX9.Location = new System.Drawing.Point(30, 291);
            this.labelX9.Name = "labelX9";
            this.labelX9.SingleLineColor = System.Drawing.Color.Black;
            this.labelX9.Size = new System.Drawing.Size(207, 20);
            this.labelX9.TabIndex = 33;
            this.labelX9.Text = "Fondo de la ventana:";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX9.WordWrap = true;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX5.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(382, 241);
            this.labelX5.Name = "labelX5";
            this.labelX5.SingleLineColor = System.Drawing.Color.Black;
            this.labelX5.Size = new System.Drawing.Size(97, 20);
            this.labelX5.TabIndex = 31;
            this.labelX5.Text = "Color actual:";
            this.labelX5.WordWrap = true;
            // 
            // btnTTextoTitulo
            // 
            this.btnTTextoTitulo.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnTTextoTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTTextoTitulo.BackColor = System.Drawing.Color.Gray;
            this.btnTTextoTitulo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTTextoTitulo.BorderRadius = 0;
            this.btnTTextoTitulo.ButtonText = "Color";
            this.btnTTextoTitulo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTTextoTitulo.DisabledColor = System.Drawing.Color.Gray;
            this.btnTTextoTitulo.Iconcolor = System.Drawing.Color.Transparent;
            this.btnTTextoTitulo.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnTTextoTitulo.Iconimage")));
            this.btnTTextoTitulo.Iconimage_right = null;
            this.btnTTextoTitulo.Iconimage_right_Selected = null;
            this.btnTTextoTitulo.Iconimage_Selected = null;
            this.btnTTextoTitulo.IconMarginLeft = 0;
            this.btnTTextoTitulo.IconMarginRight = 0;
            this.btnTTextoTitulo.IconRightVisible = true;
            this.btnTTextoTitulo.IconRightZoom = 0D;
            this.btnTTextoTitulo.IconVisible = true;
            this.btnTTextoTitulo.IconZoom = 40D;
            this.btnTTextoTitulo.IsTab = false;
            this.btnTTextoTitulo.Location = new System.Drawing.Point(243, 234);
            this.btnTTextoTitulo.Name = "btnTTextoTitulo";
            this.btnTTextoTitulo.Normalcolor = System.Drawing.Color.Gray;
            this.btnTTextoTitulo.OnHovercolor = System.Drawing.Color.DarkCyan;
            this.btnTTextoTitulo.OnHoverTextColor = System.Drawing.Color.White;
            this.btnTTextoTitulo.selected = false;
            this.btnTTextoTitulo.Size = new System.Drawing.Size(119, 34);
            this.btnTTextoTitulo.TabIndex = 4;
            this.btnTTextoTitulo.Text = "Color";
            this.btnTTextoTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnTTextoTitulo.Textcolor = System.Drawing.Color.White;
            this.btnTTextoTitulo.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTTextoTitulo.Click += new System.EventHandler(this.btnTTextoTitulo_Click);
            // 
            // labelX6
            // 
            this.labelX6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX6.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(30, 241);
            this.labelX6.Name = "labelX6";
            this.labelX6.SingleLineColor = System.Drawing.Color.Black;
            this.labelX6.Size = new System.Drawing.Size(207, 20);
            this.labelX6.TabIndex = 29;
            this.labelX6.Text = "Texto del título:";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX6.WordWrap = true;
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(382, 192);
            this.labelX2.Name = "labelX2";
            this.labelX2.SingleLineColor = System.Drawing.Color.Black;
            this.labelX2.Size = new System.Drawing.Size(97, 20);
            this.labelX2.TabIndex = 27;
            this.labelX2.Text = "Color actual:";
            this.labelX2.WordWrap = true;
            // 
            // btnTFondoTitulo
            // 
            this.btnTFondoTitulo.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnTFondoTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTFondoTitulo.BackColor = System.Drawing.Color.Gray;
            this.btnTFondoTitulo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTFondoTitulo.BorderRadius = 0;
            this.btnTFondoTitulo.ButtonText = "Color";
            this.btnTFondoTitulo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTFondoTitulo.DisabledColor = System.Drawing.Color.Gray;
            this.btnTFondoTitulo.Iconcolor = System.Drawing.Color.Transparent;
            this.btnTFondoTitulo.Iconimage = global::SyncBD.Properties.Resources.RGBCircle1_16px;
            this.btnTFondoTitulo.Iconimage_right = null;
            this.btnTFondoTitulo.Iconimage_right_Selected = null;
            this.btnTFondoTitulo.Iconimage_Selected = null;
            this.btnTFondoTitulo.IconMarginLeft = 0;
            this.btnTFondoTitulo.IconMarginRight = 0;
            this.btnTFondoTitulo.IconRightVisible = true;
            this.btnTFondoTitulo.IconRightZoom = 0D;
            this.btnTFondoTitulo.IconVisible = true;
            this.btnTFondoTitulo.IconZoom = 40D;
            this.btnTFondoTitulo.IsTab = false;
            this.btnTFondoTitulo.Location = new System.Drawing.Point(243, 185);
            this.btnTFondoTitulo.Name = "btnTFondoTitulo";
            this.btnTFondoTitulo.Normalcolor = System.Drawing.Color.Gray;
            this.btnTFondoTitulo.OnHovercolor = System.Drawing.Color.DarkCyan;
            this.btnTFondoTitulo.OnHoverTextColor = System.Drawing.Color.White;
            this.btnTFondoTitulo.selected = false;
            this.btnTFondoTitulo.Size = new System.Drawing.Size(119, 34);
            this.btnTFondoTitulo.TabIndex = 3;
            this.btnTFondoTitulo.Text = "Color";
            this.btnTFondoTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnTFondoTitulo.Textcolor = System.Drawing.Color.White;
            this.btnTFondoTitulo.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTFondoTitulo.Click += new System.EventHandler(this.btnTFondoTitulo_Click);
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(30, 192);
            this.labelX1.Name = "labelX1";
            this.labelX1.SingleLineColor = System.Drawing.Color.Black;
            this.labelX1.Size = new System.Drawing.Size(207, 20);
            this.labelX1.TabIndex = 25;
            this.labelX1.Text = "Barra de título:";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            this.labelX1.WordWrap = true;
            // 
            // btnTGuardarTema
            // 
            this.btnTGuardarTema.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnTGuardarTema.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTGuardarTema.BackColor = System.Drawing.Color.Gray;
            this.btnTGuardarTema.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTGuardarTema.BorderRadius = 0;
            this.btnTGuardarTema.ButtonText = "Guardar tema";
            this.btnTGuardarTema.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTGuardarTema.DisabledColor = System.Drawing.Color.Gray;
            this.btnTGuardarTema.Iconcolor = System.Drawing.Color.Transparent;
            this.btnTGuardarTema.Iconimage = global::SyncBD.Properties.Resources.Ok_16px;
            this.btnTGuardarTema.Iconimage_right = null;
            this.btnTGuardarTema.Iconimage_right_Selected = null;
            this.btnTGuardarTema.Iconimage_Selected = null;
            this.btnTGuardarTema.IconMarginLeft = 0;
            this.btnTGuardarTema.IconMarginRight = 0;
            this.btnTGuardarTema.IconRightVisible = true;
            this.btnTGuardarTema.IconRightZoom = 0D;
            this.btnTGuardarTema.IconVisible = true;
            this.btnTGuardarTema.IconZoom = 40D;
            this.btnTGuardarTema.IsTab = false;
            this.btnTGuardarTema.Location = new System.Drawing.Point(635, 515);
            this.btnTGuardarTema.Name = "btnTGuardarTema";
            this.btnTGuardarTema.Normalcolor = System.Drawing.Color.Gray;
            this.btnTGuardarTema.OnHovercolor = System.Drawing.Color.Green;
            this.btnTGuardarTema.OnHoverTextColor = System.Drawing.Color.White;
            this.btnTGuardarTema.selected = false;
            this.btnTGuardarTema.Size = new System.Drawing.Size(153, 43);
            this.btnTGuardarTema.TabIndex = 11;
            this.btnTGuardarTema.Text = "Guardar tema";
            this.btnTGuardarTema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnTGuardarTema.Textcolor = System.Drawing.Color.White;
            this.btnTGuardarTema.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTGuardarTema.Click += new System.EventHandler(this.btnTGuardarTema_Click);
            // 
            // lblSREncabezado1
            // 
            this.lblSREncabezado1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSREncabezado1.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSREncabezado1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSREncabezado1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblSREncabezado1.ForeColor = System.Drawing.Color.Black;
            this.lblSREncabezado1.Location = new System.Drawing.Point(6, 59);
            this.lblSREncabezado1.Name = "lblSREncabezado1";
            this.lblSREncabezado1.SingleLineColor = System.Drawing.Color.Black;
            this.lblSREncabezado1.Size = new System.Drawing.Size(782, 20);
            this.lblSREncabezado1.TabIndex = 9;
            this.lblSREncabezado1.Text = "Selección de color:";
            this.lblSREncabezado1.WordWrap = true;
            // 
            // lineSRDivisor
            // 
            this.lineSRDivisor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lineSRDivisor.ForeColor = System.Drawing.Color.Black;
            this.lineSRDivisor.Location = new System.Drawing.Point(6, 39);
            this.lineSRDivisor.Name = "lineSRDivisor";
            this.lineSRDivisor.Size = new System.Drawing.Size(782, 23);
            this.lineSRDivisor.TabIndex = 7;
            this.lineSRDivisor.Text = "line3";
            // 
            // lblSREncabezado
            // 
            this.lblSREncabezado.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSREncabezado.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.lblSREncabezado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblSREncabezado.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblSREncabezado.ForeColor = System.Drawing.Color.Black;
            this.lblSREncabezado.Location = new System.Drawing.Point(6, 6);
            this.lblSREncabezado.Name = "lblSREncabezado";
            this.lblSREncabezado.SingleLineColor = System.Drawing.Color.Black;
            this.lblSREncabezado.Size = new System.Drawing.Size(782, 39);
            this.lblSREncabezado.TabIndex = 6;
            this.lblSREncabezado.Text = "Personalización visual de SyncBD";
            this.lblSREncabezado.WordWrap = true;
            // 
            // Temas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.pnlTemas);
            this.Controls.Add(this.pnlTitleBar);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Temas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Temas";
            this.pnlTitleBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).EndInit();
            this.pnlTemas.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTitleBar;
        private DevComponents.DotNetBar.LabelX lblTTitulo;
        private System.Windows.Forms.PictureBox pbxMinimizar;
        private System.Windows.Forms.PictureBox pbxCerrar;
        private System.Windows.Forms.PictureBox pbxIcono;
        private System.Windows.Forms.Panel pnlTemas;
        private DevComponents.DotNetBar.LabelX labelX1;
        private Bunifu.Framework.UI.BunifuFlatButton btnTGuardarTema;
        private DevComponents.DotNetBar.LabelX lblSREncabezado1;
        private DevComponents.DotNetBar.Controls.Line lineSRDivisor;
        private DevComponents.DotNetBar.LabelX lblSREncabezado;
        private DevComponents.DotNetBar.LabelX labelX2;
        private Bunifu.Framework.UI.BunifuFlatButton btnTFondoTitulo;
        private DevComponents.DotNetBar.LabelX labelX14;
        private Bunifu.Framework.UI.BunifuFlatButton btnTFondoBotones;
        private DevComponents.DotNetBar.LabelX labelX15;
        private DevComponents.DotNetBar.LabelX labelX11;
        private Bunifu.Framework.UI.BunifuFlatButton btnTTextoBotones;
        private DevComponents.DotNetBar.LabelX labelX12;
        private DevComponents.DotNetBar.LabelX labelX8;
        private Bunifu.Framework.UI.BunifuFlatButton btnTFondoVentana;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX5;
        private Bunifu.Framework.UI.BunifuFlatButton btnTTextoTitulo;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX22;
        private Bunifu.Framework.UI.BunifuFlatButton btnTAyuda;
        private Bunifu.Framework.UI.BunifuFlatButton btnTCancelarTema;
        private System.Windows.Forms.ComboBox cbxTTemaDisponible;
        private DevComponents.DotNetBar.LabelX labelX23;
        private Bunifu.Framework.UI.BunifuFlatButton btnTTextoVentana;
        private DevComponents.DotNetBar.LabelX labelX24;
        private DevComponents.DotNetBar.ColorPickers.ColorCombControl colorFondoTitulo;
        private DevComponents.DotNetBar.ColorPickers.ColorCombControl colorFondoBotones;
        private DevComponents.DotNetBar.ColorPickers.ColorCombControl colorTextoBotones;
        private DevComponents.DotNetBar.ColorPickers.ColorCombControl colorTextoVentana;
        private DevComponents.DotNetBar.ColorPickers.ColorCombControl colorFondoVentana;
        private DevComponents.DotNetBar.ColorPickers.ColorCombControl colorTextoTitulo;
        private DevComponents.DotNetBar.LabelX lblTNombreTema;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtTNombreTema;
        private Bunifu.Framework.UI.BunifuFlatButton btnTBorrarTema;
    }
}
﻿namespace SyncDB.Vistas
{
    partial class Bienvenido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Bienvenido));
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblA1Titulo = new DevComponents.DotNetBar.LabelX();
            this.pbxMinimizar = new System.Windows.Forms.PictureBox();
            this.pbxCerrar = new System.Windows.Forms.PictureBox();
            this.pbxIcono = new System.Windows.Forms.PictureBox();
            this.pnlA1Banner = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lineA1Divisor = new DevComponents.DotNetBar.Controls.Line();
            this.lblA1Cuerpo = new DevComponents.DotNetBar.LabelX();
            this.lblA1Encabezado = new DevComponents.DotNetBar.LabelX();
            this.lblA1Confirmacion = new DevComponents.DotNetBar.LabelX();
            this.btnA1Cancelar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnA1Continuar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).BeginInit();
            this.pnlA1Banner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.Green;
            this.pnlTitleBar.Controls.Add(this.lblA1Titulo);
            this.pnlTitleBar.Controls.Add(this.pbxMinimizar);
            this.pnlTitleBar.Controls.Add(this.pbxCerrar);
            this.pnlTitleBar.Controls.Add(this.pbxIcono);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(600, 30);
            this.pnlTitleBar.TabIndex = 1;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // lblA1Titulo
            // 
            // 
            // 
            // 
            this.lblA1Titulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA1Titulo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA1Titulo.ForeColor = System.Drawing.Color.White;
            this.lblA1Titulo.Location = new System.Drawing.Point(28, 6);
            this.lblA1Titulo.Name = "lblA1Titulo";
            this.lblA1Titulo.Size = new System.Drawing.Size(508, 20);
            this.lblA1Titulo.TabIndex = 9;
            this.lblA1Titulo.Text = "SyncBD - Asistente de configuración";
            this.lblA1Titulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pbxMinimizar
            // 
            this.pbxMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.pbxMinimizar.Image = global::SyncBD.Properties.Resources.Compress_24px;
            this.pbxMinimizar.Location = new System.Drawing.Point(542, 5);
            this.pbxMinimizar.Name = "pbxMinimizar";
            this.pbxMinimizar.Size = new System.Drawing.Size(20, 20);
            this.pbxMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMinimizar.TabIndex = 3;
            this.pbxMinimizar.TabStop = false;
            this.pbxMinimizar.Click += new System.EventHandler(this.pbxMinimizar_Click);
            // 
            // pbxCerrar
            // 
            this.pbxCerrar.BackColor = System.Drawing.Color.Transparent;
            this.pbxCerrar.Image = global::SyncBD.Properties.Resources.CloseWindow_24px;
            this.pbxCerrar.Location = new System.Drawing.Point(568, 5);
            this.pbxCerrar.Name = "pbxCerrar";
            this.pbxCerrar.Size = new System.Drawing.Size(20, 20);
            this.pbxCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCerrar.TabIndex = 2;
            this.pbxCerrar.TabStop = false;
            this.pbxCerrar.Click += new System.EventHandler(this.pbxCerrar_Click);
            // 
            // pbxIcono
            // 
            this.pbxIcono.BackColor = System.Drawing.Color.Transparent;
            this.pbxIcono.Image = global::SyncBD.Properties.Resources.SyncBDLogo;
            this.pbxIcono.Location = new System.Drawing.Point(6, 8);
            this.pbxIcono.Name = "pbxIcono";
            this.pbxIcono.Size = new System.Drawing.Size(16, 16);
            this.pbxIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxIcono.TabIndex = 1;
            this.pbxIcono.TabStop = false;
            this.pbxIcono.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pnlA1Banner
            // 
            this.pnlA1Banner.Controls.Add(this.pictureBox1);
            this.pnlA1Banner.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlA1Banner.Location = new System.Drawing.Point(0, 30);
            this.pnlA1Banner.Name = "pnlA1Banner";
            this.pnlA1Banner.Size = new System.Drawing.Size(186, 450);
            this.pnlA1Banner.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::SyncBD.Properties.Resources.SyncBD;
            this.pictureBox1.Location = new System.Drawing.Point(4, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lineA1Divisor
            // 
            this.lineA1Divisor.ForeColor = System.Drawing.Color.Black;
            this.lineA1Divisor.Location = new System.Drawing.Point(192, 80);
            this.lineA1Divisor.Name = "lineA1Divisor";
            this.lineA1Divisor.Size = new System.Drawing.Size(396, 23);
            this.lineA1Divisor.TabIndex = 8;
            this.lineA1Divisor.Text = "line1";
            // 
            // lblA1Cuerpo
            // 
            // 
            // 
            // 
            this.lblA1Cuerpo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA1Cuerpo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA1Cuerpo.ForeColor = System.Drawing.Color.Black;
            this.lblA1Cuerpo.Location = new System.Drawing.Point(192, 109);
            this.lblA1Cuerpo.Name = "lblA1Cuerpo";
            this.lblA1Cuerpo.SingleLineColor = System.Drawing.Color.White;
            this.lblA1Cuerpo.Size = new System.Drawing.Size(396, 151);
            this.lblA1Cuerpo.TabIndex = 7;
            this.lblA1Cuerpo.Text = resources.GetString("lblA1Cuerpo.Text");
            this.lblA1Cuerpo.WordWrap = true;
            // 
            // lblA1Encabezado
            // 
            // 
            // 
            // 
            this.lblA1Encabezado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA1Encabezado.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblA1Encabezado.ForeColor = System.Drawing.Color.Black;
            this.lblA1Encabezado.Location = new System.Drawing.Point(192, 47);
            this.lblA1Encabezado.Name = "lblA1Encabezado";
            this.lblA1Encabezado.SingleLineColor = System.Drawing.Color.Black;
            this.lblA1Encabezado.Size = new System.Drawing.Size(385, 39);
            this.lblA1Encabezado.TabIndex = 6;
            this.lblA1Encabezado.Text = "Bienvenido al asistente de configuración";
            this.lblA1Encabezado.WordWrap = true;
            // 
            // lblA1Confirmacion
            // 
            // 
            // 
            // 
            this.lblA1Confirmacion.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA1Confirmacion.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA1Confirmacion.ForeColor = System.Drawing.Color.Black;
            this.lblA1Confirmacion.Location = new System.Drawing.Point(192, 394);
            this.lblA1Confirmacion.Name = "lblA1Confirmacion";
            this.lblA1Confirmacion.SingleLineColor = System.Drawing.Color.White;
            this.lblA1Confirmacion.Size = new System.Drawing.Size(396, 25);
            this.lblA1Confirmacion.TabIndex = 11;
            this.lblA1Confirmacion.Text = "Haga clic en <b>\"Continuar\"</b> si de sea seguir con la configuración.";
            this.lblA1Confirmacion.WordWrap = true;
            // 
            // btnA1Cancelar
            // 
            this.btnA1Cancelar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnA1Cancelar.BackColor = System.Drawing.Color.Gray;
            this.btnA1Cancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnA1Cancelar.BorderRadius = 0;
            this.btnA1Cancelar.ButtonText = "Cancelar";
            this.btnA1Cancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnA1Cancelar.DisabledColor = System.Drawing.Color.Gray;
            this.btnA1Cancelar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnA1Cancelar.Iconimage = global::SyncBD.Properties.Resources.Cancel_16px;
            this.btnA1Cancelar.Iconimage_right = null;
            this.btnA1Cancelar.Iconimage_right_Selected = null;
            this.btnA1Cancelar.Iconimage_Selected = null;
            this.btnA1Cancelar.IconMarginLeft = 0;
            this.btnA1Cancelar.IconMarginRight = 0;
            this.btnA1Cancelar.IconRightVisible = true;
            this.btnA1Cancelar.IconRightZoom = 0D;
            this.btnA1Cancelar.IconVisible = true;
            this.btnA1Cancelar.IconZoom = 35D;
            this.btnA1Cancelar.IsTab = false;
            this.btnA1Cancelar.Location = new System.Drawing.Point(334, 425);
            this.btnA1Cancelar.Name = "btnA1Cancelar";
            this.btnA1Cancelar.Normalcolor = System.Drawing.Color.Gray;
            this.btnA1Cancelar.OnHovercolor = System.Drawing.Color.DarkRed;
            this.btnA1Cancelar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnA1Cancelar.selected = false;
            this.btnA1Cancelar.Size = new System.Drawing.Size(124, 43);
            this.btnA1Cancelar.TabIndex = 10;
            this.btnA1Cancelar.Text = "Cancelar";
            this.btnA1Cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnA1Cancelar.Textcolor = System.Drawing.Color.White;
            this.btnA1Cancelar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA1Cancelar.Click += new System.EventHandler(this.btnA1Cancelar_Click);
            // 
            // btnA1Continuar
            // 
            this.btnA1Continuar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnA1Continuar.BackColor = System.Drawing.Color.Gray;
            this.btnA1Continuar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnA1Continuar.BorderRadius = 0;
            this.btnA1Continuar.ButtonText = "Continuar";
            this.btnA1Continuar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnA1Continuar.DisabledColor = System.Drawing.Color.Gray;
            this.btnA1Continuar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnA1Continuar.Iconimage = global::SyncBD.Properties.Resources.Ok_16px;
            this.btnA1Continuar.Iconimage_right = null;
            this.btnA1Continuar.Iconimage_right_Selected = null;
            this.btnA1Continuar.Iconimage_Selected = null;
            this.btnA1Continuar.IconMarginLeft = 0;
            this.btnA1Continuar.IconMarginRight = 0;
            this.btnA1Continuar.IconRightVisible = true;
            this.btnA1Continuar.IconRightZoom = 0D;
            this.btnA1Continuar.IconVisible = true;
            this.btnA1Continuar.IconZoom = 40D;
            this.btnA1Continuar.IsTab = false;
            this.btnA1Continuar.Location = new System.Drawing.Point(464, 425);
            this.btnA1Continuar.Name = "btnA1Continuar";
            this.btnA1Continuar.Normalcolor = System.Drawing.Color.Gray;
            this.btnA1Continuar.OnHovercolor = System.Drawing.Color.Green;
            this.btnA1Continuar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnA1Continuar.selected = false;
            this.btnA1Continuar.Size = new System.Drawing.Size(124, 43);
            this.btnA1Continuar.TabIndex = 9;
            this.btnA1Continuar.Text = "Continuar";
            this.btnA1Continuar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnA1Continuar.Textcolor = System.Drawing.Color.White;
            this.btnA1Continuar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA1Continuar.Click += new System.EventHandler(this.btnA1Continuar_Click);
            // 
            // Bienvenido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(600, 480);
            this.Controls.Add(this.lblA1Confirmacion);
            this.Controls.Add(this.btnA1Cancelar);
            this.Controls.Add(this.btnA1Continuar);
            this.Controls.Add(this.lineA1Divisor);
            this.Controls.Add(this.lblA1Cuerpo);
            this.Controls.Add(this.lblA1Encabezado);
            this.Controls.Add(this.pnlA1Banner);
            this.Controls.Add(this.pnlTitleBar);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Bienvenido";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asistente de configuración | SyncBD";
            this.pnlTitleBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).EndInit();
            this.pnlA1Banner.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTitleBar;
        private DevComponents.DotNetBar.LabelX lblA1Titulo;
        private System.Windows.Forms.PictureBox pbxMinimizar;
        private System.Windows.Forms.PictureBox pbxCerrar;
        private System.Windows.Forms.PictureBox pbxIcono;
        private System.Windows.Forms.Panel pnlA1Banner;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.Controls.Line lineA1Divisor;
        private DevComponents.DotNetBar.LabelX lblA1Cuerpo;
        private DevComponents.DotNetBar.LabelX lblA1Encabezado;
        private Bunifu.Framework.UI.BunifuFlatButton btnA1Cancelar;
        private Bunifu.Framework.UI.BunifuFlatButton btnA1Continuar;
        private DevComponents.DotNetBar.LabelX lblA1Confirmacion;
    }
}
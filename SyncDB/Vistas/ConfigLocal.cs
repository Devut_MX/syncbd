﻿using SyncBD.Controladores;
using SyncBD.Vistas;
using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Logger;

namespace SyncDB.Vistas
{
    public partial class ConfigLocal : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int Param);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private static ConfigLocal singleton;

        public static ConfigLocal ObtenerInstancia()
        {
            if (singleton == null || singleton.IsDisposed)
            {
                singleton = new ConfigLocal();
            }

            singleton.BringToFront();

            return singleton;
        }

        private ConfigLocal()
        {
            InitializeComponent();

            cbxA4Encriptacion.SelectedIndex = 0;

            cbxA4Protocolo.SelectedIndex = 0;

            LeerConfiguracionesPrevias();
        }

        ToLog log = new ToLog();

        Nucleo nucleo = new Nucleo();

        Seguridad seguridad = new Seguridad();

        Servidores servidores = new Servidores();

        public static bool combinado = false;

        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void pbxMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void LeerConfiguracionesPrevias()
        {
            try
            {
                Servidores configuraciones = nucleo.LeerConfiguracionServidores();

                if (configuraciones != null)
                {
                    if (configuraciones.Local.Servidor != null)
                    {
                        txtA4Servidor.Text = configuraciones.Local.Servidor != "" ? seguridad.Desencriptar(configuraciones.Local.Servidor) : "";
                        txtA4Usuario.Text = "";
                        txtA4Password.Text = "";
                        //txtA4Usuario.Text = seguridad.Desencriptar(configuraciones.Local.Usuario);
                        //txtA4Password.Text = seguridad.Desencriptar(configuraciones.Local.Password);
                        txtA4Puerto.Text = configuraciones.Local.Puerto != "" ? seguridad.Desencriptar(configuraciones.Local.Puerto) : "";
                        txtA4Directorio.Text = configuraciones.Local.Directorio != "" ? seguridad.Desencriptar(configuraciones.Local.Directorio) : "";
                        cbxA4Encriptacion.SelectedIndex = configuraciones.Local.Encriptacion != "" ? Convert.ToInt32(seguridad.Desencriptar(configuraciones.Local.Encriptacion)) : 0;
                        cbxA4Protocolo.SelectedItem = configuraciones.Local.Protocolo != "" ? seguridad.Desencriptar(configuraciones.Local.Protocolo) : "FTP";

                        if (seguridad.Desencriptar(configuraciones.Local.Anonimo) == "True")
                        {
                            chkA4Anonimo.Value = true;
                        }

                        if (seguridad.Desencriptar(configuraciones.Local.Modo) == "True")
                        {
                            chkA4Modo.Value = true;
                        }

                        if (seguridad.Desencriptar(configuraciones.Local.Autenticar) == "True")
                        {
                            chkA4Autenticar.Value = true;
                        }

                        if (seguridad.Desencriptar(configuraciones.Local.Combinado) == "True")
                        {
                            combinado = true;
                        }
                    }

                    log.AppendLog("[Configuraciones] Ajustes del servidor local cargados correctamente");
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x003, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x003", PopUp.Icono.Error);
            }
        }



        private void pbxCerrar_Click(object sender, EventArgs e)
        {
            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿Desea cancelar la configuración del servidor local?\nNo se guardará ningún ajuste realizado en está sección", "SyncBD | Confirme cierre", ExtraMessageBox.Boton.OkCancel, ExtraMessageBox.Icono.Info);

            if (DialogResult.OK == mensaje.ShowDialog())
            {
                mensaje.Dispose();

                DialogResult = DialogResult.Cancel;

                log.AppendLog("[Configuraciones] El usuario cancelo la configuración");

                Close();
            }

            mensaje.Dispose();
        }

        private void pbxA4Ver_MouseDown(object sender, MouseEventArgs e)
        {
            txtA4Password.isPassword = false;
            pbxA4Ver.Image = SyncBD.Properties.Resources.Unlock_32px;
        }

        private void pbxA4Ver_MouseUp(object sender, MouseEventArgs e)
        {
            txtA4Password.isPassword = true;
            pbxA4Ver.Image = SyncBD.Properties.Resources.Lock_32px;
        }

        private void btnA4Ayuda_Click(object sender, EventArgs e)
        {
            AyudaConfig mostrar = AyudaConfig.ObtenerInstancia();

            Hide();

            mostrar.ShowDialog();

            Show();

            mostrar.Dispose();
        }

        private void btnA4Cancelar_Click(object sender, EventArgs e)
        {
            ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿Desea cancelar la configuración del servidor local?\nNo se guardará ningún ajuste realizado en está sección", "SyncBD | Confirme cierre", ExtraMessageBox.Boton.OkCancel, ExtraMessageBox.Icono.Info);

            if (DialogResult.OK == mensaje.ShowDialog())
            {
                mensaje.Dispose();

                DialogResult = DialogResult.Cancel;

                log.AppendLog("[Configuraciones] El usuario cancelo la configuración");

                Close();
            }

            mensaje.Dispose();
        }

        private void btnA4Continuar_Click(object sender, EventArgs e)
        {
            try
            {
                RealizarValidaciones();

                if (txtA4Servidor.Text == "" || txtA4Usuario.Text == "" || txtA4Password.Text == "" || txtA4Puerto.Text == "" || Convert.ToInt32(txtA4Puerto.Text) < 0 || cbxA4Encriptacion.SelectedIndex < 0 || cbxA4Protocolo.SelectedIndex < 0)
                {
                    ExtraMessageBox mostrar = ExtraMessageBox.ObtenerInstancia("Es necesario que proporcione todos los datos de los campos marcados con \"*\" para poder realizar la prueba de conexión con el servidor.", "SyncBD | Faltan datos", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Warning);

                    mostrar.ShowDialog();

                    mostrar.Dispose();

                    txtA4Servidor.Focus();
                }

                else
                {
                    string servidorFTP = "";

                    if (txtA4Servidor.Text.Contains("localhost") || txtA4Servidor.Text.Contains("127.0.0.1"))
                    {
                        servidorFTP = "localhost";
                    }

                    else
                    {
                        //Agregar validaciones para IP
                        //servidorFTP = txtA4Servidor.Text.Contains("ftp.") ? txtA4Servidor.Text : "ftp." + txtA4Servidor.Text;
                        servidorFTP = txtA4Servidor.Text;
                    }

                    //Via FTP
                    if (cbxA4Protocolo.SelectedIndex == 0)
                    {
                        servidores = new Servidores { Local = new Local { Servidor = seguridad.Encriptar(servidorFTP), Anonimo = seguridad.Encriptar(chkA4Anonimo.Value.ToString()), Usuario = seguridad.Encriptar(txtA4Usuario.Text), Password = seguridad.Encriptar(txtA4Password.Text), Puerto = seguridad.Encriptar(txtA4Puerto.Text), Directorio = seguridad.Encriptar(txtA4Directorio.Text == "" ? "/" : txtA4Directorio.Text), Encriptacion = seguridad.Encriptar(cbxA4Encriptacion.SelectedIndex.ToString()), Modo = seguridad.Encriptar(chkA4Modo.Value.ToString()), Autenticar = seguridad.Encriptar(chkA4Autenticar.Value.ToString()), Combinado = seguridad.Encriptar(combinado.ToString()), Protocolo = seguridad.Encriptar("FTP") }, Remoto = new Remoto { Servidor = "", Anonimo = "", Usuario = "", Password = "", Puerto = "", Directorio = "", Encriptacion = "", Modo = "", Autenticar = "", Combinado = seguridad.Encriptar(combinado.ToString()), Protocolo = "" } };
                    }

                    //Via SFTP
                    else
                    {
                        servidores = new Servidores { Local = new Local { Servidor = seguridad.Encriptar(servidorFTP), Anonimo = seguridad.Encriptar(chkA4Anonimo.Value.ToString()), Usuario = seguridad.Encriptar(txtA4Usuario.Text), Password = seguridad.Encriptar(txtA4Password.Text), Puerto = seguridad.Encriptar(txtA4Puerto.Text), Directorio = seguridad.Encriptar(txtA4Directorio.Text == "" ? "/" : txtA4Directorio.Text), Encriptacion = "", Modo = seguridad.Encriptar(chkA4Modo.Value.ToString()), Autenticar = seguridad.Encriptar(chkA4Autenticar.Value.ToString()), Combinado = seguridad.Encriptar(combinado.ToString()), Protocolo = seguridad.Encriptar("SFTP") }, Remoto = new Remoto { Servidor = "", Anonimo = "", Usuario = "", Password = "", Puerto = "", Directorio = "", Encriptacion = "", Modo = "", Autenticar = "", Combinado = seguridad.Encriptar(combinado.ToString()), Protocolo = "" } };
                    }

                    Procesando mostrarOcupado = Procesando.ObtenerInstancia("Conectando, espere por favor...");

                    //Enabled = false;

                    mostrarOcupado.Show();

                    if (nucleo.ValidarServidorLocal(servidores) != null)
                    {
                        if (nucleo.ValidarYGuardarAjustesServidores(servidores.Local, servidores.Remoto))
                        {
                            log.AppendLog("[Configuraciones] Ajustes de conexión con el servidor Local correctas");

                            mostrarOcupado.Close();

                            mostrarOcupado.Dispose();

                            //Enabled = true;

                            ExtraMessageBox mostrar = ExtraMessageBox.ObtenerInstancia("Se ha conectado al servidor local correctamente.", "SyncBD | Conexión con servidor local establecida", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Ok);

                            mostrar.ShowDialog();

                            mostrar.Dispose();

                            DialogResult = DialogResult.OK;

                            Close();
                        }
                    }

                    else
                    {
                        log.AppendLog("[Configuraciones] Imposible establecer conexión con el servidor Local");

                        mostrarOcupado.Close();

                        mostrarOcupado.Dispose();

                        //Enabled = true;

                        ExtraMessageBox mostrar = ExtraMessageBox.ObtenerInstancia("No se ha podido establecer conexión con el servidor local, verifique si los datos son correctos y reintente", "SyncBD | Imposible conectar con servidor local", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Warning);

                        mostrar.ShowDialog();

                        mostrar.Dispose();

                        txtA4Servidor.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x004, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x004", PopUp.Icono.Error);
            }
        }

        private void txtA4Puerto_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (Char.IsNumber(e.KeyChar))
                {
                    e.Handled = false;
                }

                else if (Char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }

                else
                {
                    e.Handled = true;
                }
            }
            catch (Exception)
            {

            }
        }

        private void RealizarValidaciones()
        {
            try
            {
                //Validar tambien al hacer clic en continuar
                if (txtA4Puerto.Text != "")
                {
                    if (txtA4Puerto.Text.Length > 5)
                    {
                        txtA4Puerto.Text = txtA4Puerto.Text.Substring(0, 5);
                    }
                }

                //Validar tambien al hacer clic en continuar
                if (txtA4Usuario.Text != "")
                {
                    txtA4Usuario.Text = txtA4Usuario.Text.TrimEnd(' ');
                    txtA4Usuario.Text = txtA4Usuario.Text.TrimStart(' ');
                }

                //Validar tambien al hacer clic en continuar
                if (txtA4Servidor.Text != "")
                {
                    txtA4Servidor.Text = txtA4Servidor.Text.TrimEnd(' ');
                    txtA4Servidor.Text = txtA4Servidor.Text.TrimStart(' ');
                }

                //Validar tambien al hacer clic en continuar
                if (txtA4Password.Text != "")
                {
                    txtA4Password.Text = txtA4Password.Text.TrimEnd(' ');
                    txtA4Password.Text = txtA4Password.Text.TrimStart(' ');
                }

                //Validar tambien al hacer clic en continuar
                if (txtA4Directorio.Text != "")
                {
                    txtA4Directorio.Text = txtA4Directorio.Text.TrimEnd(' ');
                    txtA4Directorio.Text = txtA4Directorio.Text.TrimStart(' ');
                }
            }
            catch (Exception ex)
            {
                log.AppendLog("[Error] Se produjo el error con Clave: 0x005, Mensaje interno: " + ex.Message);

                nucleo.MostrarPopUp("Abra el centro de ayuda para obtener detalles del error.", "Error 0x005", PopUp.Icono.Error);
            }
        }

        private void txtA4Puerto_Leave(object sender, EventArgs e)
        {
            RealizarValidaciones();
        }

        private void txtA4Usuario_Leave(object sender, EventArgs e)
        {
            RealizarValidaciones();
        }

        private void txtA4Servidor_Leave(object sender, EventArgs e)
        {
            RealizarValidaciones();
        }

        private void txtA4Password_Leave(object sender, EventArgs e)
        {
            RealizarValidaciones();
        }

        private void txtA4Directorio_Leave(object sender, EventArgs e)
        {
            RealizarValidaciones();
        }

        private void chkA4Anonimo_OnValueChange(object sender, EventArgs e)
        {
            if (chkA4Anonimo.Value)
            {
                txtA4Usuario.Text = "anonymous";
                txtA4Usuario.Enabled = false;
                txtA4Password.Text = "anonymous@example.com";
                txtA4Password.Enabled = false;
            }

            else
            {
                txtA4Usuario.Text = "";
                txtA4Usuario.Enabled = true;
                txtA4Password.Text = "";
                txtA4Password.Enabled = true;
            }
        }

        private void chkA4Avanzados_OnValueChange(object sender, EventArgs e)
        {
            if(chkA4Avanzados.Value)
            {
                chkA4Modo.Visible = true;
                lblA4Modo.Visible = true;
                chkA4Autenticar.Visible = true;
                lblA4Autenticar.Visible = true;
                chkA4Anonimo.Visible = true;
                lblA4Anonimo.Visible = true;
            }

            else
            {
                chkA4Modo.Visible = false;
                chkA4Modo.Value = false;
                lblA4Modo.Visible = false;
                chkA4Autenticar.Visible = false;
                chkA4Autenticar.Value = false;
                lblA4Autenticar.Visible = false;
                chkA4Anonimo.Visible = false;
                chkA4Anonimo.Value = false;
                lblA4Anonimo.Visible = false;
            }
        }

        private void cbxA4Protocolo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbxA4Protocolo.SelectedIndex == 1)
                {
                    cbxA4Encriptacion.Visible = false;
                    lblA4Encriptacion.Visible = false;
                }

                else
                {
                    cbxA4Encriptacion.Visible = true;
                    lblA4Encriptacion.Visible = true;
                }
            }
            catch (Exception)
            {

            }
        }
    }
}

﻿namespace SyncDB.Vistas
{
    partial class ConfigLocal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigLocal));
            this.cbxA4Encriptacion = new System.Windows.Forms.ComboBox();
            this.lblA4Autenticar = new DevComponents.DotNetBar.LabelX();
            this.lblA4Anonimo = new DevComponents.DotNetBar.LabelX();
            this.lblA4Modo = new DevComponents.DotNetBar.LabelX();
            this.lblA4Directorio = new DevComponents.DotNetBar.LabelX();
            this.lblA4Puerto = new DevComponents.DotNetBar.LabelX();
            this.lblA4Encriptacion = new DevComponents.DotNetBar.LabelX();
            this.lblA4Password = new DevComponents.DotNetBar.LabelX();
            this.lblA4Usuario = new DevComponents.DotNetBar.LabelX();
            this.lblA4Servidor = new DevComponents.DotNetBar.LabelX();
            this.txtA4Directorio = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtA4Puerto = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtA4Password = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtA4Usuario = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtA4Servidor = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.lblA4Cuerpo = new DevComponents.DotNetBar.LabelX();
            this.lineA4Encabezado = new DevComponents.DotNetBar.Controls.Line();
            this.lblA4Encabezado = new DevComponents.DotNetBar.LabelX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblA4Titulo = new DevComponents.DotNetBar.LabelX();
            this.pbxMinimizar = new System.Windows.Forms.PictureBox();
            this.pbxCerrar = new System.Windows.Forms.PictureBox();
            this.pbxIcono = new System.Windows.Forms.PictureBox();
            this.pbxA4Ver = new System.Windows.Forms.PictureBox();
            this.chkA4Modo = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.chkA4Anonimo = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.chkA4Autenticar = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.btnA4Ayuda = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnA4Cancelar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnA4Continuar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.lblA4Avanzados = new DevComponents.DotNetBar.LabelX();
            this.chkA4Avanzados = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.cbxA4Protocolo = new System.Windows.Forms.ComboBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA4Ver)).BeginInit();
            this.SuspendLayout();
            // 
            // cbxA4Encriptacion
            // 
            this.cbxA4Encriptacion.BackColor = System.Drawing.Color.SeaGreen;
            this.cbxA4Encriptacion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxA4Encriptacion.DropDownHeight = 120;
            this.cbxA4Encriptacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxA4Encriptacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxA4Encriptacion.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbxA4Encriptacion.ForeColor = System.Drawing.Color.White;
            this.cbxA4Encriptacion.FormattingEnabled = true;
            this.cbxA4Encriptacion.IntegralHeight = false;
            this.cbxA4Encriptacion.Items.AddRange(new object[] {
            "Sin encriptar",
            "Implícita",
            "Explícita"});
            this.cbxA4Encriptacion.Location = new System.Drawing.Point(291, 266);
            this.cbxA4Encriptacion.Name = "cbxA4Encriptacion";
            this.cbxA4Encriptacion.Size = new System.Drawing.Size(125, 27);
            this.cbxA4Encriptacion.TabIndex = 107;
            // 
            // lblA4Autenticar
            // 
            // 
            // 
            // 
            this.lblA4Autenticar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA4Autenticar.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA4Autenticar.ForeColor = System.Drawing.Color.Black;
            this.lblA4Autenticar.Location = new System.Drawing.Point(241, 346);
            this.lblA4Autenticar.Name = "lblA4Autenticar";
            this.lblA4Autenticar.SingleLineColor = System.Drawing.Color.Black;
            this.lblA4Autenticar.Size = new System.Drawing.Size(161, 26);
            this.lblA4Autenticar.TabIndex = 106;
            this.lblA4Autenticar.Text = "Quitar autenticación";
            this.lblA4Autenticar.Visible = false;
            this.lblA4Autenticar.WordWrap = true;
            // 
            // lblA4Anonimo
            // 
            // 
            // 
            // 
            this.lblA4Anonimo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA4Anonimo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA4Anonimo.ForeColor = System.Drawing.Color.Black;
            this.lblA4Anonimo.Location = new System.Drawing.Point(241, 375);
            this.lblA4Anonimo.Name = "lblA4Anonimo";
            this.lblA4Anonimo.SingleLineColor = System.Drawing.Color.Black;
            this.lblA4Anonimo.Size = new System.Drawing.Size(161, 26);
            this.lblA4Anonimo.TabIndex = 105;
            this.lblA4Anonimo.Text = "Ingresar como anónimo";
            this.lblA4Anonimo.Visible = false;
            this.lblA4Anonimo.WordWrap = true;
            // 
            // lblA4Modo
            // 
            // 
            // 
            // 
            this.lblA4Modo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA4Modo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA4Modo.ForeColor = System.Drawing.Color.Black;
            this.lblA4Modo.Location = new System.Drawing.Point(449, 378);
            this.lblA4Modo.Name = "lblA4Modo";
            this.lblA4Modo.SingleLineColor = System.Drawing.Color.Black;
            this.lblA4Modo.Size = new System.Drawing.Size(135, 26);
            this.lblA4Modo.TabIndex = 104;
            this.lblA4Modo.Text = "Modo activo";
            this.lblA4Modo.Visible = false;
            this.lblA4Modo.WordWrap = true;
            // 
            // lblA4Directorio
            // 
            // 
            // 
            // 
            this.lblA4Directorio.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA4Directorio.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA4Directorio.ForeColor = System.Drawing.Color.Black;
            this.lblA4Directorio.Location = new System.Drawing.Point(192, 299);
            this.lblA4Directorio.Name = "lblA4Directorio";
            this.lblA4Directorio.SingleLineColor = System.Drawing.Color.Black;
            this.lblA4Directorio.Size = new System.Drawing.Size(93, 26);
            this.lblA4Directorio.TabIndex = 103;
            this.lblA4Directorio.Text = "Directorio:";
            this.lblA4Directorio.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblA4Directorio.WordWrap = true;
            // 
            // lblA4Puerto
            // 
            // 
            // 
            // 
            this.lblA4Puerto.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA4Puerto.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA4Puerto.ForeColor = System.Drawing.Color.Black;
            this.lblA4Puerto.Location = new System.Drawing.Point(426, 267);
            this.lblA4Puerto.Name = "lblA4Puerto";
            this.lblA4Puerto.SingleLineColor = System.Drawing.Color.Black;
            this.lblA4Puerto.Size = new System.Drawing.Size(73, 26);
            this.lblA4Puerto.TabIndex = 102;
            this.lblA4Puerto.Text = "*Puerto:";
            this.lblA4Puerto.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblA4Puerto.WordWrap = true;
            // 
            // lblA4Encriptacion
            // 
            // 
            // 
            // 
            this.lblA4Encriptacion.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA4Encriptacion.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA4Encriptacion.ForeColor = System.Drawing.Color.Black;
            this.lblA4Encriptacion.Location = new System.Drawing.Point(192, 270);
            this.lblA4Encriptacion.Name = "lblA4Encriptacion";
            this.lblA4Encriptacion.SingleLineColor = System.Drawing.Color.Black;
            this.lblA4Encriptacion.Size = new System.Drawing.Size(93, 26);
            this.lblA4Encriptacion.TabIndex = 101;
            this.lblA4Encriptacion.Text = "*Encriptación:";
            this.lblA4Encriptacion.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblA4Encriptacion.WordWrap = true;
            // 
            // lblA4Password
            // 
            // 
            // 
            // 
            this.lblA4Password.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA4Password.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA4Password.ForeColor = System.Drawing.Color.Black;
            this.lblA4Password.Location = new System.Drawing.Point(192, 237);
            this.lblA4Password.Name = "lblA4Password";
            this.lblA4Password.SingleLineColor = System.Drawing.Color.Black;
            this.lblA4Password.Size = new System.Drawing.Size(93, 26);
            this.lblA4Password.TabIndex = 100;
            this.lblA4Password.Text = "*Contraseña:";
            this.lblA4Password.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblA4Password.WordWrap = true;
            // 
            // lblA4Usuario
            // 
            // 
            // 
            // 
            this.lblA4Usuario.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA4Usuario.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA4Usuario.ForeColor = System.Drawing.Color.Black;
            this.lblA4Usuario.Location = new System.Drawing.Point(192, 207);
            this.lblA4Usuario.Name = "lblA4Usuario";
            this.lblA4Usuario.SingleLineColor = System.Drawing.Color.Black;
            this.lblA4Usuario.Size = new System.Drawing.Size(93, 26);
            this.lblA4Usuario.TabIndex = 99;
            this.lblA4Usuario.Text = "*Usuario:";
            this.lblA4Usuario.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblA4Usuario.WordWrap = true;
            // 
            // lblA4Servidor
            // 
            // 
            // 
            // 
            this.lblA4Servidor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA4Servidor.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA4Servidor.ForeColor = System.Drawing.Color.Black;
            this.lblA4Servidor.Location = new System.Drawing.Point(192, 177);
            this.lblA4Servidor.Name = "lblA4Servidor";
            this.lblA4Servidor.SingleLineColor = System.Drawing.Color.Black;
            this.lblA4Servidor.Size = new System.Drawing.Size(93, 26);
            this.lblA4Servidor.TabIndex = 98;
            this.lblA4Servidor.Text = "*Servidor:";
            this.lblA4Servidor.TextAlignment = System.Drawing.StringAlignment.Far;
            this.lblA4Servidor.WordWrap = true;
            // 
            // txtA4Directorio
            // 
            this.txtA4Directorio.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtA4Directorio.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtA4Directorio.ForeColor = System.Drawing.Color.Black;
            this.txtA4Directorio.HintForeColor = System.Drawing.Color.Empty;
            this.txtA4Directorio.HintText = "";
            this.txtA4Directorio.isPassword = false;
            this.txtA4Directorio.LineFocusedColor = System.Drawing.Color.Blue;
            this.txtA4Directorio.LineIdleColor = System.Drawing.Color.Gray;
            this.txtA4Directorio.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.txtA4Directorio.LineThickness = 2;
            this.txtA4Directorio.Location = new System.Drawing.Point(292, 293);
            this.txtA4Directorio.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtA4Directorio.Name = "txtA4Directorio";
            this.txtA4Directorio.Size = new System.Drawing.Size(270, 26);
            this.txtA4Directorio.TabIndex = 93;
            this.txtA4Directorio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtA4Directorio.Leave += new System.EventHandler(this.txtA4Directorio_Leave);
            // 
            // txtA4Puerto
            // 
            this.txtA4Puerto.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtA4Puerto.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtA4Puerto.ForeColor = System.Drawing.Color.Black;
            this.txtA4Puerto.HintForeColor = System.Drawing.Color.Empty;
            this.txtA4Puerto.HintText = "";
            this.txtA4Puerto.isPassword = false;
            this.txtA4Puerto.LineFocusedColor = System.Drawing.Color.Blue;
            this.txtA4Puerto.LineIdleColor = System.Drawing.Color.Gray;
            this.txtA4Puerto.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.txtA4Puerto.LineThickness = 2;
            this.txtA4Puerto.Location = new System.Drawing.Point(506, 267);
            this.txtA4Puerto.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtA4Puerto.Name = "txtA4Puerto";
            this.txtA4Puerto.Size = new System.Drawing.Size(67, 26);
            this.txtA4Puerto.TabIndex = 92;
            this.txtA4Puerto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtA4Puerto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtA4Puerto_KeyPress);
            this.txtA4Puerto.Leave += new System.EventHandler(this.txtA4Puerto_Leave);
            // 
            // txtA4Password
            // 
            this.txtA4Password.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtA4Password.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtA4Password.ForeColor = System.Drawing.Color.Black;
            this.txtA4Password.HintForeColor = System.Drawing.Color.Empty;
            this.txtA4Password.HintText = "";
            this.txtA4Password.isPassword = true;
            this.txtA4Password.LineFocusedColor = System.Drawing.Color.Blue;
            this.txtA4Password.LineIdleColor = System.Drawing.Color.Gray;
            this.txtA4Password.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.txtA4Password.LineThickness = 2;
            this.txtA4Password.Location = new System.Drawing.Point(292, 231);
            this.txtA4Password.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtA4Password.Name = "txtA4Password";
            this.txtA4Password.Size = new System.Drawing.Size(230, 27);
            this.txtA4Password.TabIndex = 91;
            this.txtA4Password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtA4Password.Leave += new System.EventHandler(this.txtA4Password_Leave);
            // 
            // txtA4Usuario
            // 
            this.txtA4Usuario.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtA4Usuario.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtA4Usuario.ForeColor = System.Drawing.Color.Black;
            this.txtA4Usuario.HintForeColor = System.Drawing.Color.Empty;
            this.txtA4Usuario.HintText = "";
            this.txtA4Usuario.isPassword = false;
            this.txtA4Usuario.LineFocusedColor = System.Drawing.Color.Blue;
            this.txtA4Usuario.LineIdleColor = System.Drawing.Color.Gray;
            this.txtA4Usuario.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.txtA4Usuario.LineThickness = 2;
            this.txtA4Usuario.Location = new System.Drawing.Point(292, 201);
            this.txtA4Usuario.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtA4Usuario.Name = "txtA4Usuario";
            this.txtA4Usuario.Size = new System.Drawing.Size(270, 26);
            this.txtA4Usuario.TabIndex = 90;
            this.txtA4Usuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtA4Usuario.Leave += new System.EventHandler(this.txtA4Usuario_Leave);
            // 
            // txtA4Servidor
            // 
            this.txtA4Servidor.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtA4Servidor.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtA4Servidor.ForeColor = System.Drawing.Color.Black;
            this.txtA4Servidor.HintForeColor = System.Drawing.Color.Empty;
            this.txtA4Servidor.HintText = "";
            this.txtA4Servidor.isPassword = false;
            this.txtA4Servidor.LineFocusedColor = System.Drawing.Color.Blue;
            this.txtA4Servidor.LineIdleColor = System.Drawing.Color.Gray;
            this.txtA4Servidor.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.txtA4Servidor.LineThickness = 2;
            this.txtA4Servidor.Location = new System.Drawing.Point(292, 173);
            this.txtA4Servidor.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtA4Servidor.Name = "txtA4Servidor";
            this.txtA4Servidor.Size = new System.Drawing.Size(270, 24);
            this.txtA4Servidor.TabIndex = 89;
            this.txtA4Servidor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtA4Servidor.Leave += new System.EventHandler(this.txtA4Servidor_Leave);
            // 
            // lblA4Cuerpo
            // 
            // 
            // 
            // 
            this.lblA4Cuerpo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA4Cuerpo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA4Cuerpo.ForeColor = System.Drawing.Color.Black;
            this.lblA4Cuerpo.Location = new System.Drawing.Point(192, 109);
            this.lblA4Cuerpo.Name = "lblA4Cuerpo";
            this.lblA4Cuerpo.SingleLineColor = System.Drawing.Color.Black;
            this.lblA4Cuerpo.Size = new System.Drawing.Size(396, 60);
            this.lblA4Cuerpo.TabIndex = 85;
            this.lblA4Cuerpo.Text = "El servidor local es al que se le cargarán los archivos desde este equipo <b>si t" +
    "iene acceso físico al servidor</b> no será necesario contar con conexión a inter" +
    "net.";
            this.lblA4Cuerpo.WordWrap = true;
            // 
            // lineA4Encabezado
            // 
            this.lineA4Encabezado.ForeColor = System.Drawing.Color.Black;
            this.lineA4Encabezado.Location = new System.Drawing.Point(192, 80);
            this.lineA4Encabezado.Name = "lineA4Encabezado";
            this.lineA4Encabezado.Size = new System.Drawing.Size(396, 23);
            this.lineA4Encabezado.TabIndex = 84;
            this.lineA4Encabezado.Text = "line1";
            // 
            // lblA4Encabezado
            // 
            // 
            // 
            // 
            this.lblA4Encabezado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA4Encabezado.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblA4Encabezado.ForeColor = System.Drawing.Color.Black;
            this.lblA4Encabezado.Location = new System.Drawing.Point(192, 47);
            this.lblA4Encabezado.Name = "lblA4Encabezado";
            this.lblA4Encabezado.SingleLineColor = System.Drawing.Color.Black;
            this.lblA4Encabezado.Size = new System.Drawing.Size(396, 39);
            this.lblA4Encabezado.TabIndex = 83;
            this.lblA4Encabezado.Text = "Configuración del servidor local";
            this.lblA4Encabezado.WordWrap = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbxA4Protocolo);
            this.panel1.Controls.Add(this.labelX1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(186, 450);
            this.panel1.TabIndex = 82;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::SyncBD.Properties.Resources.SyncBD;
            this.pictureBox1.Location = new System.Drawing.Point(4, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.Green;
            this.pnlTitleBar.Controls.Add(this.lblA4Titulo);
            this.pnlTitleBar.Controls.Add(this.pbxMinimizar);
            this.pnlTitleBar.Controls.Add(this.pbxCerrar);
            this.pnlTitleBar.Controls.Add(this.pbxIcono);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(600, 30);
            this.pnlTitleBar.TabIndex = 81;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // lblA4Titulo
            // 
            // 
            // 
            // 
            this.lblA4Titulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA4Titulo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA4Titulo.ForeColor = System.Drawing.Color.White;
            this.lblA4Titulo.Location = new System.Drawing.Point(28, 5);
            this.lblA4Titulo.Name = "lblA4Titulo";
            this.lblA4Titulo.Size = new System.Drawing.Size(508, 20);
            this.lblA4Titulo.TabIndex = 11;
            this.lblA4Titulo.Text = "SyncBD - Asistente de configuración";
            this.lblA4Titulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pbxMinimizar
            // 
            this.pbxMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.pbxMinimizar.Image = global::SyncBD.Properties.Resources.Compress_24px;
            this.pbxMinimizar.Location = new System.Drawing.Point(542, 5);
            this.pbxMinimizar.Name = "pbxMinimizar";
            this.pbxMinimizar.Size = new System.Drawing.Size(20, 20);
            this.pbxMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMinimizar.TabIndex = 3;
            this.pbxMinimizar.TabStop = false;
            this.pbxMinimizar.Click += new System.EventHandler(this.pbxMinimizar_Click);
            // 
            // pbxCerrar
            // 
            this.pbxCerrar.BackColor = System.Drawing.Color.Transparent;
            this.pbxCerrar.Image = global::SyncBD.Properties.Resources.CloseWindow_24px;
            this.pbxCerrar.Location = new System.Drawing.Point(568, 5);
            this.pbxCerrar.Name = "pbxCerrar";
            this.pbxCerrar.Size = new System.Drawing.Size(20, 20);
            this.pbxCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCerrar.TabIndex = 2;
            this.pbxCerrar.TabStop = false;
            this.pbxCerrar.Click += new System.EventHandler(this.pbxCerrar_Click);
            // 
            // pbxIcono
            // 
            this.pbxIcono.BackColor = System.Drawing.Color.Transparent;
            this.pbxIcono.Image = global::SyncBD.Properties.Resources.SyncBDLogo;
            this.pbxIcono.Location = new System.Drawing.Point(6, 8);
            this.pbxIcono.Name = "pbxIcono";
            this.pbxIcono.Size = new System.Drawing.Size(16, 16);
            this.pbxIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxIcono.TabIndex = 1;
            this.pbxIcono.TabStop = false;
            this.pbxIcono.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pbxA4Ver
            // 
            this.pbxA4Ver.BackColor = System.Drawing.Color.Transparent;
            this.pbxA4Ver.Image = global::SyncBD.Properties.Resources.Lock_32px;
            this.pbxA4Ver.Location = new System.Drawing.Point(545, 233);
            this.pbxA4Ver.Name = "pbxA4Ver";
            this.pbxA4Ver.Size = new System.Drawing.Size(28, 28);
            this.pbxA4Ver.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxA4Ver.TabIndex = 97;
            this.pbxA4Ver.TabStop = false;
            this.pbxA4Ver.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbxA4Ver_MouseDown);
            this.pbxA4Ver.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbxA4Ver_MouseUp);
            // 
            // chkA4Modo
            // 
            this.chkA4Modo.BackColor = System.Drawing.Color.Transparent;
            this.chkA4Modo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkA4Modo.BackgroundImage")));
            this.chkA4Modo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkA4Modo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkA4Modo.Location = new System.Drawing.Point(408, 380);
            this.chkA4Modo.Name = "chkA4Modo";
            this.chkA4Modo.OffColor = System.Drawing.Color.Red;
            this.chkA4Modo.OnColor = System.Drawing.Color.Green;
            this.chkA4Modo.Size = new System.Drawing.Size(35, 20);
            this.chkA4Modo.TabIndex = 96;
            this.chkA4Modo.Value = false;
            this.chkA4Modo.Visible = false;
            // 
            // chkA4Anonimo
            // 
            this.chkA4Anonimo.BackColor = System.Drawing.Color.Transparent;
            this.chkA4Anonimo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkA4Anonimo.BackgroundImage")));
            this.chkA4Anonimo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkA4Anonimo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkA4Anonimo.Location = new System.Drawing.Point(200, 378);
            this.chkA4Anonimo.Name = "chkA4Anonimo";
            this.chkA4Anonimo.OffColor = System.Drawing.Color.Red;
            this.chkA4Anonimo.OnColor = System.Drawing.Color.Green;
            this.chkA4Anonimo.Size = new System.Drawing.Size(35, 20);
            this.chkA4Anonimo.TabIndex = 95;
            this.chkA4Anonimo.Value = false;
            this.chkA4Anonimo.Visible = false;
            this.chkA4Anonimo.OnValueChange += new System.EventHandler(this.chkA4Anonimo_OnValueChange);
            // 
            // chkA4Autenticar
            // 
            this.chkA4Autenticar.BackColor = System.Drawing.Color.Transparent;
            this.chkA4Autenticar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkA4Autenticar.BackgroundImage")));
            this.chkA4Autenticar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkA4Autenticar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkA4Autenticar.Location = new System.Drawing.Point(200, 349);
            this.chkA4Autenticar.Name = "chkA4Autenticar";
            this.chkA4Autenticar.OffColor = System.Drawing.Color.Red;
            this.chkA4Autenticar.OnColor = System.Drawing.Color.Green;
            this.chkA4Autenticar.Size = new System.Drawing.Size(35, 20);
            this.chkA4Autenticar.TabIndex = 94;
            this.chkA4Autenticar.Value = false;
            this.chkA4Autenticar.Visible = false;
            // 
            // btnA4Ayuda
            // 
            this.btnA4Ayuda.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnA4Ayuda.BackColor = System.Drawing.Color.Gray;
            this.btnA4Ayuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnA4Ayuda.BorderRadius = 0;
            this.btnA4Ayuda.ButtonText = "Ayuda";
            this.btnA4Ayuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnA4Ayuda.DisabledColor = System.Drawing.Color.Gray;
            this.btnA4Ayuda.Iconcolor = System.Drawing.Color.Transparent;
            this.btnA4Ayuda.Iconimage = global::SyncBD.Properties.Resources.Help_16px;
            this.btnA4Ayuda.Iconimage_right = null;
            this.btnA4Ayuda.Iconimage_right_Selected = null;
            this.btnA4Ayuda.Iconimage_Selected = null;
            this.btnA4Ayuda.IconMarginLeft = 0;
            this.btnA4Ayuda.IconMarginRight = 0;
            this.btnA4Ayuda.IconRightVisible = true;
            this.btnA4Ayuda.IconRightZoom = 0D;
            this.btnA4Ayuda.IconVisible = true;
            this.btnA4Ayuda.IconZoom = 35D;
            this.btnA4Ayuda.IsTab = false;
            this.btnA4Ayuda.Location = new System.Drawing.Point(192, 425);
            this.btnA4Ayuda.Name = "btnA4Ayuda";
            this.btnA4Ayuda.Normalcolor = System.Drawing.Color.Gray;
            this.btnA4Ayuda.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.btnA4Ayuda.OnHoverTextColor = System.Drawing.Color.White;
            this.btnA4Ayuda.selected = false;
            this.btnA4Ayuda.Size = new System.Drawing.Size(93, 43);
            this.btnA4Ayuda.TabIndex = 88;
            this.btnA4Ayuda.Text = "Ayuda";
            this.btnA4Ayuda.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnA4Ayuda.Textcolor = System.Drawing.Color.White;
            this.btnA4Ayuda.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA4Ayuda.Click += new System.EventHandler(this.btnA4Ayuda_Click);
            // 
            // btnA4Cancelar
            // 
            this.btnA4Cancelar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnA4Cancelar.BackColor = System.Drawing.Color.Gray;
            this.btnA4Cancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnA4Cancelar.BorderRadius = 0;
            this.btnA4Cancelar.ButtonText = "Cancelar";
            this.btnA4Cancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnA4Cancelar.DisabledColor = System.Drawing.Color.Gray;
            this.btnA4Cancelar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnA4Cancelar.Iconimage = global::SyncBD.Properties.Resources.Cancel_16px;
            this.btnA4Cancelar.Iconimage_right = null;
            this.btnA4Cancelar.Iconimage_right_Selected = null;
            this.btnA4Cancelar.Iconimage_Selected = null;
            this.btnA4Cancelar.IconMarginLeft = 0;
            this.btnA4Cancelar.IconMarginRight = 0;
            this.btnA4Cancelar.IconRightVisible = true;
            this.btnA4Cancelar.IconRightZoom = 0D;
            this.btnA4Cancelar.IconVisible = true;
            this.btnA4Cancelar.IconZoom = 35D;
            this.btnA4Cancelar.IsTab = false;
            this.btnA4Cancelar.Location = new System.Drawing.Point(334, 425);
            this.btnA4Cancelar.Name = "btnA4Cancelar";
            this.btnA4Cancelar.Normalcolor = System.Drawing.Color.Gray;
            this.btnA4Cancelar.OnHovercolor = System.Drawing.Color.DarkRed;
            this.btnA4Cancelar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnA4Cancelar.selected = false;
            this.btnA4Cancelar.Size = new System.Drawing.Size(124, 43);
            this.btnA4Cancelar.TabIndex = 87;
            this.btnA4Cancelar.Text = "Cancelar";
            this.btnA4Cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnA4Cancelar.Textcolor = System.Drawing.Color.White;
            this.btnA4Cancelar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA4Cancelar.Click += new System.EventHandler(this.btnA4Cancelar_Click);
            // 
            // btnA4Continuar
            // 
            this.btnA4Continuar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnA4Continuar.BackColor = System.Drawing.Color.Gray;
            this.btnA4Continuar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnA4Continuar.BorderRadius = 0;
            this.btnA4Continuar.ButtonText = "Continuar";
            this.btnA4Continuar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnA4Continuar.DisabledColor = System.Drawing.Color.Gray;
            this.btnA4Continuar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnA4Continuar.Iconimage = global::SyncBD.Properties.Resources.Ok_16px;
            this.btnA4Continuar.Iconimage_right = null;
            this.btnA4Continuar.Iconimage_right_Selected = null;
            this.btnA4Continuar.Iconimage_Selected = null;
            this.btnA4Continuar.IconMarginLeft = 0;
            this.btnA4Continuar.IconMarginRight = 0;
            this.btnA4Continuar.IconRightVisible = true;
            this.btnA4Continuar.IconRightZoom = 0D;
            this.btnA4Continuar.IconVisible = true;
            this.btnA4Continuar.IconZoom = 40D;
            this.btnA4Continuar.IsTab = false;
            this.btnA4Continuar.Location = new System.Drawing.Point(464, 425);
            this.btnA4Continuar.Name = "btnA4Continuar";
            this.btnA4Continuar.Normalcolor = System.Drawing.Color.Gray;
            this.btnA4Continuar.OnHovercolor = System.Drawing.Color.Green;
            this.btnA4Continuar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnA4Continuar.selected = false;
            this.btnA4Continuar.Size = new System.Drawing.Size(124, 43);
            this.btnA4Continuar.TabIndex = 86;
            this.btnA4Continuar.Text = "Continuar";
            this.btnA4Continuar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnA4Continuar.Textcolor = System.Drawing.Color.White;
            this.btnA4Continuar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA4Continuar.Click += new System.EventHandler(this.btnA4Continuar_Click);
            // 
            // lblA4Avanzados
            // 
            // 
            // 
            // 
            this.lblA4Avanzados.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA4Avanzados.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA4Avanzados.ForeColor = System.Drawing.Color.Black;
            this.lblA4Avanzados.Location = new System.Drawing.Point(449, 347);
            this.lblA4Avanzados.Name = "lblA4Avanzados";
            this.lblA4Avanzados.SingleLineColor = System.Drawing.Color.Black;
            this.lblA4Avanzados.Size = new System.Drawing.Size(135, 26);
            this.lblA4Avanzados.TabIndex = 109;
            this.lblA4Avanzados.Text = "Ajustes Avanzados";
            this.lblA4Avanzados.WordWrap = true;
            // 
            // chkA4Avanzados
            // 
            this.chkA4Avanzados.BackColor = System.Drawing.Color.Transparent;
            this.chkA4Avanzados.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkA4Avanzados.BackgroundImage")));
            this.chkA4Avanzados.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkA4Avanzados.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkA4Avanzados.Location = new System.Drawing.Point(408, 349);
            this.chkA4Avanzados.Name = "chkA4Avanzados";
            this.chkA4Avanzados.OffColor = System.Drawing.Color.Red;
            this.chkA4Avanzados.OnColor = System.Drawing.Color.Green;
            this.chkA4Avanzados.Size = new System.Drawing.Size(35, 20);
            this.chkA4Avanzados.TabIndex = 108;
            this.chkA4Avanzados.Value = false;
            this.chkA4Avanzados.OnValueChange += new System.EventHandler(this.chkA4Avanzados_OnValueChange);
            // 
            // cbxA4Protocolo
            // 
            this.cbxA4Protocolo.BackColor = System.Drawing.Color.SeaGreen;
            this.cbxA4Protocolo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxA4Protocolo.DropDownHeight = 120;
            this.cbxA4Protocolo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxA4Protocolo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxA4Protocolo.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbxA4Protocolo.ForeColor = System.Drawing.Color.White;
            this.cbxA4Protocolo.FormattingEnabled = true;
            this.cbxA4Protocolo.IntegralHeight = false;
            this.cbxA4Protocolo.Items.AddRange(new object[] {
            "FTP",
            "SFTP"});
            this.cbxA4Protocolo.Location = new System.Drawing.Point(4, 155);
            this.cbxA4Protocolo.Name = "cbxA4Protocolo";
            this.cbxA4Protocolo.Size = new System.Drawing.Size(177, 27);
            this.cbxA4Protocolo.TabIndex = 105;
            this.cbxA4Protocolo.SelectedIndexChanged += new System.EventHandler(this.cbxA4Protocolo_SelectedIndexChanged);
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(4, 123);
            this.labelX1.Name = "labelX1";
            this.labelX1.SingleLineColor = System.Drawing.Color.Black;
            this.labelX1.Size = new System.Drawing.Size(160, 26);
            this.labelX1.TabIndex = 106;
            this.labelX1.Text = "Usar protocolo:";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Center;
            this.labelX1.WordWrap = true;
            // 
            // ConfigLocal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(600, 480);
            this.Controls.Add(this.lblA4Avanzados);
            this.Controls.Add(this.lblA4Puerto);
            this.Controls.Add(this.chkA4Avanzados);
            this.Controls.Add(this.txtA4Puerto);
            this.Controls.Add(this.cbxA4Encriptacion);
            this.Controls.Add(this.lblA4Autenticar);
            this.Controls.Add(this.lblA4Directorio);
            this.Controls.Add(this.chkA4Autenticar);
            this.Controls.Add(this.lblA4Anonimo);
            this.Controls.Add(this.lblA4Encriptacion);
            this.Controls.Add(this.chkA4Anonimo);
            this.Controls.Add(this.lblA4Password);
            this.Controls.Add(this.lblA4Modo);
            this.Controls.Add(this.lblA4Usuario);
            this.Controls.Add(this.chkA4Modo);
            this.Controls.Add(this.lblA4Servidor);
            this.Controls.Add(this.pbxA4Ver);
            this.Controls.Add(this.txtA4Directorio);
            this.Controls.Add(this.txtA4Password);
            this.Controls.Add(this.txtA4Usuario);
            this.Controls.Add(this.txtA4Servidor);
            this.Controls.Add(this.btnA4Ayuda);
            this.Controls.Add(this.btnA4Cancelar);
            this.Controls.Add(this.btnA4Continuar);
            this.Controls.Add(this.lblA4Cuerpo);
            this.Controls.Add(this.lineA4Encabezado);
            this.Controls.Add(this.lblA4Encabezado);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlTitleBar);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConfigLocal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConfigLocal";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlTitleBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA4Ver)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxA4Encriptacion;
        private DevComponents.DotNetBar.LabelX lblA4Autenticar;
        private DevComponents.DotNetBar.LabelX lblA4Anonimo;
        private DevComponents.DotNetBar.LabelX lblA4Modo;
        private DevComponents.DotNetBar.LabelX lblA4Directorio;
        private DevComponents.DotNetBar.LabelX lblA4Puerto;
        private DevComponents.DotNetBar.LabelX lblA4Encriptacion;
        private DevComponents.DotNetBar.LabelX lblA4Password;
        private DevComponents.DotNetBar.LabelX lblA4Usuario;
        private DevComponents.DotNetBar.LabelX lblA4Servidor;
        private System.Windows.Forms.PictureBox pbxA4Ver;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkA4Modo;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkA4Anonimo;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkA4Autenticar;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtA4Directorio;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtA4Puerto;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtA4Password;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtA4Usuario;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtA4Servidor;
        private Bunifu.Framework.UI.BunifuFlatButton btnA4Ayuda;
        private Bunifu.Framework.UI.BunifuFlatButton btnA4Cancelar;
        private Bunifu.Framework.UI.BunifuFlatButton btnA4Continuar;
        private DevComponents.DotNetBar.LabelX lblA4Cuerpo;
        private DevComponents.DotNetBar.Controls.Line lineA4Encabezado;
        private DevComponents.DotNetBar.LabelX lblA4Encabezado;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlTitleBar;
        private DevComponents.DotNetBar.LabelX lblA4Titulo;
        private System.Windows.Forms.PictureBox pbxMinimizar;
        private System.Windows.Forms.PictureBox pbxCerrar;
        private System.Windows.Forms.PictureBox pbxIcono;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevComponents.DotNetBar.LabelX lblA4Avanzados;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkA4Avanzados;
        private System.Windows.Forms.ComboBox cbxA4Protocolo;
        private DevComponents.DotNetBar.LabelX labelX1;
    }
}
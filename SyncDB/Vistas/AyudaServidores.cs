﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SyncDB.Vistas
{
    public partial class AyudaServidores : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int Param);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private static AyudaServidores singleton;

        public static AyudaServidores ObtenerInstancia()
        {
            if (singleton == null || singleton.IsDisposed)
            {
                singleton = new AyudaServidores();
            }

            singleton.BringToFront();

            return singleton;
        }

        private AyudaServidores()
        {
            InitializeComponent();
        }

        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void pbxMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void pbxCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAH1Regresar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

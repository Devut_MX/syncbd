﻿using SyncBD.Controladores;
using SyncDB.Vistas;
using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SyncBD.Vistas
{
    public partial class Temas : Form
    {
        #region Metodos de creacion

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams encargar = base.CreateParams;
                encargar.ExStyle |= 0x02000000;
                return encargar;
            }
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int Param);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        public string ObtenerVersion
        {
            get
            {
                return ApplicationDeployment.IsNetworkDeployed ? ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString() : Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        #endregion

        private static Temas singleton;

        public static Temas ObtenerInstancia()
        {
            if (singleton == null || singleton.IsDisposed)
            {
                singleton = new Temas();
            }

            singleton.BringToFront();

            return singleton;
        }

        private Temas()
        {
            InitializeComponent();

            ObtenerTemasDisponibles();
        }

        Nucleo nucleo = new Nucleo();

        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        ColorDialog seleccionColor = new ColorDialog();

        private void pbxMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void pbxCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ObtenerTemasDisponibles()
        {
            try
            {
                List<string> temas = nucleo.ObtenerTemas();

                if(temas != null)
                {
                    if (temas.Count > 0)
                    {
                        cbxTTemaDisponible.Items.Clear();

                        cbxTTemaDisponible.Items.Add("Crear nuevo...");

                        foreach (string tema in temas)
                        {
                            cbxTTemaDisponible.Items.Add(Path.GetFileNameWithoutExtension(tema));
                        }

                        cbxTTemaDisponible.SelectedIndex = 0;
                        btnTBorrarTema.Visible = false;
                        btnTCancelarTema.Location = new Point(476, 515);
                    }

                    else
                    {
                        cbxTTemaDisponible.Items.Clear();
                        cbxTTemaDisponible.Items.Add("Crear nuevo...");
                        cbxTTemaDisponible.SelectedIndex = 0;
                        btnTBorrarTema.Visible = false;
                        btnTCancelarTema.Location = new Point(476, 515);
                    }
                }

                else
                {
                    cbxTTemaDisponible.Items.Clear();
                    cbxTTemaDisponible.Items.Add("Crear nuevo...");
                    cbxTTemaDisponible.SelectedIndex = 0;
                    btnTBorrarTema.Visible = false;
                    btnTCancelarTema.Location = new Point(476, 515);
                }
            }
            catch (Exception)
            {
                //Agregar codigo de error
            }
        }

        private void btnTFondoTitulo_Click(object sender, EventArgs e)
        {
            try
            {
                if(seleccionColor.ShowDialog() == DialogResult.OK)
                {
                    colorFondoTitulo.BackColor = seleccionColor.Color;

                    pnlTitleBar.BackColor = seleccionColor.Color;
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnTTextoTitulo_Click(object sender, EventArgs e)
        {
            try
            {
                if (seleccionColor.ShowDialog() == DialogResult.OK)
                {
                    colorTextoTitulo.BackColor = seleccionColor.Color;

                    lblTTitulo.ForeColor = seleccionColor.Color;
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnTFondoVentana_Click(object sender, EventArgs e)
        {
            try
            {
                if (seleccionColor.ShowDialog() == DialogResult.OK)
                {
                    colorFondoVentana.BackColor = seleccionColor.Color;

                    pnlTemas.BackColor = seleccionColor.Color;
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnTTextoVentana_Click(object sender, EventArgs e)
        {
            try
            {
                if (seleccionColor.ShowDialog() == DialogResult.OK)
                {
                    colorTextoVentana.BackColor = seleccionColor.Color;

                    foreach (Control control in pnlTemas.Controls)
                    {
                        if (control is DevComponents.DotNetBar.LabelX)
                        {
                            ((DevComponents.DotNetBar.LabelX)control).ForeColor = seleccionColor.Color;
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnTTextoBotones_Click(object sender, EventArgs e)
        {
            try
            {
                if (seleccionColor.ShowDialog() == DialogResult.OK)
                {
                    colorTextoBotones.BackColor = seleccionColor.Color;

                    foreach (Control control in pnlTemas.Controls)
                    {
                        if(control is Bunifu.Framework.UI.BunifuFlatButton)
                        {
                            ((Bunifu.Framework.UI.BunifuFlatButton)control).Textcolor = seleccionColor.Color;
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnTFondoAyuda_Click(object sender, EventArgs e)
        {
            try
            {
                if (seleccionColor.ShowDialog() == DialogResult.OK)
                {
                    colorFondoBotones.BackColor = seleccionColor.Color;

                    foreach (Control control in pnlTemas.Controls)
                    {
                        if (control is Bunifu.Framework.UI.BunifuFlatButton)
                        {
                            ((Bunifu.Framework.UI.BunifuFlatButton)control).Normalcolor = seleccionColor.Color;
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnTGuardarTema_Click(object sender, EventArgs e)
        {
            try
            {
                if(colorFondoTitulo.BackColor == Color.Transparent || colorFondoVentana.BackColor == Color.Transparent || colorFondoBotones.BackColor == Color.Transparent || colorTextoBotones.BackColor == Color.Transparent || colorTextoTitulo.BackColor == Color.Transparent || colorTextoVentana.BackColor == Color.Transparent || txtTNombreTema.Text == "")
                {
                    ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("Es necesario que proporcione los colores y/o el nombre para guadar el tema", "SyncBD | Faltan colores", ExtraMessageBox.Boton.Ok, ExtraMessageBox.Icono.Warning);

                    mensaje.ShowDialog();

                    mensaje.Dispose();
                }

                else
                {
                    ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿En verdad desea guardar el tema con las configuraciones actuales?", "SyncBD | Confirme acción", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Warning);

                    if (mensaje.ShowDialog() == DialogResult.OK)
                    {
                        mensaje.Dispose();

                        if(cbxTTemaDisponible.SelectedIndex == 0)
                        {
                            if (nucleo.GuardarTema(new Tema { Nombre = txtTNombreTema.Text, FondoTitulo = ColorTranslator.ToHtml(colorFondoTitulo.BackColor), TextoTitulo = ColorTranslator.ToHtml(colorTextoTitulo.BackColor), FondoVentana = ColorTranslator.ToHtml(colorFondoVentana.BackColor), TextoVentana = ColorTranslator.ToHtml(colorTextoVentana.BackColor), FondoBoton = ColorTranslator.ToHtml(colorFondoBotones.BackColor), TextoBoton = ColorTranslator.ToHtml(colorTextoBotones.BackColor) }))
                            {
                                nucleo.MostrarPopUp("Se ha creado el tema personalizado", "¡Listo!", PopUp.Icono.Ok);

                                txtTNombreTema.Text = "";

                                ObtenerTemasDisponibles();
                            }

                            else
                            {

                            }
                        }

                        else
                        {
                            if (nucleo.GuardarTema(new Tema { Nombre = cbxTTemaDisponible.SelectedItem.ToString(), FondoTitulo = ColorTranslator.ToHtml(colorFondoTitulo.BackColor), TextoTitulo = ColorTranslator.ToHtml(colorTextoTitulo.BackColor), FondoVentana = ColorTranslator.ToHtml(colorFondoVentana.BackColor), TextoVentana = ColorTranslator.ToHtml(colorTextoVentana.BackColor), FondoBoton = ColorTranslator.ToHtml(colorFondoBotones.BackColor), TextoBoton = ColorTranslator.ToHtml(colorTextoBotones.BackColor) }))
                            {
                                nucleo.MostrarPopUp("Se ha creado el tema personalizado", "¡Listo!", PopUp.Icono.Ok);

                                txtTNombreTema.Text = "";

                                ObtenerTemasDisponibles();
                            }

                            else
                            {

                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void cbxTTemaDisponible_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbxTTemaDisponible.SelectedIndex == 0)
                {
                    lblTNombreTema.Visible = true;
                    txtTNombreTema.Visible = true;
                    btnTBorrarTema.Visible = false;
                    btnTCancelarTema.Location = new Point(476, 515);

                    colorFondoTitulo.BackColor = Color.Transparent;
                    pnlTitleBar.BackColor = Color.Green;
                    colorTextoTitulo.BackColor = Color.Transparent;
                    lblTTitulo.ForeColor = Color.White;
                    colorFondoVentana.BackColor = Color.Transparent;
                    pnlTemas.BackColor = Color.WhiteSmoke;
                    colorTextoVentana.BackColor = Color.Transparent;
                    foreach (Control control in pnlTemas.Controls)
                    {
                        if (control is DevComponents.DotNetBar.LabelX)
                        {
                            ((DevComponents.DotNetBar.LabelX)control).ForeColor = Color.Black;
                        }
                    }
                    colorFondoBotones.BackColor = Color.Transparent;
                    foreach (Control control in pnlTemas.Controls)
                    {
                        if (control is Bunifu.Framework.UI.BunifuFlatButton)
                        {
                            ((Bunifu.Framework.UI.BunifuFlatButton)control).Normalcolor = Color.Gray;
                        }
                    }
                    colorTextoBotones.BackColor = Color.Transparent;
                    foreach (Control control in pnlTemas.Controls)
                    {
                        if (control is Bunifu.Framework.UI.BunifuFlatButton)
                        {
                            ((Bunifu.Framework.UI.BunifuFlatButton)control).Textcolor = Color.White;
                        }
                    }
                }

                else
                {
                    lblTNombreTema.Visible = false;
                    txtTNombreTema.Visible = false;
                    btnTBorrarTema.Visible = true;
                    btnTCancelarTema.Location = new Point(317, 515);

                    Tema tema = nucleo.LeerTema(cbxTTemaDisponible.SelectedItem.ToString());

                    if(tema != null)
                    {
                        colorFondoTitulo.BackColor = ColorTranslator.FromHtml(tema.FondoTitulo);
                        pnlTitleBar.BackColor = ColorTranslator.FromHtml(tema.FondoTitulo);
                        colorTextoTitulo.BackColor = ColorTranslator.FromHtml(tema.TextoTitulo);
                        lblTTitulo.ForeColor = ColorTranslator.FromHtml(tema.TextoTitulo);
                        colorFondoVentana.BackColor = ColorTranslator.FromHtml(tema.FondoVentana);
                        pnlTemas.BackColor = ColorTranslator.FromHtml(tema.FondoVentana);
                        colorTextoVentana.BackColor = ColorTranslator.FromHtml(tema.TextoVentana);
                        foreach (Control control in pnlTemas.Controls)
                        {
                            if (control is DevComponents.DotNetBar.LabelX)
                            {
                                ((DevComponents.DotNetBar.LabelX)control).ForeColor = ColorTranslator.FromHtml(tema.TextoVentana);
                            }
                        }
                        colorFondoBotones.BackColor = ColorTranslator.FromHtml(tema.FondoBoton);
                        foreach (Control control in pnlTemas.Controls)
                        {
                            if (control is Bunifu.Framework.UI.BunifuFlatButton)
                            {
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Normalcolor = ColorTranslator.FromHtml(tema.FondoBoton);
                            }
                        }
                        colorTextoBotones.BackColor = ColorTranslator.FromHtml(tema.TextoBoton);
                        foreach (Control control in pnlTemas.Controls)
                        {
                            if (control is Bunifu.Framework.UI.BunifuFlatButton)
                            {
                                ((Bunifu.Framework.UI.BunifuFlatButton)control).Textcolor = ColorTranslator.FromHtml(tema.TextoBoton);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnTBorrarTema_Click(object sender, EventArgs e)
        {
            try
            {
                if(cbxTTemaDisponible.SelectedIndex > 0)
                {
                    ExtraMessageBox mensaje = ExtraMessageBox.ObtenerInstancia("¿En verdad desea borrar el tema "+ cbxTTemaDisponible.SelectedItem.ToString() +"?", "SyncBD | Confirme acción", ExtraMessageBox.Boton.YesNo, ExtraMessageBox.Icono.Warning);

                    if (mensaje.ShowDialog() == DialogResult.OK)
                    {
                        mensaje.Dispose();

                        if(nucleo.BorrarTema(cbxTTemaDisponible.SelectedItem.ToString()))
                        {
                            nucleo.MostrarPopUp("Se ha borrado el tema correctamente", "¡Listo!", PopUp.Icono.Ok);

                            txtTNombreTema.Text = "";

                            ObtenerTemasDisponibles();
                        }

                        else
                        {

                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnTCancelarTema_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

﻿namespace SyncDB.Vistas
{
    partial class AyudaConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AyudaConfig));
            this.cbxAH2Opcion = new System.Windows.Forms.ComboBox();
            this.lblAH2Opcion = new DevComponents.DotNetBar.LabelX();
            this.lblAH2Cuerpo2 = new DevComponents.DotNetBar.LabelX();
            this.btnAH2Regresar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.lblAH2Cuerpo1 = new DevComponents.DotNetBar.LabelX();
            this.lineAH2Divisor = new DevComponents.DotNetBar.Controls.Line();
            this.lblAH2Encabezado = new DevComponents.DotNetBar.LabelX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblAH2Titulo = new DevComponents.DotNetBar.LabelX();
            this.pbxMinimizar = new System.Windows.Forms.PictureBox();
            this.pbxCerrar = new System.Windows.Forms.PictureBox();
            this.pbxIcono = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).BeginInit();
            this.SuspendLayout();
            // 
            // cbxAH2Opcion
            // 
            this.cbxAH2Opcion.BackColor = System.Drawing.Color.SeaGreen;
            this.cbxAH2Opcion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxAH2Opcion.DropDownHeight = 120;
            this.cbxAH2Opcion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAH2Opcion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxAH2Opcion.Font = new System.Drawing.Font("Tahoma", 12F);
            this.cbxAH2Opcion.ForeColor = System.Drawing.Color.White;
            this.cbxAH2Opcion.FormattingEnabled = true;
            this.cbxAH2Opcion.IntegralHeight = false;
            this.cbxAH2Opcion.Items.AddRange(new object[] {
            "Servidor",
            "Usuario y/o contraseña",
            "Puerto",
            "Directorio",
            "Encriptación",
            "Autenticación",
            "Modo activo",
            "Protocolo"});
            this.cbxAH2Opcion.Location = new System.Drawing.Point(192, 184);
            this.cbxAH2Opcion.Name = "cbxAH2Opcion";
            this.cbxAH2Opcion.Size = new System.Drawing.Size(240, 27);
            this.cbxAH2Opcion.TabIndex = 83;
            this.cbxAH2Opcion.SelectedIndexChanged += new System.EventHandler(this.cbxAH2Opcion_SelectedIndexChanged);
            // 
            // lblAH2Opcion
            // 
            // 
            // 
            // 
            this.lblAH2Opcion.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAH2Opcion.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAH2Opcion.ForeColor = System.Drawing.Color.Black;
            this.lblAH2Opcion.Location = new System.Drawing.Point(192, 165);
            this.lblAH2Opcion.Name = "lblAH2Opcion";
            this.lblAH2Opcion.SingleLineColor = System.Drawing.Color.Black;
            this.lblAH2Opcion.Size = new System.Drawing.Size(396, 16);
            this.lblAH2Opcion.TabIndex = 82;
            this.lblAH2Opcion.Text = "Conocer más sobre:";
            this.lblAH2Opcion.WordWrap = true;
            // 
            // lblAH2Cuerpo2
            // 
            // 
            // 
            // 
            this.lblAH2Cuerpo2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAH2Cuerpo2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAH2Cuerpo2.ForeColor = System.Drawing.Color.Black;
            this.lblAH2Cuerpo2.Location = new System.Drawing.Point(195, 217);
            this.lblAH2Cuerpo2.Name = "lblAH2Cuerpo2";
            this.lblAH2Cuerpo2.SingleLineColor = System.Drawing.Color.Black;
            this.lblAH2Cuerpo2.Size = new System.Drawing.Size(396, 202);
            this.lblAH2Cuerpo2.TabIndex = 81;
            this.lblAH2Cuerpo2.Text = resources.GetString("lblAH2Cuerpo2.Text");
            this.lblAH2Cuerpo2.WordWrap = true;
            // 
            // btnAH2Regresar
            // 
            this.btnAH2Regresar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAH2Regresar.BackColor = System.Drawing.Color.Gray;
            this.btnAH2Regresar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAH2Regresar.BorderRadius = 0;
            this.btnAH2Regresar.ButtonText = "Regresar";
            this.btnAH2Regresar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAH2Regresar.DisabledColor = System.Drawing.Color.Gray;
            this.btnAH2Regresar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAH2Regresar.Iconimage = global::SyncBD.Properties.Resources.Back_16px;
            this.btnAH2Regresar.Iconimage_right = null;
            this.btnAH2Regresar.Iconimage_right_Selected = null;
            this.btnAH2Regresar.Iconimage_Selected = null;
            this.btnAH2Regresar.IconMarginLeft = 0;
            this.btnAH2Regresar.IconMarginRight = 0;
            this.btnAH2Regresar.IconRightVisible = true;
            this.btnAH2Regresar.IconRightZoom = 0D;
            this.btnAH2Regresar.IconVisible = true;
            this.btnAH2Regresar.IconZoom = 40D;
            this.btnAH2Regresar.IsTab = false;
            this.btnAH2Regresar.Location = new System.Drawing.Point(464, 425);
            this.btnAH2Regresar.Name = "btnAH2Regresar";
            this.btnAH2Regresar.Normalcolor = System.Drawing.Color.Gray;
            this.btnAH2Regresar.OnHovercolor = System.Drawing.Color.Green;
            this.btnAH2Regresar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAH2Regresar.selected = false;
            this.btnAH2Regresar.Size = new System.Drawing.Size(124, 43);
            this.btnAH2Regresar.TabIndex = 80;
            this.btnAH2Regresar.Text = "Regresar";
            this.btnAH2Regresar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAH2Regresar.Textcolor = System.Drawing.Color.White;
            this.btnAH2Regresar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAH2Regresar.Click += new System.EventHandler(this.btnAH2Regresar_Click);
            // 
            // lblAH2Cuerpo1
            // 
            // 
            // 
            // 
            this.lblAH2Cuerpo1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAH2Cuerpo1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAH2Cuerpo1.ForeColor = System.Drawing.Color.Black;
            this.lblAH2Cuerpo1.Location = new System.Drawing.Point(192, 109);
            this.lblAH2Cuerpo1.Name = "lblAH2Cuerpo1";
            this.lblAH2Cuerpo1.SingleLineColor = System.Drawing.Color.Black;
            this.lblAH2Cuerpo1.Size = new System.Drawing.Size(396, 50);
            this.lblAH2Cuerpo1.TabIndex = 79;
            this.lblAH2Cuerpo1.Text = "Si tiene algún problema para configurar su servidor de datos o no conoce alguna d" +
    "e las funciones, seleccione un tópico de la lista para mostrar más detalles.";
            this.lblAH2Cuerpo1.WordWrap = true;
            // 
            // lineAH2Divisor
            // 
            this.lineAH2Divisor.ForeColor = System.Drawing.Color.Black;
            this.lineAH2Divisor.Location = new System.Drawing.Point(192, 80);
            this.lineAH2Divisor.Name = "lineAH2Divisor";
            this.lineAH2Divisor.Size = new System.Drawing.Size(396, 23);
            this.lineAH2Divisor.TabIndex = 78;
            this.lineAH2Divisor.Text = "line1";
            // 
            // lblAH2Encabezado
            // 
            // 
            // 
            // 
            this.lblAH2Encabezado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAH2Encabezado.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblAH2Encabezado.ForeColor = System.Drawing.Color.Black;
            this.lblAH2Encabezado.Location = new System.Drawing.Point(192, 47);
            this.lblAH2Encabezado.Name = "lblAH2Encabezado";
            this.lblAH2Encabezado.SingleLineColor = System.Drawing.Color.Black;
            this.lblAH2Encabezado.Size = new System.Drawing.Size(385, 39);
            this.lblAH2Encabezado.TabIndex = 77;
            this.lblAH2Encabezado.Text = "Ayuda para la configuración del servidor";
            this.lblAH2Encabezado.WordWrap = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(186, 450);
            this.panel1.TabIndex = 76;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::SyncBD.Properties.Resources.SyncBD;
            this.pictureBox1.Location = new System.Drawing.Point(4, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.Green;
            this.pnlTitleBar.Controls.Add(this.lblAH2Titulo);
            this.pnlTitleBar.Controls.Add(this.pbxMinimizar);
            this.pnlTitleBar.Controls.Add(this.pbxCerrar);
            this.pnlTitleBar.Controls.Add(this.pbxIcono);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(600, 30);
            this.pnlTitleBar.TabIndex = 75;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // lblAH2Titulo
            // 
            // 
            // 
            // 
            this.lblAH2Titulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblAH2Titulo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblAH2Titulo.ForeColor = System.Drawing.Color.White;
            this.lblAH2Titulo.Location = new System.Drawing.Point(28, 5);
            this.lblAH2Titulo.Name = "lblAH2Titulo";
            this.lblAH2Titulo.Size = new System.Drawing.Size(508, 20);
            this.lblAH2Titulo.TabIndex = 12;
            this.lblAH2Titulo.Text = "SyncBD - Asistente de configuración";
            this.lblAH2Titulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pbxMinimizar
            // 
            this.pbxMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.pbxMinimizar.Image = global::SyncBD.Properties.Resources.Compress_24px;
            this.pbxMinimizar.Location = new System.Drawing.Point(542, 5);
            this.pbxMinimizar.Name = "pbxMinimizar";
            this.pbxMinimizar.Size = new System.Drawing.Size(20, 20);
            this.pbxMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMinimizar.TabIndex = 3;
            this.pbxMinimizar.TabStop = false;
            this.pbxMinimizar.Click += new System.EventHandler(this.pbxMinimizar_Click);
            // 
            // pbxCerrar
            // 
            this.pbxCerrar.BackColor = System.Drawing.Color.Transparent;
            this.pbxCerrar.Image = global::SyncBD.Properties.Resources.CloseWindow_24px;
            this.pbxCerrar.Location = new System.Drawing.Point(568, 5);
            this.pbxCerrar.Name = "pbxCerrar";
            this.pbxCerrar.Size = new System.Drawing.Size(20, 20);
            this.pbxCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCerrar.TabIndex = 2;
            this.pbxCerrar.TabStop = false;
            this.pbxCerrar.Click += new System.EventHandler(this.pbxCerrar_Click);
            // 
            // pbxIcono
            // 
            this.pbxIcono.BackColor = System.Drawing.Color.Transparent;
            this.pbxIcono.Image = global::SyncBD.Properties.Resources.SyncBDLogo;
            this.pbxIcono.Location = new System.Drawing.Point(6, 8);
            this.pbxIcono.Name = "pbxIcono";
            this.pbxIcono.Size = new System.Drawing.Size(16, 16);
            this.pbxIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxIcono.TabIndex = 1;
            this.pbxIcono.TabStop = false;
            this.pbxIcono.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // AyudaConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(600, 480);
            this.Controls.Add(this.cbxAH2Opcion);
            this.Controls.Add(this.lblAH2Opcion);
            this.Controls.Add(this.lblAH2Cuerpo2);
            this.Controls.Add(this.btnAH2Regresar);
            this.Controls.Add(this.lblAH2Cuerpo1);
            this.Controls.Add(this.lineAH2Divisor);
            this.Controls.Add(this.lblAH2Encabezado);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlTitleBar);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AyudaConfig";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asistente en configuración | SyncBD";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlTitleBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxAH2Opcion;
        private DevComponents.DotNetBar.LabelX lblAH2Opcion;
        private DevComponents.DotNetBar.LabelX lblAH2Cuerpo2;
        private Bunifu.Framework.UI.BunifuFlatButton btnAH2Regresar;
        private DevComponents.DotNetBar.LabelX lblAH2Cuerpo1;
        private DevComponents.DotNetBar.Controls.Line lineAH2Divisor;
        private DevComponents.DotNetBar.LabelX lblAH2Encabezado;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlTitleBar;
        private DevComponents.DotNetBar.LabelX lblAH2Titulo;
        private System.Windows.Forms.PictureBox pbxMinimizar;
        private System.Windows.Forms.PictureBox pbxCerrar;
        private System.Windows.Forms.PictureBox pbxIcono;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
﻿namespace SyncDB.Vistas
{
    partial class MenuServidores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuServidores));
            this.lblA2Remoto = new DevComponents.DotNetBar.LabelX();
            this.lblA2Local = new DevComponents.DotNetBar.LabelX();
            this.lblA2Ambos = new DevComponents.DotNetBar.LabelX();
            this.lblA2Cuerpo = new DevComponents.DotNetBar.LabelX();
            this.lineA2Divisor = new DevComponents.DotNetBar.Controls.Line();
            this.lblA2Encabezado = new DevComponents.DotNetBar.LabelX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblA2Titulo = new DevComponents.DotNetBar.LabelX();
            this.pbxMinimizar = new System.Windows.Forms.PictureBox();
            this.pbxCerrar = new System.Windows.Forms.PictureBox();
            this.pbxIcono = new System.Windows.Forms.PictureBox();
            this.chkA2Ambos = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.chkA2Local = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.chkA2Remoto = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.btnA2Ayuda = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnA2Cancelar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnA2Continuar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).BeginInit();
            this.SuspendLayout();
            // 
            // lblA2Remoto
            // 
            // 
            // 
            // 
            this.lblA2Remoto.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA2Remoto.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA2Remoto.ForeColor = System.Drawing.Color.Black;
            this.lblA2Remoto.Location = new System.Drawing.Point(323, 245);
            this.lblA2Remoto.Name = "lblA2Remoto";
            this.lblA2Remoto.SingleLineColor = System.Drawing.Color.Black;
            this.lblA2Remoto.Size = new System.Drawing.Size(254, 26);
            this.lblA2Remoto.TabIndex = 55;
            this.lblA2Remoto.Text = "Configurar el servidor Remoto";
            this.lblA2Remoto.WordWrap = true;
            // 
            // lblA2Local
            // 
            // 
            // 
            // 
            this.lblA2Local.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA2Local.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA2Local.ForeColor = System.Drawing.Color.Black;
            this.lblA2Local.Location = new System.Drawing.Point(322, 284);
            this.lblA2Local.Name = "lblA2Local";
            this.lblA2Local.SingleLineColor = System.Drawing.Color.Black;
            this.lblA2Local.Size = new System.Drawing.Size(254, 26);
            this.lblA2Local.TabIndex = 56;
            this.lblA2Local.Text = "Configurar el servidor Local";
            this.lblA2Local.WordWrap = true;
            // 
            // lblA2Ambos
            // 
            // 
            // 
            // 
            this.lblA2Ambos.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA2Ambos.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA2Ambos.ForeColor = System.Drawing.Color.Black;
            this.lblA2Ambos.Location = new System.Drawing.Point(323, 325);
            this.lblA2Ambos.Name = "lblA2Ambos";
            this.lblA2Ambos.SingleLineColor = System.Drawing.Color.Black;
            this.lblA2Ambos.Size = new System.Drawing.Size(254, 26);
            this.lblA2Ambos.TabIndex = 54;
            this.lblA2Ambos.Text = "Configurar ambos Servidores";
            this.lblA2Ambos.WordWrap = true;
            // 
            // lblA2Cuerpo
            // 
            // 
            // 
            // 
            this.lblA2Cuerpo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA2Cuerpo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA2Cuerpo.ForeColor = System.Drawing.Color.Black;
            this.lblA2Cuerpo.Location = new System.Drawing.Point(192, 109);
            this.lblA2Cuerpo.Name = "lblA2Cuerpo";
            this.lblA2Cuerpo.SingleLineColor = System.Drawing.Color.Black;
            this.lblA2Cuerpo.Size = new System.Drawing.Size(396, 110);
            this.lblA2Cuerpo.TabIndex = 47;
            this.lblA2Cuerpo.Text = resources.GetString("lblA2Cuerpo.Text");
            this.lblA2Cuerpo.WordWrap = true;
            // 
            // lineA2Divisor
            // 
            this.lineA2Divisor.ForeColor = System.Drawing.Color.Black;
            this.lineA2Divisor.Location = new System.Drawing.Point(192, 80);
            this.lineA2Divisor.Name = "lineA2Divisor";
            this.lineA2Divisor.Size = new System.Drawing.Size(396, 23);
            this.lineA2Divisor.TabIndex = 46;
            this.lineA2Divisor.Text = "line1";
            // 
            // lblA2Encabezado
            // 
            // 
            // 
            // 
            this.lblA2Encabezado.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA2Encabezado.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblA2Encabezado.ForeColor = System.Drawing.Color.Black;
            this.lblA2Encabezado.Location = new System.Drawing.Point(192, 47);
            this.lblA2Encabezado.Name = "lblA2Encabezado";
            this.lblA2Encabezado.SingleLineColor = System.Drawing.Color.Black;
            this.lblA2Encabezado.Size = new System.Drawing.Size(396, 39);
            this.lblA2Encabezado.TabIndex = 45;
            this.lblA2Encabezado.Text = "Configuración de servidores";
            this.lblA2Encabezado.WordWrap = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(186, 450);
            this.panel1.TabIndex = 44;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::SyncBD.Properties.Resources.SyncBD;
            this.pictureBox1.Location = new System.Drawing.Point(4, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(177, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.Green;
            this.pnlTitleBar.Controls.Add(this.lblA2Titulo);
            this.pnlTitleBar.Controls.Add(this.pbxMinimizar);
            this.pnlTitleBar.Controls.Add(this.pbxCerrar);
            this.pnlTitleBar.Controls.Add(this.pbxIcono);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(600, 30);
            this.pnlTitleBar.TabIndex = 43;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // lblA2Titulo
            // 
            // 
            // 
            // 
            this.lblA2Titulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblA2Titulo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblA2Titulo.ForeColor = System.Drawing.Color.White;
            this.lblA2Titulo.Location = new System.Drawing.Point(28, 5);
            this.lblA2Titulo.Name = "lblA2Titulo";
            this.lblA2Titulo.Size = new System.Drawing.Size(508, 20);
            this.lblA2Titulo.TabIndex = 9;
            this.lblA2Titulo.Text = "SyncBD - Asistente de configuración";
            this.lblA2Titulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // pbxMinimizar
            // 
            this.pbxMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.pbxMinimizar.Image = global::SyncBD.Properties.Resources.Compress_24px;
            this.pbxMinimizar.Location = new System.Drawing.Point(542, 5);
            this.pbxMinimizar.Name = "pbxMinimizar";
            this.pbxMinimizar.Size = new System.Drawing.Size(20, 20);
            this.pbxMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMinimizar.TabIndex = 3;
            this.pbxMinimizar.TabStop = false;
            this.pbxMinimizar.Click += new System.EventHandler(this.pbxMinimizar_Click);
            // 
            // pbxCerrar
            // 
            this.pbxCerrar.BackColor = System.Drawing.Color.Transparent;
            this.pbxCerrar.Image = global::SyncBD.Properties.Resources.CloseWindow_24px;
            this.pbxCerrar.Location = new System.Drawing.Point(568, 5);
            this.pbxCerrar.Name = "pbxCerrar";
            this.pbxCerrar.Size = new System.Drawing.Size(20, 20);
            this.pbxCerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCerrar.TabIndex = 2;
            this.pbxCerrar.TabStop = false;
            this.pbxCerrar.Click += new System.EventHandler(this.pbxCerrar_Click);
            // 
            // pbxIcono
            // 
            this.pbxIcono.BackColor = System.Drawing.Color.Transparent;
            this.pbxIcono.Image = global::SyncBD.Properties.Resources.SyncBDLogo;
            this.pbxIcono.Location = new System.Drawing.Point(6, 8);
            this.pbxIcono.Name = "pbxIcono";
            this.pbxIcono.Size = new System.Drawing.Size(16, 16);
            this.pbxIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxIcono.TabIndex = 1;
            this.pbxIcono.TabStop = false;
            this.pbxIcono.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // chkA2Ambos
            // 
            this.chkA2Ambos.BackColor = System.Drawing.Color.Transparent;
            this.chkA2Ambos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkA2Ambos.BackgroundImage")));
            this.chkA2Ambos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkA2Ambos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkA2Ambos.Location = new System.Drawing.Point(281, 327);
            this.chkA2Ambos.Name = "chkA2Ambos";
            this.chkA2Ambos.OffColor = System.Drawing.Color.Red;
            this.chkA2Ambos.OnColor = System.Drawing.Color.Green;
            this.chkA2Ambos.Size = new System.Drawing.Size(35, 20);
            this.chkA2Ambos.TabIndex = 53;
            this.chkA2Ambos.Value = false;
            this.chkA2Ambos.OnValueChange += new System.EventHandler(this.chkA2Ambos_OnValueChange);
            // 
            // chkA2Local
            // 
            this.chkA2Local.BackColor = System.Drawing.Color.Transparent;
            this.chkA2Local.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkA2Local.BackgroundImage")));
            this.chkA2Local.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkA2Local.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkA2Local.Location = new System.Drawing.Point(281, 287);
            this.chkA2Local.Name = "chkA2Local";
            this.chkA2Local.OffColor = System.Drawing.Color.Red;
            this.chkA2Local.OnColor = System.Drawing.Color.Green;
            this.chkA2Local.Size = new System.Drawing.Size(35, 20);
            this.chkA2Local.TabIndex = 52;
            this.chkA2Local.Value = false;
            this.chkA2Local.OnValueChange += new System.EventHandler(this.chkA2Local_OnValueChange);
            // 
            // chkA2Remoto
            // 
            this.chkA2Remoto.BackColor = System.Drawing.Color.Transparent;
            this.chkA2Remoto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkA2Remoto.BackgroundImage")));
            this.chkA2Remoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkA2Remoto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkA2Remoto.Location = new System.Drawing.Point(281, 248);
            this.chkA2Remoto.Name = "chkA2Remoto";
            this.chkA2Remoto.OffColor = System.Drawing.Color.Red;
            this.chkA2Remoto.OnColor = System.Drawing.Color.Green;
            this.chkA2Remoto.Size = new System.Drawing.Size(35, 20);
            this.chkA2Remoto.TabIndex = 51;
            this.chkA2Remoto.Value = false;
            this.chkA2Remoto.OnValueChange += new System.EventHandler(this.chkA2Remoto_OnValueChange);
            // 
            // btnA2Ayuda
            // 
            this.btnA2Ayuda.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnA2Ayuda.BackColor = System.Drawing.Color.Gray;
            this.btnA2Ayuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnA2Ayuda.BorderRadius = 0;
            this.btnA2Ayuda.ButtonText = "Ayuda";
            this.btnA2Ayuda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnA2Ayuda.DisabledColor = System.Drawing.Color.Gray;
            this.btnA2Ayuda.Iconcolor = System.Drawing.Color.Transparent;
            this.btnA2Ayuda.Iconimage = global::SyncBD.Properties.Resources.Help_16px;
            this.btnA2Ayuda.Iconimage_right = null;
            this.btnA2Ayuda.Iconimage_right_Selected = null;
            this.btnA2Ayuda.Iconimage_Selected = null;
            this.btnA2Ayuda.IconMarginLeft = 0;
            this.btnA2Ayuda.IconMarginRight = 0;
            this.btnA2Ayuda.IconRightVisible = true;
            this.btnA2Ayuda.IconRightZoom = 0D;
            this.btnA2Ayuda.IconVisible = true;
            this.btnA2Ayuda.IconZoom = 35D;
            this.btnA2Ayuda.IsTab = false;
            this.btnA2Ayuda.Location = new System.Drawing.Point(192, 425);
            this.btnA2Ayuda.Name = "btnA2Ayuda";
            this.btnA2Ayuda.Normalcolor = System.Drawing.Color.Gray;
            this.btnA2Ayuda.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.btnA2Ayuda.OnHoverTextColor = System.Drawing.Color.White;
            this.btnA2Ayuda.selected = false;
            this.btnA2Ayuda.Size = new System.Drawing.Size(93, 43);
            this.btnA2Ayuda.TabIndex = 50;
            this.btnA2Ayuda.Text = "Ayuda";
            this.btnA2Ayuda.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnA2Ayuda.Textcolor = System.Drawing.Color.White;
            this.btnA2Ayuda.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA2Ayuda.Click += new System.EventHandler(this.btnA2Ayuda_Click);
            // 
            // btnA2Cancelar
            // 
            this.btnA2Cancelar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnA2Cancelar.BackColor = System.Drawing.Color.Gray;
            this.btnA2Cancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnA2Cancelar.BorderRadius = 0;
            this.btnA2Cancelar.ButtonText = "Cancelar";
            this.btnA2Cancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnA2Cancelar.DisabledColor = System.Drawing.Color.Gray;
            this.btnA2Cancelar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnA2Cancelar.Iconimage = global::SyncBD.Properties.Resources.Cancel_16px;
            this.btnA2Cancelar.Iconimage_right = null;
            this.btnA2Cancelar.Iconimage_right_Selected = null;
            this.btnA2Cancelar.Iconimage_Selected = null;
            this.btnA2Cancelar.IconMarginLeft = 0;
            this.btnA2Cancelar.IconMarginRight = 0;
            this.btnA2Cancelar.IconRightVisible = true;
            this.btnA2Cancelar.IconRightZoom = 0D;
            this.btnA2Cancelar.IconVisible = true;
            this.btnA2Cancelar.IconZoom = 35D;
            this.btnA2Cancelar.IsTab = false;
            this.btnA2Cancelar.Location = new System.Drawing.Point(334, 425);
            this.btnA2Cancelar.Name = "btnA2Cancelar";
            this.btnA2Cancelar.Normalcolor = System.Drawing.Color.Gray;
            this.btnA2Cancelar.OnHovercolor = System.Drawing.Color.DarkRed;
            this.btnA2Cancelar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnA2Cancelar.selected = false;
            this.btnA2Cancelar.Size = new System.Drawing.Size(124, 43);
            this.btnA2Cancelar.TabIndex = 49;
            this.btnA2Cancelar.Text = "Cancelar";
            this.btnA2Cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnA2Cancelar.Textcolor = System.Drawing.Color.White;
            this.btnA2Cancelar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA2Cancelar.Click += new System.EventHandler(this.btnA2Cancelar_Click);
            // 
            // btnA2Continuar
            // 
            this.btnA2Continuar.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnA2Continuar.BackColor = System.Drawing.Color.Gray;
            this.btnA2Continuar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnA2Continuar.BorderRadius = 0;
            this.btnA2Continuar.ButtonText = "Continuar";
            this.btnA2Continuar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnA2Continuar.DisabledColor = System.Drawing.Color.Gray;
            this.btnA2Continuar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnA2Continuar.Iconimage = global::SyncBD.Properties.Resources.Ok_16px;
            this.btnA2Continuar.Iconimage_right = null;
            this.btnA2Continuar.Iconimage_right_Selected = null;
            this.btnA2Continuar.Iconimage_Selected = null;
            this.btnA2Continuar.IconMarginLeft = 0;
            this.btnA2Continuar.IconMarginRight = 0;
            this.btnA2Continuar.IconRightVisible = true;
            this.btnA2Continuar.IconRightZoom = 0D;
            this.btnA2Continuar.IconVisible = true;
            this.btnA2Continuar.IconZoom = 40D;
            this.btnA2Continuar.IsTab = false;
            this.btnA2Continuar.Location = new System.Drawing.Point(464, 425);
            this.btnA2Continuar.Name = "btnA2Continuar";
            this.btnA2Continuar.Normalcolor = System.Drawing.Color.Gray;
            this.btnA2Continuar.OnHovercolor = System.Drawing.Color.Green;
            this.btnA2Continuar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnA2Continuar.selected = false;
            this.btnA2Continuar.Size = new System.Drawing.Size(124, 43);
            this.btnA2Continuar.TabIndex = 48;
            this.btnA2Continuar.Text = "Continuar";
            this.btnA2Continuar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnA2Continuar.Textcolor = System.Drawing.Color.White;
            this.btnA2Continuar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA2Continuar.Click += new System.EventHandler(this.btnA2Continuar_Click);
            // 
            // MenuServidores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(600, 480);
            this.Controls.Add(this.lblA2Remoto);
            this.Controls.Add(this.lblA2Local);
            this.Controls.Add(this.lblA2Ambos);
            this.Controls.Add(this.chkA2Ambos);
            this.Controls.Add(this.chkA2Local);
            this.Controls.Add(this.chkA2Remoto);
            this.Controls.Add(this.btnA2Ayuda);
            this.Controls.Add(this.btnA2Cancelar);
            this.Controls.Add(this.btnA2Continuar);
            this.Controls.Add(this.lblA2Cuerpo);
            this.Controls.Add(this.lineA2Divisor);
            this.Controls.Add(this.lblA2Encabezado);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlTitleBar);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MenuServidores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asistente de configuración | SyncBD";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlTitleBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxMinimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX lblA2Remoto;
        private DevComponents.DotNetBar.LabelX lblA2Local;
        private DevComponents.DotNetBar.LabelX lblA2Ambos;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkA2Ambos;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkA2Local;
        private Bunifu.Framework.UI.BunifuiOSSwitch chkA2Remoto;
        private Bunifu.Framework.UI.BunifuFlatButton btnA2Ayuda;
        private Bunifu.Framework.UI.BunifuFlatButton btnA2Cancelar;
        private Bunifu.Framework.UI.BunifuFlatButton btnA2Continuar;
        private DevComponents.DotNetBar.LabelX lblA2Cuerpo;
        private DevComponents.DotNetBar.Controls.Line lineA2Divisor;
        private DevComponents.DotNetBar.LabelX lblA2Encabezado;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlTitleBar;
        private DevComponents.DotNetBar.LabelX lblA2Titulo;
        private System.Windows.Forms.PictureBox pbxMinimizar;
        private System.Windows.Forms.PictureBox pbxCerrar;
        private System.Windows.Forms.PictureBox pbxIcono;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
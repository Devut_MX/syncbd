﻿namespace SyncDB.Vistas
{
    partial class PopUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.lblMensaje = new DevComponents.DotNetBar.LabelX();
            this.lblTitulo = new DevComponents.DotNetBar.LabelX();
            this.tmpVisible = new System.Windows.Forms.Timer(this.components);
            this.pbxIcono = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).BeginInit();
            this.SuspendLayout();
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(9, 62);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(44, 17);
            this.labelX3.TabIndex = 7;
            this.labelX3.Text = "SyncBD";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Center;
            this.labelX3.WordWrap = true;
            this.labelX3.Click += new System.EventHandler(this.PopUp_Click);
            // 
            // lblMensaje
            // 
            // 
            // 
            // 
            this.lblMensaje.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblMensaje.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblMensaje.ForeColor = System.Drawing.Color.Black;
            this.lblMensaje.Location = new System.Drawing.Point(60, 38);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(271, 41);
            this.lblMensaje.TabIndex = 6;
            this.lblMensaje.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblMensaje.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.lblMensaje.WordWrap = true;
            this.lblMensaje.Click += new System.EventHandler(this.PopUp_Click);
            // 
            // lblTitulo
            // 
            // 
            // 
            // 
            this.lblTitulo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblTitulo.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblTitulo.ForeColor = System.Drawing.Color.Black;
            this.lblTitulo.Location = new System.Drawing.Point(60, 6);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(271, 19);
            this.lblTitulo.TabIndex = 5;
            this.lblTitulo.TextAlignment = System.Drawing.StringAlignment.Center;
            this.lblTitulo.WordWrap = true;
            this.lblTitulo.Click += new System.EventHandler(this.PopUp_Click);
            // 
            // tmpVisible
            // 
            this.tmpVisible.Interval = 4000;
            this.tmpVisible.Tick += new System.EventHandler(this.tmpVisible_Tick);
            // 
            // pbxIcono
            // 
            this.pbxIcono.Location = new System.Drawing.Point(10, 6);
            this.pbxIcono.Name = "pbxIcono";
            this.pbxIcono.Size = new System.Drawing.Size(44, 44);
            this.pbxIcono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxIcono.TabIndex = 4;
            this.pbxIcono.TabStop = false;
            this.pbxIcono.Click += new System.EventHandler(this.PopUp_Click);
            // 
            // PopUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(115)))), ((int)(((byte)(115)))));
            this.ClientSize = new System.Drawing.Size(340, 90);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.pbxIcono);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PopUp";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "PopUp";
            this.Click += new System.EventHandler(this.PopUp_Click);
            ((System.ComponentModel.ISupportInitialize)(this.pbxIcono)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX lblMensaje;
        private DevComponents.DotNetBar.LabelX lblTitulo;
        private System.Windows.Forms.PictureBox pbxIcono;
        private System.Windows.Forms.Timer tmpVisible;
    }
}
﻿namespace SyncBD.Vistas
{
    partial class Procesando
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Procesando));
            this.pbxProceso = new System.Windows.Forms.PictureBox();
            this.lblCuerpo = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            ((System.ComponentModel.ISupportInitialize)(this.pbxProceso)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxProceso
            // 
            this.pbxProceso.Location = new System.Drawing.Point(12, 12);
            this.pbxProceso.Name = "pbxProceso";
            this.pbxProceso.Size = new System.Drawing.Size(48, 48);
            this.pbxProceso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxProceso.TabIndex = 0;
            this.pbxProceso.TabStop = false;
            // 
            // lblCuerpo
            // 
            // 
            // 
            // 
            this.lblCuerpo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblCuerpo.ForeColor = System.Drawing.Color.Black;
            this.lblCuerpo.Location = new System.Drawing.Point(66, 37);
            this.lblCuerpo.Name = "lblCuerpo";
            this.lblCuerpo.Size = new System.Drawing.Size(264, 23);
            this.lblCuerpo.TabIndex = 1;
            this.lblCuerpo.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(66, 12);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(264, 23);
            this.labelX2.TabIndex = 2;
            this.labelX2.Text = "SyncBD";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // Procesando
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(198)))), ((int)(((byte)(107)))));
            this.ClientSize = new System.Drawing.Size(342, 74);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.lblCuerpo);
            this.Controls.Add(this.pbxProceso);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Procesando";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Procesando";
            ((System.ComponentModel.ISupportInitialize)(this.pbxProceso)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxProceso;
        private DevComponents.DotNetBar.LabelX lblCuerpo;
        private DevComponents.DotNetBar.LabelX labelX2;
    }
}